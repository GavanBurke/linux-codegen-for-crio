/*
 * M220Model_v2_types.h
 *
 * Code generation for model "M220Model_v2".
 *
 * Model version              : 1.474
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Mon Nov  1 19:15:20 2021
 *
 * Target selection: NIVeriStand_Linux_64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Generic->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_M220Model_v2_types_h_
#define RTW_HEADER_M220Model_v2_types_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#include "zero_crossing_types.h"

/* Model Code Variants */
#ifndef SS_INT64
#define SS_INT64                       16
#endif

#ifndef SS_UINT64
#define SS_UINT64                      17
#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM_M220Model_v2_T RT_MODEL_M220Model_v2_T;

#endif                                 /* RTW_HEADER_M220Model_v2_types_h_ */
