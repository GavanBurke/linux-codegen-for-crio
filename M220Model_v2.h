/*
 * M220Model_v2.h
 *
 * Code generation for model "M220Model_v2".
 *
 * Model version              : 1.474
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Mon Nov  1 19:15:20 2021
 *
 * Target selection: NIVeriStand_Linux_64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Generic->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_M220Model_v2_h_
#define RTW_HEADER_M220Model_v2_h_
#include <cmath>
#include <cstring>
#include <stddef.h>
#include "rtwtypes.h"
#include "zero_crossing_types.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "rt_logging.h"
#include "M220Model_v2_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "rtGetInf.h"
#include "rt_nonfinite.h"
#include "rtGetNaN.h"
#include "rt_defines.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetBlockIO
#define rtmGetBlockIO(rtm)             ((rtm)->blockIO)
#endif

#ifndef rtmSetBlockIO
#define rtmSetBlockIO(rtm, val)        ((rtm)->blockIO = (val))
#endif

#ifndef rtmGetChecksums
#define rtmGetChecksums(rtm)           ((rtm)->Sizes.checksums)
#endif

#ifndef rtmSetChecksums
#define rtmSetChecksums(rtm, val)      ((rtm)->Sizes.checksums = (val))
#endif

#ifndef rtmGetConstBlockIO
#define rtmGetConstBlockIO(rtm)        ((rtm)->constBlockIO)
#endif

#ifndef rtmSetConstBlockIO
#define rtmSetConstBlockIO(rtm, val)   ((rtm)->constBlockIO = (val))
#endif

#ifndef rtmGetContStateDisabled
#define rtmGetContStateDisabled(rtm)   ((rtm)->contStateDisabled)
#endif

#ifndef rtmSetContStateDisabled
#define rtmSetContStateDisabled(rtm, val) ((rtm)->contStateDisabled = (val))
#endif

#ifndef rtmGetContStates
#define rtmGetContStates(rtm)          ((rtm)->contStates)
#endif

#ifndef rtmSetContStates
#define rtmSetContStates(rtm, val)     ((rtm)->contStates = (val))
#endif

#ifndef rtmGetContTimeOutputInconsistentWithStateAtMajorStepFlag
#define rtmGetContTimeOutputInconsistentWithStateAtMajorStepFlag(rtm) ((rtm)->CTOutputIncnstWithState)
#endif

#ifndef rtmSetContTimeOutputInconsistentWithStateAtMajorStepFlag
#define rtmSetContTimeOutputInconsistentWithStateAtMajorStepFlag(rtm, val) ((rtm)->CTOutputIncnstWithState = (val))
#endif

#ifndef rtmGetCtrlRateMdlRefTiming
#define rtmGetCtrlRateMdlRefTiming(rtm) ()
#endif

#ifndef rtmSetCtrlRateMdlRefTiming
#define rtmSetCtrlRateMdlRefTiming(rtm, val) ()
#endif

#ifndef rtmGetCtrlRateMdlRefTimingPtr
#define rtmGetCtrlRateMdlRefTimingPtr(rtm) ()
#endif

#ifndef rtmSetCtrlRateMdlRefTimingPtr
#define rtmSetCtrlRateMdlRefTimingPtr(rtm, val) ()
#endif

#ifndef rtmGetCtrlRateNumTicksToNextHit
#define rtmGetCtrlRateNumTicksToNextHit(rtm) ()
#endif

#ifndef rtmSetCtrlRateNumTicksToNextHit
#define rtmSetCtrlRateNumTicksToNextHit(rtm, val) ()
#endif

#ifndef rtmGetDataMapInfo
#define rtmGetDataMapInfo(rtm)         ()
#endif

#ifndef rtmSetDataMapInfo
#define rtmSetDataMapInfo(rtm, val)    ()
#endif

#ifndef rtmGetDefaultParam
#define rtmGetDefaultParam(rtm)        ((rtm)->defaultParam)
#endif

#ifndef rtmSetDefaultParam
#define rtmSetDefaultParam(rtm, val)   ((rtm)->defaultParam = (val))
#endif

#ifndef rtmGetDerivCacheNeedsReset
#define rtmGetDerivCacheNeedsReset(rtm) ((rtm)->derivCacheNeedsReset)
#endif

#ifndef rtmSetDerivCacheNeedsReset
#define rtmSetDerivCacheNeedsReset(rtm, val) ((rtm)->derivCacheNeedsReset = (val))
#endif

#ifndef rtmGetDirectFeedThrough
#define rtmGetDirectFeedThrough(rtm)   ((rtm)->Sizes.sysDirFeedThru)
#endif

#ifndef rtmSetDirectFeedThrough
#define rtmSetDirectFeedThrough(rtm, val) ((rtm)->Sizes.sysDirFeedThru = (val))
#endif

#ifndef rtmGetErrorStatusFlag
#define rtmGetErrorStatusFlag(rtm)     ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatusFlag
#define rtmSetErrorStatusFlag(rtm, val) ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetFinalTime
#define rtmGetFinalTime(rtm)           ((rtm)->Timing.tFinal)
#endif

#ifndef rtmSetFinalTime
#define rtmSetFinalTime(rtm, val)      ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetFirstInitCondFlag
#define rtmGetFirstInitCondFlag(rtm)   ()
#endif

#ifndef rtmSetFirstInitCondFlag
#define rtmSetFirstInitCondFlag(rtm, val) ()
#endif

#ifndef rtmGetIntgData
#define rtmGetIntgData(rtm)            ((rtm)->intgData)
#endif

#ifndef rtmSetIntgData
#define rtmSetIntgData(rtm, val)       ((rtm)->intgData = (val))
#endif

#ifndef rtmGetMdlRefGlobalRuntimeEventIndices
#define rtmGetMdlRefGlobalRuntimeEventIndices(rtm) ()
#endif

#ifndef rtmSetMdlRefGlobalRuntimeEventIndices
#define rtmSetMdlRefGlobalRuntimeEventIndices(rtm, val) ()
#endif

#ifndef rtmGetMdlRefGlobalTID
#define rtmGetMdlRefGlobalTID(rtm)     ()
#endif

#ifndef rtmSetMdlRefGlobalTID
#define rtmSetMdlRefGlobalTID(rtm, val) ()
#endif

#ifndef rtmGetMdlRefTriggerTID
#define rtmGetMdlRefTriggerTID(rtm)    ()
#endif

#ifndef rtmSetMdlRefTriggerTID
#define rtmSetMdlRefTriggerTID(rtm, val) ()
#endif

#ifndef rtmGetModelMappingInfo
#define rtmGetModelMappingInfo(rtm)    ((rtm)->SpecialInfo.mappingInfo)
#endif

#ifndef rtmSetModelMappingInfo
#define rtmSetModelMappingInfo(rtm, val) ((rtm)->SpecialInfo.mappingInfo = (val))
#endif

#ifndef rtmGetModelName
#define rtmGetModelName(rtm)           ((rtm)->modelName)
#endif

#ifndef rtmSetModelName
#define rtmSetModelName(rtm, val)      ((rtm)->modelName = (val))
#endif

#ifndef rtmGetNonInlinedSFcns
#define rtmGetNonInlinedSFcns(rtm)     ()
#endif

#ifndef rtmSetNonInlinedSFcns
#define rtmSetNonInlinedSFcns(rtm, val) ()
#endif

#ifndef rtmGetNumBlockIO
#define rtmGetNumBlockIO(rtm)          ((rtm)->Sizes.numBlockIO)
#endif

#ifndef rtmSetNumBlockIO
#define rtmSetNumBlockIO(rtm, val)     ((rtm)->Sizes.numBlockIO = (val))
#endif

#ifndef rtmGetNumBlockParams
#define rtmGetNumBlockParams(rtm)      ((rtm)->Sizes.numBlockPrms)
#endif

#ifndef rtmSetNumBlockParams
#define rtmSetNumBlockParams(rtm, val) ((rtm)->Sizes.numBlockPrms = (val))
#endif

#ifndef rtmGetNumBlocks
#define rtmGetNumBlocks(rtm)           ((rtm)->Sizes.numBlocks)
#endif

#ifndef rtmSetNumBlocks
#define rtmSetNumBlocks(rtm, val)      ((rtm)->Sizes.numBlocks = (val))
#endif

#ifndef rtmGetNumContStates
#define rtmGetNumContStates(rtm)       ((rtm)->Sizes.numContStates)
#endif

#ifndef rtmSetNumContStates
#define rtmSetNumContStates(rtm, val)  ((rtm)->Sizes.numContStates = (val))
#endif

#ifndef rtmGetNumDWork
#define rtmGetNumDWork(rtm)            ((rtm)->Sizes.numDwork)
#endif

#ifndef rtmSetNumDWork
#define rtmSetNumDWork(rtm, val)       ((rtm)->Sizes.numDwork = (val))
#endif

#ifndef rtmGetNumInputPorts
#define rtmGetNumInputPorts(rtm)       ((rtm)->Sizes.numIports)
#endif

#ifndef rtmSetNumInputPorts
#define rtmSetNumInputPorts(rtm, val)  ((rtm)->Sizes.numIports = (val))
#endif

#ifndef rtmGetNumNonSampledZCs
#define rtmGetNumNonSampledZCs(rtm)    ((rtm)->Sizes.numNonSampZCs)
#endif

#ifndef rtmSetNumNonSampledZCs
#define rtmSetNumNonSampledZCs(rtm, val) ((rtm)->Sizes.numNonSampZCs = (val))
#endif

#ifndef rtmGetNumOutputPorts
#define rtmGetNumOutputPorts(rtm)      ((rtm)->Sizes.numOports)
#endif

#ifndef rtmSetNumOutputPorts
#define rtmSetNumOutputPorts(rtm, val) ((rtm)->Sizes.numOports = (val))
#endif

#ifndef rtmGetNumPeriodicContStates
#define rtmGetNumPeriodicContStates(rtm) ((rtm)->Sizes.numPeriodicContStates)
#endif

#ifndef rtmSetNumPeriodicContStates
#define rtmSetNumPeriodicContStates(rtm, val) ((rtm)->Sizes.numPeriodicContStates = (val))
#endif

#ifndef rtmGetNumSFcnParams
#define rtmGetNumSFcnParams(rtm)       ((rtm)->Sizes.numSFcnPrms)
#endif

#ifndef rtmSetNumSFcnParams
#define rtmSetNumSFcnParams(rtm, val)  ((rtm)->Sizes.numSFcnPrms = (val))
#endif

#ifndef rtmGetNumSFunctions
#define rtmGetNumSFunctions(rtm)       ((rtm)->Sizes.numSFcns)
#endif

#ifndef rtmSetNumSFunctions
#define rtmSetNumSFunctions(rtm, val)  ((rtm)->Sizes.numSFcns = (val))
#endif

#ifndef rtmGetNumSampleTimes
#define rtmGetNumSampleTimes(rtm)      ((rtm)->Sizes.numSampTimes)
#endif

#ifndef rtmSetNumSampleTimes
#define rtmSetNumSampleTimes(rtm, val) ((rtm)->Sizes.numSampTimes = (val))
#endif

#ifndef rtmGetNumU
#define rtmGetNumU(rtm)                ((rtm)->Sizes.numU)
#endif

#ifndef rtmSetNumU
#define rtmSetNumU(rtm, val)           ((rtm)->Sizes.numU = (val))
#endif

#ifndef rtmGetNumY
#define rtmGetNumY(rtm)                ((rtm)->Sizes.numY)
#endif

#ifndef rtmSetNumY
#define rtmSetNumY(rtm, val)           ((rtm)->Sizes.numY = (val))
#endif

#ifndef rtmGetOdeF
#define rtmGetOdeF(rtm)                ((rtm)->odeF)
#endif

#ifndef rtmSetOdeF
#define rtmSetOdeF(rtm, val)           ((rtm)->odeF = (val))
#endif

#ifndef rtmGetOdeY
#define rtmGetOdeY(rtm)                ((rtm)->odeY)
#endif

#ifndef rtmSetOdeY
#define rtmSetOdeY(rtm, val)           ((rtm)->odeY = (val))
#endif

#ifndef rtmGetOffsetTimeArray
#define rtmGetOffsetTimeArray(rtm)     ((rtm)->Timing.offsetTimesArray)
#endif

#ifndef rtmSetOffsetTimeArray
#define rtmSetOffsetTimeArray(rtm, val) ((rtm)->Timing.offsetTimesArray = (val))
#endif

#ifndef rtmGetOffsetTimePtr
#define rtmGetOffsetTimePtr(rtm)       ((rtm)->Timing.offsetTimes)
#endif

#ifndef rtmSetOffsetTimePtr
#define rtmSetOffsetTimePtr(rtm, val)  ((rtm)->Timing.offsetTimes = (val))
#endif

#ifndef rtmGetOptions
#define rtmGetOptions(rtm)             ((rtm)->Sizes.options)
#endif

#ifndef rtmSetOptions
#define rtmSetOptions(rtm, val)        ((rtm)->Sizes.options = (val))
#endif

#ifndef rtmGetParamIsMalloced
#define rtmGetParamIsMalloced(rtm)     ()
#endif

#ifndef rtmSetParamIsMalloced
#define rtmSetParamIsMalloced(rtm, val) ()
#endif

#ifndef rtmGetPath
#define rtmGetPath(rtm)                ((rtm)->path)
#endif

#ifndef rtmSetPath
#define rtmSetPath(rtm, val)           ((rtm)->path = (val))
#endif

#ifndef rtmGetPerTaskSampleHits
#define rtmGetPerTaskSampleHits(rtm)   ()
#endif

#ifndef rtmSetPerTaskSampleHits
#define rtmSetPerTaskSampleHits(rtm, val) ()
#endif

#ifndef rtmGetPerTaskSampleHitsArray
#define rtmGetPerTaskSampleHitsArray(rtm) ((rtm)->Timing.perTaskSampleHitsArray)
#endif

#ifndef rtmSetPerTaskSampleHitsArray
#define rtmSetPerTaskSampleHitsArray(rtm, val) ((rtm)->Timing.perTaskSampleHitsArray = (val))
#endif

#ifndef rtmGetPerTaskSampleHitsPtr
#define rtmGetPerTaskSampleHitsPtr(rtm) ((rtm)->Timing.perTaskSampleHits)
#endif

#ifndef rtmSetPerTaskSampleHitsPtr
#define rtmSetPerTaskSampleHitsPtr(rtm, val) ((rtm)->Timing.perTaskSampleHits = (val))
#endif

#ifndef rtmGetPeriodicContStateIndices
#define rtmGetPeriodicContStateIndices(rtm) ((rtm)->periodicContStateIndices)
#endif

#ifndef rtmSetPeriodicContStateIndices
#define rtmSetPeriodicContStateIndices(rtm, val) ((rtm)->periodicContStateIndices = (val))
#endif

#ifndef rtmGetPeriodicContStateRanges
#define rtmGetPeriodicContStateRanges(rtm) ((rtm)->periodicContStateRanges)
#endif

#ifndef rtmSetPeriodicContStateRanges
#define rtmSetPeriodicContStateRanges(rtm, val) ((rtm)->periodicContStateRanges = (val))
#endif

#ifndef rtmGetPrevZCSigState
#define rtmGetPrevZCSigState(rtm)      ((rtm)->prevZCSigState)
#endif

#ifndef rtmSetPrevZCSigState
#define rtmSetPrevZCSigState(rtm, val) ((rtm)->prevZCSigState = (val))
#endif

#ifndef rtmGetRTWExtModeInfo
#define rtmGetRTWExtModeInfo(rtm)      ((rtm)->extModeInfo)
#endif

#ifndef rtmSetRTWExtModeInfo
#define rtmSetRTWExtModeInfo(rtm, val) ((rtm)->extModeInfo = (val))
#endif

#ifndef rtmGetRTWGeneratedSFcn
#define rtmGetRTWGeneratedSFcn(rtm)    ((rtm)->Sizes.rtwGenSfcn)
#endif

#ifndef rtmSetRTWGeneratedSFcn
#define rtmSetRTWGeneratedSFcn(rtm, val) ((rtm)->Sizes.rtwGenSfcn = (val))
#endif

#ifndef rtmGetRTWLogInfo
#define rtmGetRTWLogInfo(rtm)          ((rtm)->rtwLogInfo)
#endif

#ifndef rtmSetRTWLogInfo
#define rtmSetRTWLogInfo(rtm, val)     ((rtm)->rtwLogInfo = (val))
#endif

#ifndef rtmGetRTWRTModelMethodsInfo
#define rtmGetRTWRTModelMethodsInfo(rtm) ()
#endif

#ifndef rtmSetRTWRTModelMethodsInfo
#define rtmSetRTWRTModelMethodsInfo(rtm, val) ()
#endif

#ifndef rtmGetRTWSfcnInfo
#define rtmGetRTWSfcnInfo(rtm)         ((rtm)->sfcnInfo)
#endif

#ifndef rtmSetRTWSfcnInfo
#define rtmSetRTWSfcnInfo(rtm, val)    ((rtm)->sfcnInfo = (val))
#endif

#ifndef rtmGetRTWSolverInfo
#define rtmGetRTWSolverInfo(rtm)       ((rtm)->solverInfo)
#endif

#ifndef rtmSetRTWSolverInfo
#define rtmSetRTWSolverInfo(rtm, val)  ((rtm)->solverInfo = (val))
#endif

#ifndef rtmGetRTWSolverInfoPtr
#define rtmGetRTWSolverInfoPtr(rtm)    ((rtm)->solverInfoPtr)
#endif

#ifndef rtmSetRTWSolverInfoPtr
#define rtmSetRTWSolverInfoPtr(rtm, val) ((rtm)->solverInfoPtr = (val))
#endif

#ifndef rtmGetReservedForXPC
#define rtmGetReservedForXPC(rtm)      ((rtm)->SpecialInfo.xpcData)
#endif

#ifndef rtmSetReservedForXPC
#define rtmSetReservedForXPC(rtm, val) ((rtm)->SpecialInfo.xpcData = (val))
#endif

#ifndef rtmGetRootDWork
#define rtmGetRootDWork(rtm)           ((rtm)->dwork)
#endif

#ifndef rtmSetRootDWork
#define rtmSetRootDWork(rtm, val)      ((rtm)->dwork = (val))
#endif

#ifndef rtmGetSFunctions
#define rtmGetSFunctions(rtm)          ((rtm)->childSfunctions)
#endif

#ifndef rtmSetSFunctions
#define rtmSetSFunctions(rtm, val)     ((rtm)->childSfunctions = (val))
#endif

#ifndef rtmGetSampleHitArray
#define rtmGetSampleHitArray(rtm)      ((rtm)->Timing.sampleHitArray)
#endif

#ifndef rtmSetSampleHitArray
#define rtmSetSampleHitArray(rtm, val) ((rtm)->Timing.sampleHitArray = (val))
#endif

#ifndef rtmGetSampleHitPtr
#define rtmGetSampleHitPtr(rtm)        ((rtm)->Timing.sampleHits)
#endif

#ifndef rtmSetSampleHitPtr
#define rtmSetSampleHitPtr(rtm, val)   ((rtm)->Timing.sampleHits = (val))
#endif

#ifndef rtmGetSampleTimeArray
#define rtmGetSampleTimeArray(rtm)     ((rtm)->Timing.sampleTimesArray)
#endif

#ifndef rtmSetSampleTimeArray
#define rtmSetSampleTimeArray(rtm, val) ((rtm)->Timing.sampleTimesArray = (val))
#endif

#ifndef rtmGetSampleTimePtr
#define rtmGetSampleTimePtr(rtm)       ((rtm)->Timing.sampleTimes)
#endif

#ifndef rtmSetSampleTimePtr
#define rtmSetSampleTimePtr(rtm, val)  ((rtm)->Timing.sampleTimes = (val))
#endif

#ifndef rtmGetSampleTimeTaskIDArray
#define rtmGetSampleTimeTaskIDArray(rtm) ((rtm)->Timing.sampleTimeTaskIDArray)
#endif

#ifndef rtmSetSampleTimeTaskIDArray
#define rtmSetSampleTimeTaskIDArray(rtm, val) ((rtm)->Timing.sampleTimeTaskIDArray = (val))
#endif

#ifndef rtmGetSampleTimeTaskIDPtr
#define rtmGetSampleTimeTaskIDPtr(rtm) ((rtm)->Timing.sampleTimeTaskIDPtr)
#endif

#ifndef rtmSetSampleTimeTaskIDPtr
#define rtmSetSampleTimeTaskIDPtr(rtm, val) ((rtm)->Timing.sampleTimeTaskIDPtr = (val))
#endif

#ifndef rtmGetSelf
#define rtmGetSelf(rtm)                ()
#endif

#ifndef rtmSetSelf
#define rtmSetSelf(rtm, val)           ()
#endif

#ifndef rtmGetSimMode
#define rtmGetSimMode(rtm)             ((rtm)->simMode)
#endif

#ifndef rtmSetSimMode
#define rtmSetSimMode(rtm, val)        ((rtm)->simMode = (val))
#endif

#ifndef rtmGetSimTimeStep
#define rtmGetSimTimeStep(rtm)         ((rtm)->Timing.simTimeStep)
#endif

#ifndef rtmSetSimTimeStep
#define rtmSetSimTimeStep(rtm, val)    ((rtm)->Timing.simTimeStep = (val))
#endif

#ifndef rtmGetStartTime
#define rtmGetStartTime(rtm)           ((rtm)->Timing.tStart)
#endif

#ifndef rtmSetStartTime
#define rtmSetStartTime(rtm, val)      ((rtm)->Timing.tStart = (val))
#endif

#ifndef rtmGetStepSize
#define rtmGetStepSize(rtm)            ((rtm)->Timing.stepSize)
#endif

#ifndef rtmSetStepSize
#define rtmSetStepSize(rtm, val)       ((rtm)->Timing.stepSize = (val))
#endif

#ifndef rtmGetStopRequestedFlag
#define rtmGetStopRequestedFlag(rtm)   ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequestedFlag
#define rtmSetStopRequestedFlag(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetTaskCounters
#define rtmGetTaskCounters(rtm)        ()
#endif

#ifndef rtmSetTaskCounters
#define rtmSetTaskCounters(rtm, val)   ()
#endif

#ifndef rtmGetTaskTimeArray
#define rtmGetTaskTimeArray(rtm)       ((rtm)->Timing.tArray)
#endif

#ifndef rtmSetTaskTimeArray
#define rtmSetTaskTimeArray(rtm, val)  ((rtm)->Timing.tArray = (val))
#endif

#ifndef rtmGetTimePtr
#define rtmGetTimePtr(rtm)             ((rtm)->Timing.t)
#endif

#ifndef rtmSetTimePtr
#define rtmSetTimePtr(rtm, val)        ((rtm)->Timing.t = (val))
#endif

#ifndef rtmGetTimingData
#define rtmGetTimingData(rtm)          ((rtm)->Timing.timingData)
#endif

#ifndef rtmSetTimingData
#define rtmSetTimingData(rtm, val)     ((rtm)->Timing.timingData = (val))
#endif

#ifndef rtmGetU
#define rtmGetU(rtm)                   ((rtm)->inputs)
#endif

#ifndef rtmSetU
#define rtmSetU(rtm, val)              ((rtm)->inputs = (val))
#endif

#ifndef rtmGetVarNextHitTimesListPtr
#define rtmGetVarNextHitTimesListPtr(rtm) ((rtm)->Timing.varNextHitTimesList)
#endif

#ifndef rtmSetVarNextHitTimesListPtr
#define rtmSetVarNextHitTimesListPtr(rtm, val) ((rtm)->Timing.varNextHitTimesList = (val))
#endif

#ifndef rtmGetY
#define rtmGetY(rtm)                   ((rtm)->outputs)
#endif

#ifndef rtmSetY
#define rtmSetY(rtm, val)              ((rtm)->outputs = (val))
#endif

#ifndef rtmGetZCCacheNeedsReset
#define rtmGetZCCacheNeedsReset(rtm)   ((rtm)->zCCacheNeedsReset)
#endif

#ifndef rtmSetZCCacheNeedsReset
#define rtmSetZCCacheNeedsReset(rtm, val) ((rtm)->zCCacheNeedsReset = (val))
#endif

#ifndef rtmGetZCSignalValues
#define rtmGetZCSignalValues(rtm)      ((rtm)->zcSignalValues)
#endif

#ifndef rtmSetZCSignalValues
#define rtmSetZCSignalValues(rtm, val) ((rtm)->zcSignalValues = (val))
#endif

#ifndef rtmGet_TimeOfLastOutput
#define rtmGet_TimeOfLastOutput(rtm)   ((rtm)->Timing.timeOfLastOutput)
#endif

#ifndef rtmSet_TimeOfLastOutput
#define rtmSet_TimeOfLastOutput(rtm, val) ((rtm)->Timing.timeOfLastOutput = (val))
#endif

#ifndef rtmGetdX
#define rtmGetdX(rtm)                  ((rtm)->derivs)
#endif

#ifndef rtmSetdX
#define rtmSetdX(rtm, val)             ((rtm)->derivs = (val))
#endif

#ifndef rtmGettimingBridge
#define rtmGettimingBridge(rtm)        ()
#endif

#ifndef rtmSettimingBridge
#define rtmSettimingBridge(rtm, val)   ()
#endif

#ifndef rtmGetChecksumVal
#define rtmGetChecksumVal(rtm, idx)    ((rtm)->Sizes.checksums[idx])
#endif

#ifndef rtmSetChecksumVal
#define rtmSetChecksumVal(rtm, idx, val) ((rtm)->Sizes.checksums[idx] = (val))
#endif

#ifndef rtmGetDWork
#define rtmGetDWork(rtm, idx)          ((rtm)->dwork[idx])
#endif

#ifndef rtmSetDWork
#define rtmSetDWork(rtm, idx, val)     ((rtm)->dwork[idx] = (val))
#endif

#ifndef rtmGetOffsetTime
#define rtmGetOffsetTime(rtm, idx)     ((rtm)->Timing.offsetTimes[idx])
#endif

#ifndef rtmSetOffsetTime
#define rtmSetOffsetTime(rtm, idx, val) ((rtm)->Timing.offsetTimes[idx] = (val))
#endif

#ifndef rtmGetSFunction
#define rtmGetSFunction(rtm, idx)      ((rtm)->childSfunctions[idx])
#endif

#ifndef rtmSetSFunction
#define rtmSetSFunction(rtm, idx, val) ((rtm)->childSfunctions[idx] = (val))
#endif

#ifndef rtmGetSampleTime
#define rtmGetSampleTime(rtm, idx)     ((rtm)->Timing.sampleTimes[idx])
#endif

#ifndef rtmSetSampleTime
#define rtmSetSampleTime(rtm, idx, val) ((rtm)->Timing.sampleTimes[idx] = (val))
#endif

#ifndef rtmGetSampleTimeTaskID
#define rtmGetSampleTimeTaskID(rtm, idx) ((rtm)->Timing.sampleTimeTaskIDPtr[idx])
#endif

#ifndef rtmSetSampleTimeTaskID
#define rtmSetSampleTimeTaskID(rtm, idx, val) ((rtm)->Timing.sampleTimeTaskIDPtr[idx] = (val))
#endif

#ifndef rtmGetVarNextHitTimeList
#define rtmGetVarNextHitTimeList(rtm, idx) ((rtm)->Timing.varNextHitTimesList[idx])
#endif

#ifndef rtmSetVarNextHitTimeList
#define rtmSetVarNextHitTimeList(rtm, idx, val) ((rtm)->Timing.varNextHitTimesList[idx] = (val))
#endif

#ifndef rtmIsContinuousTask
#define rtmIsContinuousTask(rtm, tid)  ((tid) == 0)
#endif

#ifndef rtmGetErrorStatus
#define rtmGetErrorStatus(rtm)         ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
#define rtmSetErrorStatus(rtm, val)    ((rtm)->errorStatus = (val))
#endif

#ifndef rtmIsMajorTimeStep
#define rtmIsMajorTimeStep(rtm)        (((rtm)->Timing.simTimeStep) == MAJOR_TIME_STEP)
#endif

#ifndef rtmIsMinorTimeStep
#define rtmIsMinorTimeStep(rtm)        (((rtm)->Timing.simTimeStep) == MINOR_TIME_STEP)
#endif

#ifndef rtmIsSampleHit
#define rtmIsSampleHit(rtm, sti, tid)  ((rtmIsMajorTimeStep((rtm)) && (rtm)->Timing.sampleHits[(rtm)->Timing.sampleTimeTaskIDPtr[sti]]))
#endif

#ifndef rtmGetStopRequested
#define rtmGetStopRequested(rtm)       ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
#define rtmSetStopRequested(rtm, val)  ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
#define rtmGetStopRequestedPtr(rtm)    (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
#define rtmGetT(rtm)                   (rtmGetTPtr((rtm))[0])
#endif

#ifndef rtmSetT
#define rtmSetT(rtm, val)                                        /* Do Nothing */
#endif

#ifndef rtmGetTFinal
#define rtmGetTFinal(rtm)              ((rtm)->Timing.tFinal)
#endif

#ifndef rtmSetTFinal
#define rtmSetTFinal(rtm, val)         ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetTPtr
#define rtmGetTPtr(rtm)                ((rtm)->Timing.t)
#endif

#ifndef rtmSetTPtr
#define rtmSetTPtr(rtm, val)           ((rtm)->Timing.t = (val))
#endif

#ifndef rtmGetTStart
#define rtmGetTStart(rtm)              ((rtm)->Timing.tStart)
#endif

#ifndef rtmSetTStart
#define rtmSetTStart(rtm, val)         ((rtm)->Timing.tStart = (val))
#endif

#ifndef rtmGetTaskTime
#define rtmGetTaskTime(rtm, sti)       (rtmGetTPtr((rtm))[(rtm)->Timing.sampleTimeTaskIDPtr[sti]])
#endif

#ifndef rtmSetTaskTime
#define rtmSetTaskTime(rtm, sti, val)  (rtmGetTPtr((rtm))[sti] = (val))
#endif

#ifndef rtmGetTimeOfLastOutput
#define rtmGetTimeOfLastOutput(rtm)    ((rtm)->Timing.timeOfLastOutput)
#endif

#ifdef rtmGetRTWSolverInfo
#undef rtmGetRTWSolverInfo
#endif

#define rtmGetRTWSolverInfo(rtm)       &((rtm)->solverInfo)

/* Definition for use in the target main file */
#define M220Model_v2_rtModel           RT_MODEL_M220Model_v2_T

/* Block signals (default storage) */
typedef struct {
  uint64_T Gain;                       /* '<S20>/Gain' */
  uint64_T Gain2;                      /* '<S20>/Gain2' */
  real_T Integrator2;                  /* '<S9>/Integrator2' */
  real_T Integrator1;                  /* '<S9>/Integrator1' */
  real_T Integrator4;                  /* '<S9>/Integrator4' */
  real_T Saturation3;                  /* '<S9>/Saturation3' */
  real_T Gain_b;                       /* '<S84>/Gain' */
  real_T MathFunction;                 /* '<S23>/Math Function' */
  real_T MathFunction1;                /* '<S23>/Math Function1' */
  real_T Integrator5;                  /* '<S9>/Integrator5' */
  real_T MathFunction2;                /* '<S23>/Math Function2' */
  real_T Add;                          /* '<S23>/Add' */
  real_T Sqrt;                         /* '<S23>/Sqrt' */
  real_T Switch;                       /* '<S23>/Switch' */
  real_T Divide;                       /* '<S23>/Divide' */
  real_T Product5;                     /* '<S23>/Product5' */
  real_T Divide2;                      /* '<S23>/Divide2' */
  real_T Product2;                     /* '<S23>/Product2' */
  real_T Divide1;                      /* '<S23>/Divide1' */
  real_T Saturation;                   /* '<S23>/Saturation' */
  real_T PulseGenerator;               /* '<S16>/Pulse Generator' */
  real_T PulseGenerator1;              /* '<S16>/Pulse Generator1' */
  real_T Square;                       /* '<S11>/Square' */
  real_T Square1;                      /* '<S11>/Square1' */
  real_T Square2;                      /* '<S11>/Square2' */
  real_T Switch_e;                     /* '<S11>/Switch' */
  real_T Product2_g;                   /* '<S2>/Product2' */
  real_T Divide1_i;                    /* '<S2>/Divide1' */
  real_T Product3;                     /* '<S2>/Product3' */
  real_T Divide2_j;                    /* '<S2>/Divide2' */
  real_T MathFunction_d;               /* '<S2>/Math Function' */
  real_T MathFunction1_n;              /* '<S2>/Math Function1' */
  real_T Add_h;                        /* '<S2>/Add' */
  real_T Sqrt_a;                       /* '<S2>/Sqrt' */
  real_T Derivative;                   /* '<S2>/Derivative' */
  real_T Product;                      /* '<S2>/Product' */
  real_T Integrator11;                 /* '<S9>/Integrator11' */
  real_T Cos;                          /* '<S2>/Cos' */
  real_T Divide_g;                     /* '<S2>/Divide' */
  real_T Product1;                     /* '<S2>/Product1' */
  real_T LookupTableDynamic3;          /* '<S5>/Lookup Table Dynamic3' */
  real_T Switch_j;                     /* '<S10>/Switch' */
  real_T Integrator;                   /* '<S24>/Integrator' */
  real_T TrigonometricFunction1;       /* '<S24>/Trigonometric Function1' */
  real_T Switch_d;                     /* '<S12>/Switch' */
  real_T Product1_l;                   /* '<S24>/Product1' */
  real_T Divide2_p;                    /* '<S31>/Divide2' */
  real_T Product1_i;                   /* '<S31>/Product1' */
  real_T LookupTableDynamic_h;         /* '<S22>/Lookup Table Dynamic' */
  real_T TrigonometricFunction2;       /* '<S24>/Trigonometric Function2' */
  real_T Product2_j;                   /* '<S24>/Product2' */
  real_T Divide_c;                     /* '<S24>/Divide' */
  real_T Product_f;                    /* '<S31>/Product' */
  real_T Add_m;                        /* '<S31>/Add' */
  real_T Add1;                         /* '<S31>/Add1' */
  real_T Switch_g;                     /* '<S31>/Switch' */
  real_T Add_n;                        /* '<S25>/Add' */
  real_T TrigonometricFunction;        /* '<S24>/Trigonometric Function' */
  real_T Product_c;                    /* '<S24>/Product' */
  real_T Product_m;                    /* '<S30>/Product' */
  real_T Subtract;                     /* '<S30>/Subtract' */
  real_T Product2_c;                   /* '<S30>/Product2' */
  real_T Subtract1;                    /* '<S30>/Subtract1' */
  real_T Divide_f;                     /* '<S30>/Divide' */
  real_T TrigonometricFunction_c;      /* '<S30>/Trigonometric Function' */
  real_T Product1_o;                   /* '<S30>/Product1' */
  real_T TrigonometricFunction_a;      /* '<S25>/Trigonometric Function' */
  real_T Divide1_o;                    /* '<S25>/Divide1' */
  real_T Divide2_m;                    /* '<S25>/Divide2' */
  real_T Add_j;                        /* '<S29>/Add' */
  real_T Derivative_h;                 /* '<S32>/Derivative' */
  real_T Product2_n;                   /* '<S32>/Product2' */
  real_T Divide3;                      /* '<S32>/Divide3' */
  real_T Derivative1;                  /* '<S32>/Derivative1' */
  real_T Product5_a;                   /* '<S32>/Product5' */
  real_T Divide1_n;                    /* '<S32>/Divide1' */
  real_T Derivative_o;                 /* '<Root>/Derivative' */
  real_T Product3_k;                   /* '<S32>/Product3' */
  real_T Add1_g;                       /* '<S32>/Add1' */
  real_T Product1_n;                   /* '<S29>/Product1' */
  real_T Product3_g;                   /* '<S29>/Product3' */
  real_T Square_d;                     /* '<S29>/Square' */
  real_T TrigonometricFunction_h;      /* '<S29>/Trigonometric Function' */
  real_T Product4;                     /* '<S29>/Product4' */
  real_T Square1_g;                    /* '<S29>/Square1' */
  real_T Add1_n;                       /* '<S29>/Add1' */
  real_T Sqrt_n;                       /* '<S29>/Sqrt' */
  real_T Product2_i;                   /* '<S29>/Product2' */
  real_T Divide_m;                     /* '<S29>/Divide' */
  real_T Switch_jk;                    /* '<S25>/Switch' */
  real_T Product1_ob;                  /* '<S25>/Product1' */
  real_T Product2_h;                   /* '<S25>/Product2' */
  real_T Divide2_c;                    /* '<S42>/Divide2' */
  real_T Product1_f;                   /* '<S42>/Product1' */
  real_T Product_i;                    /* '<S42>/Product' */
  real_T Add_p;                        /* '<S42>/Add' */
  real_T Add1_d;                       /* '<S42>/Add1' */
  real_T Switch_bb;                    /* '<S42>/Switch' */
  real_T Add_c;                        /* '<S26>/Add' */
  real_T Product_p;                    /* '<S41>/Product' */
  real_T Subtract_g;                   /* '<S41>/Subtract' */
  real_T Product2_e;                   /* '<S41>/Product2' */
  real_T Subtract1_d;                  /* '<S41>/Subtract1' */
  real_T Divide_n;                     /* '<S41>/Divide' */
  real_T TrigonometricFunction_f;      /* '<S41>/Trigonometric Function' */
  real_T Product1_j;                   /* '<S41>/Product1' */
  real_T TrigonometricFunction_n;      /* '<S26>/Trigonometric Function' */
  real_T Divide1_c;                    /* '<S26>/Divide1' */
  real_T Divide2_l;                    /* '<S26>/Divide2' */
  real_T Add_ci;                       /* '<S40>/Add' */
  real_T Derivative_b;                 /* '<S43>/Derivative' */
  real_T Product2_gb;                  /* '<S43>/Product2' */
  real_T Divide3_m;                    /* '<S43>/Divide3' */
  real_T Derivative1_d;                /* '<S43>/Derivative1' */
  real_T Product5_m;                   /* '<S43>/Product5' */
  real_T Divide1_p;                    /* '<S43>/Divide1' */
  real_T Product3_j;                   /* '<S43>/Product3' */
  real_T Add1_h;                       /* '<S43>/Add1' */
  real_T Product1_c;                   /* '<S40>/Product1' */
  real_T Product3_kf;                  /* '<S40>/Product3' */
  real_T Square_n;                     /* '<S40>/Square' */
  real_T TrigonometricFunction_i;      /* '<S40>/Trigonometric Function' */
  real_T Product4_l;                   /* '<S40>/Product4' */
  real_T Square1_o;                    /* '<S40>/Square1' */
  real_T Add1_n2;                      /* '<S40>/Add1' */
  real_T Sqrt_nj;                      /* '<S40>/Sqrt' */
  real_T Product2_l;                   /* '<S40>/Product2' */
  real_T Divide_p;                     /* '<S40>/Divide' */
  real_T Switch_a;                     /* '<S26>/Switch' */
  real_T Product1_oo;                  /* '<S26>/Product1' */
  real_T Product2_f;                   /* '<S26>/Product2' */
  real_T Divide2_k;                    /* '<S53>/Divide2' */
  real_T Product1_nt;                  /* '<S53>/Product1' */
  real_T Product_d;                    /* '<S53>/Product' */
  real_T Add_f;                        /* '<S53>/Add' */
  real_T TrigonometricFunction1_j;     /* '<S53>/Trigonometric Function1' */
  real_T Gain1_b;                      /* '<S58>/Gain1' */
  real_T Product3_b;                   /* '<S53>/Product3' */
  real_T Add2;                         /* '<S53>/Add2' */
  real_T TrigonometricFunction_cb;     /* '<S53>/Trigonometric Function' */
  real_T Product2_n0;                  /* '<S53>/Product2' */
  real_T Add3;                         /* '<S53>/Add3' */
  real_T Add1_n1;                      /* '<S53>/Add1' */
  real_T Switch_on;                    /* '<S53>/Switch' */
  real_T Add_cia;                      /* '<S27>/Add' */
  real_T Product_ir;                   /* '<S52>/Product' */
  real_T Subtract_i;                   /* '<S52>/Subtract' */
  real_T Product2_ee;                  /* '<S52>/Product2' */
  real_T Subtract1_j;                  /* '<S52>/Subtract1' */
  real_T Divide_k;                     /* '<S52>/Divide' */
  real_T TrigonometricFunction_l;      /* '<S52>/Trigonometric Function' */
  real_T Gain1_p;                      /* '<S55>/Gain1' */
  real_T Subtract2;                    /* '<S52>/Subtract2' */
  real_T TrigonometricFunction_e;      /* '<S27>/Trigonometric Function' */
  real_T Divide1_l;                    /* '<S27>/Divide1' */
  real_T Divide2_e;                    /* '<S27>/Divide2' */
  real_T Add_k;                        /* '<S51>/Add' */
  real_T Derivative_p;                 /* '<S54>/Derivative' */
  real_T Product2_m;                   /* '<S54>/Product2' */
  real_T Divide3_b;                    /* '<S54>/Divide3' */
  real_T Derivative1_p;                /* '<S54>/Derivative1' */
  real_T Product5_l;                   /* '<S54>/Product5' */
  real_T Divide1_po;                   /* '<S54>/Divide1' */
  real_T Product3_m;                   /* '<S54>/Product3' */
  real_T Add1_i;                       /* '<S54>/Add1' */
  real_T Product1_jp;                  /* '<S51>/Product1' */
  real_T Product3_gk;                  /* '<S51>/Product3' */
  real_T Square_du;                    /* '<S51>/Square' */
  real_T TrigonometricFunction_k;      /* '<S51>/Trigonometric Function' */
  real_T Product4_lj;                  /* '<S51>/Product4' */
  real_T Square1_h;                    /* '<S51>/Square1' */
  real_T Add1_f;                       /* '<S51>/Add1' */
  real_T Sqrt_b;                       /* '<S51>/Sqrt' */
  real_T Product2_mf;                  /* '<S51>/Product2' */
  real_T Divide_d;                     /* '<S51>/Divide' */
  real_T Switch_ge;                    /* '<S27>/Switch' */
  real_T Product1_p;                   /* '<S27>/Product1' */
  real_T Product2_ng;                  /* '<S27>/Product2' */
  real_T Divide2_pv;                   /* '<S66>/Divide2' */
  real_T Product1_e;                   /* '<S66>/Product1' */
  real_T Product_n;                    /* '<S66>/Product' */
  real_T Add_e;                        /* '<S66>/Add' */
  real_T TrigonometricFunction1_jd;    /* '<S66>/Trigonometric Function1' */
  real_T Gain1_f;                      /* '<S71>/Gain1' */
  real_T Product3_mz;                  /* '<S66>/Product3' */
  real_T Add2_h;                       /* '<S66>/Add2' */
  real_T TrigonometricFunction_g;      /* '<S66>/Trigonometric Function' */
  real_T Product2_k;                   /* '<S66>/Product2' */
  real_T Add3_c;                       /* '<S66>/Add3' */
  real_T Add1_f0;                      /* '<S66>/Add1' */
  real_T Switch_ez;                    /* '<S66>/Switch' */
  real_T Add_a;                        /* '<S28>/Add' */
  real_T Product_e;                    /* '<S65>/Product' */
  real_T Subtract_f;                   /* '<S65>/Subtract' */
  real_T Product2_d;                   /* '<S65>/Product2' */
  real_T Subtract1_f;                  /* '<S65>/Subtract1' */
  real_T Divide_b;                     /* '<S65>/Divide' */
  real_T TrigonometricFunction_nu;     /* '<S65>/Trigonometric Function' */
  real_T Gain1_e;                      /* '<S68>/Gain1' */
  real_T Subtract2_p;                  /* '<S65>/Subtract2' */
  real_T TrigonometricFunction_d;      /* '<S28>/Trigonometric Function' */
  real_T Divide1_ix;                   /* '<S28>/Divide1' */
  real_T Divide2_e2;                   /* '<S28>/Divide2' */
  real_T Add_km;                       /* '<S64>/Add' */
  real_T Derivative_n;                 /* '<S67>/Derivative' */
  real_T Product2_go;                  /* '<S67>/Product2' */
  real_T Divide3_f;                    /* '<S67>/Divide3' */
  real_T Derivative1_g;                /* '<S67>/Derivative1' */
  real_T Product5_o;                   /* '<S67>/Product5' */
  real_T Divide1_b;                    /* '<S67>/Divide1' */
  real_T Product3_d;                   /* '<S67>/Product3' */
  real_T Add1_c;                       /* '<S67>/Add1' */
  real_T Product1_jy;                  /* '<S64>/Product1' */
  real_T Product3_a;                   /* '<S64>/Product3' */
  real_T Square_m;                     /* '<S64>/Square' */
  real_T TrigonometricFunction_kf;     /* '<S64>/Trigonometric Function' */
  real_T Product4_b;                   /* '<S64>/Product4' */
  real_T Square1_e;                    /* '<S64>/Square1' */
  real_T Add1_gb;                      /* '<S64>/Add1' */
  real_T Sqrt_d;                       /* '<S64>/Sqrt' */
  real_T Product2_o;                   /* '<S64>/Product2' */
  real_T Divide_h;                     /* '<S64>/Divide' */
  real_T Switch_p;                     /* '<S28>/Switch' */
  real_T Product1_fn;                  /* '<S28>/Product1' */
  real_T Product2_oz;                  /* '<S28>/Product2' */
  real_T Gain1_j;                      /* '<S3>/Gain1' */
  real_T Steering_Angle_Limits;        /* '<S9>/Steering_Angle_Limits' */
  real_T Gain1_fo;                     /* '<S78>/Gain1' */
  real_T TrigonometricFunction1_k;     /* '<S9>/Trigonometric Function1' */
  real_T Add3_cg;                      /* '<S9>/Add3' */
  real_T Product6;                     /* '<S9>/Product6' */
  real_T Add4;                         /* '<S9>/Add4' */
  real_T TrigonometricFunction2_f;     /* '<S9>/Trigonometric Function2' */
  real_T Product7;                     /* '<S9>/Product7' */
  real_T Add10;                        /* '<S9>/Add10' */
  real_T Product2_cz;                  /* '<S9>/Product2' */
  real_T Delay;                        /* '<S9>/Delay' */
  real_T Gain_a;                       /* '<S9>/Gain' */
  real_T Delay2;                       /* '<S9>/Delay2' */
  real_T Product1_jb;                  /* '<S9>/Product1' */
  real_T Add1_fr;                      /* '<S9>/Add1' */
  real_T Add11;                        /* '<S9>/Add11' */
  real_T Add12;                        /* '<S9>/Add12' */
  real_T Add14;                        /* '<S9>/Add14' */
  real_T Delay4;                       /* '<S9>/Delay4' */
  real_T Product19;                    /* '<S9>/Product19' */
  real_T Delay1;                       /* '<S9>/Delay1' */
  real_T Delay3;                       /* '<S9>/Delay3' */
  real_T Product20;                    /* '<S9>/Product20' */
  real_T Add16;                        /* '<S9>/Add16' */
  real_T Add_i;                        /* '<S83>/Add' */
  real_T TrigonometricFunction_az;     /* '<S83>/Trigonometric Function' */
  real_T Product_nj;                   /* '<S83>/Product' */
  real_T TrigonometricFunction1_i;     /* '<S83>/Trigonometric Function1' */
  real_T Add2_l;                       /* '<S83>/Add2' */
  real_T Product1_j1;                  /* '<S83>/Product1' */
  real_T Add1_l;                       /* '<S83>/Add1' */
  real_T Product27;                    /* '<S9>/Product27' */
  real_T Product28;                    /* '<S9>/Product28' */
  real_T Add17;                        /* '<S9>/Add17' */
  real_T Product9;                     /* '<S9>/Product9' */
  real_T Product10;                    /* '<S9>/Product10' */
  real_T Add2_j;                       /* '<S9>/Add2' */
  real_T Product35;                    /* '<S9>/Product35' */
  real_T Add_o;                        /* '<S82>/Add' */
  real_T TrigonometricFunction_b;      /* '<S82>/Trigonometric Function' */
  real_T Product_j;                    /* '<S82>/Product' */
  real_T TrigonometricFunction1_d;     /* '<S82>/Trigonometric Function1' */
  real_T Add2_f;                       /* '<S82>/Add2' */
  real_T Product1_h;                   /* '<S82>/Product1' */
  real_T Add1_j;                       /* '<S82>/Add1' */
  real_T Product31;                    /* '<S9>/Product31' */
  real_T Add20;                        /* '<S9>/Add20' */
  real_T Product14;                    /* '<S9>/Product14' */
  real_T Product15;                    /* '<S9>/Product15' */
  real_T MagnitudeSquared;             /* '<S9>/Magnitude Squared' */
  real_T Sign;                         /* '<S9>/Sign' */
  real_T Product16;                    /* '<S9>/Product16' */
  real_T Add5;                         /* '<S9>/Add5' */
  real_T Product12;                    /* '<S9>/Product12' */
  real_T Product11;                    /* '<S9>/Product11' */
  real_T Add6;                         /* '<S9>/Add6' */
  real_T Add7;                         /* '<S9>/Add7' */
  real_T Product3_h;                   /* '<S9>/Product3' */
  real_T Add9;                         /* '<S9>/Add9' */
  real_T Product4_m;                   /* '<S9>/Product4' */
  real_T Product5_d;                   /* '<S9>/Product5' */
  real_T Product18;                    /* '<S9>/Product18' */
  real_T Add8;                         /* '<S9>/Add8' */
  real_T Integrator_j;                 /* '<S9>/Integrator' */
  real_T Integrator10;                 /* '<S9>/Integrator10' */
  real_T Integrator3;                  /* '<S9>/Integrator3' */
  real_T Integrator6;                  /* '<S9>/Integrator6' */
  real_T Integrator7;                  /* '<S9>/Integrator7' */
  real_T Integrator8;                  /* '<S9>/Integrator8' */
  real_T Integrator9;                  /* '<S9>/Integrator9' */
  real_T Product17;                    /* '<S9>/Product17' */
  real_T Product29;                    /* '<S9>/Product29' */
  real_T Product30;                    /* '<S9>/Product30' */
  real_T Gain_d;                       /* '<S85>/Gain' */
  real_T Gain_l;                       /* '<S86>/Gain' */
  real_T Switch_ak;                    /* '<S13>/Switch' */
  real_T Add_l;                        /* '<S13>/Add' */
  real_T Switch2_l;                    /* '<S13>/Switch2' */
  real_T Add1_hd;                      /* '<S13>/Add1' */
  real_T Switch17;                     /* '<S13>/Switch17' */
  real_T Add10_e;                      /* '<S13>/Add10' */
  real_T Switch19;                     /* '<S13>/Switch19' */
  real_T Add11_a;                      /* '<S13>/Add11' */
  real_T Switch1_b;                    /* '<S13>/Switch1' */
  real_T Add2_c;                       /* '<S13>/Add2' */
  real_T Switch3;                      /* '<S13>/Switch3' */
  real_T Add3_n;                       /* '<S13>/Add3' */
  real_T Switch8;                      /* '<S13>/Switch8' */
  real_T Add4_h;                       /* '<S13>/Add4' */
  real_T Switch10;                     /* '<S13>/Switch10' */
  real_T Add5_h;                       /* '<S13>/Add5' */
  real_T Switch9;                      /* '<S13>/Switch9' */
  real_T Add6_c;                       /* '<S13>/Add6' */
  real_T Switch11;                     /* '<S13>/Switch11' */
  real_T Add7_f;                       /* '<S13>/Add7' */
  real_T Switch16;                     /* '<S13>/Switch16' */
  real_T Add8_a;                       /* '<S13>/Add8' */
  real_T Switch18;                     /* '<S13>/Switch18' */
  real_T Add9_b;                       /* '<S13>/Add9' */
  real_T Switch4;                      /* '<S13>/Switch4' */
  real_T RateLimiter;                  /* '<S13>/Rate Limiter' */
  real_T RateLimiter2;                 /* '<S13>/Rate Limiter2' */
  real_T RateLimiter1;                 /* '<S13>/Rate Limiter1' */
  real_T RateLimiter3;                 /* '<S13>/Rate Limiter3' */
  real_T MultiportSwitch;              /* '<S13>/Multiport Switch' */
  real_T Switch12;                     /* '<S13>/Switch12' */
  real_T RateLimiter4;                 /* '<S13>/Rate Limiter4' */
  real_T RateLimiter6;                 /* '<S13>/Rate Limiter6' */
  real_T RateLimiter5;                 /* '<S13>/Rate Limiter5' */
  real_T RateLimiter7;                 /* '<S13>/Rate Limiter7' */
  real_T MultiportSwitch1;             /* '<S13>/Multiport Switch1' */
  real_T Switch20;                     /* '<S13>/Switch20' */
  real_T RateLimiter8;                 /* '<S13>/Rate Limiter8' */
  real_T RateLimiter10;                /* '<S13>/Rate Limiter10' */
  real_T RateLimiter9;                 /* '<S13>/Rate Limiter9' */
  real_T RateLimiter11;                /* '<S13>/Rate Limiter11' */
  real_T MultiportSwitch2;             /* '<S13>/Multiport Switch2' */
  real_T Product1_o5;                  /* '<S23>/Product1' */
  real_T Switch5;                      /* '<S13>/Switch5' */
  real_T Switch6;                      /* '<S13>/Switch6' */
  real_T Switch7;                      /* '<S13>/Switch7' */
  real_T Switch21;                     /* '<S13>/Switch21' */
  real_T Switch22;                     /* '<S13>/Switch22' */
  real_T Switch23;                     /* '<S13>/Switch23' */
  real_T Switch13;                     /* '<S13>/Switch13' */
  real_T Switch14;                     /* '<S13>/Switch14' */
  real_T Switch15;                     /* '<S13>/Switch15' */
  real_T LookupTableDynamic1_g;        /* '<S22>/Lookup Table Dynamic1' */
  real_T LookupTableDynamic2_k;        /* '<S22>/Lookup Table Dynamic2' */
  real_T Switch1_m;                    /* '<S11>/Switch1' */
  real_T Square3;                      /* '<S11>/Square3' */
  real_T Add_a2;                       /* '<S11>/Add' */
  real_T Sqrt_l;                       /* '<S11>/Sqrt' */
  real_T LookupTableDynamic2_f;        /* '<S11>/Lookup Table Dynamic2' */
  real_T Switch1_c;                    /* '<S10>/Switch1' */
  real_T LookupTableDynamic_f;         /* '<S10>/Lookup Table Dynamic' */
  real_T Subtract_k;                   /* '<S10>/Subtract' */
  real_T LookupTableDynamic_i;         /* '<S8>/Lookup Table Dynamic' */
  real_T Divide1_g;                    /* '<S66>/Divide1' */
  real_T Divide_j;                     /* '<S66>/Divide' */
  real_T Subtract_c;                   /* '<S28>/Subtract' */
  real_T Product3_f;                   /* '<S28>/Product3' */
  real_T Divide1_j;                    /* '<S53>/Divide1' */
  real_T Divide_l;                     /* '<S53>/Divide' */
  real_T Subtract_iq;                  /* '<S27>/Subtract' */
  real_T Product3_c;                   /* '<S27>/Product3' */
  real_T Divide1_gd;                   /* '<S42>/Divide1' */
  real_T Divide_i;                     /* '<S42>/Divide' */
  real_T Subtract_e;                   /* '<S26>/Subtract' */
  real_T Product3_i;                   /* '<S26>/Product3' */
  real_T Divide1_bf;                   /* '<S31>/Divide1' */
  real_T Divide_ls;                    /* '<S31>/Divide' */
  real_T Subtract_n;                   /* '<S25>/Subtract' */
  real_T Product3_ks;                  /* '<S25>/Product3' */
  uint32_T DataTypeConversion;         /* '<S23>/Data Type Conversion' */
  uint32_T DataTypeConversion1;        /* '<S23>/Data Type Conversion1' */
  boolean_T Compare;                   /* '<S92>/Compare' */
  boolean_T LogicalOperator1;          /* '<S12>/Logical Operator1' */
  boolean_T LogicalOperator4;          /* '<S12>/Logical Operator4' */
  boolean_T LogicalOperator;           /* '<S12>/Logical Operator' */
  boolean_T RelationalOperator;        /* '<S31>/Relational Operator' */
  boolean_T RelationalOperator_b;      /* '<S42>/Relational Operator' */
  boolean_T RelationalOperator_k;      /* '<S53>/Relational Operator' */
  boolean_T RelationalOperator_m;      /* '<S66>/Relational Operator' */
  boolean_T Compare_m;                 /* '<S115>/Compare' */
  boolean_T Compare_ib;                /* '<S93>/Compare' */
  boolean_T LogicalOperator4_i;        /* '<S13>/Logical Operator4' */
  boolean_T LogicalOperator8;          /* '<S13>/Logical Operator8' */
  boolean_T LogicalOperator_b;         /* '<S13>/Logical Operator' */
  boolean_T Compare_ie;                /* '<S116>/Compare' */
  boolean_T Compare_ii;                /* '<S119>/Compare' */
  boolean_T LogicalOperator5;          /* '<S13>/Logical Operator5' */
  boolean_T LogicalOperator9;          /* '<S13>/Logical Operator9' */
  boolean_T LogicalOperator1_m;        /* '<S13>/Logical Operator1' */
  boolean_T LogicalOperator28;         /* '<S13>/Logical Operator28' */
  boolean_T Compare_k;                 /* '<S106>/Compare' */
  boolean_T Compare_l;                 /* '<S107>/Compare' */
  boolean_T LogicalOperator22;         /* '<S13>/Logical Operator22' */
  boolean_T LogicalOperator26;         /* '<S13>/Logical Operator26' */
  boolean_T LogicalOperator29;         /* '<S13>/Logical Operator29' */
  boolean_T Compare_h;                 /* '<S110>/Compare' */
  boolean_T Compare_b;                 /* '<S111>/Compare' */
  boolean_T LogicalOperator23;         /* '<S13>/Logical Operator23' */
  boolean_T LogicalOperator27;         /* '<S13>/Logical Operator27' */
  boolean_T Compare_hd;                /* '<S113>/Compare' */
  boolean_T Compare_kv;                /* '<S114>/Compare' */
  boolean_T LogicalOperator2;          /* '<S13>/Logical Operator2' */
  boolean_T LogicalOperator6;          /* '<S13>/Logical Operator6' */
  boolean_T Compare_hr;                /* '<S117>/Compare' */
  boolean_T Compare_n2;                /* '<S118>/Compare' */
  boolean_T LogicalOperator3;          /* '<S13>/Logical Operator3' */
  boolean_T LogicalOperator7;          /* '<S13>/Logical Operator7' */
  boolean_T Compare_md;                /* '<S98>/Compare' */
  boolean_T Compare_mr;                /* '<S94>/Compare' */
  boolean_T LogicalOperator14;         /* '<S13>/Logical Operator14' */
  boolean_T LogicalOperator18;         /* '<S13>/Logical Operator18' */
  boolean_T LogicalOperator10;         /* '<S13>/Logical Operator10' */
  boolean_T Compare_e;                 /* '<S99>/Compare' */
  boolean_T Compare_ei;                /* '<S102>/Compare' */
  boolean_T LogicalOperator15;         /* '<S13>/Logical Operator15' */
  boolean_T LogicalOperator19;         /* '<S13>/Logical Operator19' */
  boolean_T LogicalOperator11;         /* '<S13>/Logical Operator11' */
  boolean_T Compare_a;                 /* '<S96>/Compare' */
  boolean_T Compare_mg;                /* '<S97>/Compare' */
  boolean_T LogicalOperator12;         /* '<S13>/Logical Operator12' */
  boolean_T LogicalOperator16;         /* '<S13>/Logical Operator16' */
  boolean_T Compare_p;                 /* '<S100>/Compare' */
  boolean_T Compare_ls;                /* '<S101>/Compare' */
  boolean_T LogicalOperator13;         /* '<S13>/Logical Operator13' */
  boolean_T LogicalOperator17;         /* '<S13>/Logical Operator17' */
  boolean_T Compare_bv;                /* '<S108>/Compare' */
  boolean_T Compare_d;                 /* '<S103>/Compare' */
  boolean_T LogicalOperator24;         /* '<S13>/Logical Operator24' */
  boolean_T LogicalOperator20;         /* '<S13>/Logical Operator20' */
  boolean_T Compare_aj;                /* '<S109>/Compare' */
  boolean_T Compare_dg;                /* '<S112>/Compare' */
  boolean_T LogicalOperator25;         /* '<S13>/Logical Operator25' */
  boolean_T LogicalOperator21;         /* '<S13>/Logical Operator21' */
} B_M220Model_v2_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  real_T Delay_DSTATE;                 /* '<S9>/Delay' */
  real_T Delay2_DSTATE;                /* '<S9>/Delay2' */
  real_T Delay4_DSTATE;                /* '<S9>/Delay4' */
  real_T Delay1_DSTATE;                /* '<S9>/Delay1' */
  real_T Delay3_DSTATE;                /* '<S9>/Delay3' */
  real_T Memory_PreviousInput;         /* '<S13>/Memory' */
  real_T Memory2_PreviousInput;        /* '<S13>/Memory2' */
  real_T Memory1_PreviousInput;        /* '<S13>/Memory1' */
  real_T TimeStampA;                   /* '<S2>/Derivative' */
  real_T LastUAtTimeA;                 /* '<S2>/Derivative' */
  real_T TimeStampB;                   /* '<S2>/Derivative' */
  real_T LastUAtTimeB;                 /* '<S2>/Derivative' */
  real_T TimeStampA_h;                 /* '<S32>/Derivative' */
  real_T LastUAtTimeA_j;               /* '<S32>/Derivative' */
  real_T TimeStampB_m;                 /* '<S32>/Derivative' */
  real_T LastUAtTimeB_h;               /* '<S32>/Derivative' */
  real_T TimeStampA_h5;                /* '<S32>/Derivative1' */
  real_T LastUAtTimeA_g;               /* '<S32>/Derivative1' */
  real_T TimeStampB_a;                 /* '<S32>/Derivative1' */
  real_T LastUAtTimeB_i;               /* '<S32>/Derivative1' */
  real_T TimeStampA_m;                 /* '<Root>/Derivative' */
  real_T LastUAtTimeA_gr;              /* '<Root>/Derivative' */
  real_T TimeStampB_k;                 /* '<Root>/Derivative' */
  real_T LastUAtTimeB_l;               /* '<Root>/Derivative' */
  real_T TimeStampA_hx;                /* '<S43>/Derivative' */
  real_T LastUAtTimeA_p;               /* '<S43>/Derivative' */
  real_T TimeStampB_p;                 /* '<S43>/Derivative' */
  real_T LastUAtTimeB_c;               /* '<S43>/Derivative' */
  real_T TimeStampA_mc;                /* '<S43>/Derivative1' */
  real_T LastUAtTimeA_ge;              /* '<S43>/Derivative1' */
  real_T TimeStampB_l;                 /* '<S43>/Derivative1' */
  real_T LastUAtTimeB_ik;              /* '<S43>/Derivative1' */
  real_T TimeStampA_o;                 /* '<S54>/Derivative' */
  real_T LastUAtTimeA_i;               /* '<S54>/Derivative' */
  real_T TimeStampB_k1;                /* '<S54>/Derivative' */
  real_T LastUAtTimeB_g;               /* '<S54>/Derivative' */
  real_T TimeStampA_d;                 /* '<S54>/Derivative1' */
  real_T LastUAtTimeA_jv;              /* '<S54>/Derivative1' */
  real_T TimeStampB_ko;                /* '<S54>/Derivative1' */
  real_T LastUAtTimeB_i2;              /* '<S54>/Derivative1' */
  real_T TimeStampA_g;                 /* '<S67>/Derivative' */
  real_T LastUAtTimeA_ju;              /* '<S67>/Derivative' */
  real_T TimeStampB_ku;                /* '<S67>/Derivative' */
  real_T LastUAtTimeB_m;               /* '<S67>/Derivative' */
  real_T TimeStampA_i;                 /* '<S67>/Derivative1' */
  real_T LastUAtTimeA_b;               /* '<S67>/Derivative1' */
  real_T TimeStampB_j;                 /* '<S67>/Derivative1' */
  real_T LastUAtTimeB_hn;              /* '<S67>/Derivative1' */
  real_T PrevY;                        /* '<S13>/Rate Limiter' */
  real_T PrevY_d;                      /* '<S13>/Rate Limiter2' */
  real_T PrevY_f;                      /* '<S13>/Rate Limiter1' */
  real_T PrevY_b;                      /* '<S13>/Rate Limiter3' */
  real_T PrevY_j;                      /* '<S13>/Rate Limiter4' */
  real_T PrevY_h;                      /* '<S13>/Rate Limiter6' */
  real_T PrevY_fv;                     /* '<S13>/Rate Limiter5' */
  real_T PrevY_e;                      /* '<S13>/Rate Limiter7' */
  real_T PrevY_o;                      /* '<S13>/Rate Limiter8' */
  real_T PrevY_d5;                     /* '<S13>/Rate Limiter10' */
  real_T PrevY_g;                      /* '<S13>/Rate Limiter9' */
  real_T PrevY_bh;                     /* '<S13>/Rate Limiter11' */
  int32_T clockTickCounter;            /* '<S16>/Pulse Generator' */
  int32_T clockTickCounter_c;          /* '<S16>/Pulse Generator1' */
} DW_M220Model_v2_T;

/* Continuous states (default storage) */
typedef struct {
  real_T Integrator2_CSTATE;           /* '<S9>/Integrator2' */
  real_T Integrator1_CSTATE;           /* '<S9>/Integrator1' */
  real_T Integrator4_CSTATE;           /* '<S9>/Integrator4' */
  real_T Integrator5_CSTATE;           /* '<S9>/Integrator5' */
  real_T Integrator11_CSTATE;          /* '<S9>/Integrator11' */
  real_T Integrator_CSTATE;            /* '<S24>/Integrator' */
  real_T Integrator_CSTATE_c;          /* '<S9>/Integrator' */
  real_T Integrator10_CSTATE;          /* '<S9>/Integrator10' */
  real_T Integrator3_CSTATE;           /* '<S9>/Integrator3' */
  real_T Integrator6_CSTATE;           /* '<S9>/Integrator6' */
  real_T Integrator7_CSTATE;           /* '<S9>/Integrator7' */
  real_T Integrator8_CSTATE;           /* '<S9>/Integrator8' */
  real_T Integrator9_CSTATE;           /* '<S9>/Integrator9' */
} X_M220Model_v2_T;

/* State derivatives (default storage) */
typedef struct {
  real_T Integrator2_CSTATE;           /* '<S9>/Integrator2' */
  real_T Integrator1_CSTATE;           /* '<S9>/Integrator1' */
  real_T Integrator4_CSTATE;           /* '<S9>/Integrator4' */
  real_T Integrator5_CSTATE;           /* '<S9>/Integrator5' */
  real_T Integrator11_CSTATE;          /* '<S9>/Integrator11' */
  real_T Integrator_CSTATE;            /* '<S24>/Integrator' */
  real_T Integrator_CSTATE_c;          /* '<S9>/Integrator' */
  real_T Integrator10_CSTATE;          /* '<S9>/Integrator10' */
  real_T Integrator3_CSTATE;           /* '<S9>/Integrator3' */
  real_T Integrator6_CSTATE;           /* '<S9>/Integrator6' */
  real_T Integrator7_CSTATE;           /* '<S9>/Integrator7' */
  real_T Integrator8_CSTATE;           /* '<S9>/Integrator8' */
  real_T Integrator9_CSTATE;           /* '<S9>/Integrator9' */
} XDot_M220Model_v2_T;

/* State disabled  */
typedef struct {
  boolean_T Integrator2_CSTATE;        /* '<S9>/Integrator2' */
  boolean_T Integrator1_CSTATE;        /* '<S9>/Integrator1' */
  boolean_T Integrator4_CSTATE;        /* '<S9>/Integrator4' */
  boolean_T Integrator5_CSTATE;        /* '<S9>/Integrator5' */
  boolean_T Integrator11_CSTATE;       /* '<S9>/Integrator11' */
  boolean_T Integrator_CSTATE;         /* '<S24>/Integrator' */
  boolean_T Integrator_CSTATE_c;       /* '<S9>/Integrator' */
  boolean_T Integrator10_CSTATE;       /* '<S9>/Integrator10' */
  boolean_T Integrator3_CSTATE;        /* '<S9>/Integrator3' */
  boolean_T Integrator6_CSTATE;        /* '<S9>/Integrator6' */
  boolean_T Integrator7_CSTATE;        /* '<S9>/Integrator7' */
  boolean_T Integrator8_CSTATE;        /* '<S9>/Integrator8' */
  boolean_T Integrator9_CSTATE;        /* '<S9>/Integrator9' */
} XDis_M220Model_v2_T;

/* Invariant block signals (default storage) */
typedef const struct tag_ConstB_M220Model_v2_T {
  real_T Product1;                     /* '<S33>/Product1' */
  real_T Product1_i;                   /* '<S34>/Product1' */
  real_T Product1_k;                   /* '<S35>/Product1' */
  real_T Product1_a;                   /* '<S36>/Product1' */
  real_T Product1_b;                   /* '<S37>/Product1' */
  real_T Product1_e;                   /* '<S38>/Product1' */
  real_T Add;                          /* '<S32>/Add' */
  real_T Gain;                         /* '<S6>/Gain' */
  real_T Divide;                       /* '<S6>/Divide' */
  real_T Product1_p;                   /* '<S32>/Product1' */
  real_T Product;                      /* '<S32>/Product' */
  real_T Divide_b;                     /* '<S32>/Divide' */
  real_T Product4;                     /* '<S32>/Product4' */
  real_T Product6;                     /* '<S32>/Product6' */
  real_T Product1_m;                   /* '<S39>/Product1' */
  real_T Product1_d;                   /* '<S44>/Product1' */
  real_T Product1_kw;                  /* '<S45>/Product1' */
  real_T Product1_bd;                  /* '<S46>/Product1' */
  real_T Product1_l;                   /* '<S47>/Product1' */
  real_T Product1_f;                   /* '<S48>/Product1' */
  real_T Product1_ip;                  /* '<S49>/Product1' */
  real_T Add_h;                        /* '<S43>/Add' */
  real_T Product1_fw;                  /* '<S43>/Product1' */
  real_T Product_a;                    /* '<S43>/Product' */
  real_T Divide_bk;                    /* '<S43>/Divide' */
  real_T Product4_p;                   /* '<S43>/Product4' */
  real_T Product6_j;                   /* '<S43>/Product6' */
  real_T Product1_o;                   /* '<S50>/Product1' */
  real_T Product1_pc;                  /* '<S56>/Product1' */
  real_T Product1_g;                   /* '<S57>/Product1' */
  real_T Product1_ek;                  /* '<S59>/Product1' */
  real_T Product1_n;                   /* '<S60>/Product1' */
  real_T Product1_h;                   /* '<S61>/Product1' */
  real_T Product1_kn;                  /* '<S62>/Product1' */
  real_T Add_k;                        /* '<S54>/Add' */
  real_T Product1_av;                  /* '<S54>/Product1' */
  real_T Product_j;                    /* '<S54>/Product' */
  real_T Divide_c;                     /* '<S54>/Divide' */
  real_T Product4_o;                   /* '<S54>/Product4' */
  real_T Product6_h;                   /* '<S54>/Product6' */
  real_T Product1_lq;                  /* '<S63>/Product1' */
  real_T Product1_nh;                  /* '<S69>/Product1' */
  real_T Product1_na;                  /* '<S70>/Product1' */
  real_T Product1_c;                   /* '<S72>/Product1' */
  real_T Product1_kv;                  /* '<S73>/Product1' */
  real_T Product1_lj;                  /* '<S74>/Product1' */
  real_T Product1_hc;                  /* '<S75>/Product1' */
  real_T Add_m;                        /* '<S67>/Add' */
  real_T Product1_lp;                  /* '<S67>/Product1' */
  real_T Product_k;                    /* '<S67>/Product' */
  real_T Divide_j;                     /* '<S67>/Divide' */
  real_T Product4_j;                   /* '<S67>/Product4' */
  real_T Product6_p;                   /* '<S67>/Product6' */
  real_T Product1_pa;                  /* '<S76>/Product1' */
  real_T Product4_h;                   /* '<S2>/Product4' */
  real_T Add1;                         /* '<S2>/Add1' */
  real_T Product1_dz;                  /* '<S77>/Product1' */
  real_T Gain_b;                       /* '<S7>/Gain' */
  real_T Divide_o;                     /* '<S7>/Divide' */
  real_T Product1_kw1;                 /* '<S89>/Product1' */
  real_T Product1_hj;                  /* '<S90>/Product1' */
  real_T Add2;                         /* '<S87>/Add2' */
  real_T Divide_bn;                    /* '<S87>/Divide' */
  real_T Product1_ia;                  /* '<S91>/Product1' */
  real_T Add1_e;                       /* '<S87>/Add1' */
  real_T Product1_gh;                  /* '<S87>/Product1' */
  real_T Product_jf;                   /* '<S87>/Product' */
  real_T Add_i;                        /* '<S87>/Add' */
  real_T Product2;                     /* '<S87>/Product2' */
  real_T Product4_n;                   /* '<S87>/Product4' */
  real_T Divide5;                      /* '<S9>/Divide5' */
  real_T Divide6;                      /* '<S9>/Divide6' */
  real_T Divide7;                      /* '<S9>/Divide7' */
  real_T Divide8;                      /* '<S9>/Divide8' */
  real_T Add13;                        /* '<S9>/Add13' */
  real_T Product23;                    /* '<S9>/Product23' */
  real_T Product24;                    /* '<S9>/Product24' */
  real_T Product25;                    /* '<S9>/Product25' */
  real_T Product26;                    /* '<S9>/Product26' */
  real_T Add15;                        /* '<S9>/Add15' */
  real_T Add18;                        /* '<S9>/Add18' */
  real_T Divide11;                     /* '<S9>/Divide11' */
  real_T Divide12;                     /* '<S9>/Divide12' */
  real_T Add19;                        /* '<S9>/Add19' */
  real_T Divide13;                     /* '<S9>/Divide13' */
  real_T Divide14;                     /* '<S9>/Divide14' */
  real_T Add21;                        /* '<S9>/Add21' */
  real_T Add22;                        /* '<S9>/Add22' */
  real_T Gain1;                        /* '<S79>/Gain1' */
  real_T Gain1_h;                      /* '<S80>/Gain1' */
  real_T Gain1_g;                      /* '<S81>/Gain1' */
  real_T Divide1;                      /* '<S9>/Divide1' */
  real_T Divide10;                     /* '<S9>/Divide10' */
  real_T Divide2;                      /* '<S9>/Divide2' */
  real_T Divide3;                      /* '<S9>/Divide3' */
  real_T Divide4;                      /* '<S9>/Divide4' */
  real_T Divide9;                      /* '<S9>/Divide9' */
  real_T Gain1_b;                      /* '<S9>/Gain1' */
  real_T TrigonometricFunction;        /* '<S9>/Trigonometric Function' */
  real_T Saturation1;                  /* '<S9>/Saturation1' */
  real_T Product13;                    /* '<S9>/Product13' */
  real_T Product21;                    /* '<S9>/Product21' */
  real_T TrigonometricFunction3;       /* '<S9>/Trigonometric Function3' */
  real_T TrigonometricFunction4;       /* '<S9>/Trigonometric Function4' */
  real_T Product22;                    /* '<S9>/Product22' */
  real_T Product32;                    /* '<S9>/Product32' */
  real_T Product33;                    /* '<S9>/Product33' */
  real_T TrigonometricFunction5;       /* '<S9>/Trigonometric Function5' */
  real_T Saturation2;                  /* '<S9>/Saturation2' */
  real_T TrigonometricFunction6;       /* '<S9>/Trigonometric Function6' */
  real_T Saturation4;                  /* '<S9>/Saturation4' */
  real_T Product8;                     /* '<S9>/Product8' */
  real_T Product1_l4;                  /* '<S88>/Product1' */
  real_T Product3;                     /* '<S23>/Product3' */
} ConstB_M220Model_v2_T;

#ifndef ODE4_INTG
#define ODE4_INTG

/* ODE4 Integration Data */
typedef struct {
  real_T *y;                           /* output */
  real_T *f[4];                        /* derivatives */
} ODE4_IntgData;

#endif

/* Constant parameters (default storage) */
typedef struct {
  /* Expression: BrakeHydraulPressYData_psi
   * Referenced by: '<Root>/BrakeHydraulPressXData_psi'
   */
  real_T BrakeHydraulPressXData_psi_Valu[2];

  /* Pooled Parameter (Mixed Expressions)
   * Referenced by:
   *   '<Root>/ControlBoxTempXData_mV'
   *   '<Root>/HeatExchangeXData_mV'
   */
  real_T pooled8[201];

  /* Pooled Parameter (Mixed Expressions)
   * Referenced by:
   *   '<Root>/ControlBoxTempYData_degC'
   *   '<Root>/HeatExchangeYData_degC'
   */
  real_T pooled9[201];

  /* Pooled Parameter (Mixed Expressions)
   * Referenced by:
   *   '<Root>/FrntDeckAngleXData_deg'
   *   '<Root>/LftDeckAngleXData_deg'
   *   '<Root>/RghtDeckAngleXData_deg'
   */
  real_T pooled12[8];

  /* Pooled Parameter (Mixed Expressions)
   * Referenced by:
   *   '<Root>/FrntDeckLiftPressureYData_psi'
   *   '<Root>/LftDeckLiftPressureYData_psi'
   *   '<Root>/RghtDeckLiftPressureYData_psi'
   */
  real_T pooled13[8];

  /* Expression: HydraulicOilLvlAct_gal
   * Referenced by: '<S11>/Constant'
   */
  real_T Constant_Value_k[2];

  /* Pooled Parameter (Mixed Expressions)
   * Referenced by:
   *   '<S22>/Can2SteerAngleX_int'
   *   '<S22>/JoystickPosCmdPct_int'
   */
  real_T pooled23[3];

  /* Expression: Can2SteerAngleY_int
   * Referenced by: '<S22>/Can2SteerAngleY_int'
   */
  real_T Can2SteerAngleY_int_Value[3];

  /* Expression: JoystickPosCmdSpeedConv_m_sec
   * Referenced by: '<S22>/JoystickPosCmdSpeedConv_m_sec'
   */
  real_T JoystickPosCmdSpeedConv_m_sec_V[3];
} ConstP_M220Model_v2_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  real_T SteeringPosMsg_int;           /* '<Root>/SteeringPosMsg_int' */
  real_T JoystickPosMsg_int;           /* '<Root>/JoystickPosMsg_int' */
  real_T JoystickButton1Msg_bool;      /* '<Root>/JoystickButton1Msg_bool' */
  real_T JoystickButton2Msg_bool;      /* '<Root>/JoystickButton2Msg_bool' */
  real_T JoystickButton3Msg_bool;      /* '<Root>/JoystickButton3Msg_bool' */
  real_T JoystickButton4Msg_bool;      /* '<Root>/JoystickButton4Msg_bool' */
  real_T JoystickButton5Msg_bool;      /* '<Root>/JoystickButton5Msg_bool' */
  real_T JoystickButton6Msg_bool;      /* '<Root>/JoystickButton6Msg_bool' */
  real_T JoystickButton7Msg_bool;      /* '<Root>/JoystickButton7Msg_bool' */
  real_T JoystickButton8Msg_bool;      /* '<Root>/JoystickButton8Msg_bool' */
  real_T JoystickButton9Msg_bool;      /* '<Root>/JoystickButton9Msg_bool' */
  real_T JoystickButton10Msg_bool;     /* '<Root>/JoystickButton10Msg_bool' */
  real_T HMIVehicleSpeed_mph;          /* '<Root>/HMIVehicleSpeed_mph' */
  boolean_T HMIVehicleSpeedOvrd_bool;  /* '<Root>/HMIVehicleSpeedOvrd_bool' */
  boolean_T RlyEStarters_bool;         /* '<Root>/RlyEStarters_bool' */
  real_T Rly1LightsRight_bool;         /* '<Root>/Rly1LightsRight_bool' */
  real_T Rly2LightsLeft_bool;          /* '<Root>/Rly2LightsLeft_bool' */
  real_T Rly3LightsRearSide_bool;      /* '<Root>/Rly3LightsRearSide_bool' */
  real_T Rly4LightsAux_bool;           /* '<Root>/Rly4LightsAux_bool' */
  real_T Rly5FlashRight_bool;          /* '<Root>/Rly5FlashRight_bool' */
  real_T Rly6FlashLeft_bool;           /* '<Root>/Rly6FlashLeft_bool' */
  real_T Rly7Horn_bool;                /* '<Root>/Rly7Horn_bool' */
  real_T NI9426_1Spare1_bool;          /* '<Root>/NI9426_1Spare1_bool' */
  real_T Ssr2Brake_bool;               /* '<Root>/Ssr2Brake_bool' */
  real_T Ssr3LoadA_bool;               /* '<Root>/Ssr3LoadA_bool' */
  real_T Ssr4FloatFront_bool;          /* '<Root>/Ssr4FloatFront_bool' */
  real_T Ssr5FloatRight_bool;          /* '<Root>/Ssr5FloatRight_bool' */
  real_T Ssr6FloatLeft_bool;           /* '<Root>/Ssr6FloatLeft_bool' */
  real_T NI9426_1Spare2_bool;          /* '<Root>/NI9426_1Spare2_bool' */
  real_T NI9426_1Spare3_bool;          /* '<Root>/NI9426_1Spare3_bool' */
  real_T NI9426_1Spare4_bool;          /* '<Root>/NI9426_1Spare4_bool' */
  boolean_T RlyCPower_bool;            /* '<Root>/RlyCPower_bool' */
  real_T NI94261Spare5_bool;           /* '<Root>/NI94261Spare5_bool' */
  real_T RlyD2HydFan_bool;             /* '<Root>/RlyD2HydFan_bool' */
  real_T LEDAMS_bool;                  /* '<Root>/LEDAMS_bool' */
  real_T LEDEnable_bool;               /* '<Root>/LEDEnable_bool' */
  real_T Buzzer_bool;                  /* '<Root>/Buzzer_bool' */
  real_T NI9426_1Spare6_bool;          /* '<Root>/NI9426_1Spare6_bool' */
  real_T NI9426_1Spare7_bool;          /* '<Root>/NI9426_1Spare7_bool' */
  real_T RlyFAltEnable_bool;           /* '<Root>/RlyFAltEnable_bool' */
  real_T SsrBlower_bool;               /* '<Root>/SsrBlower_bool' */
  real_T BackupAlarm_bool;             /* '<Root>/BackupAlarm_bool' */
  boolean_T RlyAutoStart_bool;         /* '<Root>/RlyAutoStart_bool' */
  real_T RlyJCBEngineKill_bool;        /* '<Root>/RlyJCBEngineKill_bool' */
  real_T NI9426_1Spare8_bool;          /* '<Root>/NI9426_1Spare8_bool' */
  real_T NI9426_1Spare9_bool;          /* '<Root>/NI9426_1Spare9_bool' */
  real_T RearEStopButton_bool;         /* '<Root>/RearEStopButton_bool' */
  real_T NI9476Spare1_bool;            /* '<Root>/NI9476Spare1_bool' */
  real_T NI9476Spare2_bool;            /* '<Root>/NI9476Spare2_bool' */
  real_T NI9476Spare3_bool;            /* '<Root>/NI9476Spare3_bool' */
  real_T NI9476Spare4_bool;            /* '<Root>/NI9476Spare4_bool' */
  real_T NI9476Spare5_bool;            /* '<Root>/NI9476Spare5_bool' */
  real_T NI9476Spare6_bool;            /* '<Root>/NI9476Spare6_bool' */
  real_T NI9476Spare7_bool;            /* '<Root>/NI9476Spare7_bool' */
  real_T NI9476Spare8_bool;            /* '<Root>/NI9476Spare8_bool' */
  real_T NI9476Spare9_bool;            /* '<Root>/NI9476Spare9_bool' */
  real_T NI9476Spare10_bool;           /* '<Root>/NI9476Spare10_bool' */
  real_T NI9476Spare11_bool;           /* '<Root>/NI9476Spare11_bool' */
  real_T NI9476Spare12_bool;           /* '<Root>/NI9476Spare12_bool' */
  real_T NI9476Spare13_bool;           /* '<Root>/NI9476Spare13_bool' */
  real_T NI9476Spare14_bool;           /* '<Root>/NI9476Spare14_bool' */
  real_T NI9476Spare15_bool;           /* '<Root>/NI9476Spare15_bool' */
  real_T NI9476Spare16_bool;           /* '<Root>/NI9476Spare16_bool' */
  real_T NI9476Spare17_bool;           /* '<Root>/NI9476Spare17_bool' */
  real_T CabEstop_bool;                /* '<Root>/CabEstop_bool' */
  real_T MachineEnableSwitch_bool;     /* '<Root>/MachineEnableSwitch_bool' */
  real_T AutonomySwitch_bool;          /* '<Root>/AutonomySwitch_bool' */
  real_T NI9476Spare18_bool;           /* '<Root>/NI9476Spare18_bool' */
  real_T MainKeySwitch_bool;           /* '<Root>/MainKeySwitch_bool' */
  real_T NI9476Spare19_bool;           /* '<Root>/NI9476Spare19_bool' */
  real_T AutonomyRemoteStopSwitch_bool;
                                    /* '<Root>/AutonomyRemoteStopSwitch_bool' */
  real_T NI9476Spare20_bool;           /* '<Root>/NI9476Spare20_bool' */
  real_T NI9476Spare21_bool;           /* '<Root>/NI9476Spare21_bool' */
  real_T HMIHydraulOilSensorOvrd_bool;
                                     /* '<Root>/HMIHydraulOilSensorOvrd_bool' */
  real_T HMIHydraulOilSensor_qts;      /* '<Root>/HMIHydraulOilSensor_qts' */
  real_T HMISeatSwitchOvrd_bool;       /* '<Root>/HMISeatSwitchOvrd_bool' */
  real_T HMISeatSwitch_bool;           /* '<Root>/HMISeatSwitch_bool' */
  real_T NI9426_2Spare1_bool;          /* '<Root>/NI9426_2Spare1_bool' */
  real_T NI9426_2Spare2_bool;          /* '<Root>/NI9426_2Spare2_bool' */
  real_T NI9426_2Spare3_bool;          /* '<Root>/NI9426_2Spare3_bool' */
  real_T NI9426_2Spare4_bool;          /* '<Root>/NI9426_2Spare4_bool' */
  real_T NI9426_2Spare5_bool;          /* '<Root>/NI9426_2Spare5_bool' */
  real_T NI9426_2Spare6_bool;          /* '<Root>/NI9426_2Spare6_bool' */
  real_T NI9426_2Spare7_bool;          /* '<Root>/NI9426_2Spare7_bool' */
  real_T NI9426_2Spare8_bool;          /* '<Root>/NI9426_2Spare8_bool' */
  real_T NI9426_2Spare9_bool;          /* '<Root>/NI9426_2Spare9_bool' */
  real_T NI9426_2Spare10_bool;         /* '<Root>/NI9426_2Spare10_bool' */
  real_T NI9426_2Spare11_bool;         /* '<Root>/NI9426_2Spare11_bool' */
  real_T NI9426_2Spare12_bool;         /* '<Root>/NI9426_2Spare12_bool' */
  real_T NI9426_2Spare13_bool;         /* '<Root>/NI9426_2Spare13_bool' */
  real_T NI9426_2Spare14_bool;         /* '<Root>/NI9426_2Spare14_bool' */
  real_T NI9426_2Spare15_bool;         /* '<Root>/NI9426_2Spare15_bool' */
  real_T NI9426_2Spare16_bool;         /* '<Root>/NI9426_2Spare16_bool' */
  real_T NI9426_2Spare17_bool;         /* '<Root>/NI9426_2Spare17_bool' */
  uint32_T hv_DriveFwd_bool;           /* '<Root>/hv_DriveFwd_bool' */
  uint32_T hv_DriveReverse_bool;       /* '<Root>/hv_DriveReverse_bool' */
  real_T hv_SteerRtA_bool;             /* '<Root>/hv_SteerRtA_bool' */
  real_T hv_SteerLfA_bool;             /* '<Root>/hv_SteerLfA_bool' */
  real_T hv_SteerLfB_bool;             /* '<Root>/hv_SteerLfB_bool' */
  real_T hv_SteerRtB_bool;             /* '<Root>/hv_SteerRtB_bool' */
  real_T hv_TracRight_bool;            /* '<Root>/hv_TracRight_bool' */
  real_T hv_TracLeft_bool;             /* '<Root>/hv_TracLeft_bool' */
  boolean_T hv_Dk_Ft_Lift_bool;        /* '<Root>/hv_Dk_Ft_Lift_bool' */
  boolean_T hv_Dk_Ft_Low_bool;         /* '<Root>/hv_Dk_Ft_Low_bool' */
  boolean_T hv_Dk_Rt_Lift_bool;        /* '<Root>/hv_Dk_Rt_Lift_bool' */
  boolean_T hv_Dk_Rt_Low_bool;         /* '<Root>/hv_Dk_Rt_Low_bool' */
  boolean_T hv_Dk_Unlatch_bool;        /* '<Root>/hv_Dk_Unlatch_bool' */
  boolean_T hv_Dk_Lf_Low_bool;         /* '<Root>/hv_Dk_Lf_Low_bool' */
  boolean_T hv_Dk_Lf_Lift_bool;        /* '<Root>/hv_Dk_Lf_Lift_bool' */
  real_T NI9475Spare1_bool;            /* '<Root>/NI9475Spare1_bool' */
  real_T NI9475Spare2_bool;            /* '<Root>/NI9475Spare2_bool' */
  real_T NI9475Spare3_bool;            /* '<Root>/NI9475Spare3_bool' */
  real_T NI9475Spare4_bool;            /* '<Root>/NI9475Spare4_bool' */
  real_T NI9264_1Spare1_int;           /* '<Root>/NI9264_1Spare1_int' */
  real_T NI9264_1Spare2_int;           /* '<Root>/NI9264_1Spare2_int' */
  real_T NI9264_1Spare3_int;           /* '<Root>/NI9264_1Spare3_int' */
  real_T NI9264_1Spare4_int;           /* '<Root>/NI9264_1Spare4_int' */
  real_T HMIOvrdPower5VMonitor_bool;   /* '<Root>/HMIOvrdPower5VMonitor_bool' */
  real_T HMIOvrdPower12VMonitor_bool; /* '<Root>/HMIOvrdPower12VMonitor_bool' */
  real_T HMIOvrdPower24VMonitor_bool; /* '<Root>/HMIOvrdPower24VMonitor_bool' */
  real_T HMIPower12VMonitor_Vdc;       /* '<Root>/HMIPower12VMonitor_Vdc' */
  real_T HMIPower24VMonitor_Vdc;       /* '<Root>/HMIPower24VMonitor_Vdc' */
  real_T HMIPower5VMonitor_Vdc;        /* '<Root>/HMIPower5VMonitor_Vdc' */
  real_T HMIOvrdHeatExchangeTemp_bool;
                                     /* '<Root>/HMIOvrdHeatExchangeTemp_bool' */
  real_T HMIHeatExchangerTemp_degC;    /* '<Root>/HMIHeatExchangerTemp_degC' */
  real_T HeatExchangerTemp_mV;         /* '<Root>/HeatExchangerTemp_mV' */
  real_T HMIOvrdControlBoxTemp_bool;   /* '<Root>/HMIOvrdControlBoxTemp_bool' */
  real_T HMIControlBoxTemp_degC;       /* '<Root>/HMIControlBoxTemp_degC' */
  real_T ControlBoxTemp_mV;            /* '<Root>/ControlBoxTemp_mV' */
} ExtU_M220Model_v2_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real_T Vy_m_sec;                     /* '<Root>/Vy_m_sec' */
  real_T Vx_m_sec;                     /* '<Root>/Vx_m_sec' */
  real_T Yr_deg_sec;                   /* '<Root>/Yr_deg_sec' */
  real_T RevFrntLftTire_RPMin;         /* '<Root>/RevFrntLftTire_RPMin' */
  real_T RevFrntRghtTire_RPMin;        /* '<Root>/RevFrntRghtTire_RPMin' */
  uint64_T SpdEnc1Line1Frq_pps;        /* '<Root>/SpdEnc1Line1Frq_pps' */
  uint64_T SpdEnc1Line2Frq_int;        /* '<Root>/SpdEnc1Line2Frq_int' */
  uint64_T SpdEnc2Line1Frq_pps;        /* '<Root>/SpdEnc2Line1Frq_pps' */
  uint64_T SpdEnc2Line1Frq_int;        /* '<Root>/SpdEnc2Line1Frq_int' */
  real_T DO4NI9475Mon_bool;            /* '<Root>/DO4NI9475Mon_bool' */
  real_T DO5NI9475Mon_bool;            /* '<Root>/DO5NI9475Mon_bool' */
  real_T DO6NI9475Mon_bool;            /* '<Root>/DO6NI9475Mon_bool' */
  real_T DO7NI9475Mon_bool;            /* '<Root>/DO7NI9475Mon_bool' */
  real_T LightsRight_bool;             /* '<Root>/LightsRight_bool' */
  real_T LightsLeft_bool;              /* '<Root>/LightsLeft_bool' */
  real_T LightsRearSide_bool;          /* '<Root>/LightsRearSide_bool' */
  real_T LightsAux_bool;               /* '<Root>/LightsAux_bool' */
  real_T Horn_bool;                    /* '<Root>/Horn_bool' */
  real_T NI9426_1Spare1Out_bool;       /* '<Root>/NI9426_1Spare1Out_bool' */
  real_T NI9426_1Spare5Out_bool;       /* '<Root>/NI9426_1Spare5Out_bool' */
  real_T NI9426_1Spare6Out_bool;       /* '<Root>/NI9426_1Spare6Out_bool' */
  real_T NI9426_1Spare7Out_bool;       /* '<Root>/NI9426_1Spare7Out_bool' */
  real_T NI9426_1Spare8Out_bool;       /* '<Root>/NI9426_1Spare8Out_bool' */
  real_T NI9426_1Spare9Out_bool;       /* '<Root>/NI9426_1Spare9Out_bool' */
  real_T NI9426_1Spare2Out_bool;       /* '<Root>/NI9426_1Spare2Out_bool' */
  real_T NI9426_1Spare3Out_bool;       /* '<Root>/NI9426_1Spare3Out_bool' */
  real_T NI9426_1Spare4Out_bool;       /* '<Root>/NI9426_1Spare4Out_bool' */
  real_T AutonomyEnabledIndicator_bool;
                                    /* '<Root>/AutonomyEnabledIndicator_bool' */
  real_T MachineEnabledIndicator_bool;
                                     /* '<Root>/MachineEnabledIndicator_bool' */
  real_T InCabBuzzer_bool;             /* '<Root>/InCabBuzzer_bool' */
  real_T Altenator408VACEnableRly_bool;
                                    /* '<Root>/Altenator408VACEnableRly_bool' */
  real_T AirKnifeRly_bool;             /* '<Root>/AirKnifeRly_bool' */
  real_T BackupAlarmEnable_bool;       /* '<Root>/BackupAlarmEnable_bool' */
  boolean_T AutoStartRelayEnable_bool; /* '<Root>/AutoStartRelayEnable_bool' */
  real_T JCBEngnieKill_bool;           /* '<Root>/JCBEngnieKill_bool' */
  real_T RearEstopButton_bool;         /* '<Root>/RearEstopButton_bool' */
  real_T LightsTurnSignalRight_bool;   /* '<Root>/LightsTurnSignalRight_bool' */
  real_T LightsTurnSignalLeft_bool;    /* '<Root>/LightsTurnSignalLeft_bool' */
  real_T HydraulCoolFanEnable_bool;    /* '<Root>/HydraulCoolFanEnable_bool' */
  boolean_T RlyCPower24VEnable_bool;   /* '<Root>/RlyCPower24VEnable_bool' */
  real_T FloatFront_bool;              /* '<Root>/FloatFront_bool' */
  real_T FloatRight_bool;              /* '<Root>/FloatRight_bool' */
  real_T FloatLeft_bool;               /* '<Root>/FloatLeft_bool' */
  real_T Ssr2BrakeOut_bool;            /* '<Root>/Ssr2BrakeOut_bool' */
  real_T LoadAHydraulPumpEnableOut_bool;
                                   /* '<Root>/LoadAHydraulPumpEnableOut_bool' */
  boolean_T RlyEStarter_bool;          /* '<Root>/RlyEStarter_bool' */
  uint32_T DriveFwd_bool;              /* '<Root>/DriveFwd_bool' */
  uint32_T DriveReverse_bool;          /* '<Root>/DriveReverse_bool' */
  real_T SteerRightA_bool;             /* '<Root>/SteerRightA_bool' */
  real_T SteerLeftA_bool;              /* '<Root>/SteerLeftA_bool' */
  real_T SteerLeftB_bool;              /* '<Root>/SteerLeftB_bool' */
  real_T SteerRightB_bool;             /* '<Root>/SteerRightB_bool' */
  real_T TracRight_bool;               /* '<Root>/TracRight_bool' */
  real_T TracLeft_bool;                /* '<Root>/TracLeft_bool' */
  boolean_T DeckFrontLift_bool;        /* '<Root>/DeckFrontLift_bool' */
  boolean_T DeckFrontLower_bool;       /* '<Root>/DeckFrontLower_bool' */
  boolean_T DeckRightLift_bool;        /* '<Root>/DeckRightLift_bool' */
  boolean_T DeckRightLower_bool;       /* '<Root>/DeckRightLower_bool' */
  boolean_T DeckLeftLift_bool;         /* '<Root>/DeckLeftLift_bool' */
  boolean_T DeckLeftLower_bool;        /* '<Root>/DeckLeftLower_bool' */
  real_T NI9426_2Spare1Out_bool;       /* '<Root>/NI9426_2Spare1Out_bool' */
  real_T NI9426_2Spare2Out_bool;       /* '<Root>/NI9426_2Spare2Out_bool' */
  real_T NI9426_2Spare3Out_bool;       /* '<Root>/NI9426_2Spare3Out_bool' */
  real_T NI9426_2Spare4Out_bool;       /* '<Root>/NI9426_2Spare4Out_bool' */
  real_T NI9426_2Spare5Out_bool;       /* '<Root>/NI9426_2Spare5Out_bool' */
  real_T NI9426_2Spare6Out_bool;       /* '<Root>/NI9426_2Spare6Out_bool' */
  real_T NI9426_2Spare7Out_bool;       /* '<Root>/NI9426_2Spare7Out_bool' */
  real_T NI9426_2Spare8Out_bool;       /* '<Root>/NI9426_2Spare8Out_bool' */
  real_T NI9426_2Spare9Out_bool;       /* '<Root>/NI9426_2Spare9Out_bool' */
  real_T NI9426_2Spare10Out_bool;      /* '<Root>/NI9426_2Spare10Out_bool' */
  real_T NI9426_2Spare11Out_bool;      /* '<Root>/NI9426_2Spare11Out_bool' */
  real_T NI9426_2Spare12Out_bool;      /* '<Root>/NI9426_2Spare12Out_bool' */
  real_T NI9426_2Spare13Out_bool;      /* '<Root>/NI9426_2Spare13Out_bool' */
  real_T NI9426_2Spare14Out_bool;      /* '<Root>/NI9426_2Spare14Out_bool' */
  real_T NI9426_2Spare15Out_bool;      /* '<Root>/NI9426_2Spare15Out_bool' */
  real_T NI9426_2Spare16Out_bool;      /* '<Root>/NI9426_2Spare16Out_bool' */
  boolean_T DeckUnlatch_bool;          /* '<Root>/DeckUnlatch_bool' */
  real_T DO1NI9476Mon_bool;            /* '<Root>/DO1NI9476Mon_bool' */
  boolean_T HydraulicOilLow_bool;      /* '<Root>/HydraulicOilLow_bool' */
  real_T DO3NI9476Mon_bool;            /* '<Root>/DO3NI9476Mon_bool' */
  boolean_T RightDeckLatched_bool;     /* '<Root>/RightDeckLatched_bool' */
  boolean_T LeftDeckLatched_bool;      /* '<Root>/LeftDeckLatched_bool' */
  real_T DO5NI9476Mon_bool;            /* '<Root>/DO5NI9476Mon_bool' */
  boolean_T FrontDeckLatched_bool;     /* '<Root>/FrontDeckLatched_bool' */
  real_T DO7NI9476Mon_bool;            /* '<Root>/DO7NI9476Mon_bool' */
  real_T DO8NI9476Mon_bool;            /* '<Root>/DO8NI9476Mon_bool' */
  real_T DO9NI9476Mon_bool;            /* '<Root>/DO9NI9476Mon_bool' */
  real_T DO11NI9476Mon_bool;           /* '<Root>/DO11NI9476Mon_bool' */
  real_T DO12NI9476Mon_bool;           /* '<Root>/DO12NI9476Mon_bool' */
  real_T DO13NI9476Mon_bool;           /* '<Root>/DO13NI9476Mon_bool' */
  real_T DO14NI9476Mon_bool;           /* '<Root>/DO14NI9476Mon_bool' */
  real_T DO15NI9476Mon_bool;           /* '<Root>/DO15NI9476Mon_bool' */
  real_T DO16NI9476Mon_bool;           /* '<Root>/DO16NI9476Mon_bool' */
  real_T DO17NI9476Mon_bool;           /* '<Root>/DO17NI9476Mon_bool' */
  real_T DO18NI9476Mon_bool;           /* '<Root>/DO18NI9476Mon_bool' */
  real_T DO19NI9476Mon_bool;           /* '<Root>/DO19NI9476Mon_bool' */
  real_T DO20NI9476Mon_bool;           /* '<Root>/DO20NI9476Mon_bool' */
  real_T DO21NI9476Mon_bool;           /* '<Root>/DO21NI9476Mon_bool' */
  real_T EStopCab_bool;                /* '<Root>/EStopCab_bool' */
  real_T ElectSeatSwitch_bool;         /* '<Root>/ElectSeatSwitch_bool' */
  real_T MachineEnableSeatSwitch_bool;
                                     /* '<Root>/MachineEnableSeatSwitch_bool' */
  real_T AutonomyEnableSwitch_bool;    /* '<Root>/AutonomyEnableSwitch_bool' */
  real_T DO26NI9476Mon_bool;           /* '<Root>/DO26NI9476Mon_bool' */
  real_T MainKeyOutSwitch_bool;        /* '<Root>/MainKeyOutSwitch_bool' */
  real_T DO28NI9476Mon_bool;           /* '<Root>/DO28NI9476Mon_bool' */
  real_T AutonomyRemoteEstopSw_bool;   /* '<Root>/AutonomyRemoteEstopSw_bool' */
  real_T DO30NI9476Mon_bool;           /* '<Root>/DO30NI9476Mon_bool' */
  real_T DO31NI9476Mon_bool;           /* '<Root>/DO31NI9476Mon_bool' */
  real_T NI9426_2Spare17Out_bool;      /* '<Root>/NI9426_2Spare17Out_bool' */
  real_T TorqueLeftWheel_nm;           /* '<Root>/TorqueLeftWheel_nm' */
  real_T TorqueRightWheel_nm;          /* '<Root>/TorqueRightWheel_nm' */
  real_T TorqueSteering_nm;            /* '<Root>/TorqueSteering_nm' */
  real_T PowerOutMon24v_Vdc;           /* '<Root>/PowerOutMon24v_Vdc' */
  real_T PowerOutMon12v_Vdc;           /* '<Root>/PowerOutMon12v_Vdc' */
  real_T PowerOutMon5v_Vdc;            /* '<Root>/PowerOutMon5v_Vdc' */
  real_T DeckLiftPFrntOut_psi;         /* '<Root>/DeckLiftPFrntOut_psi' */
  real_T AO0NI9264_1Mon2_int;          /* '<Root>/AO0NI9264_1Mon2_int' */
  real_T AO0NI9264_1Mon1_int;          /* '<Root>/AO0NI9264_1Mon1_int' */
  real_T DeckLiftPRightOut_psi;        /* '<Root>/DeckLiftPRightOut_psi' */
  real_T DeckLiftPLftOut_psi;          /* '<Root>/DeckLiftPLftOut_psi' */
  real_T BrakeHydraulPrsOut_psi;       /* '<Root>/BrakeHydraulPrsOut_psi' */
  real_T HeatExchangeTemp_degC;        /* '<Root>/HeatExchangeTemp_degC' */
  real_T FrontDeckAngleSns_deg;        /* '<Root>/FrontDeckAngleSns_deg' */
  real_T RightDeckAngleSns_deg;        /* '<Root>/RightDeckAngleSns_deg' */
  real_T LeftDeckAngleSns_deg;         /* '<Root>/LeftDeckAngleSns_deg' */
  real_T AO0NI9264_1Mon3_int;          /* '<Root>/AO0NI9264_1Mon3_int' */
  real_T AO0NI9264_1Mon4_int;          /* '<Root>/AO0NI9264_1Mon4_int' */
  real_T ControlBoxTemp_degC;          /* '<Root>/ControlBoxTemp_degC' */
} ExtY_M220Model_v2_T;

/* Backward compatible GRT Identifiers */
#define rtB                            M220Model_v2_B
#define BlockIO                        B_M220Model_v2_T
#define rtU                            M220Model_v2_U
#define ExternalInputs                 ExtU_M220Model_v2_T
#define rtX                            M220Model_v2_X
#define ContinuousStates               X_M220Model_v2_T
#define rtXdot                         M220Model_v2_XDot
#define StateDerivatives               XDot_M220Model_v2_T
#define tXdis                          M220Model_v2_XDis
#define StateDisabled                  XDis_M220Model_v2_T
#define rtY                            M220Model_v2_Y
#define ExternalOutputs                ExtY_M220Model_v2_T
#define rtDWork                        M220Model_v2_DW
#define D_Work                         DW_M220Model_v2_T
#define tConstBlockIOType              ConstB_M220Model_v2_T
#define rtC                            M220Model_v2_ConstB
#define ConstParam                     ConstP_M220Model_v2_T
#define rtcP                           M220Model_v2_ConstP

/* Real-time Model Data Structure */
struct tag_RTM_M220Model_v2_T {
  const char_T *path;
  const char_T *modelName;
  struct SimStruct_tag * *childSfunctions;
  const char_T *errorStatus;
  SS_SimMode simMode;
  RTWLogInfo *rtwLogInfo;
  RTWExtModeInfo *extModeInfo;
  RTWSolverInfo solverInfo;
  RTWSolverInfo *solverInfoPtr;
  void *sfcnInfo;
  void *blockIO;
  const void *constBlockIO;
  void *defaultParam;
  ZCSigState *prevZCSigState;
  real_T *contStates;
  int_T *periodicContStateIndices;
  real_T *periodicContStateRanges;
  real_T *derivs;
  void *zcSignalValues;
  void *inputs;
  void *outputs;
  boolean_T *contStateDisabled;
  boolean_T zCCacheNeedsReset;
  boolean_T derivCacheNeedsReset;
  boolean_T CTOutputIncnstWithState;
  real_T odeY[13];
  real_T odeF[4][13];
  ODE4_IntgData intgData;
  void *dwork;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
    uint32_T options;
    int_T numContStates;
    int_T numPeriodicContStates;
    int_T numU;
    int_T numY;
    int_T numSampTimes;
    int_T numBlocks;
    int_T numBlockIO;
    int_T numBlockPrms;
    int_T numDwork;
    int_T numSFcnPrms;
    int_T numSFcns;
    int_T numIports;
    int_T numOports;
    int_T numNonSampZCs;
    int_T sysDirFeedThru;
    int_T rtwGenSfcn;
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
    void *xpcData;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T stepSize;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    uint32_T clockTick1;
    uint32_T clockTickH1;
    time_T stepSize1;
    time_T tStart;
    time_T tFinal;
    time_T timeOfLastOutput;
    void *timingData;
    real_T *varNextHitTimesList;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *sampleTimes;
    time_T *offsetTimes;
    int_T *sampleTimeTaskIDPtr;
    int_T *sampleHits;
    int_T *perTaskSampleHits;
    time_T *t;
    time_T sampleTimesArray[2];
    time_T offsetTimesArray[2];
    int_T sampleTimeTaskIDArray[2];
    int_T sampleHitArray[2];
    int_T perTaskSampleHitsArray[4];
    time_T tArray[2];
  } Timing;
};

/* Block signals (default storage) */
#ifdef __cplusplus

extern "C" {

#endif

  extern B_M220Model_v2_T M220Model_v2_B;

#ifdef __cplusplus

}
#endif

/* Continuous states (default storage) */
extern X_M220Model_v2_T M220Model_v2_X;

/* Block states (default storage) */
extern DW_M220Model_v2_T M220Model_v2_DW;

#ifdef __cplusplus

extern "C" {

#endif

  /* External inputs (root inport signals with default storage) */
  extern ExtU_M220Model_v2_T M220Model_v2_U;

  /* External outputs (root outports fed by signals with default storage) */
  extern ExtY_M220Model_v2_T M220Model_v2_Y;

#ifdef __cplusplus

}
#endif

extern const ConstB_M220Model_v2_T M220Model_v2_ConstB;/* constant block i/o */

/* Constant parameters (default storage) */
extern const ConstP_M220Model_v2_T M220Model_v2_ConstP;

#ifdef __cplusplus

extern "C" {

#endif

  /* Model entry point functions */
  extern void M220Model_v2_initialize(void);
  extern void M220Model_v2_output(void);
  extern void M220Model_v2_update(void);
  extern void M220Model_v2_terminate(void);

#ifdef __cplusplus

}
#endif

/*====================*
 * External functions *
 *====================*/
#ifdef __cplusplus

extern "C" {

#endif

  extern M220Model_v2_rtModel *M220Model_v2(void);
  extern void MdlInitializeSizes(void);
  extern void MdlInitializeSampleTimes(void);
  extern void MdlInitialize(void);
  extern void MdlStart(void);
  extern void MdlOutputs(int_T tid);
  extern void MdlUpdate(int_T tid);
  extern void MdlTerminate(void);

#ifdef __cplusplus

}
#endif

/* Real-time Model object */
#ifdef __cplusplus

extern "C" {

#endif

  extern RT_MODEL_M220Model_v2_T *const M220Model_v2_M;

#ifdef __cplusplus

}
#endif

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S22>/Derivative' : Unused code path elimination
 * Block '<S23>/Display' : Unused code path elimination
 * Block '<Root>/RearTireDia_in' : Unused code path elimination
 * Block '<S17>/Bias' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias1' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias10' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias11' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias12' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias13' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias14' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias15' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias2' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias3' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias4' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias5' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias6' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias7' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias8' : Eliminated nontunable bias of 0
 * Block '<S17>/Bias9' : Eliminated nontunable bias of 0
 * Block '<S17>/Gain' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain1' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain10' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain11' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain12' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain13' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain14' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain15' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain2' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain3' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain4' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain5' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain6' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain7' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain8' : Eliminated nontunable gain of 1
 * Block '<S17>/Gain9' : Eliminated nontunable gain of 1
 * Block '<S20>/Bias' : Eliminated nontunable bias of 0
 * Block '<S20>/Bias1' : Eliminated nontunable bias of 0
 * Block '<S20>/Bias2' : Eliminated nontunable bias of 0
 * Block '<S20>/Bias3' : Eliminated nontunable bias of 0
 * Block '<S20>/Bias4' : Eliminated nontunable bias of 0
 * Block '<S20>/Bias5' : Eliminated nontunable bias of 0
 * Block '<S20>/Bias6' : Eliminated nontunable bias of 0
 * Block '<S20>/Bias7' : Eliminated nontunable bias of 0
 * Block '<S20>/Gain4' : Eliminated nontunable gain of 1
 * Block '<S20>/Gain5' : Eliminated nontunable gain of 1
 * Block '<S20>/Gain6' : Eliminated nontunable gain of 1
 * Block '<S20>/Gain7' : Eliminated nontunable gain of 1
 * Block '<S22>/Bias' : Eliminated nontunable bias of 0
 * Block '<S22>/Bias1' : Eliminated nontunable bias of 0
 * Block '<S22>/Gain' : Eliminated nontunable gain of 1
 * Block '<S22>/Gain1' : Eliminated nontunable gain of 1
 * Block '<S23>/Data Type Conversion2' : Eliminate redundant data type conversion
 * Block '<S23>/Data Type Conversion3' : Eliminate redundant data type conversion
 * Block '<S23>/Data Type Conversion4' : Eliminate redundant data type conversion
 * Block '<S23>/Data Type Conversion5' : Eliminate redundant data type conversion
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'M220Model_v2'
 * '<S1>'   : 'M220Model_v2/4TireTractionForces'
 * '<S2>'   : 'M220Model_v2/ConvertVehicleVel2Torques'
 * '<S3>'   : 'M220Model_v2/Degrees to Radians'
 * '<S4>'   : 'M220Model_v2/DocBlock2'
 * '<S5>'   : 'M220Model_v2/HydraulicLiftPressureSubsystem'
 * '<S6>'   : 'M220Model_v2/Lb2MassKgConv'
 * '<S7>'   : 'M220Model_v2/Lb2MassKgConv1'
 * '<S8>'   : 'M220Model_v2/M220ControlBoxTempSubsystem'
 * '<S9>'   : 'M220Model_v2/M220DynamicsModel'
 * '<S10>'  : 'M220Model_v2/M220HeatExchangerTempSubsystem'
 * '<S11>'  : 'M220Model_v2/M220HydraulicOil'
 * '<S12>'  : 'M220Model_v2/M220MachineEnableFunction'
 * '<S13>'  : 'M220Model_v2/M220MowerDecksSubsystem'
 * '<S14>'  : 'M220Model_v2/M220PowerMonitorSubsystem'
 * '<S15>'  : 'M220Model_v2/M220SeatSwitchSubsystem'
 * '<S16>'  : 'M220Model_v2/M220TurnSignalLightsSubsystem'
 * '<S17>'  : 'M220Model_v2/NI9264Subsystem1'
 * '<S18>'  : 'M220Model_v2/NI9426 Subsystem1'
 * '<S19>'  : 'M220Model_v2/NI9426 Subsystem2'
 * '<S20>'  : 'M220Model_v2/NI9475Subsystem'
 * '<S21>'  : 'M220Model_v2/NI9476Subsystem'
 * '<S22>'  : 'M220Model_v2/NI9853_1 Subsystem'
 * '<S23>'  : 'M220Model_v2/OdometrySubsystem'
 * '<S24>'  : 'M220Model_v2/SteeringAndJoyVelocityConvert'
 * '<S25>'  : 'M220Model_v2/4TireTractionForces/Tire1TractionForces'
 * '<S26>'  : 'M220Model_v2/4TireTractionForces/Tire2TractionForces'
 * '<S27>'  : 'M220Model_v2/4TireTractionForces/Tire3TractionForces'
 * '<S28>'  : 'M220Model_v2/4TireTractionForces/Tire4TractionForces'
 * '<S29>'  : 'M220Model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1'
 * '<S30>'  : 'M220Model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipAngleTire1'
 * '<S31>'  : 'M220Model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipRatioTire1'
 * '<S32>'  : 'M220Model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/VerticalTire1ForcePitchRollWeightTransfer'
 * '<S33>'  : 'M220Model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipAngleTire1/in2m1'
 * '<S34>'  : 'M220Model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipAngleTire1/in2m2'
 * '<S35>'  : 'M220Model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipRatioTire1/in2m'
 * '<S36>'  : 'M220Model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipRatioTire1/in2m_1'
 * '<S37>'  : 'M220Model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/VerticalTire1ForcePitchRollWeightTransfer/in2m'
 * '<S38>'  : 'M220Model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/VerticalTire1ForcePitchRollWeightTransfer/in2m1'
 * '<S39>'  : 'M220Model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/VerticalTire1ForcePitchRollWeightTransfer/in2m2'
 * '<S40>'  : 'M220Model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2'
 * '<S41>'  : 'M220Model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipAngleTire2'
 * '<S42>'  : 'M220Model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipRatioTire2'
 * '<S43>'  : 'M220Model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/VerticalTireForcePitchRollWeightTransfer2'
 * '<S44>'  : 'M220Model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipAngleTire2/in2m1'
 * '<S45>'  : 'M220Model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipAngleTire2/in2m2'
 * '<S46>'  : 'M220Model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipRatioTire2/in2m'
 * '<S47>'  : 'M220Model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipRatioTire2/in2m_1'
 * '<S48>'  : 'M220Model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/VerticalTireForcePitchRollWeightTransfer2/in2m'
 * '<S49>'  : 'M220Model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/VerticalTireForcePitchRollWeightTransfer2/in2m1'
 * '<S50>'  : 'M220Model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/VerticalTireForcePitchRollWeightTransfer2/in2m2'
 * '<S51>'  : 'M220Model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3'
 * '<S52>'  : 'M220Model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipAngleTire3'
 * '<S53>'  : 'M220Model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3'
 * '<S54>'  : 'M220Model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/VerticalTireForcePitchRollWeightTransfer3'
 * '<S55>'  : 'M220Model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipAngleTire3/Degrees to Radians'
 * '<S56>'  : 'M220Model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipAngleTire3/in2m1'
 * '<S57>'  : 'M220Model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipAngleTire3/in2m2'
 * '<S58>'  : 'M220Model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Degrees to Radians'
 * '<S59>'  : 'M220Model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/in2m'
 * '<S60>'  : 'M220Model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/in2m_1'
 * '<S61>'  : 'M220Model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/VerticalTireForcePitchRollWeightTransfer3/in2m'
 * '<S62>'  : 'M220Model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/VerticalTireForcePitchRollWeightTransfer3/in2m1'
 * '<S63>'  : 'M220Model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/VerticalTireForcePitchRollWeightTransfer3/in2m2'
 * '<S64>'  : 'M220Model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4'
 * '<S65>'  : 'M220Model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipAngleTire4'
 * '<S66>'  : 'M220Model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4'
 * '<S67>'  : 'M220Model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/VerticalTireForcePitchRollWeightTransfer4'
 * '<S68>'  : 'M220Model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipAngleTire4/Degrees to Radians'
 * '<S69>'  : 'M220Model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipAngleTire4/in2m1'
 * '<S70>'  : 'M220Model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipAngleTire4/in2m2'
 * '<S71>'  : 'M220Model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Degrees to Radians'
 * '<S72>'  : 'M220Model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/in2m'
 * '<S73>'  : 'M220Model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/in2m_1'
 * '<S74>'  : 'M220Model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/VerticalTireForcePitchRollWeightTransfer4/in2m'
 * '<S75>'  : 'M220Model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/VerticalTireForcePitchRollWeightTransfer4/in2m1'
 * '<S76>'  : 'M220Model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/VerticalTireForcePitchRollWeightTransfer4/in2m2'
 * '<S77>'  : 'M220Model_v2/ConvertVehicleVel2Torques/in2m'
 * '<S78>'  : 'M220Model_v2/M220DynamicsModel/Degrees to Radians'
 * '<S79>'  : 'M220Model_v2/M220DynamicsModel/Degrees to Radians1'
 * '<S80>'  : 'M220Model_v2/M220DynamicsModel/Degrees to Radians2'
 * '<S81>'  : 'M220Model_v2/M220DynamicsModel/Degrees to Radians3'
 * '<S82>'  : 'M220Model_v2/M220DynamicsModel/FxTotalCalc'
 * '<S83>'  : 'M220Model_v2/M220DynamicsModel/FyTotalCalc'
 * '<S84>'  : 'M220Model_v2/M220DynamicsModel/Radians to Degrees'
 * '<S85>'  : 'M220Model_v2/M220DynamicsModel/Radians to Degrees1'
 * '<S86>'  : 'M220Model_v2/M220DynamicsModel/Radians to Degrees2'
 * '<S87>'  : 'M220Model_v2/M220DynamicsModel/ZRockerEquilibriumCalc'
 * '<S88>'  : 'M220Model_v2/M220DynamicsModel/in2m'
 * '<S89>'  : 'M220Model_v2/M220DynamicsModel/ZRockerEquilibriumCalc/in2m'
 * '<S90>'  : 'M220Model_v2/M220DynamicsModel/ZRockerEquilibriumCalc/in2m1'
 * '<S91>'  : 'M220Model_v2/M220DynamicsModel/ZRockerEquilibriumCalc/in2m2'
 * '<S92>'  : 'M220Model_v2/M220HydraulicOil/Compare To Constant'
 * '<S93>'  : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant1'
 * '<S94>'  : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant10'
 * '<S95>'  : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant11'
 * '<S96>'  : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant12'
 * '<S97>'  : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant13'
 * '<S98>'  : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant14'
 * '<S99>'  : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant15'
 * '<S100>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant16'
 * '<S101>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant17'
 * '<S102>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant18'
 * '<S103>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant19'
 * '<S104>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant2'
 * '<S105>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant20'
 * '<S106>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant21'
 * '<S107>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant22'
 * '<S108>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant23'
 * '<S109>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant24'
 * '<S110>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant25'
 * '<S111>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant26'
 * '<S112>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant27'
 * '<S113>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant3'
 * '<S114>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant4'
 * '<S115>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant5'
 * '<S116>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant6'
 * '<S117>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant7'
 * '<S118>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant8'
 * '<S119>' : 'M220Model_v2/M220MowerDecksSubsystem/Compare To Constant9'
 */
#endif                                 /* RTW_HEADER_M220Model_v2_h_ */
