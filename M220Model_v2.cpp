/*
 * M220Model_v2.cpp
 *
 * Code generation for model "M220Model_v2".
 *
 * Model version              : 1.474
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Mon Nov  1 19:15:20 2021
 *
 * Target selection: NIVeriStand_Linux_64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Generic->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "M220Model_v2.h"
#include "M220Model_v2_private.h"

/* Block signals (default storage) */
B_M220Model_v2_T M220Model_v2_B;

/* Continuous states */
X_M220Model_v2_T M220Model_v2_X;

/* Block states (default storage) */
DW_M220Model_v2_T M220Model_v2_DW;

/* External inputs (root inport signals with default storage) */
ExtU_M220Model_v2_T M220Model_v2_U;

/* External outputs (root outports fed by signals with default storage) */
ExtY_M220Model_v2_T M220Model_v2_Y;

/* Real-time model */
RT_MODEL_M220Model_v2_T M220Model_v2_M_ = RT_MODEL_M220Model_v2_T();
RT_MODEL_M220Model_v2_T *const M220Model_v2_M = &M220Model_v2_M_;

/* Lookup 1D UtilityLookUpEven_real_T_real_T */
void LookUpEven_real_T_real_T(real_T *pY, const real_T *pYData, real_T u, real_T
  valueLo, uint32_T iHi, real_T uSpacing)
{
  if (u <= valueLo ) {
    (*pY) = (*pYData);
  } else {
    real_T uAdjusted = u - valueLo;
    real_T tmpIdxLeft = uAdjusted / uSpacing;
    uint64_T iLeft = (uint64_T)tmpIdxLeft;
    if ((tmpIdxLeft >= 18446744073709551616.0) || (iLeft >= iHi) ) {
      (*pY) = pYData[iHi];
    } else {
      {
        real_T lambda;

        {
          real_T num = (real_T)uAdjusted - ( iLeft * uSpacing );
          lambda = num / uSpacing;
        }

        {
          real_T yLeftCast;
          real_T yRghtCast;
          yLeftCast = pYData[iLeft];
          yRghtCast = pYData[((iLeft)+1)];
          yLeftCast += lambda * ( yRghtCast - yLeftCast );
          (*pY) = yLeftCast;
        }
      }
    }
  }
}

/* Lookup Binary Search Utility BINARYSEARCH_real_T */
void BINARYSEARCH_real_T(uint32_T *piLeft, uint32_T *piRght, real_T u, const
  real_T *pData, uint32_T iHi)
{
  /* Find the location of current input value in the data table. */
  *piLeft = 0U;
  *piRght = iHi;
  if (u <= pData[0] ) {
    /* Less than or equal to the smallest point in the table. */
    *piRght = 0U;
  } else if (u >= pData[iHi] ) {
    /* Greater than or equal to the largest point in the table. */
    *piLeft = iHi;
  } else {
    uint32_T i;

    /* Do a binary search. */
    while (( *piRght - *piLeft ) > 1U ) {
      /* Get the average of the left and right indices using to Floor rounding. */
      i = (*piLeft + *piRght) >> 1;

      /* Move either the right index or the left index so that */
      /*  LeftDataPoint <= CurrentValue < RightDataPoint */
      if (u < pData[i] ) {
        *piRght = i;
      } else {
        *piLeft = i;
      }
    }
  }
}

/* Lookup Utility LookUp_real_T_real_T */
void LookUp_real_T_real_T(real_T *pY, const real_T *pYData, real_T u, const
  real_T *pUData, uint32_T iHi)
{
  uint32_T iLeft;
  uint32_T iRght;
  BINARYSEARCH_real_T( &(iLeft), &(iRght), u, pUData, iHi);

  {
    real_T lambda;
    if (pUData[iRght] > pUData[iLeft] ) {
      real_T num;
      real_T den;
      den = pUData[iRght];
      den -= pUData[iLeft];
      num = u;
      num -= pUData[iLeft];
      lambda = num / den;
    } else {
      lambda = 0.0;
    }

    {
      real_T yLeftCast;
      real_T yRghtCast;
      yLeftCast = pYData[iLeft];
      yRghtCast = pYData[iRght];
      yLeftCast += lambda * ( yRghtCast - yLeftCast );
      (*pY) = yLeftCast;
    }
  }
}

/*
 * This function updates continuous states using the ODE4 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T t = rtsiGetT(si);
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE4_IntgData *id = static_cast<ODE4_IntgData *>(rtsiGetSolverData(si));
  real_T *y = id->y;
  real_T *f0 = id->f[0];
  real_T *f1 = id->f[1];
  real_T *f2 = id->f[2];
  real_T *f3 = id->f[3];
  real_T temp;
  int_T i;
  int_T nXc = 13;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);

  /* Save the state values at time t in y, we'll use x as ynew. */
  (void) std::memcpy(y, x,
                     static_cast<uint_T>(nXc)*sizeof(real_T));

  /* Assumes that rtsiSetT and ModelOutputs are up-to-date */
  /* f0 = f(t,y) */
  rtsiSetdX(si, f0);
  M220Model_v2_derivatives();

  /* f1 = f(t + (h/2), y + (h/2)*f0) */
  temp = 0.5 * h;
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (temp*f0[i]);
  }

  rtsiSetT(si, t + temp);
  rtsiSetdX(si, f1);
  M220Model_v2_output();
  M220Model_v2_derivatives();

  /* f2 = f(t + (h/2), y + (h/2)*f1) */
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (temp*f1[i]);
  }

  rtsiSetdX(si, f2);
  M220Model_v2_output();
  M220Model_v2_derivatives();

  /* f3 = f(t + h, y + h*f2) */
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + (h*f2[i]);
  }

  rtsiSetT(si, tnew);
  rtsiSetdX(si, f3);
  M220Model_v2_output();
  M220Model_v2_derivatives();

  /* tnew = t + h
     ynew = y + (h/6)*(f0 + 2*f1 + 2*f2 + 2*f3) */
  temp = h / 6.0;
  for (i = 0; i < nXc; i++) {
    x[i] = y[i] + temp*(f0[i] + 2.0*f1[i] + 2.0*f2[i] + f3[i]);
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model output function */
void M220Model_v2_output(void)
{
  real_T u0;
  real_T *lastU;
  if (rtmIsMajorTimeStep(M220Model_v2_M)) {
    /* set solver stop time */
    if (!(M220Model_v2_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&M220Model_v2_M->solverInfo,
                            ((M220Model_v2_M->Timing.clockTickH0 + 1) *
        M220Model_v2_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&M220Model_v2_M->solverInfo,
                            ((M220Model_v2_M->Timing.clockTick0 + 1) *
        M220Model_v2_M->Timing.stepSize0 + M220Model_v2_M->Timing.clockTickH0 *
        M220Model_v2_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(M220Model_v2_M)) {
    M220Model_v2_M->Timing.t[0] = rtsiGetT(&M220Model_v2_M->solverInfo);
  }

  /* Integrator: '<S9>/Integrator2' */
  M220Model_v2_B.Integrator2 = M220Model_v2_X.Integrator2_CSTATE;

  /* Outport: '<Root>/Vy_m_sec' */
  M220Model_v2_Y.Vy_m_sec = M220Model_v2_B.Integrator2;

  /* Integrator: '<S9>/Integrator1' */
  M220Model_v2_B.Integrator1 = M220Model_v2_X.Integrator1_CSTATE;

  /* Outport: '<Root>/Vx_m_sec' */
  M220Model_v2_Y.Vx_m_sec = M220Model_v2_B.Integrator1;

  /* Integrator: '<S9>/Integrator4' */
  M220Model_v2_B.Integrator4 = M220Model_v2_X.Integrator4_CSTATE;

  /* Saturate: '<S9>/Saturation3' */
  u0 = M220Model_v2_B.Integrator4;
  if (u0 > 3.1415926535897931) {
    /* Saturate: '<S9>/Saturation3' */
    M220Model_v2_B.Saturation3 = 3.1415926535897931;
  } else if (u0 < -3.1415926535897931) {
    /* Saturate: '<S9>/Saturation3' */
    M220Model_v2_B.Saturation3 = -3.1415926535897931;
  } else {
    /* Saturate: '<S9>/Saturation3' */
    M220Model_v2_B.Saturation3 = u0;
  }

  /* End of Saturate: '<S9>/Saturation3' */

  /* Gain: '<S84>/Gain' */
  M220Model_v2_B.Gain_b = 57.295779513082323 * M220Model_v2_B.Saturation3;

  /* Outport: '<Root>/Yr_deg_sec' */
  M220Model_v2_Y.Yr_deg_sec = M220Model_v2_B.Gain_b;

  /* Math: '<S23>/Math Function' */
  M220Model_v2_B.MathFunction = M220Model_v2_B.Integrator1 *
    M220Model_v2_B.Integrator1;

  /* Math: '<S23>/Math Function1' */
  M220Model_v2_B.MathFunction1 = M220Model_v2_B.Integrator2 *
    M220Model_v2_B.Integrator2;

  /* Integrator: '<S9>/Integrator5' */
  M220Model_v2_B.Integrator5 = M220Model_v2_X.Integrator5_CSTATE;

  /* Math: '<S23>/Math Function2' */
  M220Model_v2_B.MathFunction2 = M220Model_v2_B.Integrator5 *
    M220Model_v2_B.Integrator5;

  /* Sum: '<S23>/Add' */
  M220Model_v2_B.Add = (M220Model_v2_B.MathFunction +
                        M220Model_v2_B.MathFunction1) +
    M220Model_v2_B.MathFunction2;

  /* Sqrt: '<S23>/Sqrt' */
  M220Model_v2_B.Sqrt = std::sqrt(M220Model_v2_B.Add);

  /* Switch: '<S23>/Switch' incorporates:
   *  Inport: '<Root>/HMIVehicleSpeedOvrd_bool'
   */
  if (M220Model_v2_U.HMIVehicleSpeedOvrd_bool) {
    /* Switch: '<S23>/Switch' incorporates:
     *  Inport: '<Root>/HMIVehicleSpeed_mph'
     */
    M220Model_v2_B.Switch = M220Model_v2_U.HMIVehicleSpeed_mph;
  } else {
    /* Product: '<S23>/Product1' incorporates:
     *  Constant: '<S23>/Constant'
     */
    M220Model_v2_B.Product1_o5 = M220Model_v2_B.Sqrt * 2.23694;

    /* Switch: '<S23>/Switch' */
    M220Model_v2_B.Switch = M220Model_v2_B.Product1_o5;
  }

  /* End of Switch: '<S23>/Switch' */

  /* Product: '<S23>/Divide' incorporates:
   *  Constant: '<S23>/Constant1'
   */
  M220Model_v2_B.Divide = M220Model_v2_B.Switch / 60.0;

  /* Product: '<S23>/Product5' incorporates:
   *  Constant: '<S23>/Constant3'
   */
  M220Model_v2_B.Product5 = M220Model_v2_B.Divide * 63360.0;

  /* Product: '<S23>/Divide2' */
  M220Model_v2_B.Divide2 = M220Model_v2_B.Product5 /
    M220Model_v2_ConstB.Product3;

  /* Outport: '<Root>/RevFrntLftTire_RPMin' */
  M220Model_v2_Y.RevFrntLftTire_RPMin = M220Model_v2_B.Divide2;

  /* Outport: '<Root>/RevFrntRghtTire_RPMin' */
  M220Model_v2_Y.RevFrntRghtTire_RPMin = M220Model_v2_B.Divide2;

  /* Product: '<S23>/Product2' incorporates:
   *  Constant: '<Root>/GearWheelTeeth_int'
   *  Constant: '<S23>/Constant5'
   */
  M220Model_v2_B.Product2 = M220Model_v2_B.Divide2 * 54.0 * 32.1;

  /* Product: '<S23>/Divide1' incorporates:
   *  Constant: '<S23>/Constant4'
   */
  M220Model_v2_B.Divide1 = M220Model_v2_B.Product2 / 60.0;

  /* Saturate: '<S23>/Saturation' */
  u0 = M220Model_v2_B.Divide1;
  if (u0 > 20000.0) {
    /* Saturate: '<S23>/Saturation' */
    M220Model_v2_B.Saturation = 20000.0;
  } else if (u0 < 0.0) {
    /* Saturate: '<S23>/Saturation' */
    M220Model_v2_B.Saturation = 0.0;
  } else {
    /* Saturate: '<S23>/Saturation' */
    M220Model_v2_B.Saturation = u0;
  }

  /* End of Saturate: '<S23>/Saturation' */

  /* DataTypeConversion: '<S23>/Data Type Conversion' */
  u0 = std::floor(M220Model_v2_B.Saturation);
  if (rtIsNaN(u0) || rtIsInf(u0)) {
    u0 = 0.0;
  } else {
    u0 = std::fmod(u0, 4.294967296E+9);
  }

  /* DataTypeConversion: '<S23>/Data Type Conversion' */
  M220Model_v2_B.DataTypeConversion = u0 < 0.0 ? static_cast<uint32_T>(-
    static_cast<int32_T>(static_cast<uint32_T>(-u0))) : static_cast<uint32_T>(u0);

  /* Gain: '<S20>/Gain' */
  M220Model_v2_B.Gain = static_cast<uint64_T>(M220Model_v2_B.DataTypeConversion)
    << 31;

  /* Outport: '<Root>/SpdEnc1Line1Frq_pps' incorporates:
   *  Gain: '<S20>/Gain'
   */
  M220Model_v2_Y.SpdEnc1Line1Frq_pps = M220Model_v2_B.Gain;
  if (rtmIsMajorTimeStep(M220Model_v2_M)) {
    /* Outport: '<Root>/SpdEnc1Line2Frq_int' incorporates:
     *  Gain: '<S20>/Gain1'
     *  Inport: '<Root>/hv_DriveFwd_bool'
     */
    M220Model_v2_Y.SpdEnc1Line2Frq_int = static_cast<uint64_T>
      (M220Model_v2_U.hv_DriveFwd_bool) << 31;
  }

  /* DataTypeConversion: '<S23>/Data Type Conversion1' */
  u0 = std::floor(M220Model_v2_B.Saturation);
  if (rtIsNaN(u0) || rtIsInf(u0)) {
    u0 = 0.0;
  } else {
    u0 = std::fmod(u0, 4.294967296E+9);
  }

  /* DataTypeConversion: '<S23>/Data Type Conversion1' */
  M220Model_v2_B.DataTypeConversion1 = u0 < 0.0 ? static_cast<uint32_T>(-
    static_cast<int32_T>(static_cast<uint32_T>(-u0))) : static_cast<uint32_T>(u0);

  /* Gain: '<S20>/Gain2' */
  M220Model_v2_B.Gain2 = static_cast<uint64_T>
    (M220Model_v2_B.DataTypeConversion1) << 31;

  /* Outport: '<Root>/SpdEnc2Line1Frq_pps' incorporates:
   *  Gain: '<S20>/Gain2'
   */
  M220Model_v2_Y.SpdEnc2Line1Frq_pps = M220Model_v2_B.Gain2;
  if (rtmIsMajorTimeStep(M220Model_v2_M)) {
    /* Outport: '<Root>/SpdEnc2Line1Frq_int' incorporates:
     *  Gain: '<S20>/Gain3'
     *  Inport: '<Root>/hv_DriveReverse_bool'
     */
    M220Model_v2_Y.SpdEnc2Line1Frq_int = static_cast<uint64_T>
      (M220Model_v2_U.hv_DriveReverse_bool) << 31;

    /* Outport: '<Root>/DO4NI9475Mon_bool' incorporates:
     *  Inport: '<Root>/NI9475Spare1_bool'
     */
    M220Model_v2_Y.DO4NI9475Mon_bool = M220Model_v2_U.NI9475Spare1_bool;

    /* Outport: '<Root>/DO5NI9475Mon_bool' incorporates:
     *  Inport: '<Root>/NI9475Spare2_bool'
     */
    M220Model_v2_Y.DO5NI9475Mon_bool = M220Model_v2_U.NI9475Spare2_bool;

    /* Outport: '<Root>/DO6NI9475Mon_bool' incorporates:
     *  Inport: '<Root>/NI9475Spare3_bool'
     */
    M220Model_v2_Y.DO6NI9475Mon_bool = M220Model_v2_U.NI9475Spare3_bool;

    /* Outport: '<Root>/DO7NI9475Mon_bool' incorporates:
     *  Inport: '<Root>/NI9475Spare4_bool'
     */
    M220Model_v2_Y.DO7NI9475Mon_bool = M220Model_v2_U.NI9475Spare4_bool;

    /* Outport: '<Root>/LightsRight_bool' incorporates:
     *  Inport: '<Root>/Rly1LightsRight_bool'
     */
    M220Model_v2_Y.LightsRight_bool = M220Model_v2_U.Rly1LightsRight_bool;

    /* Outport: '<Root>/LightsLeft_bool' incorporates:
     *  Inport: '<Root>/Rly2LightsLeft_bool'
     */
    M220Model_v2_Y.LightsLeft_bool = M220Model_v2_U.Rly2LightsLeft_bool;

    /* Outport: '<Root>/LightsRearSide_bool' incorporates:
     *  Inport: '<Root>/Rly3LightsRearSide_bool'
     */
    M220Model_v2_Y.LightsRearSide_bool = M220Model_v2_U.Rly3LightsRearSide_bool;

    /* Outport: '<Root>/LightsAux_bool' incorporates:
     *  Inport: '<Root>/Rly4LightsAux_bool'
     */
    M220Model_v2_Y.LightsAux_bool = M220Model_v2_U.Rly4LightsAux_bool;

    /* Outport: '<Root>/Horn_bool' incorporates:
     *  Inport: '<Root>/Rly7Horn_bool'
     */
    M220Model_v2_Y.Horn_bool = M220Model_v2_U.Rly7Horn_bool;

    /* Outport: '<Root>/NI9426_1Spare1Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_1Spare1_bool'
     */
    M220Model_v2_Y.NI9426_1Spare1Out_bool = M220Model_v2_U.NI9426_1Spare1_bool;

    /* Outport: '<Root>/NI9426_1Spare5Out_bool' incorporates:
     *  Inport: '<Root>/NI94261Spare5_bool'
     */
    M220Model_v2_Y.NI9426_1Spare5Out_bool = M220Model_v2_U.NI94261Spare5_bool;

    /* Outport: '<Root>/NI9426_1Spare6Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_1Spare6_bool'
     */
    M220Model_v2_Y.NI9426_1Spare6Out_bool = M220Model_v2_U.NI9426_1Spare6_bool;

    /* Outport: '<Root>/NI9426_1Spare7Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_1Spare7_bool'
     */
    M220Model_v2_Y.NI9426_1Spare7Out_bool = M220Model_v2_U.NI9426_1Spare7_bool;

    /* Outport: '<Root>/NI9426_1Spare8Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_1Spare8_bool'
     */
    M220Model_v2_Y.NI9426_1Spare8Out_bool = M220Model_v2_U.NI9426_1Spare8_bool;

    /* Outport: '<Root>/NI9426_1Spare9Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_1Spare9_bool'
     */
    M220Model_v2_Y.NI9426_1Spare9Out_bool = M220Model_v2_U.NI9426_1Spare9_bool;

    /* Outport: '<Root>/NI9426_1Spare2Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_1Spare2_bool'
     */
    M220Model_v2_Y.NI9426_1Spare2Out_bool = M220Model_v2_U.NI9426_1Spare2_bool;

    /* Outport: '<Root>/NI9426_1Spare3Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_1Spare3_bool'
     */
    M220Model_v2_Y.NI9426_1Spare3Out_bool = M220Model_v2_U.NI9426_1Spare3_bool;

    /* Outport: '<Root>/NI9426_1Spare4Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_1Spare4_bool'
     */
    M220Model_v2_Y.NI9426_1Spare4Out_bool = M220Model_v2_U.NI9426_1Spare4_bool;

    /* Outport: '<Root>/AutonomyEnabledIndicator_bool' incorporates:
     *  Inport: '<Root>/LEDAMS_bool'
     */
    M220Model_v2_Y.AutonomyEnabledIndicator_bool = M220Model_v2_U.LEDAMS_bool;

    /* Outport: '<Root>/MachineEnabledIndicator_bool' incorporates:
     *  Inport: '<Root>/LEDEnable_bool'
     */
    M220Model_v2_Y.MachineEnabledIndicator_bool = M220Model_v2_U.LEDEnable_bool;

    /* Outport: '<Root>/InCabBuzzer_bool' incorporates:
     *  Inport: '<Root>/Buzzer_bool'
     */
    M220Model_v2_Y.InCabBuzzer_bool = M220Model_v2_U.Buzzer_bool;

    /* Outport: '<Root>/Altenator408VACEnableRly_bool' incorporates:
     *  Inport: '<Root>/RlyFAltEnable_bool'
     */
    M220Model_v2_Y.Altenator408VACEnableRly_bool =
      M220Model_v2_U.RlyFAltEnable_bool;

    /* Outport: '<Root>/AirKnifeRly_bool' incorporates:
     *  Inport: '<Root>/SsrBlower_bool'
     */
    M220Model_v2_Y.AirKnifeRly_bool = M220Model_v2_U.SsrBlower_bool;

    /* Outport: '<Root>/BackupAlarmEnable_bool' incorporates:
     *  Inport: '<Root>/BackupAlarm_bool'
     */
    M220Model_v2_Y.BackupAlarmEnable_bool = M220Model_v2_U.BackupAlarm_bool;

    /* Outport: '<Root>/RearEstopButton_bool' incorporates:
     *  Inport: '<Root>/RearEStopButton_bool'
     */
    M220Model_v2_Y.RearEstopButton_bool = M220Model_v2_U.RearEStopButton_bool;

    /* DiscretePulseGenerator: '<S16>/Pulse Generator' */
    M220Model_v2_B.PulseGenerator = ((M220Model_v2_DW.clockTickCounter < 10) &&
      (M220Model_v2_DW.clockTickCounter >= 0));

    /* DiscretePulseGenerator: '<S16>/Pulse Generator' */
    if (M220Model_v2_DW.clockTickCounter >= 19) {
      M220Model_v2_DW.clockTickCounter = 0;
    } else {
      M220Model_v2_DW.clockTickCounter++;
    }

    /* Switch: '<S16>/Switch' incorporates:
     *  Inport: '<Root>/Rly5FlashRight_bool'
     */
    if (M220Model_v2_U.Rly5FlashRight_bool > 0.0) {
      /* Outport: '<Root>/LightsTurnSignalRight_bool' */
      M220Model_v2_Y.LightsTurnSignalRight_bool = M220Model_v2_B.PulseGenerator;
    } else {
      /* Outport: '<Root>/LightsTurnSignalRight_bool' incorporates:
       *  Constant: '<S16>/Constant2'
       */
      M220Model_v2_Y.LightsTurnSignalRight_bool = 0.0;
    }

    /* End of Switch: '<S16>/Switch' */

    /* DiscretePulseGenerator: '<S16>/Pulse Generator1' */
    M220Model_v2_B.PulseGenerator1 = ((M220Model_v2_DW.clockTickCounter_c < 10) &&
      (M220Model_v2_DW.clockTickCounter_c >= 0));

    /* DiscretePulseGenerator: '<S16>/Pulse Generator1' */
    if (M220Model_v2_DW.clockTickCounter_c >= 19) {
      M220Model_v2_DW.clockTickCounter_c = 0;
    } else {
      M220Model_v2_DW.clockTickCounter_c++;
    }

    /* Switch: '<S16>/Switch1' incorporates:
     *  Inport: '<Root>/Rly6FlashLeft_bool'
     */
    if (M220Model_v2_U.Rly6FlashLeft_bool > 0.0) {
      /* Outport: '<Root>/LightsTurnSignalLeft_bool' */
      M220Model_v2_Y.LightsTurnSignalLeft_bool = M220Model_v2_B.PulseGenerator1;
    } else {
      /* Outport: '<Root>/LightsTurnSignalLeft_bool' incorporates:
       *  Constant: '<S16>/Constant3'
       */
      M220Model_v2_Y.LightsTurnSignalLeft_bool = 0.0;
    }

    /* End of Switch: '<S16>/Switch1' */
  }

  /* Outport: '<Root>/AutoStartRelayEnable_bool' incorporates:
   *  Inport: '<Root>/RlyAutoStart_bool'
   */
  M220Model_v2_Y.AutoStartRelayEnable_bool = M220Model_v2_U.RlyAutoStart_bool;

  /* Outport: '<Root>/JCBEngnieKill_bool' incorporates:
   *  Inport: '<Root>/RlyJCBEngineKill_bool'
   */
  M220Model_v2_Y.JCBEngnieKill_bool = M220Model_v2_U.RlyJCBEngineKill_bool;

  /* Outport: '<Root>/HydraulCoolFanEnable_bool' incorporates:
   *  Inport: '<Root>/RlyD2HydFan_bool'
   */
  M220Model_v2_Y.HydraulCoolFanEnable_bool = M220Model_v2_U.RlyD2HydFan_bool;

  /* Outport: '<Root>/RlyCPower24VEnable_bool' incorporates:
   *  Inport: '<Root>/RlyCPower_bool'
   */
  M220Model_v2_Y.RlyCPower24VEnable_bool = M220Model_v2_U.RlyCPower_bool;
  if (rtmIsMajorTimeStep(M220Model_v2_M)) {
    /* Outport: '<Root>/FloatFront_bool' incorporates:
     *  Inport: '<Root>/Ssr4FloatFront_bool'
     */
    M220Model_v2_Y.FloatFront_bool = M220Model_v2_U.Ssr4FloatFront_bool;

    /* Outport: '<Root>/FloatRight_bool' incorporates:
     *  Inport: '<Root>/Ssr5FloatRight_bool'
     */
    M220Model_v2_Y.FloatRight_bool = M220Model_v2_U.Ssr5FloatRight_bool;

    /* Outport: '<Root>/FloatLeft_bool' incorporates:
     *  Inport: '<Root>/Ssr6FloatLeft_bool'
     */
    M220Model_v2_Y.FloatLeft_bool = M220Model_v2_U.Ssr6FloatLeft_bool;

    /* Outport: '<Root>/LoadAHydraulPumpEnableOut_bool' incorporates:
     *  Inport: '<Root>/Ssr3LoadA_bool'
     */
    M220Model_v2_Y.LoadAHydraulPumpEnableOut_bool =
      M220Model_v2_U.Ssr3LoadA_bool;

    /* Outport: '<Root>/RlyEStarter_bool' incorporates:
     *  Inport: '<Root>/RlyEStarters_bool'
     */
    M220Model_v2_Y.RlyEStarter_bool = M220Model_v2_U.RlyEStarters_bool;

    /* Outport: '<Root>/DriveFwd_bool' incorporates:
     *  Inport: '<Root>/hv_DriveFwd_bool'
     */
    M220Model_v2_Y.DriveFwd_bool = M220Model_v2_U.hv_DriveFwd_bool;

    /* Outport: '<Root>/DriveReverse_bool' incorporates:
     *  Inport: '<Root>/hv_DriveReverse_bool'
     */
    M220Model_v2_Y.DriveReverse_bool = M220Model_v2_U.hv_DriveReverse_bool;

    /* Outport: '<Root>/SteerRightA_bool' incorporates:
     *  Inport: '<Root>/hv_SteerRtA_bool'
     */
    M220Model_v2_Y.SteerRightA_bool = M220Model_v2_U.hv_SteerRtA_bool;

    /* Outport: '<Root>/SteerLeftA_bool' incorporates:
     *  Inport: '<Root>/hv_SteerLfA_bool'
     */
    M220Model_v2_Y.SteerLeftA_bool = M220Model_v2_U.hv_SteerLfA_bool;

    /* Outport: '<Root>/SteerLeftB_bool' incorporates:
     *  Inport: '<Root>/hv_SteerLfB_bool'
     */
    M220Model_v2_Y.SteerLeftB_bool = M220Model_v2_U.hv_SteerLfB_bool;

    /* Outport: '<Root>/SteerRightB_bool' incorporates:
     *  Inport: '<Root>/hv_SteerRtB_bool'
     */
    M220Model_v2_Y.SteerRightB_bool = M220Model_v2_U.hv_SteerRtB_bool;

    /* Outport: '<Root>/TracRight_bool' incorporates:
     *  Inport: '<Root>/hv_TracRight_bool'
     */
    M220Model_v2_Y.TracRight_bool = M220Model_v2_U.hv_TracRight_bool;

    /* Outport: '<Root>/TracLeft_bool' incorporates:
     *  Inport: '<Root>/hv_TracLeft_bool'
     */
    M220Model_v2_Y.TracLeft_bool = M220Model_v2_U.hv_TracLeft_bool;

    /* Outport: '<Root>/DeckFrontLift_bool' incorporates:
     *  Inport: '<Root>/hv_Dk_Ft_Lift_bool'
     */
    M220Model_v2_Y.DeckFrontLift_bool = M220Model_v2_U.hv_Dk_Ft_Lift_bool;

    /* Outport: '<Root>/DeckFrontLower_bool' incorporates:
     *  Inport: '<Root>/hv_Dk_Ft_Low_bool'
     */
    M220Model_v2_Y.DeckFrontLower_bool = M220Model_v2_U.hv_Dk_Ft_Low_bool;

    /* Outport: '<Root>/DeckRightLift_bool' incorporates:
     *  Inport: '<Root>/hv_Dk_Rt_Lift_bool'
     */
    M220Model_v2_Y.DeckRightLift_bool = M220Model_v2_U.hv_Dk_Rt_Lift_bool;

    /* Outport: '<Root>/DeckRightLower_bool' incorporates:
     *  Inport: '<Root>/hv_Dk_Rt_Low_bool'
     */
    M220Model_v2_Y.DeckRightLower_bool = M220Model_v2_U.hv_Dk_Rt_Low_bool;

    /* Outport: '<Root>/DeckLeftLift_bool' incorporates:
     *  Inport: '<Root>/hv_Dk_Lf_Lift_bool'
     */
    M220Model_v2_Y.DeckLeftLift_bool = M220Model_v2_U.hv_Dk_Lf_Lift_bool;

    /* Outport: '<Root>/DeckLeftLower_bool' incorporates:
     *  Inport: '<Root>/hv_Dk_Lf_Low_bool'
     */
    M220Model_v2_Y.DeckLeftLower_bool = M220Model_v2_U.hv_Dk_Lf_Low_bool;

    /* Outport: '<Root>/NI9426_2Spare1Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare1_bool'
     */
    M220Model_v2_Y.NI9426_2Spare1Out_bool = M220Model_v2_U.NI9426_2Spare1_bool;

    /* Outport: '<Root>/NI9426_2Spare2Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare2_bool'
     */
    M220Model_v2_Y.NI9426_2Spare2Out_bool = M220Model_v2_U.NI9426_2Spare2_bool;

    /* Outport: '<Root>/NI9426_2Spare3Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare3_bool'
     */
    M220Model_v2_Y.NI9426_2Spare3Out_bool = M220Model_v2_U.NI9426_2Spare3_bool;

    /* Outport: '<Root>/NI9426_2Spare4Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare4_bool'
     */
    M220Model_v2_Y.NI9426_2Spare4Out_bool = M220Model_v2_U.NI9426_2Spare4_bool;

    /* Outport: '<Root>/NI9426_2Spare5Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare5_bool'
     */
    M220Model_v2_Y.NI9426_2Spare5Out_bool = M220Model_v2_U.NI9426_2Spare5_bool;

    /* Outport: '<Root>/NI9426_2Spare6Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare6_bool'
     */
    M220Model_v2_Y.NI9426_2Spare6Out_bool = M220Model_v2_U.NI9426_2Spare6_bool;

    /* Outport: '<Root>/NI9426_2Spare7Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare7_bool'
     */
    M220Model_v2_Y.NI9426_2Spare7Out_bool = M220Model_v2_U.NI9426_2Spare7_bool;

    /* Outport: '<Root>/NI9426_2Spare8Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare8_bool'
     */
    M220Model_v2_Y.NI9426_2Spare8Out_bool = M220Model_v2_U.NI9426_2Spare8_bool;

    /* Outport: '<Root>/NI9426_2Spare9Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare9_bool'
     */
    M220Model_v2_Y.NI9426_2Spare9Out_bool = M220Model_v2_U.NI9426_2Spare9_bool;

    /* Outport: '<Root>/NI9426_2Spare10Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare10_bool'
     */
    M220Model_v2_Y.NI9426_2Spare10Out_bool = M220Model_v2_U.NI9426_2Spare10_bool;

    /* Outport: '<Root>/NI9426_2Spare11Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare11_bool'
     */
    M220Model_v2_Y.NI9426_2Spare11Out_bool = M220Model_v2_U.NI9426_2Spare11_bool;

    /* Outport: '<Root>/NI9426_2Spare12Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare12_bool'
     */
    M220Model_v2_Y.NI9426_2Spare12Out_bool = M220Model_v2_U.NI9426_2Spare12_bool;

    /* Outport: '<Root>/NI9426_2Spare13Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare13_bool'
     */
    M220Model_v2_Y.NI9426_2Spare13Out_bool = M220Model_v2_U.NI9426_2Spare13_bool;

    /* Outport: '<Root>/NI9426_2Spare14Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare14_bool'
     */
    M220Model_v2_Y.NI9426_2Spare14Out_bool = M220Model_v2_U.NI9426_2Spare14_bool;

    /* Outport: '<Root>/NI9426_2Spare15Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare15_bool'
     */
    M220Model_v2_Y.NI9426_2Spare15Out_bool = M220Model_v2_U.NI9426_2Spare15_bool;

    /* Outport: '<Root>/NI9426_2Spare16Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare16_bool'
     */
    M220Model_v2_Y.NI9426_2Spare16Out_bool = M220Model_v2_U.NI9426_2Spare16_bool;

    /* Outport: '<Root>/DeckUnlatch_bool' incorporates:
     *  Inport: '<Root>/hv_Dk_Unlatch_bool'
     */
    M220Model_v2_Y.DeckUnlatch_bool = M220Model_v2_U.hv_Dk_Unlatch_bool;

    /* Outport: '<Root>/DO1NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare1_bool'
     */
    M220Model_v2_Y.DO1NI9476Mon_bool = M220Model_v2_U.NI9476Spare1_bool;

    /* Outport: '<Root>/FrontDeckAngleSns_deg' incorporates:
     *  Memory: '<S13>/Memory'
     */
    M220Model_v2_Y.FrontDeckAngleSns_deg = M220Model_v2_DW.Memory_PreviousInput;

    /* Math: '<S11>/Square' incorporates:
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     */
    M220Model_v2_B.Square = M220Model_v2_Y.FrontDeckAngleSns_deg *
      M220Model_v2_Y.FrontDeckAngleSns_deg;

    /* Outport: '<Root>/RightDeckAngleSns_deg' incorporates:
     *  Memory: '<S13>/Memory2'
     */
    M220Model_v2_Y.RightDeckAngleSns_deg = M220Model_v2_DW.Memory2_PreviousInput;

    /* Math: '<S11>/Square1' incorporates:
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     */
    M220Model_v2_B.Square1 = M220Model_v2_Y.RightDeckAngleSns_deg *
      M220Model_v2_Y.RightDeckAngleSns_deg;

    /* Outport: '<Root>/LeftDeckAngleSns_deg' incorporates:
     *  Memory: '<S13>/Memory1'
     */
    M220Model_v2_Y.LeftDeckAngleSns_deg = M220Model_v2_DW.Memory1_PreviousInput;

    /* Math: '<S11>/Square2' incorporates:
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     */
    M220Model_v2_B.Square2 = M220Model_v2_Y.LeftDeckAngleSns_deg *
      M220Model_v2_Y.LeftDeckAngleSns_deg;
  }

  /* Outport: '<Root>/Ssr2BrakeOut_bool' incorporates:
   *  Inport: '<Root>/Ssr2Brake_bool'
   */
  M220Model_v2_Y.Ssr2BrakeOut_bool = M220Model_v2_U.Ssr2Brake_bool;

  /* Switch: '<S11>/Switch' incorporates:
   *  Inport: '<Root>/HMIHydraulOilSensorOvrd_bool'
   *  Inport: '<Root>/RlyD2HydFan_bool'
   *  Switch: '<S11>/Switch1'
   */
  if (M220Model_v2_U.HMIHydraulOilSensorOvrd_bool > 0.0) {
    /* Switch: '<S11>/Switch' incorporates:
     *  Inport: '<Root>/HMIHydraulOilSensor_qts'
     */
    M220Model_v2_B.Switch_e = M220Model_v2_U.HMIHydraulOilSensor_qts;
  } else {
    if (M220Model_v2_U.RlyD2HydFan_bool > 0.0) {
      /* Switch: '<S11>/Switch1' incorporates:
       *  Constant: '<Root>/HydraulicOilLevel_gal'
       */
      M220Model_v2_B.Switch1_m = 25.0;
    } else {
      /* Math: '<S11>/Square3' incorporates:
       *  Switch: '<S11>/Switch1'
       */
      M220Model_v2_B.Square3 = M220Model_v2_B.Sqrt * M220Model_v2_B.Sqrt;

      /* Sum: '<S11>/Add' incorporates:
       *  Switch: '<S11>/Switch1'
       */
      M220Model_v2_B.Add_a2 = ((M220Model_v2_B.Square3 + M220Model_v2_B.Square)
        + M220Model_v2_B.Square1) + M220Model_v2_B.Square2;

      /* Sqrt: '<S11>/Sqrt' incorporates:
       *  Switch: '<S11>/Switch1'
       */
      M220Model_v2_B.Sqrt_l = std::sqrt(M220Model_v2_B.Add_a2);

      /* S-Function (sfix_look1_dyn): '<S11>/Lookup Table Dynamic2' incorporates:
       *  Constant: '<S11>/Constant'
       *  Constant: '<S11>/Constant1'
       *  Switch: '<S11>/Switch1'
       */
      /* Dynamic Look-Up Table Block: '<S11>/Lookup Table Dynamic2'
       * Input0  Data Type:  Floating Point real_T
       * Input1  Data Type:  Floating Point real_T
       * Input2  Data Type:  Floating Point real_T
       * Output0 Data Type:  Floating Point real_T
       * Lookup Method: Linear_Endpoint
       *
       */
      LookUpEven_real_T_real_T( &(M220Model_v2_B.LookupTableDynamic2_f),
        M220Model_v2_ConstP.Constant_Value_k, M220Model_v2_B.Sqrt_l, 90.0, 1U,
        -90.0);

      /* Switch: '<S11>/Switch1' */
      M220Model_v2_B.Switch1_m = M220Model_v2_B.LookupTableDynamic2_f;
    }

    /* Switch: '<S11>/Switch' */
    M220Model_v2_B.Switch_e = M220Model_v2_B.Switch1_m;
  }

  /* End of Switch: '<S11>/Switch' */

  /* RelationalOperator: '<S92>/Compare' incorporates:
   *  Constant: '<S92>/Constant'
   */
  M220Model_v2_B.Compare = (M220Model_v2_B.Switch_e < 15.0);

  /* Outport: '<Root>/HydraulicOilLow_bool' */
  M220Model_v2_Y.HydraulicOilLow_bool = M220Model_v2_B.Compare;
  if (rtmIsMajorTimeStep(M220Model_v2_M)) {
    /* Outport: '<Root>/DO3NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare2_bool'
     */
    M220Model_v2_Y.DO3NI9476Mon_bool = M220Model_v2_U.NI9476Spare2_bool;

    /* Outport: '<Root>/RightDeckLatched_bool' incorporates:
     *  Constant: '<S105>/Constant'
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     *  RelationalOperator: '<S105>/Compare'
     */
    M220Model_v2_Y.RightDeckLatched_bool = (M220Model_v2_Y.RightDeckAngleSns_deg
      > 85.9);

    /* Outport: '<Root>/LeftDeckLatched_bool' incorporates:
     *  Constant: '<S95>/Constant'
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     *  RelationalOperator: '<S95>/Compare'
     */
    M220Model_v2_Y.LeftDeckLatched_bool = (M220Model_v2_Y.LeftDeckAngleSns_deg >
      85.9);

    /* Outport: '<Root>/DO5NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare3_bool'
     */
    M220Model_v2_Y.DO5NI9476Mon_bool = M220Model_v2_U.NI9476Spare3_bool;

    /* Outport: '<Root>/FrontDeckLatched_bool' incorporates:
     *  Constant: '<S104>/Constant'
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     *  RelationalOperator: '<S104>/Compare'
     */
    M220Model_v2_Y.FrontDeckLatched_bool = (M220Model_v2_Y.FrontDeckAngleSns_deg
      > 85.9);

    /* Outport: '<Root>/DO7NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare4_bool'
     */
    M220Model_v2_Y.DO7NI9476Mon_bool = M220Model_v2_U.NI9476Spare4_bool;

    /* Outport: '<Root>/DO8NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare5_bool'
     */
    M220Model_v2_Y.DO8NI9476Mon_bool = M220Model_v2_U.NI9476Spare5_bool;

    /* Outport: '<Root>/DO9NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare6_bool'
     */
    M220Model_v2_Y.DO9NI9476Mon_bool = M220Model_v2_U.NI9476Spare6_bool;

    /* Outport: '<Root>/DO11NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare7_bool'
     */
    M220Model_v2_Y.DO11NI9476Mon_bool = M220Model_v2_U.NI9476Spare7_bool;

    /* Outport: '<Root>/DO12NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare8_bool'
     */
    M220Model_v2_Y.DO12NI9476Mon_bool = M220Model_v2_U.NI9476Spare8_bool;

    /* Outport: '<Root>/DO13NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare9_bool'
     */
    M220Model_v2_Y.DO13NI9476Mon_bool = M220Model_v2_U.NI9476Spare9_bool;

    /* Outport: '<Root>/DO14NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare10_bool'
     */
    M220Model_v2_Y.DO14NI9476Mon_bool = M220Model_v2_U.NI9476Spare10_bool;

    /* Outport: '<Root>/DO15NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare11_bool'
     */
    M220Model_v2_Y.DO15NI9476Mon_bool = M220Model_v2_U.NI9476Spare11_bool;

    /* Outport: '<Root>/DO16NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare12_bool'
     */
    M220Model_v2_Y.DO16NI9476Mon_bool = M220Model_v2_U.NI9476Spare12_bool;

    /* Outport: '<Root>/DO17NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare13_bool'
     */
    M220Model_v2_Y.DO17NI9476Mon_bool = M220Model_v2_U.NI9476Spare13_bool;

    /* Outport: '<Root>/DO18NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare14_bool'
     */
    M220Model_v2_Y.DO18NI9476Mon_bool = M220Model_v2_U.NI9476Spare14_bool;

    /* Outport: '<Root>/DO19NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare15_bool'
     */
    M220Model_v2_Y.DO19NI9476Mon_bool = M220Model_v2_U.NI9476Spare15_bool;

    /* Outport: '<Root>/DO20NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare16_bool'
     */
    M220Model_v2_Y.DO20NI9476Mon_bool = M220Model_v2_U.NI9476Spare16_bool;

    /* Outport: '<Root>/DO21NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare17_bool'
     */
    M220Model_v2_Y.DO21NI9476Mon_bool = M220Model_v2_U.NI9476Spare17_bool;

    /* Outport: '<Root>/EStopCab_bool' incorporates:
     *  Inport: '<Root>/CabEstop_bool'
     */
    M220Model_v2_Y.EStopCab_bool = M220Model_v2_U.CabEstop_bool;

    /* Switch: '<S15>/Switch' incorporates:
     *  Inport: '<Root>/HMISeatSwitchOvrd_bool'
     */
    if (M220Model_v2_U.HMISeatSwitchOvrd_bool > 0.0) {
      /* Outport: '<Root>/ElectSeatSwitch_bool' incorporates:
       *  Inport: '<Root>/HMISeatSwitch_bool'
       */
      M220Model_v2_Y.ElectSeatSwitch_bool = M220Model_v2_U.HMISeatSwitch_bool;
    } else {
      /* Outport: '<Root>/ElectSeatSwitch_bool' incorporates:
       *  Constant: '<S15>/Constant'
       */
      M220Model_v2_Y.ElectSeatSwitch_bool = 1.0;
    }

    /* End of Switch: '<S15>/Switch' */

    /* Outport: '<Root>/MachineEnableSeatSwitch_bool' incorporates:
     *  Inport: '<Root>/MachineEnableSwitch_bool'
     */
    M220Model_v2_Y.MachineEnableSeatSwitch_bool =
      M220Model_v2_U.MachineEnableSwitch_bool;

    /* Outport: '<Root>/AutonomyEnableSwitch_bool' incorporates:
     *  Inport: '<Root>/AutonomySwitch_bool'
     */
    M220Model_v2_Y.AutonomyEnableSwitch_bool =
      M220Model_v2_U.AutonomySwitch_bool;

    /* Outport: '<Root>/DO26NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare18_bool'
     */
    M220Model_v2_Y.DO26NI9476Mon_bool = M220Model_v2_U.NI9476Spare18_bool;

    /* Outport: '<Root>/MainKeyOutSwitch_bool' incorporates:
     *  Inport: '<Root>/MainKeySwitch_bool'
     */
    M220Model_v2_Y.MainKeyOutSwitch_bool = M220Model_v2_U.MainKeySwitch_bool;

    /* Outport: '<Root>/DO28NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare19_bool'
     */
    M220Model_v2_Y.DO28NI9476Mon_bool = M220Model_v2_U.NI9476Spare19_bool;

    /* Outport: '<Root>/AutonomyRemoteEstopSw_bool' incorporates:
     *  Inport: '<Root>/AutonomyRemoteStopSwitch_bool'
     */
    M220Model_v2_Y.AutonomyRemoteEstopSw_bool =
      M220Model_v2_U.AutonomyRemoteStopSwitch_bool;

    /* Outport: '<Root>/DO30NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare20_bool'
     */
    M220Model_v2_Y.DO30NI9476Mon_bool = M220Model_v2_U.NI9476Spare20_bool;

    /* Outport: '<Root>/DO31NI9476Mon_bool' incorporates:
     *  Inport: '<Root>/NI9476Spare21_bool'
     */
    M220Model_v2_Y.DO31NI9476Mon_bool = M220Model_v2_U.NI9476Spare21_bool;

    /* Outport: '<Root>/NI9426_2Spare17Out_bool' incorporates:
     *  Inport: '<Root>/NI9426_2Spare17_bool'
     */
    M220Model_v2_Y.NI9426_2Spare17Out_bool = M220Model_v2_U.NI9426_2Spare17_bool;
  }

  /* Product: '<S2>/Product2' incorporates:
   *  Constant: '<S2>/Constant'
   */
  M220Model_v2_B.Product2_g = 0.10471975511965977 * M220Model_v2_B.Divide2;

  /* Product: '<S2>/Divide1' */
  M220Model_v2_B.Divide1_i = M220Model_v2_ConstB.Product4_h /
    M220Model_v2_B.Product2_g;

  /* Outport: '<Root>/TorqueLeftWheel_nm' */
  M220Model_v2_Y.TorqueLeftWheel_nm = M220Model_v2_B.Divide1_i;

  /* Product: '<S2>/Product3' incorporates:
   *  Constant: '<S2>/Constant'
   */
  M220Model_v2_B.Product3 = 0.10471975511965977 * M220Model_v2_B.Divide2;

  /* Product: '<S2>/Divide2' */
  M220Model_v2_B.Divide2_j = M220Model_v2_ConstB.Product4_h /
    M220Model_v2_B.Product3;

  /* Outport: '<Root>/TorqueRightWheel_nm' */
  M220Model_v2_Y.TorqueRightWheel_nm = M220Model_v2_B.Divide2_j;

  /* Math: '<S2>/Math Function' */
  M220Model_v2_B.MathFunction_d = M220Model_v2_B.Integrator1 *
    M220Model_v2_B.Integrator1;

  /* Math: '<S2>/Math Function1' */
  M220Model_v2_B.MathFunction1_n = M220Model_v2_B.Integrator2 *
    M220Model_v2_B.Integrator2;

  /* Sum: '<S2>/Add' */
  M220Model_v2_B.Add_h = M220Model_v2_B.MathFunction_d +
    M220Model_v2_B.MathFunction1_n;

  /* Sqrt: '<S2>/Sqrt' */
  M220Model_v2_B.Sqrt_a = std::sqrt(M220Model_v2_B.Add_h);

  /* Derivative: '<S2>/Derivative' */
  if ((M220Model_v2_DW.TimeStampA >= M220Model_v2_M->Timing.t[0]) &&
      (M220Model_v2_DW.TimeStampB >= M220Model_v2_M->Timing.t[0])) {
    /* Derivative: '<S2>/Derivative' */
    M220Model_v2_B.Derivative = 0.0;
  } else {
    u0 = M220Model_v2_DW.TimeStampA;
    lastU = &M220Model_v2_DW.LastUAtTimeA;
    if (M220Model_v2_DW.TimeStampA < M220Model_v2_DW.TimeStampB) {
      if (M220Model_v2_DW.TimeStampB < M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB;
        lastU = &M220Model_v2_DW.LastUAtTimeB;
      }
    } else {
      if (M220Model_v2_DW.TimeStampA >= M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB;
        lastU = &M220Model_v2_DW.LastUAtTimeB;
      }
    }

    u0 = M220Model_v2_M->Timing.t[0] - u0;

    /* Derivative: '<S2>/Derivative' */
    M220Model_v2_B.Derivative = (M220Model_v2_B.Sqrt_a - *lastU) / u0;
  }

  /* End of Derivative: '<S2>/Derivative' */

  /* Product: '<S2>/Product' */
  M220Model_v2_B.Product = M220Model_v2_B.Derivative *
    M220Model_v2_ConstB.Divide;

  /* Integrator: '<S9>/Integrator11' */
  M220Model_v2_B.Integrator11 = M220Model_v2_X.Integrator11_CSTATE;

  /* Trigonometry: '<S2>/Cos' */
  M220Model_v2_B.Cos = std::tan(M220Model_v2_B.Integrator11);

  /* Product: '<S2>/Divide' */
  M220Model_v2_B.Divide_g = M220Model_v2_ConstB.Product1_dz / M220Model_v2_B.Cos;

  /* Product: '<S2>/Product1' */
  M220Model_v2_B.Product1 = M220Model_v2_B.Product * M220Model_v2_B.Divide_g;

  /* Outport: '<Root>/TorqueSteering_nm' */
  M220Model_v2_Y.TorqueSteering_nm = M220Model_v2_B.Product1;
  if (rtmIsMajorTimeStep(M220Model_v2_M)) {
    /* Switch: '<S14>/Switch2' incorporates:
     *  Inport: '<Root>/HMIOvrdPower24VMonitor_bool'
     */
    if (M220Model_v2_U.HMIOvrdPower24VMonitor_bool > 0.0) {
      /* Outport: '<Root>/PowerOutMon24v_Vdc' incorporates:
       *  Inport: '<Root>/HMIPower24VMonitor_Vdc'
       */
      M220Model_v2_Y.PowerOutMon24v_Vdc = M220Model_v2_U.HMIPower24VMonitor_Vdc;
    } else {
      /* Outport: '<Root>/PowerOutMon24v_Vdc' incorporates:
       *  Constant: '<Root>/Power24V_Vdc'
       */
      M220Model_v2_Y.PowerOutMon24v_Vdc = 24.0;
    }

    /* End of Switch: '<S14>/Switch2' */

    /* Switch: '<S14>/Switch1' incorporates:
     *  Inport: '<Root>/HMIOvrdPower12VMonitor_bool'
     */
    if (M220Model_v2_U.HMIOvrdPower12VMonitor_bool > 0.0) {
      /* Outport: '<Root>/PowerOutMon12v_Vdc' incorporates:
       *  Inport: '<Root>/HMIPower12VMonitor_Vdc'
       */
      M220Model_v2_Y.PowerOutMon12v_Vdc = M220Model_v2_U.HMIPower12VMonitor_Vdc;
    } else {
      /* Outport: '<Root>/PowerOutMon12v_Vdc' incorporates:
       *  Constant: '<Root>/Power12V_Vdc'
       */
      M220Model_v2_Y.PowerOutMon12v_Vdc = 12.0;
    }

    /* End of Switch: '<S14>/Switch1' */

    /* Switch: '<S14>/Switch' incorporates:
     *  Inport: '<Root>/HMIOvrdPower5VMonitor_bool'
     */
    if (M220Model_v2_U.HMIOvrdPower5VMonitor_bool > 0.0) {
      /* Outport: '<Root>/PowerOutMon5v_Vdc' incorporates:
       *  Inport: '<Root>/HMIPower5VMonitor_Vdc'
       */
      M220Model_v2_Y.PowerOutMon5v_Vdc = M220Model_v2_U.HMIPower5VMonitor_Vdc;
    } else {
      /* Outport: '<Root>/PowerOutMon5v_Vdc' incorporates:
       *  Constant: '<Root>/Power5V_Vdc'
       */
      M220Model_v2_Y.PowerOutMon5v_Vdc = 5.0;
    }

    /* End of Switch: '<S14>/Switch' */

    /* S-Function (sfix_look1_dyn): '<S5>/Lookup Table Dynamic' incorporates:
     *  Constant: '<Root>/FrntDeckAngleXData_deg'
     *  Constant: '<Root>/FrntDeckLiftPressureYData_psi'
     *  Outport: '<Root>/DeckLiftPFrntOut_psi'
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     */
    /* Dynamic Look-Up Table Block: '<S5>/Lookup Table Dynamic'
     * Input0  Data Type:  Floating Point real_T
     * Input1  Data Type:  Floating Point real_T
     * Input2  Data Type:  Floating Point real_T
     * Output0 Data Type:  Floating Point real_T
     * Lookup Method: Linear_Endpoint
     *
     */
    LookUp_real_T_real_T( &(M220Model_v2_Y.DeckLiftPFrntOut_psi),
                         M220Model_v2_ConstP.pooled13,
                         M220Model_v2_Y.FrontDeckAngleSns_deg,
                         M220Model_v2_ConstP.pooled12, 7U);

    /* Outport: '<Root>/AO0NI9264_1Mon2_int' incorporates:
     *  Inport: '<Root>/NI9264_1Spare2_int'
     */
    M220Model_v2_Y.AO0NI9264_1Mon2_int = M220Model_v2_U.NI9264_1Spare2_int;

    /* Outport: '<Root>/AO0NI9264_1Mon1_int' incorporates:
     *  Inport: '<Root>/NI9264_1Spare1_int'
     */
    M220Model_v2_Y.AO0NI9264_1Mon1_int = M220Model_v2_U.NI9264_1Spare1_int;

    /* S-Function (sfix_look1_dyn): '<S5>/Lookup Table Dynamic1' incorporates:
     *  Constant: '<Root>/RghtDeckAngleXData_deg'
     *  Constant: '<Root>/RghtDeckLiftPressureYData_psi'
     *  Outport: '<Root>/DeckLiftPRightOut_psi'
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     */
    /* Dynamic Look-Up Table Block: '<S5>/Lookup Table Dynamic1'
     * Input0  Data Type:  Floating Point real_T
     * Input1  Data Type:  Floating Point real_T
     * Input2  Data Type:  Floating Point real_T
     * Output0 Data Type:  Floating Point real_T
     * Lookup Method: Linear_Endpoint
     *
     */
    LookUp_real_T_real_T( &(M220Model_v2_Y.DeckLiftPRightOut_psi),
                         M220Model_v2_ConstP.pooled13,
                         M220Model_v2_Y.RightDeckAngleSns_deg,
                         M220Model_v2_ConstP.pooled12, 7U);

    /* S-Function (sfix_look1_dyn): '<S5>/Lookup Table Dynamic2' incorporates:
     *  Constant: '<Root>/LftDeckAngleXData_deg'
     *  Constant: '<Root>/LftDeckLiftPressureYData_psi'
     *  Outport: '<Root>/DeckLiftPLftOut_psi'
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     */
    /* Dynamic Look-Up Table Block: '<S5>/Lookup Table Dynamic2'
     * Input0  Data Type:  Floating Point real_T
     * Input1  Data Type:  Floating Point real_T
     * Input2  Data Type:  Floating Point real_T
     * Output0 Data Type:  Floating Point real_T
     * Lookup Method: Linear_Endpoint
     *
     */
    LookUp_real_T_real_T( &(M220Model_v2_Y.DeckLiftPLftOut_psi),
                         M220Model_v2_ConstP.pooled13,
                         M220Model_v2_Y.LeftDeckAngleSns_deg,
                         M220Model_v2_ConstP.pooled12, 7U);
  }

  /* S-Function (sfix_look1_dyn): '<S5>/Lookup Table Dynamic3' incorporates:
   *  Constant: '<Root>/BrakeHydraulPressXData'
   *  Constant: '<Root>/BrakeHydraulPressXData_psi'
   *  Inport: '<Root>/Ssr2Brake_bool'
   */
  /* Dynamic Look-Up Table Block: '<S5>/Lookup Table Dynamic3'
   * Input0  Data Type:  Floating Point real_T
   * Input1  Data Type:  Floating Point real_T
   * Input2  Data Type:  Floating Point real_T
   * Output0 Data Type:  Floating Point real_T
   * Lookup Method: Linear_Endpoint
   *
   */
  LookUpEven_real_T_real_T( &(M220Model_v2_B.LookupTableDynamic3),
    M220Model_v2_ConstP.BrakeHydraulPressXData_psi_Valu,
    M220Model_v2_U.Ssr2Brake_bool, 0.0, 1U, 1.0);

  /* Outport: '<Root>/BrakeHydraulPrsOut_psi' */
  M220Model_v2_Y.BrakeHydraulPrsOut_psi = M220Model_v2_B.LookupTableDynamic3;

  /* Switch: '<S10>/Switch' incorporates:
   *  Inport: '<Root>/HMIOvrdHeatExchangeTemp_bool'
   *  Inport: '<Root>/RlyD2HydFan_bool'
   *  Switch: '<S10>/Switch1'
   */
  if (M220Model_v2_U.HMIOvrdHeatExchangeTemp_bool > 0.0) {
    /* Switch: '<S10>/Switch' incorporates:
     *  Inport: '<Root>/HMIHeatExchangerTemp_degC'
     */
    M220Model_v2_B.Switch_j = M220Model_v2_U.HMIHeatExchangerTemp_degC;
  } else {
    if (M220Model_v2_U.RlyD2HydFan_bool > 0.0) {
      /* Sum: '<S10>/Subtract' incorporates:
       *  Constant: '<S10>/Constant'
       *  Inport: '<Root>/HeatExchangerTemp_mV'
       *  Switch: '<S10>/Switch1'
       */
      M220Model_v2_B.Subtract_k = M220Model_v2_U.HeatExchangerTemp_mV - -100.0;

      /* Switch: '<S10>/Switch1' */
      M220Model_v2_B.Switch1_c = M220Model_v2_B.Subtract_k;
    } else {
      /* Switch: '<S10>/Switch1' incorporates:
       *  Inport: '<Root>/HeatExchangerTemp_mV'
       */
      M220Model_v2_B.Switch1_c = M220Model_v2_U.HeatExchangerTemp_mV;
    }

    /* S-Function (sfix_look1_dyn): '<S10>/Lookup Table Dynamic' incorporates:
     *  Constant: '<Root>/HeatExchangeXData_mV'
     *  Constant: '<Root>/HeatExchangeYData_degC'
     */
    /* Dynamic Look-Up Table Block: '<S10>/Lookup Table Dynamic'
     * Input0  Data Type:  Floating Point real_T
     * Input1  Data Type:  Floating Point real_T
     * Input2  Data Type:  Floating Point real_T
     * Output0 Data Type:  Floating Point real_T
     * Lookup Method: Linear_Endpoint
     *
     */
    LookUp_real_T_real_T( &(M220Model_v2_B.LookupTableDynamic_f),
                         M220Model_v2_ConstP.pooled9, M220Model_v2_B.Switch1_c,
                         M220Model_v2_ConstP.pooled8, 200U);

    /* Switch: '<S10>/Switch' */
    M220Model_v2_B.Switch_j = M220Model_v2_B.LookupTableDynamic_f;
  }

  /* End of Switch: '<S10>/Switch' */

  /* Outport: '<Root>/HeatExchangeTemp_degC' */
  M220Model_v2_Y.HeatExchangeTemp_degC = M220Model_v2_B.Switch_j;
  if (rtmIsMajorTimeStep(M220Model_v2_M)) {
    /* Outport: '<Root>/AO0NI9264_1Mon3_int' incorporates:
     *  Inport: '<Root>/NI9264_1Spare3_int'
     */
    M220Model_v2_Y.AO0NI9264_1Mon3_int = M220Model_v2_U.NI9264_1Spare3_int;

    /* Outport: '<Root>/AO0NI9264_1Mon4_int' incorporates:
     *  Inport: '<Root>/NI9264_1Spare4_int'
     */
    M220Model_v2_Y.AO0NI9264_1Mon4_int = M220Model_v2_U.NI9264_1Spare4_int;

    /* Switch: '<S8>/Switch' incorporates:
     *  Inport: '<Root>/HMIOvrdControlBoxTemp_bool'
     */
    if (M220Model_v2_U.HMIOvrdControlBoxTemp_bool > 0.0) {
      /* Outport: '<Root>/ControlBoxTemp_degC' incorporates:
       *  Inport: '<Root>/HMIControlBoxTemp_degC'
       */
      M220Model_v2_Y.ControlBoxTemp_degC = M220Model_v2_U.HMIControlBoxTemp_degC;
    } else {
      /* S-Function (sfix_look1_dyn): '<S8>/Lookup Table Dynamic' incorporates:
       *  Constant: '<Root>/ControlBoxTempXData_mV'
       *  Constant: '<Root>/ControlBoxTempYData_degC'
       *  Inport: '<Root>/ControlBoxTemp_mV'
       */
      /* Dynamic Look-Up Table Block: '<S8>/Lookup Table Dynamic'
       * Input0  Data Type:  Floating Point real_T
       * Input1  Data Type:  Floating Point real_T
       * Input2  Data Type:  Floating Point real_T
       * Output0 Data Type:  Floating Point real_T
       * Lookup Method: Linear_Endpoint
       *
       */
      LookUp_real_T_real_T( &(M220Model_v2_B.LookupTableDynamic_i),
                           M220Model_v2_ConstP.pooled9,
                           M220Model_v2_U.ControlBoxTemp_mV,
                           M220Model_v2_ConstP.pooled8, 200U);

      /* Outport: '<Root>/ControlBoxTemp_degC' */
      M220Model_v2_Y.ControlBoxTemp_degC = M220Model_v2_B.LookupTableDynamic_i;
    }

    /* End of Switch: '<S8>/Switch' */
  }

  /* Integrator: '<S24>/Integrator' */
  M220Model_v2_B.Integrator = M220Model_v2_X.Integrator_CSTATE;

  /* Trigonometry: '<S24>/Trigonometric Function1' */
  M220Model_v2_B.TrigonometricFunction1 = std::cos(M220Model_v2_B.Integrator);

  /* Logic: '<S12>/Logical Operator1' incorporates:
   *  Inport: '<Root>/RlyAutoStart_bool'
   */
  M220Model_v2_B.LogicalOperator1 = !M220Model_v2_U.RlyAutoStart_bool;

  /* Logic: '<S12>/Logical Operator4' incorporates:
   *  Inport: '<Root>/RlyCPower_bool'
   */
  M220Model_v2_B.LogicalOperator4 = !M220Model_v2_U.RlyCPower_bool;

  /* Logic: '<S12>/Logical Operator' incorporates:
   *  Inport: '<Root>/RlyJCBEngineKill_bool'
   *  Inport: '<Root>/Ssr2Brake_bool'
   */
  M220Model_v2_B.LogicalOperator = ((M220Model_v2_U.Ssr2Brake_bool != 0.0) ||
    M220Model_v2_B.LogicalOperator1 || (M220Model_v2_U.RlyJCBEngineKill_bool !=
    0.0) || M220Model_v2_B.LogicalOperator4);

  /* Switch: '<S12>/Switch' */
  if (M220Model_v2_B.LogicalOperator) {
    /* Switch: '<S12>/Switch' incorporates:
     *  Constant: '<S12>/Constant'
     */
    M220Model_v2_B.Switch_d = 0.0;
  } else {
    /* S-Function (sfix_look1_dyn): '<S22>/Lookup Table Dynamic1' incorporates:
     *  Constant: '<S22>/JoystickPosCmdPct_int'
     *  Constant: '<S22>/JoystickPosCmdX_int'
     *  Inport: '<Root>/JoystickPosMsg_int'
     */
    /* Dynamic Look-Up Table Block: '<S22>/Lookup Table Dynamic1'
     * Input0  Data Type:  Floating Point real_T
     * Input1  Data Type:  Floating Point real_T
     * Input2  Data Type:  Floating Point real_T
     * Output0 Data Type:  Floating Point real_T
     * Lookup Method: Linear_Endpoint
     *
     */
    LookUpEven_real_T_real_T( &(M220Model_v2_B.LookupTableDynamic1_g),
      M220Model_v2_ConstP.pooled23, M220Model_v2_U.JoystickPosMsg_int, -1000.0,
      2U, 1000.0);

    /* S-Function (sfix_look1_dyn): '<S22>/Lookup Table Dynamic2' incorporates:
     *  Constant: '<S22>/JoystickPosCmdPct_int'
     *  Constant: '<S22>/JoystickPosCmdSpeedConv_m_sec'
     */
    /* Dynamic Look-Up Table Block: '<S22>/Lookup Table Dynamic2'
     * Input0  Data Type:  Floating Point real_T
     * Input1  Data Type:  Floating Point real_T
     * Input2  Data Type:  Floating Point real_T
     * Output0 Data Type:  Floating Point real_T
     * Lookup Method: Linear_Endpoint
     *
     */
    LookUpEven_real_T_real_T( &(M220Model_v2_B.LookupTableDynamic2_k),
      M220Model_v2_ConstP.JoystickPosCmdSpeedConv_m_sec_V,
      M220Model_v2_B.LookupTableDynamic1_g, -100.0, 2U, 100.0);

    /* Switch: '<S12>/Switch' */
    M220Model_v2_B.Switch_d = M220Model_v2_B.LookupTableDynamic2_k;
  }

  /* End of Switch: '<S12>/Switch' */

  /* Product: '<S24>/Product1' */
  M220Model_v2_B.Product1_l = M220Model_v2_B.TrigonometricFunction1 *
    M220Model_v2_B.Switch_d;

  /* Product: '<S31>/Divide2' */
  M220Model_v2_B.Divide2_p = 1.0 / M220Model_v2_ConstB.Product1_k *
    M220Model_v2_B.Product1_l;

  /* Product: '<S31>/Product1' */
  M220Model_v2_B.Product1_i = M220Model_v2_ConstB.Product1_k *
    M220Model_v2_B.Divide2_p;

  /* S-Function (sfix_look1_dyn): '<S22>/Lookup Table Dynamic' incorporates:
   *  Constant: '<S22>/Can2SteerAngleX_int'
   *  Constant: '<S22>/Can2SteerAngleY_int'
   *  Inport: '<Root>/SteeringPosMsg_int'
   */
  /* Dynamic Look-Up Table Block: '<S22>/Lookup Table Dynamic'
   * Input0  Data Type:  Floating Point real_T
   * Input1  Data Type:  Floating Point real_T
   * Input2  Data Type:  Floating Point real_T
   * Output0 Data Type:  Floating Point real_T
   * Lookup Method: Linear_Endpoint
   *
   */
  LookUpEven_real_T_real_T( &(M220Model_v2_B.LookupTableDynamic_h),
    M220Model_v2_ConstP.Can2SteerAngleY_int_Value,
    M220Model_v2_U.SteeringPosMsg_int, -100.0, 2U, 100.0);

  /* Trigonometry: '<S24>/Trigonometric Function2' */
  M220Model_v2_B.TrigonometricFunction2 = std::tan
    (M220Model_v2_B.LookupTableDynamic_h);

  /* Product: '<S24>/Product2' */
  M220Model_v2_B.Product2_j = M220Model_v2_B.Switch_d *
    M220Model_v2_B.TrigonometricFunction2;

  /* Product: '<S24>/Divide' incorporates:
   *  Constant: '<Root>/C'
   */
  M220Model_v2_B.Divide_c = M220Model_v2_B.Product2_j / 33.9805;

  /* Product: '<S31>/Product' */
  M220Model_v2_B.Product_f = M220Model_v2_ConstB.Product1_a *
    M220Model_v2_B.Divide_c;

  /* Sum: '<S31>/Add' */
  M220Model_v2_B.Add_m = M220Model_v2_B.Product1_l + M220Model_v2_B.Product_f;

  /* Sum: '<S31>/Add1' */
  M220Model_v2_B.Add1 = M220Model_v2_B.Product1_i - M220Model_v2_B.Add_m;

  /* RelationalOperator: '<S31>/Relational Operator' */
  M220Model_v2_B.RelationalOperator = (M220Model_v2_B.Product1_i >=
    M220Model_v2_B.Add_m);

  /* Switch: '<S31>/Switch' */
  if (M220Model_v2_B.RelationalOperator) {
    /* Product: '<S31>/Divide' */
    M220Model_v2_B.Divide_ls = 1.0 / M220Model_v2_B.Product1_i *
      M220Model_v2_B.Add1;

    /* Switch: '<S31>/Switch' */
    M220Model_v2_B.Switch_g = M220Model_v2_B.Divide_ls;
  } else {
    /* Product: '<S31>/Divide1' */
    M220Model_v2_B.Divide1_bf = M220Model_v2_B.Add1 / M220Model_v2_B.Add_m;

    /* Switch: '<S31>/Switch' */
    M220Model_v2_B.Switch_g = M220Model_v2_B.Divide1_bf;
  }

  /* End of Switch: '<S31>/Switch' */

  /* Sum: '<S25>/Add' incorporates:
   *  Constant: '<S25>/Constant'
   */
  M220Model_v2_B.Add_n = M220Model_v2_B.Switch_g + 1.0;

  /* Trigonometry: '<S24>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction = std::sin(M220Model_v2_B.Integrator);

  /* Product: '<S24>/Product' */
  M220Model_v2_B.Product_c = M220Model_v2_B.Switch_d *
    M220Model_v2_B.TrigonometricFunction;

  /* Product: '<S30>/Product' */
  M220Model_v2_B.Product_m = M220Model_v2_ConstB.Product1 *
    M220Model_v2_B.Divide_c;

  /* Sum: '<S30>/Subtract' */
  M220Model_v2_B.Subtract = M220Model_v2_B.Product_c - M220Model_v2_B.Product_m;

  /* Product: '<S30>/Product2' */
  M220Model_v2_B.Product2_c = M220Model_v2_B.Divide_c *
    M220Model_v2_ConstB.Product1_i;

  /* Sum: '<S30>/Subtract1' */
  M220Model_v2_B.Subtract1 = M220Model_v2_B.Product1_l +
    M220Model_v2_B.Product2_c;

  /* Product: '<S30>/Divide' */
  M220Model_v2_B.Divide_f = M220Model_v2_B.Subtract / M220Model_v2_B.Subtract1;

  /* Trigonometry: '<S30>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_c = std::atan(M220Model_v2_B.Divide_f);

  /* Product: '<S30>/Product1' */
  M220Model_v2_B.Product1_o = -M220Model_v2_B.TrigonometricFunction_c;

  /* Trigonometry: '<S25>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_a = std::tan(M220Model_v2_B.Product1_o);

  /* Product: '<S25>/Divide1' */
  M220Model_v2_B.Divide1_o = 1.0 / M220Model_v2_B.Add_n *
    M220Model_v2_B.TrigonometricFunction_a;

  /* Product: '<S25>/Divide2' */
  M220Model_v2_B.Divide2_m = M220Model_v2_B.Switch_g / M220Model_v2_B.Add_n;

  /* Sum: '<S29>/Add' incorporates:
   *  Constant: '<S29>/Constant'
   */
  M220Model_v2_B.Add_j = M220Model_v2_B.Switch_g + 1.0;

  /* Derivative: '<S32>/Derivative' */
  if ((M220Model_v2_DW.TimeStampA_h >= M220Model_v2_M->Timing.t[0]) &&
      (M220Model_v2_DW.TimeStampB_m >= M220Model_v2_M->Timing.t[0])) {
    /* Derivative: '<S32>/Derivative' */
    M220Model_v2_B.Derivative_h = 0.0;
  } else {
    u0 = M220Model_v2_DW.TimeStampA_h;
    lastU = &M220Model_v2_DW.LastUAtTimeA_j;
    if (M220Model_v2_DW.TimeStampA_h < M220Model_v2_DW.TimeStampB_m) {
      if (M220Model_v2_DW.TimeStampB_m < M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_m;
        lastU = &M220Model_v2_DW.LastUAtTimeB_h;
      }
    } else {
      if (M220Model_v2_DW.TimeStampA_h >= M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_m;
        lastU = &M220Model_v2_DW.LastUAtTimeB_h;
      }
    }

    u0 = M220Model_v2_M->Timing.t[0] - u0;

    /* Derivative: '<S32>/Derivative' */
    M220Model_v2_B.Derivative_h = (M220Model_v2_B.Product1_l - *lastU) / u0;
  }

  /* End of Derivative: '<S32>/Derivative' */

  /* Product: '<S32>/Product2' */
  M220Model_v2_B.Product2_n = M220Model_v2_ConstB.Divide *
    M220Model_v2_ConstB.Product1_m * M220Model_v2_B.Derivative_h;

  /* Product: '<S32>/Divide3' */
  M220Model_v2_B.Divide3 = 1.0 / M220Model_v2_ConstB.Add *
    M220Model_v2_B.Product2_n;

  /* Derivative: '<S32>/Derivative1' */
  if ((M220Model_v2_DW.TimeStampA_h5 >= M220Model_v2_M->Timing.t[0]) &&
      (M220Model_v2_DW.TimeStampB_a >= M220Model_v2_M->Timing.t[0])) {
    /* Derivative: '<S32>/Derivative1' */
    M220Model_v2_B.Derivative1 = 0.0;
  } else {
    u0 = M220Model_v2_DW.TimeStampA_h5;
    lastU = &M220Model_v2_DW.LastUAtTimeA_g;
    if (M220Model_v2_DW.TimeStampA_h5 < M220Model_v2_DW.TimeStampB_a) {
      if (M220Model_v2_DW.TimeStampB_a < M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_a;
        lastU = &M220Model_v2_DW.LastUAtTimeB_i;
      }
    } else {
      if (M220Model_v2_DW.TimeStampA_h5 >= M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_a;
        lastU = &M220Model_v2_DW.LastUAtTimeB_i;
      }
    }

    u0 = M220Model_v2_M->Timing.t[0] - u0;

    /* Derivative: '<S32>/Derivative1' */
    M220Model_v2_B.Derivative1 = (M220Model_v2_B.Product_c - *lastU) / u0;
  }

  /* End of Derivative: '<S32>/Derivative1' */

  /* Product: '<S32>/Product5' */
  M220Model_v2_B.Product5_a = M220Model_v2_ConstB.Product1_m *
    M220Model_v2_ConstB.Divide * M220Model_v2_ConstB.Product1_b *
    M220Model_v2_B.Derivative1;

  /* Product: '<S32>/Divide1' */
  M220Model_v2_B.Divide1_n = M220Model_v2_B.Product5_a /
    M220Model_v2_ConstB.Product4;

  /* Derivative: '<Root>/Derivative' */
  if ((M220Model_v2_DW.TimeStampA_m >= M220Model_v2_M->Timing.t[0]) &&
      (M220Model_v2_DW.TimeStampB_k >= M220Model_v2_M->Timing.t[0])) {
    /* Derivative: '<Root>/Derivative' */
    M220Model_v2_B.Derivative_o = 0.0;
  } else {
    u0 = M220Model_v2_DW.TimeStampA_m;
    lastU = &M220Model_v2_DW.LastUAtTimeA_gr;
    if (M220Model_v2_DW.TimeStampA_m < M220Model_v2_DW.TimeStampB_k) {
      if (M220Model_v2_DW.TimeStampB_k < M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_k;
        lastU = &M220Model_v2_DW.LastUAtTimeB_l;
      }
    } else {
      if (M220Model_v2_DW.TimeStampA_m >= M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_k;
        lastU = &M220Model_v2_DW.LastUAtTimeB_l;
      }
    }

    u0 = M220Model_v2_M->Timing.t[0] - u0;

    /* Derivative: '<Root>/Derivative' incorporates:
     *  Constant: '<Root>/LonTerrainAngle_deg'
     */
    M220Model_v2_B.Derivative_o = (0.0 - *lastU) / u0;
  }

  /* End of Derivative: '<Root>/Derivative' */

  /* Product: '<S32>/Product3' incorporates:
   *  Constant: '<Root>/DampConstSusp1_Cs'
   *  Constant: '<Root>/e_const_in'
   */
  M220Model_v2_B.Product3_k = 2.5E-17 * M220Model_v2_B.Derivative_o;

  /* Sum: '<S32>/Add1' */
  M220Model_v2_B.Add1_g = (((M220Model_v2_ConstB.Divide_b +
    M220Model_v2_B.Divide3) - M220Model_v2_B.Divide1_n) -
    M220Model_v2_B.Product3_k) - M220Model_v2_ConstB.Product6;

  /* Product: '<S29>/Product1' incorporates:
   *  Constant: '<Root>/GroundFrictionCoeff_mu'
   */
  M220Model_v2_B.Product1_n = M220Model_v2_B.Add_j * M220Model_v2_B.Add1_g * 0.6;

  /* Product: '<S29>/Product3' incorporates:
   *  Constant: '<Root>/LongCornerStiffTire1_Ca'
   */
  M220Model_v2_B.Product3_g = M220Model_v2_B.Add1_g * 12000.0;

  /* Math: '<S29>/Square' */
  M220Model_v2_B.Square_d = M220Model_v2_B.Product3_g *
    M220Model_v2_B.Product3_g;

  /* Trigonometry: '<S29>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_h = std::tan(M220Model_v2_B.Product1_o);

  /* Product: '<S29>/Product4' incorporates:
   *  Constant: '<Root>/LatCornerStiffTire1_Cb'
   */
  M220Model_v2_B.Product4 = 13000.0 * M220Model_v2_B.TrigonometricFunction_h;

  /* Math: '<S29>/Square1' */
  M220Model_v2_B.Square1_g = M220Model_v2_B.Product4 * M220Model_v2_B.Product4;

  /* Sum: '<S29>/Add1' */
  M220Model_v2_B.Add1_n = M220Model_v2_B.Square_d + M220Model_v2_B.Square1_g;

  /* Sqrt: '<S29>/Sqrt' */
  M220Model_v2_B.Sqrt_n = std::sqrt(M220Model_v2_B.Add1_n);

  /* Product: '<S29>/Product2' incorporates:
   *  Constant: '<S29>/Constant1'
   */
  M220Model_v2_B.Product2_i = 2.0 * M220Model_v2_B.Sqrt_n;

  /* Product: '<S29>/Divide' */
  M220Model_v2_B.Divide_m = M220Model_v2_B.Product1_n /
    M220Model_v2_B.Product2_i;

  /* Switch: '<S25>/Switch' */
  if (M220Model_v2_B.Divide_m >= 1.0) {
    /* Switch: '<S25>/Switch' incorporates:
     *  Constant: '<S25>/Constant2'
     */
    M220Model_v2_B.Switch_jk = 1.0;
  } else {
    /* Sum: '<S25>/Subtract' incorporates:
     *  Constant: '<S25>/Constant1'
     */
    M220Model_v2_B.Subtract_n = 2.0 - M220Model_v2_B.Divide_m;

    /* Product: '<S25>/Product3' */
    M220Model_v2_B.Product3_ks = M220Model_v2_B.Divide_m *
      M220Model_v2_B.Subtract_n;

    /* Switch: '<S25>/Switch' */
    M220Model_v2_B.Switch_jk = M220Model_v2_B.Product3_ks;
  }

  /* End of Switch: '<S25>/Switch' */

  /* Product: '<S25>/Product1' incorporates:
   *  Constant: '<Root>/LongCornerStiffTire1_Ca'
   */
  M220Model_v2_B.Product1_ob = M220Model_v2_B.Switch_jk * 12000.0 *
    M220Model_v2_B.Divide2_m;

  /* Product: '<S25>/Product2' incorporates:
   *  Constant: '<Root>/LatCornerStiffTire1_Cb'
   */
  M220Model_v2_B.Product2_h = M220Model_v2_B.Switch_jk *
    M220Model_v2_B.Divide1_o * 13000.0;

  /* Product: '<S42>/Divide2' */
  M220Model_v2_B.Divide2_c = 1.0 / M220Model_v2_ConstB.Product1_bd *
    M220Model_v2_B.Product1_l;

  /* Product: '<S42>/Product1' */
  M220Model_v2_B.Product1_f = M220Model_v2_ConstB.Product1_bd *
    M220Model_v2_B.Divide2_c;

  /* Product: '<S42>/Product' */
  M220Model_v2_B.Product_i = M220Model_v2_ConstB.Product1_l *
    M220Model_v2_B.Divide_c;

  /* Sum: '<S42>/Add' */
  M220Model_v2_B.Add_p = M220Model_v2_B.Product1_l - M220Model_v2_B.Product_i;

  /* Sum: '<S42>/Add1' */
  M220Model_v2_B.Add1_d = M220Model_v2_B.Product1_f - M220Model_v2_B.Add_p;

  /* RelationalOperator: '<S42>/Relational Operator' */
  M220Model_v2_B.RelationalOperator_b = (M220Model_v2_B.Product1_f >=
    M220Model_v2_B.Add_p);

  /* Switch: '<S42>/Switch' */
  if (M220Model_v2_B.RelationalOperator_b) {
    /* Product: '<S42>/Divide' */
    M220Model_v2_B.Divide_i = 1.0 / M220Model_v2_B.Product1_f *
      M220Model_v2_B.Add1_d;

    /* Switch: '<S42>/Switch' */
    M220Model_v2_B.Switch_bb = M220Model_v2_B.Divide_i;
  } else {
    /* Product: '<S42>/Divide1' */
    M220Model_v2_B.Divide1_gd = M220Model_v2_B.Add1_d / M220Model_v2_B.Add_p;

    /* Switch: '<S42>/Switch' */
    M220Model_v2_B.Switch_bb = M220Model_v2_B.Divide1_gd;
  }

  /* End of Switch: '<S42>/Switch' */

  /* Sum: '<S26>/Add' incorporates:
   *  Constant: '<S26>/Constant'
   */
  M220Model_v2_B.Add_c = M220Model_v2_B.Switch_bb + 1.0;

  /* Product: '<S41>/Product' */
  M220Model_v2_B.Product_p = M220Model_v2_ConstB.Product1_d *
    M220Model_v2_B.Divide_c;

  /* Sum: '<S41>/Subtract' */
  M220Model_v2_B.Subtract_g = M220Model_v2_B.Product_c -
    M220Model_v2_B.Product_p;

  /* Product: '<S41>/Product2' */
  M220Model_v2_B.Product2_e = M220Model_v2_B.Divide_c *
    M220Model_v2_ConstB.Product1_kw;

  /* Sum: '<S41>/Subtract1' */
  M220Model_v2_B.Subtract1_d = M220Model_v2_B.Product1_l -
    M220Model_v2_B.Product2_e;

  /* Product: '<S41>/Divide' */
  M220Model_v2_B.Divide_n = M220Model_v2_B.Subtract_g /
    M220Model_v2_B.Subtract1_d;

  /* Trigonometry: '<S41>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_f = std::atan(M220Model_v2_B.Divide_n);

  /* Product: '<S41>/Product1' */
  M220Model_v2_B.Product1_j = -M220Model_v2_B.TrigonometricFunction_f;

  /* Trigonometry: '<S26>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_n = std::tan(M220Model_v2_B.Product1_j);

  /* Product: '<S26>/Divide1' */
  M220Model_v2_B.Divide1_c = 1.0 / M220Model_v2_B.Add_c *
    M220Model_v2_B.TrigonometricFunction_n;

  /* Product: '<S26>/Divide2' */
  M220Model_v2_B.Divide2_l = M220Model_v2_B.Switch_bb / M220Model_v2_B.Add_c;

  /* Sum: '<S40>/Add' incorporates:
   *  Constant: '<S40>/Constant'
   */
  M220Model_v2_B.Add_ci = M220Model_v2_B.Switch_bb + 1.0;

  /* Derivative: '<S43>/Derivative' */
  if ((M220Model_v2_DW.TimeStampA_hx >= M220Model_v2_M->Timing.t[0]) &&
      (M220Model_v2_DW.TimeStampB_p >= M220Model_v2_M->Timing.t[0])) {
    /* Derivative: '<S43>/Derivative' */
    M220Model_v2_B.Derivative_b = 0.0;
  } else {
    u0 = M220Model_v2_DW.TimeStampA_hx;
    lastU = &M220Model_v2_DW.LastUAtTimeA_p;
    if (M220Model_v2_DW.TimeStampA_hx < M220Model_v2_DW.TimeStampB_p) {
      if (M220Model_v2_DW.TimeStampB_p < M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_p;
        lastU = &M220Model_v2_DW.LastUAtTimeB_c;
      }
    } else {
      if (M220Model_v2_DW.TimeStampA_hx >= M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_p;
        lastU = &M220Model_v2_DW.LastUAtTimeB_c;
      }
    }

    u0 = M220Model_v2_M->Timing.t[0] - u0;

    /* Derivative: '<S43>/Derivative' */
    M220Model_v2_B.Derivative_b = (M220Model_v2_B.Product1_l - *lastU) / u0;
  }

  /* End of Derivative: '<S43>/Derivative' */

  /* Product: '<S43>/Product2' */
  M220Model_v2_B.Product2_gb = M220Model_v2_ConstB.Divide *
    M220Model_v2_ConstB.Product1_o * M220Model_v2_B.Derivative_b;

  /* Product: '<S43>/Divide3' */
  M220Model_v2_B.Divide3_m = 1.0 / M220Model_v2_ConstB.Add_h *
    M220Model_v2_B.Product2_gb;

  /* Derivative: '<S43>/Derivative1' */
  if ((M220Model_v2_DW.TimeStampA_mc >= M220Model_v2_M->Timing.t[0]) &&
      (M220Model_v2_DW.TimeStampB_l >= M220Model_v2_M->Timing.t[0])) {
    /* Derivative: '<S43>/Derivative1' */
    M220Model_v2_B.Derivative1_d = 0.0;
  } else {
    u0 = M220Model_v2_DW.TimeStampA_mc;
    lastU = &M220Model_v2_DW.LastUAtTimeA_ge;
    if (M220Model_v2_DW.TimeStampA_mc < M220Model_v2_DW.TimeStampB_l) {
      if (M220Model_v2_DW.TimeStampB_l < M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_l;
        lastU = &M220Model_v2_DW.LastUAtTimeB_ik;
      }
    } else {
      if (M220Model_v2_DW.TimeStampA_mc >= M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_l;
        lastU = &M220Model_v2_DW.LastUAtTimeB_ik;
      }
    }

    u0 = M220Model_v2_M->Timing.t[0] - u0;

    /* Derivative: '<S43>/Derivative1' */
    M220Model_v2_B.Derivative1_d = (M220Model_v2_B.Product_c - *lastU) / u0;
  }

  /* End of Derivative: '<S43>/Derivative1' */

  /* Product: '<S43>/Product5' */
  M220Model_v2_B.Product5_m = M220Model_v2_ConstB.Product1_o *
    M220Model_v2_ConstB.Divide * M220Model_v2_ConstB.Product1_f *
    M220Model_v2_B.Derivative1_d;

  /* Product: '<S43>/Divide1' */
  M220Model_v2_B.Divide1_p = M220Model_v2_B.Product5_m /
    M220Model_v2_ConstB.Product4_p;

  /* Product: '<S43>/Product3' incorporates:
   *  Constant: '<Root>/DampConstSusp2_Cs'
   *  Constant: '<Root>/e_const_in'
   */
  M220Model_v2_B.Product3_j = 2.5E-17 * M220Model_v2_B.Derivative_o;

  /* Sum: '<S43>/Add1' */
  M220Model_v2_B.Add1_h = (((M220Model_v2_ConstB.Divide_bk +
    M220Model_v2_B.Divide3_m) - M220Model_v2_B.Divide1_p) +
    M220Model_v2_B.Product3_j) + M220Model_v2_ConstB.Product6_j;

  /* Product: '<S40>/Product1' incorporates:
   *  Constant: '<Root>/GroundFrictionCoeff_mu'
   */
  M220Model_v2_B.Product1_c = M220Model_v2_B.Add_ci * M220Model_v2_B.Add1_h *
    0.6;

  /* Product: '<S40>/Product3' incorporates:
   *  Constant: '<Root>/LongCornerStiffTire2_Ca'
   */
  M220Model_v2_B.Product3_kf = M220Model_v2_B.Add1_h * 12000.0;

  /* Math: '<S40>/Square' */
  M220Model_v2_B.Square_n = M220Model_v2_B.Product3_kf *
    M220Model_v2_B.Product3_kf;

  /* Trigonometry: '<S40>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_i = std::tan(M220Model_v2_B.Product1_j);

  /* Product: '<S40>/Product4' incorporates:
   *  Constant: '<Root>/LatCornerStiffTire2_Cb'
   */
  M220Model_v2_B.Product4_l = 13000.0 * M220Model_v2_B.TrigonometricFunction_i;

  /* Math: '<S40>/Square1' */
  M220Model_v2_B.Square1_o = M220Model_v2_B.Product4_l *
    M220Model_v2_B.Product4_l;

  /* Sum: '<S40>/Add1' */
  M220Model_v2_B.Add1_n2 = M220Model_v2_B.Square_n + M220Model_v2_B.Square1_o;

  /* Sqrt: '<S40>/Sqrt' */
  M220Model_v2_B.Sqrt_nj = std::sqrt(M220Model_v2_B.Add1_n2);

  /* Product: '<S40>/Product2' incorporates:
   *  Constant: '<S40>/Constant1'
   */
  M220Model_v2_B.Product2_l = 2.0 * M220Model_v2_B.Sqrt_nj;

  /* Product: '<S40>/Divide' */
  M220Model_v2_B.Divide_p = M220Model_v2_B.Product1_c /
    M220Model_v2_B.Product2_l;

  /* Switch: '<S26>/Switch' */
  if (M220Model_v2_B.Divide_p >= 1.0) {
    /* Switch: '<S26>/Switch' incorporates:
     *  Constant: '<S26>/Constant2'
     */
    M220Model_v2_B.Switch_a = 1.0;
  } else {
    /* Sum: '<S26>/Subtract' incorporates:
     *  Constant: '<S26>/Constant1'
     */
    M220Model_v2_B.Subtract_e = 2.0 - M220Model_v2_B.Divide_p;

    /* Product: '<S26>/Product3' */
    M220Model_v2_B.Product3_i = M220Model_v2_B.Divide_p *
      M220Model_v2_B.Subtract_e;

    /* Switch: '<S26>/Switch' */
    M220Model_v2_B.Switch_a = M220Model_v2_B.Product3_i;
  }

  /* End of Switch: '<S26>/Switch' */

  /* Product: '<S26>/Product1' incorporates:
   *  Constant: '<Root>/LongCornerStiffTire2_Ca'
   */
  M220Model_v2_B.Product1_oo = M220Model_v2_B.Switch_a * 12000.0 *
    M220Model_v2_B.Divide2_l;

  /* Product: '<S26>/Product2' incorporates:
   *  Constant: '<Root>/LatCornerStiffTire2_Cb'
   */
  M220Model_v2_B.Product2_f = M220Model_v2_B.Switch_a * M220Model_v2_B.Divide1_c
    * 13000.0;

  /* Product: '<S53>/Divide2' */
  M220Model_v2_B.Divide2_k = 1.0 / M220Model_v2_ConstB.Product1_ek *
    M220Model_v2_B.Product1_l;

  /* Product: '<S53>/Product1' */
  M220Model_v2_B.Product1_nt = M220Model_v2_ConstB.Product1_ek *
    M220Model_v2_B.Divide2_k;

  /* Product: '<S53>/Product' */
  M220Model_v2_B.Product_d = M220Model_v2_ConstB.Product1_n *
    M220Model_v2_B.Divide_c;

  /* Sum: '<S53>/Add' */
  M220Model_v2_B.Add_f = M220Model_v2_B.Product1_l - M220Model_v2_B.Product_d;

  /* Trigonometry: '<S53>/Trigonometric Function1' */
  M220Model_v2_B.TrigonometricFunction1_j = std::cos(M220Model_v2_B.Add_f);

  /* Gain: '<S58>/Gain1' */
  M220Model_v2_B.Gain1_b = 0.017453292519943295 *
    M220Model_v2_B.LookupTableDynamic_h;

  /* Product: '<S53>/Product3' */
  M220Model_v2_B.Product3_b = M220Model_v2_B.TrigonometricFunction1_j *
    M220Model_v2_B.Gain1_b;

  /* Sum: '<S53>/Add2' */
  M220Model_v2_B.Add2 = M220Model_v2_B.Product_d + M220Model_v2_B.Product1_l;

  /* Trigonometry: '<S53>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_cb = std::sin(M220Model_v2_B.Add2);

  /* Product: '<S53>/Product2' */
  M220Model_v2_B.Product2_n0 = M220Model_v2_B.TrigonometricFunction_cb *
    M220Model_v2_B.Gain1_b;

  /* Sum: '<S53>/Add3' */
  M220Model_v2_B.Add3 = M220Model_v2_B.Product3_b + M220Model_v2_B.Product2_n0;

  /* Sum: '<S53>/Add1' */
  M220Model_v2_B.Add1_n1 = M220Model_v2_B.Product1_nt - M220Model_v2_B.Add3;

  /* RelationalOperator: '<S53>/Relational Operator' */
  M220Model_v2_B.RelationalOperator_k = (M220Model_v2_B.Product1_nt >=
    M220Model_v2_B.Add3);

  /* Switch: '<S53>/Switch' */
  if (M220Model_v2_B.RelationalOperator_k) {
    /* Product: '<S53>/Divide' */
    M220Model_v2_B.Divide_l = 1.0 / M220Model_v2_B.Product1_nt *
      M220Model_v2_B.Add1_n1;

    /* Switch: '<S53>/Switch' */
    M220Model_v2_B.Switch_on = M220Model_v2_B.Divide_l;
  } else {
    /* Product: '<S53>/Divide1' */
    M220Model_v2_B.Divide1_j = M220Model_v2_B.Add1_n1 / M220Model_v2_B.Add3;

    /* Switch: '<S53>/Switch' */
    M220Model_v2_B.Switch_on = M220Model_v2_B.Divide1_j;
  }

  /* End of Switch: '<S53>/Switch' */

  /* Sum: '<S27>/Add' incorporates:
   *  Constant: '<S27>/Constant'
   */
  M220Model_v2_B.Add_cia = M220Model_v2_B.Switch_on + 1.0;

  /* Product: '<S52>/Product' */
  M220Model_v2_B.Product_ir = M220Model_v2_ConstB.Product1_pc *
    M220Model_v2_B.Divide_c;

  /* Sum: '<S52>/Subtract' */
  M220Model_v2_B.Subtract_i = M220Model_v2_B.Product_c +
    M220Model_v2_B.Product_ir;

  /* Product: '<S52>/Product2' */
  M220Model_v2_B.Product2_ee = M220Model_v2_B.Divide_c *
    M220Model_v2_ConstB.Product1_g;

  /* Sum: '<S52>/Subtract1' */
  M220Model_v2_B.Subtract1_j = M220Model_v2_B.Product1_l -
    M220Model_v2_B.Product2_ee;

  /* Product: '<S52>/Divide' */
  M220Model_v2_B.Divide_k = M220Model_v2_B.Subtract_i /
    M220Model_v2_B.Subtract1_j;

  /* Trigonometry: '<S52>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_l = std::atan(M220Model_v2_B.Divide_k);

  /* Gain: '<S55>/Gain1' */
  M220Model_v2_B.Gain1_p = 0.017453292519943295 *
    M220Model_v2_B.LookupTableDynamic_h;

  /* Sum: '<S52>/Subtract2' */
  M220Model_v2_B.Subtract2 = M220Model_v2_B.Gain1_p -
    M220Model_v2_B.TrigonometricFunction_l;

  /* Trigonometry: '<S27>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_e = std::tan(M220Model_v2_B.Subtract2);

  /* Product: '<S27>/Divide1' */
  M220Model_v2_B.Divide1_l = 1.0 / M220Model_v2_B.Add_cia *
    M220Model_v2_B.TrigonometricFunction_e;

  /* Product: '<S27>/Divide2' */
  M220Model_v2_B.Divide2_e = M220Model_v2_B.Switch_on / M220Model_v2_B.Add_cia;

  /* Sum: '<S51>/Add' incorporates:
   *  Constant: '<S51>/Constant'
   */
  M220Model_v2_B.Add_k = M220Model_v2_B.Switch_on + 1.0;

  /* Derivative: '<S54>/Derivative' */
  if ((M220Model_v2_DW.TimeStampA_o >= M220Model_v2_M->Timing.t[0]) &&
      (M220Model_v2_DW.TimeStampB_k1 >= M220Model_v2_M->Timing.t[0])) {
    /* Derivative: '<S54>/Derivative' */
    M220Model_v2_B.Derivative_p = 0.0;
  } else {
    u0 = M220Model_v2_DW.TimeStampA_o;
    lastU = &M220Model_v2_DW.LastUAtTimeA_i;
    if (M220Model_v2_DW.TimeStampA_o < M220Model_v2_DW.TimeStampB_k1) {
      if (M220Model_v2_DW.TimeStampB_k1 < M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_k1;
        lastU = &M220Model_v2_DW.LastUAtTimeB_g;
      }
    } else {
      if (M220Model_v2_DW.TimeStampA_o >= M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_k1;
        lastU = &M220Model_v2_DW.LastUAtTimeB_g;
      }
    }

    u0 = M220Model_v2_M->Timing.t[0] - u0;

    /* Derivative: '<S54>/Derivative' */
    M220Model_v2_B.Derivative_p = (M220Model_v2_B.Product1_l - *lastU) / u0;
  }

  /* End of Derivative: '<S54>/Derivative' */

  /* Product: '<S54>/Product2' */
  M220Model_v2_B.Product2_m = M220Model_v2_ConstB.Divide *
    M220Model_v2_ConstB.Product1_lq * M220Model_v2_B.Derivative_p;

  /* Product: '<S54>/Divide3' */
  M220Model_v2_B.Divide3_b = 1.0 / M220Model_v2_ConstB.Add_k *
    M220Model_v2_B.Product2_m;

  /* Derivative: '<S54>/Derivative1' */
  if ((M220Model_v2_DW.TimeStampA_d >= M220Model_v2_M->Timing.t[0]) &&
      (M220Model_v2_DW.TimeStampB_ko >= M220Model_v2_M->Timing.t[0])) {
    /* Derivative: '<S54>/Derivative1' */
    M220Model_v2_B.Derivative1_p = 0.0;
  } else {
    u0 = M220Model_v2_DW.TimeStampA_d;
    lastU = &M220Model_v2_DW.LastUAtTimeA_jv;
    if (M220Model_v2_DW.TimeStampA_d < M220Model_v2_DW.TimeStampB_ko) {
      if (M220Model_v2_DW.TimeStampB_ko < M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_ko;
        lastU = &M220Model_v2_DW.LastUAtTimeB_i2;
      }
    } else {
      if (M220Model_v2_DW.TimeStampA_d >= M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_ko;
        lastU = &M220Model_v2_DW.LastUAtTimeB_i2;
      }
    }

    u0 = M220Model_v2_M->Timing.t[0] - u0;

    /* Derivative: '<S54>/Derivative1' */
    M220Model_v2_B.Derivative1_p = (M220Model_v2_B.Product_c - *lastU) / u0;
  }

  /* End of Derivative: '<S54>/Derivative1' */

  /* Product: '<S54>/Product5' */
  M220Model_v2_B.Product5_l = M220Model_v2_ConstB.Product1_lq *
    M220Model_v2_ConstB.Divide * M220Model_v2_ConstB.Product1_h *
    M220Model_v2_B.Derivative1_p;

  /* Product: '<S54>/Divide1' */
  M220Model_v2_B.Divide1_po = M220Model_v2_B.Product5_l /
    M220Model_v2_ConstB.Product4_o;

  /* Product: '<S54>/Product3' incorporates:
   *  Constant: '<Root>/DampConstSusp3_Cs'
   *  Constant: '<Root>/e_const_in'
   */
  M220Model_v2_B.Product3_m = 3.0E-17 * M220Model_v2_B.Derivative_o;

  /* Sum: '<S54>/Add1' */
  M220Model_v2_B.Add1_i = (((M220Model_v2_ConstB.Divide_c -
    M220Model_v2_B.Divide3_b) + M220Model_v2_B.Divide1_po) -
    M220Model_v2_B.Product3_m) - M220Model_v2_ConstB.Product6_h;

  /* Product: '<S51>/Product1' incorporates:
   *  Constant: '<Root>/GroundFrictionCoeff_mu'
   */
  M220Model_v2_B.Product1_jp = M220Model_v2_B.Add_k * M220Model_v2_B.Add1_i *
    0.6;

  /* Product: '<S51>/Product3' incorporates:
   *  Constant: '<Root>/LongCornerStiffTire3_Ca'
   */
  M220Model_v2_B.Product3_gk = M220Model_v2_B.Add1_i * 15000.0;

  /* Math: '<S51>/Square' */
  M220Model_v2_B.Square_du = M220Model_v2_B.Product3_gk *
    M220Model_v2_B.Product3_gk;

  /* Trigonometry: '<S51>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_k = std::tan(M220Model_v2_B.Subtract2);

  /* Product: '<S51>/Product4' incorporates:
   *  Constant: '<Root>/LatCornerStiffTire3_Cb'
   */
  M220Model_v2_B.Product4_lj = 18000.0 * M220Model_v2_B.TrigonometricFunction_k;

  /* Math: '<S51>/Square1' */
  M220Model_v2_B.Square1_h = M220Model_v2_B.Product4_lj *
    M220Model_v2_B.Product4_lj;

  /* Sum: '<S51>/Add1' */
  M220Model_v2_B.Add1_f = M220Model_v2_B.Square_du + M220Model_v2_B.Square1_h;

  /* Sqrt: '<S51>/Sqrt' */
  M220Model_v2_B.Sqrt_b = std::sqrt(M220Model_v2_B.Add1_f);

  /* Product: '<S51>/Product2' incorporates:
   *  Constant: '<S51>/Constant1'
   */
  M220Model_v2_B.Product2_mf = 2.0 * M220Model_v2_B.Sqrt_b;

  /* Product: '<S51>/Divide' */
  M220Model_v2_B.Divide_d = M220Model_v2_B.Product1_jp /
    M220Model_v2_B.Product2_mf;

  /* Switch: '<S27>/Switch' */
  if (M220Model_v2_B.Divide_d >= 1.0) {
    /* Switch: '<S27>/Switch' incorporates:
     *  Constant: '<S27>/Constant2'
     */
    M220Model_v2_B.Switch_ge = 1.0;
  } else {
    /* Sum: '<S27>/Subtract' incorporates:
     *  Constant: '<S27>/Constant1'
     */
    M220Model_v2_B.Subtract_iq = 2.0 - M220Model_v2_B.Divide_d;

    /* Product: '<S27>/Product3' */
    M220Model_v2_B.Product3_c = M220Model_v2_B.Divide_d *
      M220Model_v2_B.Subtract_iq;

    /* Switch: '<S27>/Switch' */
    M220Model_v2_B.Switch_ge = M220Model_v2_B.Product3_c;
  }

  /* End of Switch: '<S27>/Switch' */

  /* Product: '<S27>/Product1' incorporates:
   *  Constant: '<Root>/LongCornerStiffTire3_Ca'
   */
  M220Model_v2_B.Product1_p = M220Model_v2_B.Switch_ge * 15000.0 *
    M220Model_v2_B.Divide2_e;

  /* Product: '<S27>/Product2' incorporates:
   *  Constant: '<Root>/LatCornerStiffTire3_Cb'
   */
  M220Model_v2_B.Product2_ng = M220Model_v2_B.Switch_ge *
    M220Model_v2_B.Divide1_l * 18000.0;

  /* Product: '<S66>/Divide2' */
  M220Model_v2_B.Divide2_pv = 1.0 / M220Model_v2_ConstB.Product1_c *
    M220Model_v2_B.Product1_l;

  /* Product: '<S66>/Product1' */
  M220Model_v2_B.Product1_e = M220Model_v2_ConstB.Product1_c *
    M220Model_v2_B.Divide2_pv;

  /* Product: '<S66>/Product' */
  M220Model_v2_B.Product_n = M220Model_v2_ConstB.Product1_kv *
    M220Model_v2_B.Divide_c;

  /* Sum: '<S66>/Add' */
  M220Model_v2_B.Add_e = M220Model_v2_B.Product1_l + M220Model_v2_B.Product_n;

  /* Trigonometry: '<S66>/Trigonometric Function1' */
  M220Model_v2_B.TrigonometricFunction1_jd = std::cos(M220Model_v2_B.Add_e);

  /* Gain: '<S71>/Gain1' */
  M220Model_v2_B.Gain1_f = 0.017453292519943295 *
    M220Model_v2_B.LookupTableDynamic_h;

  /* Product: '<S66>/Product3' */
  M220Model_v2_B.Product3_mz = M220Model_v2_B.TrigonometricFunction1_jd *
    M220Model_v2_B.Gain1_f;

  /* Sum: '<S66>/Add2' */
  M220Model_v2_B.Add2_h = M220Model_v2_B.Product_n + M220Model_v2_B.Product1_l;

  /* Trigonometry: '<S66>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_g = std::sin(M220Model_v2_B.Add2_h);

  /* Product: '<S66>/Product2' */
  M220Model_v2_B.Product2_k = M220Model_v2_B.TrigonometricFunction_g *
    M220Model_v2_B.Gain1_f;

  /* Sum: '<S66>/Add3' */
  M220Model_v2_B.Add3_c = M220Model_v2_B.Product3_mz + M220Model_v2_B.Product2_k;

  /* Sum: '<S66>/Add1' */
  M220Model_v2_B.Add1_f0 = M220Model_v2_B.Product1_e - M220Model_v2_B.Add3_c;

  /* RelationalOperator: '<S66>/Relational Operator' */
  M220Model_v2_B.RelationalOperator_m = (M220Model_v2_B.Product1_e >=
    M220Model_v2_B.Add3_c);

  /* Switch: '<S66>/Switch' */
  if (M220Model_v2_B.RelationalOperator_m) {
    /* Product: '<S66>/Divide' */
    M220Model_v2_B.Divide_j = 1.0 / M220Model_v2_B.Product1_e *
      M220Model_v2_B.Add1_f0;

    /* Switch: '<S66>/Switch' */
    M220Model_v2_B.Switch_ez = M220Model_v2_B.Divide_j;
  } else {
    /* Product: '<S66>/Divide1' */
    M220Model_v2_B.Divide1_g = M220Model_v2_B.Add1_f0 / M220Model_v2_B.Add3_c;

    /* Switch: '<S66>/Switch' */
    M220Model_v2_B.Switch_ez = M220Model_v2_B.Divide1_g;
  }

  /* End of Switch: '<S66>/Switch' */

  /* Sum: '<S28>/Add' incorporates:
   *  Constant: '<S28>/Constant'
   */
  M220Model_v2_B.Add_a = M220Model_v2_B.Switch_ez + 1.0;

  /* Product: '<S65>/Product' */
  M220Model_v2_B.Product_e = M220Model_v2_ConstB.Product1_nh *
    M220Model_v2_B.Divide_c;

  /* Sum: '<S65>/Subtract' */
  M220Model_v2_B.Subtract_f = M220Model_v2_B.Product_c +
    M220Model_v2_B.Product_e;

  /* Product: '<S65>/Product2' */
  M220Model_v2_B.Product2_d = M220Model_v2_B.Divide_c *
    M220Model_v2_ConstB.Product1_na;

  /* Sum: '<S65>/Subtract1' */
  M220Model_v2_B.Subtract1_f = M220Model_v2_B.Product1_l +
    M220Model_v2_B.Product2_d;

  /* Product: '<S65>/Divide' */
  M220Model_v2_B.Divide_b = M220Model_v2_B.Subtract_f /
    M220Model_v2_B.Subtract1_f;

  /* Trigonometry: '<S65>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_nu = std::atan(M220Model_v2_B.Divide_b);

  /* Gain: '<S68>/Gain1' */
  M220Model_v2_B.Gain1_e = 0.017453292519943295 *
    M220Model_v2_B.LookupTableDynamic_h;

  /* Sum: '<S65>/Subtract2' */
  M220Model_v2_B.Subtract2_p = M220Model_v2_B.Gain1_e -
    M220Model_v2_B.TrigonometricFunction_nu;

  /* Trigonometry: '<S28>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_d = std::tan(M220Model_v2_B.Subtract2_p);

  /* Product: '<S28>/Divide1' */
  M220Model_v2_B.Divide1_ix = 1.0 / M220Model_v2_B.Add_a *
    M220Model_v2_B.TrigonometricFunction_d;

  /* Product: '<S28>/Divide2' */
  M220Model_v2_B.Divide2_e2 = M220Model_v2_B.Switch_ez / M220Model_v2_B.Add_a;

  /* Sum: '<S64>/Add' incorporates:
   *  Constant: '<S64>/Constant'
   */
  M220Model_v2_B.Add_km = M220Model_v2_B.Switch_ez + 1.0;

  /* Derivative: '<S67>/Derivative' */
  if ((M220Model_v2_DW.TimeStampA_g >= M220Model_v2_M->Timing.t[0]) &&
      (M220Model_v2_DW.TimeStampB_ku >= M220Model_v2_M->Timing.t[0])) {
    /* Derivative: '<S67>/Derivative' */
    M220Model_v2_B.Derivative_n = 0.0;
  } else {
    u0 = M220Model_v2_DW.TimeStampA_g;
    lastU = &M220Model_v2_DW.LastUAtTimeA_ju;
    if (M220Model_v2_DW.TimeStampA_g < M220Model_v2_DW.TimeStampB_ku) {
      if (M220Model_v2_DW.TimeStampB_ku < M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_ku;
        lastU = &M220Model_v2_DW.LastUAtTimeB_m;
      }
    } else {
      if (M220Model_v2_DW.TimeStampA_g >= M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_ku;
        lastU = &M220Model_v2_DW.LastUAtTimeB_m;
      }
    }

    u0 = M220Model_v2_M->Timing.t[0] - u0;

    /* Derivative: '<S67>/Derivative' */
    M220Model_v2_B.Derivative_n = (M220Model_v2_B.Product1_l - *lastU) / u0;
  }

  /* End of Derivative: '<S67>/Derivative' */

  /* Product: '<S67>/Product2' */
  M220Model_v2_B.Product2_go = M220Model_v2_ConstB.Divide *
    M220Model_v2_ConstB.Product1_pa * M220Model_v2_B.Derivative_n;

  /* Product: '<S67>/Divide3' */
  M220Model_v2_B.Divide3_f = 1.0 / M220Model_v2_ConstB.Add_m *
    M220Model_v2_B.Product2_go;

  /* Derivative: '<S67>/Derivative1' */
  if ((M220Model_v2_DW.TimeStampA_i >= M220Model_v2_M->Timing.t[0]) &&
      (M220Model_v2_DW.TimeStampB_j >= M220Model_v2_M->Timing.t[0])) {
    /* Derivative: '<S67>/Derivative1' */
    M220Model_v2_B.Derivative1_g = 0.0;
  } else {
    u0 = M220Model_v2_DW.TimeStampA_i;
    lastU = &M220Model_v2_DW.LastUAtTimeA_b;
    if (M220Model_v2_DW.TimeStampA_i < M220Model_v2_DW.TimeStampB_j) {
      if (M220Model_v2_DW.TimeStampB_j < M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_j;
        lastU = &M220Model_v2_DW.LastUAtTimeB_hn;
      }
    } else {
      if (M220Model_v2_DW.TimeStampA_i >= M220Model_v2_M->Timing.t[0]) {
        u0 = M220Model_v2_DW.TimeStampB_j;
        lastU = &M220Model_v2_DW.LastUAtTimeB_hn;
      }
    }

    u0 = M220Model_v2_M->Timing.t[0] - u0;

    /* Derivative: '<S67>/Derivative1' */
    M220Model_v2_B.Derivative1_g = (M220Model_v2_B.Product_c - *lastU) / u0;
  }

  /* End of Derivative: '<S67>/Derivative1' */

  /* Product: '<S67>/Product5' */
  M220Model_v2_B.Product5_o = M220Model_v2_ConstB.Product1_pa *
    M220Model_v2_ConstB.Divide * M220Model_v2_ConstB.Product1_lj *
    M220Model_v2_B.Derivative1_g;

  /* Product: '<S67>/Divide1' */
  M220Model_v2_B.Divide1_b = M220Model_v2_B.Product5_o /
    M220Model_v2_ConstB.Product4_j;

  /* Product: '<S67>/Product3' incorporates:
   *  Constant: '<Root>/DampConstSusp4_Cs'
   *  Constant: '<Root>/e_const_in'
   */
  M220Model_v2_B.Product3_d = 3.0E-17 * M220Model_v2_B.Derivative_o;

  /* Sum: '<S67>/Add1' */
  M220Model_v2_B.Add1_c = (((M220Model_v2_ConstB.Divide_j -
    M220Model_v2_B.Divide3_f) + M220Model_v2_B.Divide1_b) +
    M220Model_v2_B.Product3_d) + M220Model_v2_ConstB.Product6_p;

  /* Product: '<S64>/Product1' incorporates:
   *  Constant: '<Root>/GroundFrictionCoeff_mu'
   */
  M220Model_v2_B.Product1_jy = M220Model_v2_B.Add_km * M220Model_v2_B.Add1_c *
    0.6;

  /* Product: '<S64>/Product3' incorporates:
   *  Constant: '<Root>/LongCornerStiffTire4_Ca'
   */
  M220Model_v2_B.Product3_a = M220Model_v2_B.Add1_c * 15000.0;

  /* Math: '<S64>/Square' */
  M220Model_v2_B.Square_m = M220Model_v2_B.Product3_a *
    M220Model_v2_B.Product3_a;

  /* Trigonometry: '<S64>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_kf = std::tan(M220Model_v2_B.Subtract2_p);

  /* Product: '<S64>/Product4' incorporates:
   *  Constant: '<Root>/LatCornerStiffTire4_Cb'
   */
  M220Model_v2_B.Product4_b = 18000.0 * M220Model_v2_B.TrigonometricFunction_kf;

  /* Math: '<S64>/Square1' */
  M220Model_v2_B.Square1_e = M220Model_v2_B.Product4_b *
    M220Model_v2_B.Product4_b;

  /* Sum: '<S64>/Add1' */
  M220Model_v2_B.Add1_gb = M220Model_v2_B.Square_m + M220Model_v2_B.Square1_e;

  /* Sqrt: '<S64>/Sqrt' */
  M220Model_v2_B.Sqrt_d = std::sqrt(M220Model_v2_B.Add1_gb);

  /* Product: '<S64>/Product2' incorporates:
   *  Constant: '<S64>/Constant1'
   */
  M220Model_v2_B.Product2_o = 2.0 * M220Model_v2_B.Sqrt_d;

  /* Product: '<S64>/Divide' */
  M220Model_v2_B.Divide_h = M220Model_v2_B.Product1_jy /
    M220Model_v2_B.Product2_o;

  /* Switch: '<S28>/Switch' */
  if (M220Model_v2_B.Divide_h >= 1.0) {
    /* Switch: '<S28>/Switch' incorporates:
     *  Constant: '<S28>/Constant2'
     */
    M220Model_v2_B.Switch_p = 1.0;
  } else {
    /* Sum: '<S28>/Subtract' incorporates:
     *  Constant: '<S28>/Constant1'
     */
    M220Model_v2_B.Subtract_c = 2.0 - M220Model_v2_B.Divide_h;

    /* Product: '<S28>/Product3' */
    M220Model_v2_B.Product3_f = M220Model_v2_B.Divide_h *
      M220Model_v2_B.Subtract_c;

    /* Switch: '<S28>/Switch' */
    M220Model_v2_B.Switch_p = M220Model_v2_B.Product3_f;
  }

  /* End of Switch: '<S28>/Switch' */

  /* Product: '<S28>/Product1' incorporates:
   *  Constant: '<Root>/LongCornerStiffTire4_Ca'
   */
  M220Model_v2_B.Product1_fn = M220Model_v2_B.Switch_p * 15000.0 *
    M220Model_v2_B.Divide2_e2;

  /* Product: '<S28>/Product2' incorporates:
   *  Constant: '<Root>/LatCornerStiffTire4_Cb'
   */
  M220Model_v2_B.Product2_oz = M220Model_v2_B.Switch_p *
    M220Model_v2_B.Divide1_ix * 18000.0;

  /* Gain: '<S3>/Gain1' */
  M220Model_v2_B.Gain1_j = 0.017453292519943295 *
    M220Model_v2_B.LookupTableDynamic_h;

  /* Saturate: '<S9>/Steering_Angle_Limits' */
  u0 = M220Model_v2_B.Gain1_j;
  if (u0 > 49.56) {
    /* Saturate: '<S9>/Steering_Angle_Limits' */
    M220Model_v2_B.Steering_Angle_Limits = 49.56;
  } else if (u0 < -34.95) {
    /* Saturate: '<S9>/Steering_Angle_Limits' */
    M220Model_v2_B.Steering_Angle_Limits = -34.95;
  } else {
    /* Saturate: '<S9>/Steering_Angle_Limits' */
    M220Model_v2_B.Steering_Angle_Limits = u0;
  }

  /* End of Saturate: '<S9>/Steering_Angle_Limits' */

  /* Gain: '<S78>/Gain1' */
  M220Model_v2_B.Gain1_fo = 0.017453292519943295 *
    M220Model_v2_B.Steering_Angle_Limits;

  /* Trigonometry: '<S9>/Trigonometric Function1' */
  M220Model_v2_B.TrigonometricFunction1_k = std::cos(M220Model_v2_B.Gain1_fo);

  /* Sum: '<S9>/Add3' */
  M220Model_v2_B.Add3_cg = M220Model_v2_B.Product2_ng +
    M220Model_v2_B.Product2_oz;

  /* Product: '<S9>/Product6' */
  M220Model_v2_B.Product6 = M220Model_v2_B.TrigonometricFunction1_k *
    M220Model_v2_B.Add3_cg;

  /* Sum: '<S9>/Add4' */
  M220Model_v2_B.Add4 = M220Model_v2_B.Product1_p + M220Model_v2_B.Product1_fn;

  /* Trigonometry: '<S9>/Trigonometric Function2' */
  M220Model_v2_B.TrigonometricFunction2_f = std::sin(M220Model_v2_B.Gain1_fo);

  /* Product: '<S9>/Product7' */
  M220Model_v2_B.Product7 = M220Model_v2_B.Add4 *
    M220Model_v2_B.TrigonometricFunction2_f;

  /* Sum: '<S9>/Add10' */
  M220Model_v2_B.Add10 = ((M220Model_v2_B.Product6 + M220Model_v2_B.Product2_h)
    + M220Model_v2_B.Product2_f) + M220Model_v2_B.Product7;

  /* Product: '<S9>/Product2' */
  M220Model_v2_B.Product2_cz = M220Model_v2_ConstB.Divide1 *
    M220Model_v2_B.Add10;
  if (rtmIsMajorTimeStep(M220Model_v2_M)) {
    /* Delay: '<S9>/Delay' */
    M220Model_v2_B.Delay = M220Model_v2_DW.Delay_DSTATE;

    /* Gain: '<S9>/Gain' */
    M220Model_v2_B.Gain_a = -M220Model_v2_B.Delay;

    /* Delay: '<S9>/Delay2' */
    M220Model_v2_B.Delay2 = M220Model_v2_DW.Delay2_DSTATE;

    /* Product: '<S9>/Product1' */
    M220Model_v2_B.Product1_jb = M220Model_v2_B.Gain_a * M220Model_v2_B.Delay2;

    /* Delay: '<S9>/Delay4' */
    M220Model_v2_B.Delay4 = M220Model_v2_DW.Delay4_DSTATE;

    /* Product: '<S9>/Product19' */
    M220Model_v2_B.Product19 = M220Model_v2_B.Delay * M220Model_v2_B.Delay4;

    /* Delay: '<S9>/Delay1' */
    M220Model_v2_B.Delay1 = M220Model_v2_DW.Delay1_DSTATE;

    /* Delay: '<S9>/Delay3' */
    M220Model_v2_B.Delay3 = M220Model_v2_DW.Delay3_DSTATE;

    /* Product: '<S9>/Product20' */
    M220Model_v2_B.Product20 = M220Model_v2_B.Delay1 * M220Model_v2_B.Delay3;

    /* Sum: '<S9>/Add16' */
    M220Model_v2_B.Add16 = ((M220Model_v2_B.Product19 - M220Model_v2_B.Product20)
      + M220Model_v2_ConstB.Product21) - M220Model_v2_ConstB.Product22;
  }

  /* Sum: '<S9>/Add1' */
  M220Model_v2_B.Add1_fr = (M220Model_v2_B.Product2_cz +
    M220Model_v2_ConstB.Product8) + M220Model_v2_B.Product1_jb;

  /* Sum: '<S9>/Add11' */
  M220Model_v2_B.Add11 = M220Model_v2_B.Product7 + M220Model_v2_B.Product6;

  /* Sum: '<S9>/Add12' */
  M220Model_v2_B.Add12 = M220Model_v2_B.Product2_ng - M220Model_v2_B.Product2_oz;

  /* Sum: '<S9>/Add14' */
  M220Model_v2_B.Add14 = M220Model_v2_B.Product1_fn - M220Model_v2_B.Product1_p;

  /* Sum: '<S83>/Add' */
  M220Model_v2_B.Add_i = M220Model_v2_B.Product2_oz + M220Model_v2_B.Product2_ng;

  /* Trigonometry: '<S83>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_az = std::cos(M220Model_v2_B.Gain1_fo);

  /* Product: '<S83>/Product' */
  M220Model_v2_B.Product_nj = M220Model_v2_B.Add_i *
    M220Model_v2_B.TrigonometricFunction_az;

  /* Trigonometry: '<S83>/Trigonometric Function1' */
  M220Model_v2_B.TrigonometricFunction1_i = std::sin(M220Model_v2_B.Gain1_fo);

  /* Sum: '<S83>/Add2' */
  M220Model_v2_B.Add2_l = M220Model_v2_B.Product1_p + M220Model_v2_B.Product1_fn;

  /* Product: '<S83>/Product1' */
  M220Model_v2_B.Product1_j1 = M220Model_v2_B.TrigonometricFunction1_i *
    M220Model_v2_B.Add2_l;

  /* Sum: '<S83>/Add1' */
  M220Model_v2_B.Add1_l = ((M220Model_v2_B.Product_nj +
    M220Model_v2_B.Product2_f) + M220Model_v2_B.Product2_h) -
    M220Model_v2_B.Product1_j1;

  /* Product: '<S9>/Product27' */
  M220Model_v2_B.Product27 = M220Model_v2_ConstB.Product1_l4 *
    M220Model_v2_B.Add1_l;
  if (rtmIsMajorTimeStep(M220Model_v2_M)) {
    /* Product: '<S9>/Product28' */
    M220Model_v2_B.Product28 = M220Model_v2_ConstB.Add18 * M220Model_v2_B.Delay2
      * M220Model_v2_B.Delay4;

    /* Product: '<S9>/Product35' */
    M220Model_v2_B.Product35 = M220Model_v2_ConstB.Add22 * M220Model_v2_B.Delay3
      * M220Model_v2_B.Delay2;
  }

  /* Sum: '<S9>/Add17' */
  M220Model_v2_B.Add17 = (M220Model_v2_ConstB.Add15 + M220Model_v2_B.Product27)
    + M220Model_v2_B.Product28;

  /* Product: '<S9>/Product9' */
  M220Model_v2_B.Product9 = M220Model_v2_B.TrigonometricFunction2_f *
    M220Model_v2_B.Add12;

  /* Product: '<S9>/Product10' */
  M220Model_v2_B.Product10 = M220Model_v2_B.TrigonometricFunction1_k *
    M220Model_v2_B.Add14;

  /* Sum: '<S9>/Add2' */
  M220Model_v2_B.Add2_j = M220Model_v2_B.Product9 + M220Model_v2_B.Product10;

  /* Sum: '<S82>/Add' */
  M220Model_v2_B.Add_o = M220Model_v2_B.Product1_fn + M220Model_v2_B.Product1_p;

  /* Trigonometry: '<S82>/Trigonometric Function' */
  M220Model_v2_B.TrigonometricFunction_b = std::cos(M220Model_v2_B.Gain1_fo);

  /* Product: '<S82>/Product' */
  M220Model_v2_B.Product_j = M220Model_v2_B.Add_o *
    M220Model_v2_B.TrigonometricFunction_b;

  /* Trigonometry: '<S82>/Trigonometric Function1' */
  M220Model_v2_B.TrigonometricFunction1_d = std::sin(M220Model_v2_B.Gain1_fo);

  /* Sum: '<S82>/Add2' */
  M220Model_v2_B.Add2_f = M220Model_v2_B.Product2_ng +
    M220Model_v2_B.Product2_oz;

  /* Product: '<S82>/Product1' */
  M220Model_v2_B.Product1_h = M220Model_v2_B.TrigonometricFunction1_d *
    M220Model_v2_B.Add2_f;

  /* Sum: '<S82>/Add1' */
  M220Model_v2_B.Add1_j = ((M220Model_v2_B.Product_j +
    M220Model_v2_B.Product1_oo) + M220Model_v2_B.Product1_ob) +
    M220Model_v2_B.Product1_h;

  /* Product: '<S9>/Product31' */
  M220Model_v2_B.Product31 = M220Model_v2_ConstB.Product1_l4 *
    M220Model_v2_B.Add1_j;

  /* Sum: '<S9>/Add20' */
  M220Model_v2_B.Add20 = ((M220Model_v2_B.Product35 +
    M220Model_v2_ConstB.Product32) - M220Model_v2_ConstB.Product33) +
    M220Model_v2_B.Product31;

  /* Product: '<S9>/Product14' */
  M220Model_v2_B.Product14 = M220Model_v2_B.TrigonometricFunction1_k *
    M220Model_v2_B.Add4;

  /* Product: '<S9>/Product15' */
  M220Model_v2_B.Product15 = M220Model_v2_B.Add3_cg *
    M220Model_v2_B.TrigonometricFunction2_f;
  if (rtmIsMajorTimeStep(M220Model_v2_M)) {
    /* Math: '<S9>/Magnitude Squared' */
    M220Model_v2_B.MagnitudeSquared = M220Model_v2_B.Delay *
      M220Model_v2_B.Delay;

    /* Signum: '<S9>/Sign' */
    u0 = M220Model_v2_B.Delay;
    if (u0 < 0.0) {
      /* Signum: '<S9>/Sign' */
      M220Model_v2_B.Sign = -1.0;
    } else if (u0 > 0.0) {
      /* Signum: '<S9>/Sign' */
      M220Model_v2_B.Sign = 1.0;
    } else if (u0 == 0.0) {
      /* Signum: '<S9>/Sign' */
      M220Model_v2_B.Sign = 0.0;
    } else {
      /* Signum: '<S9>/Sign' */
      M220Model_v2_B.Sign = (rtNaN);
    }

    /* End of Signum: '<S9>/Sign' */

    /* Product: '<S9>/Product16' incorporates:
     *  Constant: '<Root>/AP'
     */
    M220Model_v2_B.Product16 = 0.51 * M220Model_v2_B.MagnitudeSquared *
      M220Model_v2_B.Sign;

    /* Product: '<S9>/Product11' */
    M220Model_v2_B.Product11 = M220Model_v2_B.Delay1 * M220Model_v2_B.Delay2;
  }

  /* Sum: '<S9>/Add5' */
  M220Model_v2_B.Add5 = (((M220Model_v2_B.Product14 + M220Model_v2_B.Product1_ob)
    + M220Model_v2_B.Product1_oo) - M220Model_v2_B.Product15) -
    M220Model_v2_B.Product16;

  /* Product: '<S9>/Product12' */
  M220Model_v2_B.Product12 = M220Model_v2_ConstB.Divide3 * M220Model_v2_B.Add5;

  /* Sum: '<S9>/Add6' */
  M220Model_v2_B.Add6 = (M220Model_v2_B.Product12 +
    M220Model_v2_ConstB.Product13) + M220Model_v2_B.Product11;

  /* Sum: '<S9>/Add7' */
  M220Model_v2_B.Add7 = M220Model_v2_B.Product2_h + M220Model_v2_B.Product2_f;

  /* Product: '<S9>/Product3' */
  M220Model_v2_B.Product3_h = M220Model_v2_B.Add7 * M220Model_v2_ConstB.Gain1_b;

  /* Sum: '<S9>/Add9' */
  M220Model_v2_B.Add9 = M220Model_v2_B.Product1_ob - M220Model_v2_B.Product1_oo;

  /* Product: '<S9>/Product4' incorporates:
   *  Constant: '<Root>/C'
   */
  M220Model_v2_B.Product4_m = M220Model_v2_B.Add9 * 33.9805;

  /* Product: '<S9>/Product5' incorporates:
   *  Constant: '<Root>/A'
   */
  M220Model_v2_B.Product5_d = M220Model_v2_B.Add11 * 65.1875;

  /* Product: '<S9>/Product18' incorporates:
   *  Constant: '<Root>/C'
   */
  M220Model_v2_B.Product18 = 33.9805 * M220Model_v2_B.Add2_j;

  /* Sum: '<S9>/Add8' */
  M220Model_v2_B.Add8 = ((M220Model_v2_B.Product3_h + M220Model_v2_B.Product4_m)
    + M220Model_v2_B.Product5_d) + M220Model_v2_B.Product18;

  /* Integrator: '<S9>/Integrator' */
  M220Model_v2_B.Integrator_j = M220Model_v2_X.Integrator_CSTATE_c;

  /* Integrator: '<S9>/Integrator10' */
  M220Model_v2_B.Integrator10 = M220Model_v2_X.Integrator10_CSTATE;

  /* Integrator: '<S9>/Integrator3' */
  M220Model_v2_B.Integrator3 = M220Model_v2_X.Integrator3_CSTATE;

  /* Integrator: '<S9>/Integrator6' */
  M220Model_v2_B.Integrator6 = M220Model_v2_X.Integrator6_CSTATE;

  /* Integrator: '<S9>/Integrator7' */
  M220Model_v2_B.Integrator7 = M220Model_v2_X.Integrator7_CSTATE;

  /* Integrator: '<S9>/Integrator8' */
  M220Model_v2_B.Integrator8 = M220Model_v2_X.Integrator8_CSTATE;

  /* Integrator: '<S9>/Integrator9' */
  M220Model_v2_B.Integrator9 = M220Model_v2_X.Integrator9_CSTATE;

  /* Product: '<S9>/Product17' */
  M220Model_v2_B.Product17 = M220Model_v2_B.Add8 * M220Model_v2_ConstB.Divide2;

  /* Product: '<S9>/Product29' */
  M220Model_v2_B.Product29 = M220Model_v2_B.Add17 * M220Model_v2_ConstB.Divide9;

  /* Product: '<S9>/Product30' */
  M220Model_v2_B.Product30 = M220Model_v2_ConstB.Divide10 * M220Model_v2_B.Add20;

  /* Gain: '<S85>/Gain' */
  M220Model_v2_B.Gain_d = 57.295779513082323 * M220Model_v2_B.Integrator7;

  /* Gain: '<S86>/Gain' */
  M220Model_v2_B.Gain_l = 57.295779513082323 * M220Model_v2_B.Integrator8;
  if (rtmIsMajorTimeStep(M220Model_v2_M)) {
    /* RelationalOperator: '<S115>/Compare' incorporates:
     *  Constant: '<S115>/Constant'
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_m = (M220Model_v2_Y.FrontDeckAngleSns_deg <= 90.0);

    /* RelationalOperator: '<S93>/Compare' incorporates:
     *  Constant: '<S93>/Constant'
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_ib = (M220Model_v2_Y.FrontDeckAngleSns_deg > 45.0);

    /* Logic: '<S13>/Logical Operator4' */
    M220Model_v2_B.LogicalOperator4_i = (M220Model_v2_B.Compare_m &&
      M220Model_v2_B.Compare_ib);

    /* Logic: '<S13>/Logical Operator8' incorporates:
     *  Inport: '<Root>/hv_Dk_Ft_Lift_bool'
     */
    M220Model_v2_B.LogicalOperator8 = !M220Model_v2_U.hv_Dk_Ft_Lift_bool;

    /* Logic: '<S13>/Logical Operator' incorporates:
     *  Inport: '<Root>/RlyEStarters_bool'
     *  Inport: '<Root>/hv_Dk_Ft_Low_bool'
     *  Inport: '<Root>/hv_Dk_Unlatch_bool'
     */
    M220Model_v2_B.LogicalOperator_b = (M220Model_v2_U.RlyEStarters_bool &&
      M220Model_v2_B.LogicalOperator4_i && M220Model_v2_U.hv_Dk_Unlatch_bool &&
      M220Model_v2_U.hv_Dk_Ft_Low_bool && M220Model_v2_B.LogicalOperator8);

    /* Switch: '<S13>/Switch' incorporates:
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     */
    M220Model_v2_B.Switch_ak = M220Model_v2_Y.FrontDeckAngleSns_deg;

    /* Sum: '<S13>/Add' incorporates:
     *  Constant: '<S13>/Constant'
     */
    M220Model_v2_B.Add_l = M220Model_v2_B.Switch_ak - 1.0;

    /* RelationalOperator: '<S116>/Compare' incorporates:
     *  Constant: '<S116>/Constant'
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_ie = (M220Model_v2_Y.FrontDeckAngleSns_deg >= -1.0);

    /* RelationalOperator: '<S119>/Compare' incorporates:
     *  Constant: '<S119>/Constant'
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_ii = (M220Model_v2_Y.FrontDeckAngleSns_deg < 45.0);

    /* Logic: '<S13>/Logical Operator5' */
    M220Model_v2_B.LogicalOperator5 = (M220Model_v2_B.Compare_ie &&
      M220Model_v2_B.Compare_ii);

    /* Logic: '<S13>/Logical Operator9' incorporates:
     *  Inport: '<Root>/hv_Dk_Ft_Low_bool'
     */
    M220Model_v2_B.LogicalOperator9 = !M220Model_v2_U.hv_Dk_Ft_Low_bool;

    /* Logic: '<S13>/Logical Operator1' incorporates:
     *  Inport: '<Root>/RlyEStarters_bool'
     *  Inport: '<Root>/hv_Dk_Ft_Lift_bool'
     *  Inport: '<Root>/hv_Dk_Unlatch_bool'
     */
    M220Model_v2_B.LogicalOperator1_m = (M220Model_v2_U.hv_Dk_Unlatch_bool &&
      M220Model_v2_U.RlyEStarters_bool && M220Model_v2_U.hv_Dk_Ft_Lift_bool &&
      M220Model_v2_B.LogicalOperator5 && M220Model_v2_B.LogicalOperator9);

    /* Switch: '<S13>/Switch2' incorporates:
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     */
    M220Model_v2_B.Switch2_l = M220Model_v2_Y.FrontDeckAngleSns_deg;

    /* Sum: '<S13>/Add1' incorporates:
     *  Constant: '<S13>/Constant1'
     */
    M220Model_v2_B.Add1_hd = M220Model_v2_B.Switch2_l + 1.0;

    /* Logic: '<S13>/Logical Operator28' incorporates:
     *  Inport: '<Root>/hv_Dk_Rt_Lift_bool'
     */
    M220Model_v2_B.LogicalOperator28 = !M220Model_v2_U.hv_Dk_Rt_Lift_bool;

    /* RelationalOperator: '<S106>/Compare' incorporates:
     *  Constant: '<S106>/Constant'
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_k = (M220Model_v2_Y.RightDeckAngleSns_deg <= 45.0);

    /* RelationalOperator: '<S107>/Compare' incorporates:
     *  Constant: '<S107>/Constant'
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_l = (M220Model_v2_Y.RightDeckAngleSns_deg >= 0.0);

    /* Logic: '<S13>/Logical Operator22' */
    M220Model_v2_B.LogicalOperator22 = (M220Model_v2_B.Compare_k &&
      M220Model_v2_B.Compare_l);

    /* Logic: '<S13>/Logical Operator26' incorporates:
     *  Inport: '<Root>/RlyEStarters_bool'
     *  Inport: '<Root>/hv_Dk_Rt_Low_bool'
     *  Inport: '<Root>/hv_Dk_Unlatch_bool'
     */
    M220Model_v2_B.LogicalOperator26 = (M220Model_v2_B.LogicalOperator28 &&
      M220Model_v2_U.RlyEStarters_bool && M220Model_v2_B.LogicalOperator22 &&
      M220Model_v2_U.hv_Dk_Unlatch_bool && M220Model_v2_U.hv_Dk_Rt_Low_bool);

    /* Switch: '<S13>/Switch17' incorporates:
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     */
    M220Model_v2_B.Switch17 = M220Model_v2_Y.RightDeckAngleSns_deg;

    /* Sum: '<S13>/Add10' incorporates:
     *  Constant: '<S13>/Constant20'
     */
    M220Model_v2_B.Add10_e = M220Model_v2_B.Switch17 - 1.0;

    /* Logic: '<S13>/Logical Operator29' incorporates:
     *  Inport: '<Root>/hv_Dk_Rt_Low_bool'
     */
    M220Model_v2_B.LogicalOperator29 = !M220Model_v2_U.hv_Dk_Rt_Low_bool;

    /* RelationalOperator: '<S110>/Compare' incorporates:
     *  Constant: '<S110>/Constant'
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_h = (M220Model_v2_Y.RightDeckAngleSns_deg <= 90.0);

    /* RelationalOperator: '<S111>/Compare' incorporates:
     *  Constant: '<S111>/Constant'
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_b = (M220Model_v2_Y.RightDeckAngleSns_deg >= 45.0);

    /* Logic: '<S13>/Logical Operator23' */
    M220Model_v2_B.LogicalOperator23 = (M220Model_v2_B.Compare_h &&
      M220Model_v2_B.Compare_b);

    /* Logic: '<S13>/Logical Operator27' incorporates:
     *  Inport: '<Root>/RlyEStarters_bool'
     *  Inport: '<Root>/hv_Dk_Rt_Lift_bool'
     *  Inport: '<Root>/hv_Dk_Unlatch_bool'
     */
    M220Model_v2_B.LogicalOperator27 = (M220Model_v2_U.RlyEStarters_bool &&
      M220Model_v2_U.hv_Dk_Rt_Lift_bool && M220Model_v2_U.hv_Dk_Unlatch_bool &&
      M220Model_v2_B.LogicalOperator29 && M220Model_v2_B.LogicalOperator23);

    /* Switch: '<S13>/Switch19' incorporates:
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     */
    M220Model_v2_B.Switch19 = M220Model_v2_Y.RightDeckAngleSns_deg;

    /* Sum: '<S13>/Add11' incorporates:
     *  Constant: '<S13>/Constant21'
     */
    M220Model_v2_B.Add11_a = M220Model_v2_B.Switch19 + 1.0;

    /* RelationalOperator: '<S113>/Compare' incorporates:
     *  Constant: '<S113>/Constant'
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_hd = (M220Model_v2_Y.FrontDeckAngleSns_deg <= 45.0);

    /* RelationalOperator: '<S114>/Compare' incorporates:
     *  Constant: '<S114>/Constant'
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_kv = (M220Model_v2_Y.FrontDeckAngleSns_deg >= 0.0);

    /* Logic: '<S13>/Logical Operator2' */
    M220Model_v2_B.LogicalOperator2 = (M220Model_v2_B.Compare_hd &&
      M220Model_v2_B.Compare_kv);

    /* Logic: '<S13>/Logical Operator6' incorporates:
     *  Inport: '<Root>/RlyEStarters_bool'
     *  Inport: '<Root>/hv_Dk_Ft_Low_bool'
     *  Inport: '<Root>/hv_Dk_Unlatch_bool'
     */
    M220Model_v2_B.LogicalOperator6 = (M220Model_v2_B.LogicalOperator8 &&
      M220Model_v2_U.RlyEStarters_bool && M220Model_v2_B.LogicalOperator2 &&
      M220Model_v2_U.hv_Dk_Unlatch_bool && M220Model_v2_U.hv_Dk_Ft_Low_bool);

    /* Switch: '<S13>/Switch1' incorporates:
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     */
    M220Model_v2_B.Switch1_b = M220Model_v2_Y.FrontDeckAngleSns_deg;

    /* Sum: '<S13>/Add2' incorporates:
     *  Constant: '<S13>/Constant2'
     */
    M220Model_v2_B.Add2_c = M220Model_v2_B.Switch1_b - 1.0;

    /* RelationalOperator: '<S117>/Compare' incorporates:
     *  Constant: '<S117>/Constant'
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_hr = (M220Model_v2_Y.FrontDeckAngleSns_deg <= 90.0);

    /* RelationalOperator: '<S118>/Compare' incorporates:
     *  Constant: '<S118>/Constant'
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_n2 = (M220Model_v2_Y.FrontDeckAngleSns_deg >= 45.0);

    /* Logic: '<S13>/Logical Operator3' */
    M220Model_v2_B.LogicalOperator3 = (M220Model_v2_B.Compare_hr &&
      M220Model_v2_B.Compare_n2);

    /* Logic: '<S13>/Logical Operator7' incorporates:
     *  Inport: '<Root>/RlyEStarters_bool'
     *  Inport: '<Root>/hv_Dk_Ft_Lift_bool'
     *  Inport: '<Root>/hv_Dk_Unlatch_bool'
     */
    M220Model_v2_B.LogicalOperator7 = (M220Model_v2_U.RlyEStarters_bool &&
      M220Model_v2_U.hv_Dk_Ft_Lift_bool && M220Model_v2_U.hv_Dk_Unlatch_bool &&
      M220Model_v2_B.LogicalOperator9 && M220Model_v2_B.LogicalOperator3);

    /* Switch: '<S13>/Switch3' incorporates:
     *  Outport: '<Root>/FrontDeckAngleSns_deg'
     */
    M220Model_v2_B.Switch3 = M220Model_v2_Y.FrontDeckAngleSns_deg;

    /* Sum: '<S13>/Add3' incorporates:
     *  Constant: '<S13>/Constant3'
     */
    M220Model_v2_B.Add3_n = M220Model_v2_B.Switch3 + 1.0;

    /* RelationalOperator: '<S98>/Compare' incorporates:
     *  Constant: '<S98>/Constant'
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_md = (M220Model_v2_Y.LeftDeckAngleSns_deg <= 90.0);

    /* RelationalOperator: '<S94>/Compare' incorporates:
     *  Constant: '<S94>/Constant'
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_mr = (M220Model_v2_Y.LeftDeckAngleSns_deg > 45.0);

    /* Logic: '<S13>/Logical Operator14' */
    M220Model_v2_B.LogicalOperator14 = (M220Model_v2_B.Compare_md &&
      M220Model_v2_B.Compare_mr);

    /* Logic: '<S13>/Logical Operator18' incorporates:
     *  Inport: '<Root>/hv_Dk_Lf_Lift_bool'
     */
    M220Model_v2_B.LogicalOperator18 = !M220Model_v2_U.hv_Dk_Lf_Lift_bool;

    /* Logic: '<S13>/Logical Operator10' incorporates:
     *  Inport: '<Root>/RlyEStarters_bool'
     *  Inport: '<Root>/hv_Dk_Lf_Low_bool'
     *  Inport: '<Root>/hv_Dk_Unlatch_bool'
     */
    M220Model_v2_B.LogicalOperator10 = (M220Model_v2_U.RlyEStarters_bool &&
      M220Model_v2_B.LogicalOperator14 && M220Model_v2_U.hv_Dk_Unlatch_bool &&
      M220Model_v2_U.hv_Dk_Lf_Low_bool && M220Model_v2_B.LogicalOperator18);

    /* Switch: '<S13>/Switch8' incorporates:
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     */
    M220Model_v2_B.Switch8 = M220Model_v2_Y.LeftDeckAngleSns_deg;

    /* Sum: '<S13>/Add4' incorporates:
     *  Constant: '<S13>/Constant9'
     */
    M220Model_v2_B.Add4_h = M220Model_v2_B.Switch8 - 1.0;

    /* RelationalOperator: '<S99>/Compare' incorporates:
     *  Constant: '<S99>/Constant'
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_e = (M220Model_v2_Y.LeftDeckAngleSns_deg >= -1.0);

    /* RelationalOperator: '<S102>/Compare' incorporates:
     *  Constant: '<S102>/Constant'
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_ei = (M220Model_v2_Y.LeftDeckAngleSns_deg < 45.0);

    /* Logic: '<S13>/Logical Operator15' */
    M220Model_v2_B.LogicalOperator15 = (M220Model_v2_B.Compare_e &&
      M220Model_v2_B.Compare_ei);

    /* Logic: '<S13>/Logical Operator19' incorporates:
     *  Inport: '<Root>/hv_Dk_Lf_Low_bool'
     */
    M220Model_v2_B.LogicalOperator19 = !M220Model_v2_U.hv_Dk_Lf_Low_bool;

    /* Logic: '<S13>/Logical Operator11' incorporates:
     *  Inport: '<Root>/RlyEStarters_bool'
     *  Inport: '<Root>/hv_Dk_Lf_Lift_bool'
     *  Inport: '<Root>/hv_Dk_Unlatch_bool'
     */
    M220Model_v2_B.LogicalOperator11 = (M220Model_v2_U.hv_Dk_Unlatch_bool &&
      M220Model_v2_U.RlyEStarters_bool && M220Model_v2_U.hv_Dk_Lf_Lift_bool &&
      M220Model_v2_B.LogicalOperator15 && M220Model_v2_B.LogicalOperator19);

    /* Switch: '<S13>/Switch10' incorporates:
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     */
    M220Model_v2_B.Switch10 = M220Model_v2_Y.LeftDeckAngleSns_deg;

    /* Sum: '<S13>/Add5' incorporates:
     *  Constant: '<S13>/Constant10'
     */
    M220Model_v2_B.Add5_h = M220Model_v2_B.Switch10 + 1.0;

    /* RelationalOperator: '<S96>/Compare' incorporates:
     *  Constant: '<S96>/Constant'
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_a = (M220Model_v2_Y.LeftDeckAngleSns_deg <= 45.0);

    /* RelationalOperator: '<S97>/Compare' incorporates:
     *  Constant: '<S97>/Constant'
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_mg = (M220Model_v2_Y.LeftDeckAngleSns_deg >= 0.0);

    /* Logic: '<S13>/Logical Operator12' */
    M220Model_v2_B.LogicalOperator12 = (M220Model_v2_B.Compare_a &&
      M220Model_v2_B.Compare_mg);

    /* Logic: '<S13>/Logical Operator16' incorporates:
     *  Inport: '<Root>/RlyEStarters_bool'
     *  Inport: '<Root>/hv_Dk_Lf_Low_bool'
     *  Inport: '<Root>/hv_Dk_Unlatch_bool'
     */
    M220Model_v2_B.LogicalOperator16 = (M220Model_v2_B.LogicalOperator18 &&
      M220Model_v2_U.RlyEStarters_bool && M220Model_v2_B.LogicalOperator12 &&
      M220Model_v2_U.hv_Dk_Unlatch_bool && M220Model_v2_U.hv_Dk_Lf_Low_bool);

    /* Switch: '<S13>/Switch9' incorporates:
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     */
    M220Model_v2_B.Switch9 = M220Model_v2_Y.LeftDeckAngleSns_deg;

    /* Sum: '<S13>/Add6' incorporates:
     *  Constant: '<S13>/Constant11'
     */
    M220Model_v2_B.Add6_c = M220Model_v2_B.Switch9 - 1.0;

    /* RelationalOperator: '<S100>/Compare' incorporates:
     *  Constant: '<S100>/Constant'
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_p = (M220Model_v2_Y.LeftDeckAngleSns_deg <= 90.0);

    /* RelationalOperator: '<S101>/Compare' incorporates:
     *  Constant: '<S101>/Constant'
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_ls = (M220Model_v2_Y.LeftDeckAngleSns_deg >= 45.0);

    /* Logic: '<S13>/Logical Operator13' */
    M220Model_v2_B.LogicalOperator13 = (M220Model_v2_B.Compare_p &&
      M220Model_v2_B.Compare_ls);

    /* Logic: '<S13>/Logical Operator17' incorporates:
     *  Inport: '<Root>/RlyEStarters_bool'
     *  Inport: '<Root>/hv_Dk_Lf_Lift_bool'
     *  Inport: '<Root>/hv_Dk_Unlatch_bool'
     */
    M220Model_v2_B.LogicalOperator17 = (M220Model_v2_U.RlyEStarters_bool &&
      M220Model_v2_U.hv_Dk_Lf_Lift_bool && M220Model_v2_U.hv_Dk_Unlatch_bool &&
      M220Model_v2_B.LogicalOperator19 && M220Model_v2_B.LogicalOperator13);

    /* Switch: '<S13>/Switch11' incorporates:
     *  Outport: '<Root>/LeftDeckAngleSns_deg'
     */
    M220Model_v2_B.Switch11 = M220Model_v2_Y.LeftDeckAngleSns_deg;

    /* Sum: '<S13>/Add7' incorporates:
     *  Constant: '<S13>/Constant12'
     */
    M220Model_v2_B.Add7_f = M220Model_v2_B.Switch11 + 1.0;

    /* RelationalOperator: '<S108>/Compare' incorporates:
     *  Constant: '<S108>/Constant'
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_bv = (M220Model_v2_Y.RightDeckAngleSns_deg <= 90.0);

    /* RelationalOperator: '<S103>/Compare' incorporates:
     *  Constant: '<S103>/Constant'
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_d = (M220Model_v2_Y.RightDeckAngleSns_deg > 45.0);

    /* Logic: '<S13>/Logical Operator24' */
    M220Model_v2_B.LogicalOperator24 = (M220Model_v2_B.Compare_bv &&
      M220Model_v2_B.Compare_d);

    /* Logic: '<S13>/Logical Operator20' incorporates:
     *  Inport: '<Root>/RlyEStarters_bool'
     *  Inport: '<Root>/hv_Dk_Rt_Low_bool'
     *  Inport: '<Root>/hv_Dk_Unlatch_bool'
     */
    M220Model_v2_B.LogicalOperator20 = (M220Model_v2_U.RlyEStarters_bool &&
      M220Model_v2_B.LogicalOperator24 && M220Model_v2_U.hv_Dk_Unlatch_bool &&
      M220Model_v2_U.hv_Dk_Rt_Low_bool && M220Model_v2_B.LogicalOperator28);

    /* Switch: '<S13>/Switch16' incorporates:
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     */
    M220Model_v2_B.Switch16 = M220Model_v2_Y.RightDeckAngleSns_deg;

    /* Sum: '<S13>/Add8' incorporates:
     *  Constant: '<S13>/Constant18'
     */
    M220Model_v2_B.Add8_a = M220Model_v2_B.Switch16 - 1.0;

    /* RelationalOperator: '<S109>/Compare' incorporates:
     *  Constant: '<S109>/Constant'
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_aj = (M220Model_v2_Y.RightDeckAngleSns_deg >= -1.0);

    /* RelationalOperator: '<S112>/Compare' incorporates:
     *  Constant: '<S112>/Constant'
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     */
    M220Model_v2_B.Compare_dg = (M220Model_v2_Y.RightDeckAngleSns_deg < 45.0);

    /* Logic: '<S13>/Logical Operator25' */
    M220Model_v2_B.LogicalOperator25 = (M220Model_v2_B.Compare_aj &&
      M220Model_v2_B.Compare_dg);

    /* Logic: '<S13>/Logical Operator21' incorporates:
     *  Inport: '<Root>/RlyEStarters_bool'
     *  Inport: '<Root>/hv_Dk_Rt_Lift_bool'
     *  Inport: '<Root>/hv_Dk_Unlatch_bool'
     */
    M220Model_v2_B.LogicalOperator21 = (M220Model_v2_U.hv_Dk_Unlatch_bool &&
      M220Model_v2_U.RlyEStarters_bool && M220Model_v2_U.hv_Dk_Rt_Lift_bool &&
      M220Model_v2_B.LogicalOperator25 && M220Model_v2_B.LogicalOperator29);

    /* Switch: '<S13>/Switch18' incorporates:
     *  Outport: '<Root>/RightDeckAngleSns_deg'
     */
    M220Model_v2_B.Switch18 = M220Model_v2_Y.RightDeckAngleSns_deg;

    /* Sum: '<S13>/Add9' incorporates:
     *  Constant: '<S13>/Constant19'
     */
    M220Model_v2_B.Add9_b = M220Model_v2_B.Switch18 + 1.0;

    /* Switch: '<S13>/Switch4' incorporates:
     *  Switch: '<S13>/Switch5'
     *  Switch: '<S13>/Switch6'
     *  Switch: '<S13>/Switch7'
     */
    if (M220Model_v2_B.LogicalOperator_b) {
      /* Switch: '<S13>/Switch4' incorporates:
       *  Constant: '<S13>/Constant4'
       */
      M220Model_v2_B.Switch4 = 1.0;
    } else {
      if (M220Model_v2_B.LogicalOperator6) {
        /* Switch: '<S13>/Switch5' incorporates:
         *  Constant: '<S13>/Constant7'
         */
        M220Model_v2_B.Switch5 = 2.0;
      } else {
        if (M220Model_v2_B.LogicalOperator1_m) {
          /* Switch: '<S13>/Switch6' incorporates:
           *  Constant: '<S13>/Constant5'
           *  Switch: '<S13>/Switch5'
           */
          M220Model_v2_B.Switch6 = 3.0;
        } else {
          if (M220Model_v2_B.LogicalOperator7) {
            /* Switch: '<S13>/Switch7' incorporates:
             *  Constant: '<S13>/Constant6'
             *  Switch: '<S13>/Switch5'
             *  Switch: '<S13>/Switch6'
             */
            M220Model_v2_B.Switch7 = 4.0;
          } else {
            /* Switch: '<S13>/Switch7' incorporates:
             *  Constant: '<S13>/Constant8'
             *  Switch: '<S13>/Switch5'
             *  Switch: '<S13>/Switch6'
             */
            M220Model_v2_B.Switch7 = 5.0;
          }

          /* Switch: '<S13>/Switch6' incorporates:
           *  Switch: '<S13>/Switch5'
           */
          M220Model_v2_B.Switch6 = M220Model_v2_B.Switch7;
        }

        /* Switch: '<S13>/Switch5' incorporates:
         *  Switch: '<S13>/Switch6'
         *  Switch: '<S13>/Switch7'
         */
        M220Model_v2_B.Switch5 = M220Model_v2_B.Switch6;
      }

      /* Switch: '<S13>/Switch4' incorporates:
       *  Switch: '<S13>/Switch5'
       *  Switch: '<S13>/Switch6'
       *  Switch: '<S13>/Switch7'
       */
      M220Model_v2_B.Switch4 = M220Model_v2_B.Switch5;
    }

    /* End of Switch: '<S13>/Switch4' */

    /* RateLimiter: '<S13>/Rate Limiter' */
    u0 = M220Model_v2_B.Add_l - M220Model_v2_DW.PrevY;
    if (u0 > 1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter' */
      M220Model_v2_B.RateLimiter = M220Model_v2_DW.PrevY + 1.0E-5;
    } else if (u0 < -1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter' */
      M220Model_v2_B.RateLimiter = M220Model_v2_DW.PrevY + -1.0E-5;
    } else {
      /* RateLimiter: '<S13>/Rate Limiter' */
      M220Model_v2_B.RateLimiter = M220Model_v2_B.Add_l;
    }

    M220Model_v2_DW.PrevY = M220Model_v2_B.RateLimiter;

    /* End of RateLimiter: '<S13>/Rate Limiter' */

    /* RateLimiter: '<S13>/Rate Limiter2' */
    u0 = M220Model_v2_B.Add2_c - M220Model_v2_DW.PrevY_d;
    if (u0 > 1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter2' */
      M220Model_v2_B.RateLimiter2 = M220Model_v2_DW.PrevY_d + 1.0E-5;
    } else if (u0 < -1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter2' */
      M220Model_v2_B.RateLimiter2 = M220Model_v2_DW.PrevY_d + -1.0E-5;
    } else {
      /* RateLimiter: '<S13>/Rate Limiter2' */
      M220Model_v2_B.RateLimiter2 = M220Model_v2_B.Add2_c;
    }

    M220Model_v2_DW.PrevY_d = M220Model_v2_B.RateLimiter2;

    /* End of RateLimiter: '<S13>/Rate Limiter2' */

    /* RateLimiter: '<S13>/Rate Limiter1' */
    u0 = M220Model_v2_B.Add1_hd - M220Model_v2_DW.PrevY_f;
    if (u0 > 1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter1' */
      M220Model_v2_B.RateLimiter1 = M220Model_v2_DW.PrevY_f + 1.0E-5;
    } else if (u0 < -1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter1' */
      M220Model_v2_B.RateLimiter1 = M220Model_v2_DW.PrevY_f + -1.0E-5;
    } else {
      /* RateLimiter: '<S13>/Rate Limiter1' */
      M220Model_v2_B.RateLimiter1 = M220Model_v2_B.Add1_hd;
    }

    M220Model_v2_DW.PrevY_f = M220Model_v2_B.RateLimiter1;

    /* End of RateLimiter: '<S13>/Rate Limiter1' */

    /* RateLimiter: '<S13>/Rate Limiter3' */
    u0 = M220Model_v2_B.Add3_n - M220Model_v2_DW.PrevY_b;
    if (u0 > 1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter3' */
      M220Model_v2_B.RateLimiter3 = M220Model_v2_DW.PrevY_b + 1.0E-5;
    } else if (u0 < -1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter3' */
      M220Model_v2_B.RateLimiter3 = M220Model_v2_DW.PrevY_b + -1.0E-5;
    } else {
      /* RateLimiter: '<S13>/Rate Limiter3' */
      M220Model_v2_B.RateLimiter3 = M220Model_v2_B.Add3_n;
    }

    M220Model_v2_DW.PrevY_b = M220Model_v2_B.RateLimiter3;

    /* End of RateLimiter: '<S13>/Rate Limiter3' */

    /* MultiPortSwitch: '<S13>/Multiport Switch' */
    switch (static_cast<int32_T>(M220Model_v2_B.Switch4)) {
     case 1:
      /* MultiPortSwitch: '<S13>/Multiport Switch' */
      M220Model_v2_B.MultiportSwitch = M220Model_v2_B.RateLimiter;
      break;

     case 2:
      /* MultiPortSwitch: '<S13>/Multiport Switch' */
      M220Model_v2_B.MultiportSwitch = M220Model_v2_B.RateLimiter2;
      break;

     case 3:
      /* MultiPortSwitch: '<S13>/Multiport Switch' */
      M220Model_v2_B.MultiportSwitch = M220Model_v2_B.RateLimiter1;
      break;

     case 4:
      /* MultiPortSwitch: '<S13>/Multiport Switch' */
      M220Model_v2_B.MultiportSwitch = M220Model_v2_B.RateLimiter3;
      break;

     default:
      /* MultiPortSwitch: '<S13>/Multiport Switch' incorporates:
       *  Outport: '<Root>/FrontDeckAngleSns_deg'
       */
      M220Model_v2_B.MultiportSwitch = M220Model_v2_Y.FrontDeckAngleSns_deg;
      break;
    }

    /* End of MultiPortSwitch: '<S13>/Multiport Switch' */

    /* Switch: '<S13>/Switch12' incorporates:
     *  Switch: '<S13>/Switch13'
     *  Switch: '<S13>/Switch14'
     *  Switch: '<S13>/Switch15'
     */
    if (M220Model_v2_B.LogicalOperator10) {
      /* Switch: '<S13>/Switch12' incorporates:
       *  Constant: '<S13>/Constant13'
       */
      M220Model_v2_B.Switch12 = 1.0;
    } else {
      if (M220Model_v2_B.LogicalOperator16) {
        /* Switch: '<S13>/Switch13' incorporates:
         *  Constant: '<S13>/Constant16'
         */
        M220Model_v2_B.Switch13 = 2.0;
      } else {
        if (M220Model_v2_B.LogicalOperator11) {
          /* Switch: '<S13>/Switch14' incorporates:
           *  Constant: '<S13>/Constant14'
           *  Switch: '<S13>/Switch13'
           */
          M220Model_v2_B.Switch14 = 3.0;
        } else {
          if (M220Model_v2_B.LogicalOperator17) {
            /* Switch: '<S13>/Switch15' incorporates:
             *  Constant: '<S13>/Constant15'
             *  Switch: '<S13>/Switch13'
             *  Switch: '<S13>/Switch14'
             */
            M220Model_v2_B.Switch15 = 4.0;
          } else {
            /* Switch: '<S13>/Switch15' incorporates:
             *  Constant: '<S13>/Constant17'
             *  Switch: '<S13>/Switch13'
             *  Switch: '<S13>/Switch14'
             */
            M220Model_v2_B.Switch15 = 5.0;
          }

          /* Switch: '<S13>/Switch14' incorporates:
           *  Switch: '<S13>/Switch13'
           */
          M220Model_v2_B.Switch14 = M220Model_v2_B.Switch15;
        }

        /* Switch: '<S13>/Switch13' incorporates:
         *  Switch: '<S13>/Switch14'
         *  Switch: '<S13>/Switch15'
         */
        M220Model_v2_B.Switch13 = M220Model_v2_B.Switch14;
      }

      /* Switch: '<S13>/Switch12' incorporates:
       *  Switch: '<S13>/Switch13'
       *  Switch: '<S13>/Switch14'
       *  Switch: '<S13>/Switch15'
       */
      M220Model_v2_B.Switch12 = M220Model_v2_B.Switch13;
    }

    /* End of Switch: '<S13>/Switch12' */

    /* RateLimiter: '<S13>/Rate Limiter4' */
    u0 = M220Model_v2_B.Add4_h - M220Model_v2_DW.PrevY_j;
    if (u0 > 1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter4' */
      M220Model_v2_B.RateLimiter4 = M220Model_v2_DW.PrevY_j + 1.0E-5;
    } else if (u0 < -1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter4' */
      M220Model_v2_B.RateLimiter4 = M220Model_v2_DW.PrevY_j + -1.0E-5;
    } else {
      /* RateLimiter: '<S13>/Rate Limiter4' */
      M220Model_v2_B.RateLimiter4 = M220Model_v2_B.Add4_h;
    }

    M220Model_v2_DW.PrevY_j = M220Model_v2_B.RateLimiter4;

    /* End of RateLimiter: '<S13>/Rate Limiter4' */

    /* RateLimiter: '<S13>/Rate Limiter6' */
    u0 = M220Model_v2_B.Add6_c - M220Model_v2_DW.PrevY_h;
    if (u0 > 1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter6' */
      M220Model_v2_B.RateLimiter6 = M220Model_v2_DW.PrevY_h + 1.0E-5;
    } else if (u0 < -1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter6' */
      M220Model_v2_B.RateLimiter6 = M220Model_v2_DW.PrevY_h + -1.0E-5;
    } else {
      /* RateLimiter: '<S13>/Rate Limiter6' */
      M220Model_v2_B.RateLimiter6 = M220Model_v2_B.Add6_c;
    }

    M220Model_v2_DW.PrevY_h = M220Model_v2_B.RateLimiter6;

    /* End of RateLimiter: '<S13>/Rate Limiter6' */

    /* RateLimiter: '<S13>/Rate Limiter5' */
    u0 = M220Model_v2_B.Add5_h - M220Model_v2_DW.PrevY_fv;
    if (u0 > 1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter5' */
      M220Model_v2_B.RateLimiter5 = M220Model_v2_DW.PrevY_fv + 1.0E-5;
    } else if (u0 < -1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter5' */
      M220Model_v2_B.RateLimiter5 = M220Model_v2_DW.PrevY_fv + -1.0E-5;
    } else {
      /* RateLimiter: '<S13>/Rate Limiter5' */
      M220Model_v2_B.RateLimiter5 = M220Model_v2_B.Add5_h;
    }

    M220Model_v2_DW.PrevY_fv = M220Model_v2_B.RateLimiter5;

    /* End of RateLimiter: '<S13>/Rate Limiter5' */

    /* RateLimiter: '<S13>/Rate Limiter7' */
    u0 = M220Model_v2_B.Add7_f - M220Model_v2_DW.PrevY_e;
    if (u0 > 1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter7' */
      M220Model_v2_B.RateLimiter7 = M220Model_v2_DW.PrevY_e + 1.0E-5;
    } else if (u0 < -1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter7' */
      M220Model_v2_B.RateLimiter7 = M220Model_v2_DW.PrevY_e + -1.0E-5;
    } else {
      /* RateLimiter: '<S13>/Rate Limiter7' */
      M220Model_v2_B.RateLimiter7 = M220Model_v2_B.Add7_f;
    }

    M220Model_v2_DW.PrevY_e = M220Model_v2_B.RateLimiter7;

    /* End of RateLimiter: '<S13>/Rate Limiter7' */

    /* MultiPortSwitch: '<S13>/Multiport Switch1' */
    switch (static_cast<int32_T>(M220Model_v2_B.Switch12)) {
     case 1:
      /* MultiPortSwitch: '<S13>/Multiport Switch1' */
      M220Model_v2_B.MultiportSwitch1 = M220Model_v2_B.RateLimiter4;
      break;

     case 2:
      /* MultiPortSwitch: '<S13>/Multiport Switch1' */
      M220Model_v2_B.MultiportSwitch1 = M220Model_v2_B.RateLimiter6;
      break;

     case 3:
      /* MultiPortSwitch: '<S13>/Multiport Switch1' */
      M220Model_v2_B.MultiportSwitch1 = M220Model_v2_B.RateLimiter5;
      break;

     case 4:
      /* MultiPortSwitch: '<S13>/Multiport Switch1' */
      M220Model_v2_B.MultiportSwitch1 = M220Model_v2_B.RateLimiter7;
      break;

     default:
      /* MultiPortSwitch: '<S13>/Multiport Switch1' incorporates:
       *  Outport: '<Root>/LeftDeckAngleSns_deg'
       */
      M220Model_v2_B.MultiportSwitch1 = M220Model_v2_Y.LeftDeckAngleSns_deg;
      break;
    }

    /* End of MultiPortSwitch: '<S13>/Multiport Switch1' */

    /* Switch: '<S13>/Switch20' incorporates:
     *  Switch: '<S13>/Switch21'
     *  Switch: '<S13>/Switch22'
     *  Switch: '<S13>/Switch23'
     */
    if (M220Model_v2_B.LogicalOperator20) {
      /* Switch: '<S13>/Switch20' incorporates:
       *  Constant: '<S13>/Constant22'
       */
      M220Model_v2_B.Switch20 = 1.0;
    } else {
      if (M220Model_v2_B.LogicalOperator26) {
        /* Switch: '<S13>/Switch21' incorporates:
         *  Constant: '<S13>/Constant25'
         */
        M220Model_v2_B.Switch21 = 2.0;
      } else {
        if (M220Model_v2_B.LogicalOperator21) {
          /* Switch: '<S13>/Switch22' incorporates:
           *  Constant: '<S13>/Constant23'
           *  Switch: '<S13>/Switch21'
           */
          M220Model_v2_B.Switch22 = 3.0;
        } else {
          if (M220Model_v2_B.LogicalOperator27) {
            /* Switch: '<S13>/Switch23' incorporates:
             *  Constant: '<S13>/Constant24'
             *  Switch: '<S13>/Switch21'
             *  Switch: '<S13>/Switch22'
             */
            M220Model_v2_B.Switch23 = 4.0;
          } else {
            /* Switch: '<S13>/Switch23' incorporates:
             *  Constant: '<S13>/Constant26'
             *  Switch: '<S13>/Switch21'
             *  Switch: '<S13>/Switch22'
             */
            M220Model_v2_B.Switch23 = 5.0;
          }

          /* Switch: '<S13>/Switch22' incorporates:
           *  Switch: '<S13>/Switch21'
           */
          M220Model_v2_B.Switch22 = M220Model_v2_B.Switch23;
        }

        /* Switch: '<S13>/Switch21' incorporates:
         *  Switch: '<S13>/Switch22'
         *  Switch: '<S13>/Switch23'
         */
        M220Model_v2_B.Switch21 = M220Model_v2_B.Switch22;
      }

      /* Switch: '<S13>/Switch20' incorporates:
       *  Switch: '<S13>/Switch21'
       *  Switch: '<S13>/Switch22'
       *  Switch: '<S13>/Switch23'
       */
      M220Model_v2_B.Switch20 = M220Model_v2_B.Switch21;
    }

    /* End of Switch: '<S13>/Switch20' */

    /* RateLimiter: '<S13>/Rate Limiter8' */
    u0 = M220Model_v2_B.Add8_a - M220Model_v2_DW.PrevY_o;
    if (u0 > 1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter8' */
      M220Model_v2_B.RateLimiter8 = M220Model_v2_DW.PrevY_o + 1.0E-5;
    } else if (u0 < -1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter8' */
      M220Model_v2_B.RateLimiter8 = M220Model_v2_DW.PrevY_o + -1.0E-5;
    } else {
      /* RateLimiter: '<S13>/Rate Limiter8' */
      M220Model_v2_B.RateLimiter8 = M220Model_v2_B.Add8_a;
    }

    M220Model_v2_DW.PrevY_o = M220Model_v2_B.RateLimiter8;

    /* End of RateLimiter: '<S13>/Rate Limiter8' */

    /* RateLimiter: '<S13>/Rate Limiter10' */
    u0 = M220Model_v2_B.Add10_e - M220Model_v2_DW.PrevY_d5;
    if (u0 > 1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter10' */
      M220Model_v2_B.RateLimiter10 = M220Model_v2_DW.PrevY_d5 + 1.0E-5;
    } else if (u0 < -1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter10' */
      M220Model_v2_B.RateLimiter10 = M220Model_v2_DW.PrevY_d5 + -1.0E-5;
    } else {
      /* RateLimiter: '<S13>/Rate Limiter10' */
      M220Model_v2_B.RateLimiter10 = M220Model_v2_B.Add10_e;
    }

    M220Model_v2_DW.PrevY_d5 = M220Model_v2_B.RateLimiter10;

    /* End of RateLimiter: '<S13>/Rate Limiter10' */

    /* RateLimiter: '<S13>/Rate Limiter9' */
    u0 = M220Model_v2_B.Add9_b - M220Model_v2_DW.PrevY_g;
    if (u0 > 1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter9' */
      M220Model_v2_B.RateLimiter9 = M220Model_v2_DW.PrevY_g + 1.0E-5;
    } else if (u0 < -1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter9' */
      M220Model_v2_B.RateLimiter9 = M220Model_v2_DW.PrevY_g + -1.0E-5;
    } else {
      /* RateLimiter: '<S13>/Rate Limiter9' */
      M220Model_v2_B.RateLimiter9 = M220Model_v2_B.Add9_b;
    }

    M220Model_v2_DW.PrevY_g = M220Model_v2_B.RateLimiter9;

    /* End of RateLimiter: '<S13>/Rate Limiter9' */

    /* RateLimiter: '<S13>/Rate Limiter11' */
    u0 = M220Model_v2_B.Add11_a - M220Model_v2_DW.PrevY_bh;
    if (u0 > 1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter11' */
      M220Model_v2_B.RateLimiter11 = M220Model_v2_DW.PrevY_bh + 1.0E-5;
    } else if (u0 < -1.0E-5) {
      /* RateLimiter: '<S13>/Rate Limiter11' */
      M220Model_v2_B.RateLimiter11 = M220Model_v2_DW.PrevY_bh + -1.0E-5;
    } else {
      /* RateLimiter: '<S13>/Rate Limiter11' */
      M220Model_v2_B.RateLimiter11 = M220Model_v2_B.Add11_a;
    }

    M220Model_v2_DW.PrevY_bh = M220Model_v2_B.RateLimiter11;

    /* End of RateLimiter: '<S13>/Rate Limiter11' */

    /* MultiPortSwitch: '<S13>/Multiport Switch2' */
    switch (static_cast<int32_T>(M220Model_v2_B.Switch20)) {
     case 1:
      /* MultiPortSwitch: '<S13>/Multiport Switch2' */
      M220Model_v2_B.MultiportSwitch2 = M220Model_v2_B.RateLimiter8;
      break;

     case 2:
      /* MultiPortSwitch: '<S13>/Multiport Switch2' */
      M220Model_v2_B.MultiportSwitch2 = M220Model_v2_B.RateLimiter10;
      break;

     case 3:
      /* MultiPortSwitch: '<S13>/Multiport Switch2' */
      M220Model_v2_B.MultiportSwitch2 = M220Model_v2_B.RateLimiter9;
      break;

     case 4:
      /* MultiPortSwitch: '<S13>/Multiport Switch2' */
      M220Model_v2_B.MultiportSwitch2 = M220Model_v2_B.RateLimiter11;
      break;

     default:
      /* MultiPortSwitch: '<S13>/Multiport Switch2' incorporates:
       *  Outport: '<Root>/RightDeckAngleSns_deg'
       */
      M220Model_v2_B.MultiportSwitch2 = M220Model_v2_Y.RightDeckAngleSns_deg;
      break;
    }

    /* End of MultiPortSwitch: '<S13>/Multiport Switch2' */
  }
}

/* Model update function */
void M220Model_v2_update(void)
{
  real_T *lastU;
  if (rtmIsMajorTimeStep(M220Model_v2_M)) {
    /* Update for Memory: '<S13>/Memory' */
    M220Model_v2_DW.Memory_PreviousInput = M220Model_v2_B.MultiportSwitch;

    /* Update for Memory: '<S13>/Memory2' */
    M220Model_v2_DW.Memory2_PreviousInput = M220Model_v2_B.MultiportSwitch2;

    /* Update for Memory: '<S13>/Memory1' */
    M220Model_v2_DW.Memory1_PreviousInput = M220Model_v2_B.MultiportSwitch1;
  }

  /* Update for Derivative: '<S2>/Derivative' */
  if (M220Model_v2_DW.TimeStampA == (rtInf)) {
    M220Model_v2_DW.TimeStampA = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA;
  } else if (M220Model_v2_DW.TimeStampB == (rtInf)) {
    M220Model_v2_DW.TimeStampB = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB;
  } else if (M220Model_v2_DW.TimeStampA < M220Model_v2_DW.TimeStampB) {
    M220Model_v2_DW.TimeStampA = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA;
  } else {
    M220Model_v2_DW.TimeStampB = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB;
  }

  *lastU = M220Model_v2_B.Sqrt_a;

  /* End of Update for Derivative: '<S2>/Derivative' */

  /* Update for Derivative: '<S32>/Derivative' */
  if (M220Model_v2_DW.TimeStampA_h == (rtInf)) {
    M220Model_v2_DW.TimeStampA_h = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_j;
  } else if (M220Model_v2_DW.TimeStampB_m == (rtInf)) {
    M220Model_v2_DW.TimeStampB_m = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_h;
  } else if (M220Model_v2_DW.TimeStampA_h < M220Model_v2_DW.TimeStampB_m) {
    M220Model_v2_DW.TimeStampA_h = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_j;
  } else {
    M220Model_v2_DW.TimeStampB_m = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_h;
  }

  *lastU = M220Model_v2_B.Product1_l;

  /* End of Update for Derivative: '<S32>/Derivative' */

  /* Update for Derivative: '<S32>/Derivative1' */
  if (M220Model_v2_DW.TimeStampA_h5 == (rtInf)) {
    M220Model_v2_DW.TimeStampA_h5 = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_g;
  } else if (M220Model_v2_DW.TimeStampB_a == (rtInf)) {
    M220Model_v2_DW.TimeStampB_a = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_i;
  } else if (M220Model_v2_DW.TimeStampA_h5 < M220Model_v2_DW.TimeStampB_a) {
    M220Model_v2_DW.TimeStampA_h5 = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_g;
  } else {
    M220Model_v2_DW.TimeStampB_a = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_i;
  }

  *lastU = M220Model_v2_B.Product_c;

  /* End of Update for Derivative: '<S32>/Derivative1' */

  /* Update for Derivative: '<Root>/Derivative' incorporates:
   *  Constant: '<Root>/LonTerrainAngle_deg'
   */
  if (M220Model_v2_DW.TimeStampA_m == (rtInf)) {
    M220Model_v2_DW.TimeStampA_m = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_gr;
  } else if (M220Model_v2_DW.TimeStampB_k == (rtInf)) {
    M220Model_v2_DW.TimeStampB_k = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_l;
  } else if (M220Model_v2_DW.TimeStampA_m < M220Model_v2_DW.TimeStampB_k) {
    M220Model_v2_DW.TimeStampA_m = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_gr;
  } else {
    M220Model_v2_DW.TimeStampB_k = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_l;
  }

  *lastU = 0.0;

  /* End of Update for Derivative: '<Root>/Derivative' */

  /* Update for Derivative: '<S43>/Derivative' */
  if (M220Model_v2_DW.TimeStampA_hx == (rtInf)) {
    M220Model_v2_DW.TimeStampA_hx = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_p;
  } else if (M220Model_v2_DW.TimeStampB_p == (rtInf)) {
    M220Model_v2_DW.TimeStampB_p = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_c;
  } else if (M220Model_v2_DW.TimeStampA_hx < M220Model_v2_DW.TimeStampB_p) {
    M220Model_v2_DW.TimeStampA_hx = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_p;
  } else {
    M220Model_v2_DW.TimeStampB_p = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_c;
  }

  *lastU = M220Model_v2_B.Product1_l;

  /* End of Update for Derivative: '<S43>/Derivative' */

  /* Update for Derivative: '<S43>/Derivative1' */
  if (M220Model_v2_DW.TimeStampA_mc == (rtInf)) {
    M220Model_v2_DW.TimeStampA_mc = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_ge;
  } else if (M220Model_v2_DW.TimeStampB_l == (rtInf)) {
    M220Model_v2_DW.TimeStampB_l = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_ik;
  } else if (M220Model_v2_DW.TimeStampA_mc < M220Model_v2_DW.TimeStampB_l) {
    M220Model_v2_DW.TimeStampA_mc = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_ge;
  } else {
    M220Model_v2_DW.TimeStampB_l = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_ik;
  }

  *lastU = M220Model_v2_B.Product_c;

  /* End of Update for Derivative: '<S43>/Derivative1' */

  /* Update for Derivative: '<S54>/Derivative' */
  if (M220Model_v2_DW.TimeStampA_o == (rtInf)) {
    M220Model_v2_DW.TimeStampA_o = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_i;
  } else if (M220Model_v2_DW.TimeStampB_k1 == (rtInf)) {
    M220Model_v2_DW.TimeStampB_k1 = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_g;
  } else if (M220Model_v2_DW.TimeStampA_o < M220Model_v2_DW.TimeStampB_k1) {
    M220Model_v2_DW.TimeStampA_o = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_i;
  } else {
    M220Model_v2_DW.TimeStampB_k1 = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_g;
  }

  *lastU = M220Model_v2_B.Product1_l;

  /* End of Update for Derivative: '<S54>/Derivative' */

  /* Update for Derivative: '<S54>/Derivative1' */
  if (M220Model_v2_DW.TimeStampA_d == (rtInf)) {
    M220Model_v2_DW.TimeStampA_d = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_jv;
  } else if (M220Model_v2_DW.TimeStampB_ko == (rtInf)) {
    M220Model_v2_DW.TimeStampB_ko = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_i2;
  } else if (M220Model_v2_DW.TimeStampA_d < M220Model_v2_DW.TimeStampB_ko) {
    M220Model_v2_DW.TimeStampA_d = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_jv;
  } else {
    M220Model_v2_DW.TimeStampB_ko = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_i2;
  }

  *lastU = M220Model_v2_B.Product_c;

  /* End of Update for Derivative: '<S54>/Derivative1' */

  /* Update for Derivative: '<S67>/Derivative' */
  if (M220Model_v2_DW.TimeStampA_g == (rtInf)) {
    M220Model_v2_DW.TimeStampA_g = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_ju;
  } else if (M220Model_v2_DW.TimeStampB_ku == (rtInf)) {
    M220Model_v2_DW.TimeStampB_ku = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_m;
  } else if (M220Model_v2_DW.TimeStampA_g < M220Model_v2_DW.TimeStampB_ku) {
    M220Model_v2_DW.TimeStampA_g = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_ju;
  } else {
    M220Model_v2_DW.TimeStampB_ku = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_m;
  }

  *lastU = M220Model_v2_B.Product1_l;

  /* End of Update for Derivative: '<S67>/Derivative' */

  /* Update for Derivative: '<S67>/Derivative1' */
  if (M220Model_v2_DW.TimeStampA_i == (rtInf)) {
    M220Model_v2_DW.TimeStampA_i = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_b;
  } else if (M220Model_v2_DW.TimeStampB_j == (rtInf)) {
    M220Model_v2_DW.TimeStampB_j = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_hn;
  } else if (M220Model_v2_DW.TimeStampA_i < M220Model_v2_DW.TimeStampB_j) {
    M220Model_v2_DW.TimeStampA_i = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeA_b;
  } else {
    M220Model_v2_DW.TimeStampB_j = M220Model_v2_M->Timing.t[0];
    lastU = &M220Model_v2_DW.LastUAtTimeB_hn;
  }

  *lastU = M220Model_v2_B.Product_c;

  /* End of Update for Derivative: '<S67>/Derivative1' */
  if (rtmIsMajorTimeStep(M220Model_v2_M)) {
    /* Update for Delay: '<S9>/Delay' */
    M220Model_v2_DW.Delay_DSTATE = M220Model_v2_B.Integrator1;

    /* Update for Delay: '<S9>/Delay2' */
    M220Model_v2_DW.Delay2_DSTATE = M220Model_v2_B.Saturation3;

    /* Update for Delay: '<S9>/Delay4' */
    M220Model_v2_DW.Delay4_DSTATE = M220Model_v2_B.Integrator8;

    /* Update for Delay: '<S9>/Delay1' */
    M220Model_v2_DW.Delay1_DSTATE = M220Model_v2_B.Integrator2;

    /* Update for Delay: '<S9>/Delay3' */
    M220Model_v2_DW.Delay3_DSTATE = M220Model_v2_B.Integrator7;
  }

  if (rtmIsMajorTimeStep(M220Model_v2_M)) {
    rt_ertODEUpdateContinuousStates(&M220Model_v2_M->solverInfo);
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++M220Model_v2_M->Timing.clockTick0)) {
    ++M220Model_v2_M->Timing.clockTickH0;
  }

  M220Model_v2_M->Timing.t[0] = rtsiGetSolverStopTime
    (&M220Model_v2_M->solverInfo);

  {
    /* Update absolute timer for sample time: [0.1s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick1"
     * and "Timing.stepSize1". Size of "clockTick1" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++M220Model_v2_M->Timing.clockTick1)) {
      ++M220Model_v2_M->Timing.clockTickH1;
    }

    M220Model_v2_M->Timing.t[1] = M220Model_v2_M->Timing.clockTick1 *
      M220Model_v2_M->Timing.stepSize1 + M220Model_v2_M->Timing.clockTickH1 *
      M220Model_v2_M->Timing.stepSize1 * 4294967296.0;
  }
}

/* Derivatives for root system: '<Root>' */
void M220Model_v2_derivatives(void)
{
  XDot_M220Model_v2_T *_rtXdot;
  _rtXdot = ((XDot_M220Model_v2_T *) M220Model_v2_M->derivs);

  /* Derivatives for Integrator: '<S9>/Integrator2' */
  _rtXdot->Integrator2_CSTATE = M220Model_v2_B.Add1_fr;

  /* Derivatives for Integrator: '<S9>/Integrator1' */
  _rtXdot->Integrator1_CSTATE = M220Model_v2_B.Add6;

  /* Derivatives for Integrator: '<S9>/Integrator4' */
  _rtXdot->Integrator4_CSTATE = M220Model_v2_B.Product17;

  /* Derivatives for Integrator: '<S9>/Integrator5' */
  _rtXdot->Integrator5_CSTATE = M220Model_v2_B.Add16;

  /* Derivatives for Integrator: '<S9>/Integrator11' */
  _rtXdot->Integrator11_CSTATE = M220Model_v2_B.Gain_b;

  /* Derivatives for Integrator: '<S24>/Integrator' */
  _rtXdot->Integrator_CSTATE = M220Model_v2_B.Divide_c;

  /* Derivatives for Integrator: '<S9>/Integrator' */
  _rtXdot->Integrator_CSTATE_c = M220Model_v2_B.Integrator1;

  /* Derivatives for Integrator: '<S9>/Integrator10' */
  _rtXdot->Integrator10_CSTATE = M220Model_v2_B.Gain_l;

  /* Derivatives for Integrator: '<S9>/Integrator3' */
  _rtXdot->Integrator3_CSTATE = M220Model_v2_B.Integrator2;

  /* Derivatives for Integrator: '<S9>/Integrator6' */
  _rtXdot->Integrator6_CSTATE = M220Model_v2_B.Integrator5;

  /* Derivatives for Integrator: '<S9>/Integrator7' */
  _rtXdot->Integrator7_CSTATE = M220Model_v2_B.Product29;

  /* Derivatives for Integrator: '<S9>/Integrator8' */
  _rtXdot->Integrator8_CSTATE = M220Model_v2_B.Product30;

  /* Derivatives for Integrator: '<S9>/Integrator9' */
  _rtXdot->Integrator9_CSTATE = M220Model_v2_B.Gain_d;
}

/* Model initialize function */
void M220Model_v2_initialize(void)
{
  /* Start for DiscretePulseGenerator: '<S16>/Pulse Generator' */
  M220Model_v2_DW.clockTickCounter = 0;

  /* Start for DiscretePulseGenerator: '<S16>/Pulse Generator1' */
  M220Model_v2_DW.clockTickCounter_c = 0;

  /* InitializeConditions for Integrator: '<S9>/Integrator2' */
  M220Model_v2_X.Integrator2_CSTATE = 0.0;

  /* InitializeConditions for Integrator: '<S9>/Integrator1' */
  M220Model_v2_X.Integrator1_CSTATE = 0.0;

  /* InitializeConditions for Integrator: '<S9>/Integrator4' */
  M220Model_v2_X.Integrator4_CSTATE = 0.0;

  /* InitializeConditions for Integrator: '<S9>/Integrator5' */
  M220Model_v2_X.Integrator5_CSTATE = 0.0;

  /* InitializeConditions for Memory: '<S13>/Memory' */
  M220Model_v2_DW.Memory_PreviousInput = 90.0;

  /* InitializeConditions for Memory: '<S13>/Memory2' */
  M220Model_v2_DW.Memory2_PreviousInput = 90.0;

  /* InitializeConditions for Memory: '<S13>/Memory1' */
  M220Model_v2_DW.Memory1_PreviousInput = 90.0;

  /* InitializeConditions for Derivative: '<S2>/Derivative' */
  M220Model_v2_DW.TimeStampA = (rtInf);
  M220Model_v2_DW.TimeStampB = (rtInf);

  /* InitializeConditions for Integrator: '<S9>/Integrator11' */
  M220Model_v2_X.Integrator11_CSTATE = 0.0;

  /* InitializeConditions for Integrator: '<S24>/Integrator' */
  M220Model_v2_X.Integrator_CSTATE = 0.0;

  /* InitializeConditions for Derivative: '<S32>/Derivative' */
  M220Model_v2_DW.TimeStampA_h = (rtInf);
  M220Model_v2_DW.TimeStampB_m = (rtInf);

  /* InitializeConditions for Derivative: '<S32>/Derivative1' */
  M220Model_v2_DW.TimeStampA_h5 = (rtInf);
  M220Model_v2_DW.TimeStampB_a = (rtInf);

  /* InitializeConditions for Derivative: '<Root>/Derivative' */
  M220Model_v2_DW.TimeStampA_m = (rtInf);
  M220Model_v2_DW.TimeStampB_k = (rtInf);

  /* InitializeConditions for Derivative: '<S43>/Derivative' */
  M220Model_v2_DW.TimeStampA_hx = (rtInf);
  M220Model_v2_DW.TimeStampB_p = (rtInf);

  /* InitializeConditions for Derivative: '<S43>/Derivative1' */
  M220Model_v2_DW.TimeStampA_mc = (rtInf);
  M220Model_v2_DW.TimeStampB_l = (rtInf);

  /* InitializeConditions for Derivative: '<S54>/Derivative' */
  M220Model_v2_DW.TimeStampA_o = (rtInf);
  M220Model_v2_DW.TimeStampB_k1 = (rtInf);

  /* InitializeConditions for Derivative: '<S54>/Derivative1' */
  M220Model_v2_DW.TimeStampA_d = (rtInf);
  M220Model_v2_DW.TimeStampB_ko = (rtInf);

  /* InitializeConditions for Derivative: '<S67>/Derivative' */
  M220Model_v2_DW.TimeStampA_g = (rtInf);
  M220Model_v2_DW.TimeStampB_ku = (rtInf);

  /* InitializeConditions for Derivative: '<S67>/Derivative1' */
  M220Model_v2_DW.TimeStampA_i = (rtInf);
  M220Model_v2_DW.TimeStampB_j = (rtInf);

  /* InitializeConditions for Delay: '<S9>/Delay' */
  M220Model_v2_DW.Delay_DSTATE = 0.0;

  /* InitializeConditions for Delay: '<S9>/Delay2' */
  M220Model_v2_DW.Delay2_DSTATE = 0.0;

  /* InitializeConditions for Delay: '<S9>/Delay4' */
  M220Model_v2_DW.Delay4_DSTATE = 0.0;

  /* InitializeConditions for Delay: '<S9>/Delay1' */
  M220Model_v2_DW.Delay1_DSTATE = 0.0;

  /* InitializeConditions for Delay: '<S9>/Delay3' */
  M220Model_v2_DW.Delay3_DSTATE = 0.0;

  /* InitializeConditions for Integrator: '<S9>/Integrator' */
  M220Model_v2_X.Integrator_CSTATE_c = 0.0;

  /* InitializeConditions for Integrator: '<S9>/Integrator10' */
  M220Model_v2_X.Integrator10_CSTATE = 0.0;

  /* InitializeConditions for Integrator: '<S9>/Integrator3' */
  M220Model_v2_X.Integrator3_CSTATE = 0.0;

  /* InitializeConditions for Integrator: '<S9>/Integrator6' */
  M220Model_v2_X.Integrator6_CSTATE = 0.0;

  /* InitializeConditions for Integrator: '<S9>/Integrator7' */
  M220Model_v2_X.Integrator7_CSTATE = 0.0;

  /* InitializeConditions for Integrator: '<S9>/Integrator8' */
  M220Model_v2_X.Integrator8_CSTATE = 0.0;

  /* InitializeConditions for Integrator: '<S9>/Integrator9' */
  M220Model_v2_X.Integrator9_CSTATE = 0.0;

  /* InitializeConditions for RateLimiter: '<S13>/Rate Limiter' */
  M220Model_v2_DW.PrevY = 90.0;

  /* InitializeConditions for RateLimiter: '<S13>/Rate Limiter2' */
  M220Model_v2_DW.PrevY_d = 90.0;

  /* InitializeConditions for RateLimiter: '<S13>/Rate Limiter1' */
  M220Model_v2_DW.PrevY_f = 90.0;

  /* InitializeConditions for RateLimiter: '<S13>/Rate Limiter3' */
  M220Model_v2_DW.PrevY_b = 90.0;

  /* InitializeConditions for RateLimiter: '<S13>/Rate Limiter4' */
  M220Model_v2_DW.PrevY_j = 90.0;

  /* InitializeConditions for RateLimiter: '<S13>/Rate Limiter6' */
  M220Model_v2_DW.PrevY_h = 90.0;

  /* InitializeConditions for RateLimiter: '<S13>/Rate Limiter5' */
  M220Model_v2_DW.PrevY_fv = 90.0;

  /* InitializeConditions for RateLimiter: '<S13>/Rate Limiter7' */
  M220Model_v2_DW.PrevY_e = 90.0;

  /* InitializeConditions for RateLimiter: '<S13>/Rate Limiter8' */
  M220Model_v2_DW.PrevY_o = 90.0;

  /* InitializeConditions for RateLimiter: '<S13>/Rate Limiter10' */
  M220Model_v2_DW.PrevY_d5 = 90.0;

  /* InitializeConditions for RateLimiter: '<S13>/Rate Limiter9' */
  M220Model_v2_DW.PrevY_g = 90.0;

  /* InitializeConditions for RateLimiter: '<S13>/Rate Limiter11' */
  M220Model_v2_DW.PrevY_bh = 90.0;
}

/* Model terminate function */
void M220Model_v2_terminate(void)
{
  /* (no terminate code required) */
}

/*========================================================================*
 * Start of Classic call interface                                        *
 *========================================================================*/

/* Solver interface called by GRT_Main */
#ifndef USE_GENERATED_SOLVER

extern "C" void rt_ODECreateIntegrationData(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

extern "C" void rt_ODEDestroyIntegrationData(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

extern "C" void rt_ODEUpdateContinuousStates(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

#endif

extern "C" void MdlOutputs(int_T tid)
{
  M220Model_v2_output();
  UNUSED_PARAMETER(tid);
}

extern "C" void MdlUpdate(int_T tid)
{
  M220Model_v2_update();
  UNUSED_PARAMETER(tid);
}

extern "C" void MdlInitializeSizes(void)
{
}

extern "C" void MdlInitializeSampleTimes(void)
{
}

extern "C" void MdlInitialize(void)
{
}

extern "C" void MdlStart(void)
{
  M220Model_v2_initialize();
}

extern "C" void MdlTerminate(void)
{
  M220Model_v2_terminate();
}

/* Registration function */
extern "C" RT_MODEL_M220Model_v2_T *M220Model_v2(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&M220Model_v2_M->solverInfo,
                          &M220Model_v2_M->Timing.simTimeStep);
    rtsiSetTPtr(&M220Model_v2_M->solverInfo, &rtmGetTPtr(M220Model_v2_M));
    rtsiSetStepSizePtr(&M220Model_v2_M->solverInfo,
                       &M220Model_v2_M->Timing.stepSize0);
    rtsiSetdXPtr(&M220Model_v2_M->solverInfo, &M220Model_v2_M->derivs);
    rtsiSetContStatesPtr(&M220Model_v2_M->solverInfo, (real_T **)
                         &M220Model_v2_M->contStates);
    rtsiSetNumContStatesPtr(&M220Model_v2_M->solverInfo,
      &M220Model_v2_M->Sizes.numContStates);
    rtsiSetNumPeriodicContStatesPtr(&M220Model_v2_M->solverInfo,
      &M220Model_v2_M->Sizes.numPeriodicContStates);
    rtsiSetPeriodicContStateIndicesPtr(&M220Model_v2_M->solverInfo,
      &M220Model_v2_M->periodicContStateIndices);
    rtsiSetPeriodicContStateRangesPtr(&M220Model_v2_M->solverInfo,
      &M220Model_v2_M->periodicContStateRanges);
    rtsiSetErrorStatusPtr(&M220Model_v2_M->solverInfo, (&rtmGetErrorStatus
      (M220Model_v2_M)));
    rtsiSetRTModelPtr(&M220Model_v2_M->solverInfo, M220Model_v2_M);
  }

  rtsiSetSimTimeStep(&M220Model_v2_M->solverInfo, MAJOR_TIME_STEP);
  M220Model_v2_M->intgData.y = M220Model_v2_M->odeY;
  M220Model_v2_M->intgData.f[0] = M220Model_v2_M->odeF[0];
  M220Model_v2_M->intgData.f[1] = M220Model_v2_M->odeF[1];
  M220Model_v2_M->intgData.f[2] = M220Model_v2_M->odeF[2];
  M220Model_v2_M->intgData.f[3] = M220Model_v2_M->odeF[3];
  M220Model_v2_M->contStates = ((real_T *) &M220Model_v2_X);
  rtsiSetSolverData(&M220Model_v2_M->solverInfo, static_cast<void *>
                    (&M220Model_v2_M->intgData));
  rtsiSetSolverName(&M220Model_v2_M->solverInfo,"ode4");

  /* Initialize timing info */
  {
    int_T *mdlTsMap = M220Model_v2_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    mdlTsMap[1] = 1;
    M220Model_v2_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    M220Model_v2_M->Timing.sampleTimes =
      (&M220Model_v2_M->Timing.sampleTimesArray[0]);
    M220Model_v2_M->Timing.offsetTimes =
      (&M220Model_v2_M->Timing.offsetTimesArray[0]);

    /* task periods */
    M220Model_v2_M->Timing.sampleTimes[0] = (0.0);
    M220Model_v2_M->Timing.sampleTimes[1] = (0.1);

    /* task offsets */
    M220Model_v2_M->Timing.offsetTimes[0] = (0.0);
    M220Model_v2_M->Timing.offsetTimes[1] = (0.0);
  }

  rtmSetTPtr(M220Model_v2_M, &M220Model_v2_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = M220Model_v2_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    mdlSampleHits[1] = 1;
    M220Model_v2_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(M220Model_v2_M, -1);
  M220Model_v2_M->Timing.stepSize0 = 0.1;
  M220Model_v2_M->Timing.stepSize1 = 0.1;

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    rt_DataLoggingInfo.loggingInterval = NULL;
    M220Model_v2_M->rtwLogInfo = &rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(M220Model_v2_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(M220Model_v2_M->rtwLogInfo, (NULL));
    rtliSetLogT(M220Model_v2_M->rtwLogInfo, "tout");
    rtliSetLogX(M220Model_v2_M->rtwLogInfo, "");
    rtliSetLogXFinal(M220Model_v2_M->rtwLogInfo, "");
    rtliSetLogVarNameModifier(M220Model_v2_M->rtwLogInfo, "rt_");
    rtliSetLogFormat(M220Model_v2_M->rtwLogInfo, 4);
    rtliSetLogMaxRows(M220Model_v2_M->rtwLogInfo, 0);
    rtliSetLogDecimation(M220Model_v2_M->rtwLogInfo, 1);
    rtliSetLogY(M220Model_v2_M->rtwLogInfo, "");
    rtliSetLogYSignalInfo(M220Model_v2_M->rtwLogInfo, (NULL));
    rtliSetLogYSignalPtrs(M220Model_v2_M->rtwLogInfo, (NULL));
  }

  M220Model_v2_M->solverInfoPtr = (&M220Model_v2_M->solverInfo);
  M220Model_v2_M->Timing.stepSize = (0.1);
  rtsiSetFixedStepSize(&M220Model_v2_M->solverInfo, 0.1);
  rtsiSetSolverMode(&M220Model_v2_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  M220Model_v2_M->blockIO = ((void *) &M220Model_v2_B);
  (void) std::memset((static_cast<void *>(&M220Model_v2_B)), 0,
                     sizeof(B_M220Model_v2_T));

  {
    M220Model_v2_B.Gain = (0UL);
    M220Model_v2_B.Gain2 = (0UL);
  }

  /* states (continuous) */
  {
    real_T *x = (real_T *) &M220Model_v2_X;
    M220Model_v2_M->contStates = (x);
    (void) std::memset(static_cast<void *>(&M220Model_v2_X), 0,
                       sizeof(X_M220Model_v2_T));
  }

  /* states (dwork) */
  M220Model_v2_M->dwork = ((void *) &M220Model_v2_DW);
  (void) std::memset(static_cast<void *>(&M220Model_v2_DW), 0,
                     sizeof(DW_M220Model_v2_T));

  /* external inputs */
  M220Model_v2_M->inputs = (((void*)&M220Model_v2_U));
  (void)std::memset(&M220Model_v2_U, 0, sizeof(ExtU_M220Model_v2_T));

  /* external outputs */
  M220Model_v2_M->outputs = (&M220Model_v2_Y);
  (void) std::memset(static_cast<void *>(&M220Model_v2_Y), 0,
                     sizeof(ExtY_M220Model_v2_T));
  M220Model_v2_Y.SpdEnc1Line1Frq_pps = (0UL);
  M220Model_v2_Y.SpdEnc1Line2Frq_int = (0UL);
  M220Model_v2_Y.SpdEnc2Line1Frq_pps = (0UL);
  M220Model_v2_Y.SpdEnc2Line1Frq_int = (0UL);

  /* Initialize Sizes */
  M220Model_v2_M->Sizes.numContStates = (13);/* Number of continuous states */
  M220Model_v2_M->Sizes.numPeriodicContStates = (0);
                                      /* Number of periodic continuous states */
  M220Model_v2_M->Sizes.numY = (128);  /* Number of model outputs */
  M220Model_v2_M->Sizes.numU = (129);  /* Number of model inputs */
  M220Model_v2_M->Sizes.sysDirFeedThru = (1);/* The model is direct feedthrough */
  M220Model_v2_M->Sizes.numSampTimes = (2);/* Number of sample times */
  M220Model_v2_M->Sizes.numBlocks = (930);/* Number of blocks */
  M220Model_v2_M->Sizes.numBlockIO = (462);/* Number of block outputs */
  return M220Model_v2_M;
}

/*========================================================================*
 * End of Classic call interface                                          *
 *========================================================================*/

/*========================================================================*
 * VeriStand Model Framework code generation
 *
 * Model : M220Model_v2
 * Model version : 1.474
 * VeriStand Model Framework version : 2020.5.0.0 (2020 R5)
 * Source generated on : Mon Nov  1 19:15:20 2021
 *========================================================================*/

/* This file contains automatically generated code for functions
 * to update the inports and outports of a Simulink/Realtime Workshop
 * model. Calls to these functions should be made before each step is taken
 * (inports, call SetExternalInputs) and after each step is taken
 * (outports, call SetExternalOutputs.)
 */
#ifdef NI_ROOTMODEL_M220Model_v2
#include "ni_modelframework_ex.h"
#include <stddef.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>

unsigned char ReadSideDirtyFlag = 0, WriteSideDirtyFlag = 0;

/*========================================================================*
 * Function: NIRT_GetValueByDataType
 *
 * Abstract:
 *		Converting to and from double and datatypes used in the model
 *
 * Output Parameters
 *      ptr : address to the source
 *      subindex : index value if vector
 *      type   : the source's data type
 *      Complex : true if a complex data type
 *
 * Returns:
 *     Return value: 0 if no error
 *========================================================================*/
extern "C" double NIRT_GetValueByDataType(void* ptr, int32_t subindex, int32_t
  type, int32_t Complex)
{
  switch (type)
  {
   case 0:
    return (double)(((real_T*)ptr)[subindex]);

   case 1:
    return (double)(((real32_T*)ptr)[subindex]);

   case 2:
    return (double)(((int8_T*)ptr)[subindex]);

   case 3:
    return (double)(((uint8_T*)ptr)[subindex]);

   case 4:
    return (double)(((int16_T*)ptr)[subindex]);

   case 5:
    return (double)(((uint16_T*)ptr)[subindex]);

   case 6:
    return (double)(((int32_T*)ptr)[subindex]);

   case 7:
    return (double)(((uint32_T*)ptr)[subindex]);

   case 8:
    return (double)(((boolean_T*)ptr)[subindex]);

   case 10:
    return NIRT_GetValueByDataType(ptr,subindex,6,Complex);

   case 13:
    return NIRT_GetValueByDataType(ptr,subindex,7,Complex);

   case 14:
    return NIRT_GetValueByDataType(ptr, subindex,7,Complex) * 1 * pow(2,0) + 0;

   case 15:
    return NIRT_GetValueByDataType(ptr, subindex,17,Complex) * 1 * pow(2,0) + 0;

   case 17:
    return (double)(((uint64_T*)ptr)[subindex]);

   case 18:
    return NIRT_GetValueByDataType(ptr,subindex,6,Complex);

   case 19:
    return NIRT_GetValueByDataType(ptr,subindex,6,Complex);

   case 20:
    return NIRT_GetValueByDataType(ptr,subindex,3,Complex);

   case 21:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 22:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 23:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 24:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 25:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 26:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 27:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 28:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 29:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 30:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 31:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 32:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 33:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 34:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 35:
    return NIRT_GetValueByDataType(ptr,subindex,0,Complex);

   case 36:
    return (( double *)ptr)[subindex];

   case 37:
    return (( double *)ptr)[subindex];

   case 38:
    return (( double *)ptr)[subindex];

   case 39:
    return (( double *)ptr)[subindex];

   case 40:
    return (( double *)ptr)[subindex];

   case 41:
    return (( double *)ptr)[subindex];

   case 42:
    return (( double *)ptr)[subindex];

   case 43:
    return (( double *)ptr)[subindex];

   case 44:
    return (( double *)ptr)[subindex];

   case 45:
    return NIRT_GetValueByDataType(ptr,subindex,14,Complex);

   case 46:
    return NIRT_GetValueByDataType(ptr,subindex,7,Complex);

   case 47:
    return NIRT_GetValueByDataType(ptr,subindex,7,Complex);
  }

  /* return ((double *)ptr)[subindex]; */
  return rtNaN;
}

/*========================================================================*
 * Function: NIRT_SetValueByDataType
 *
 * Abstract:
 *		Converting to and from double and datatypes used in the model
 *
 * Output Parameters
 *      ptr : address to the destination
 *      subindex : index value if vector
 *      value : value to set on the destination
 *      type   : the destination's data type
 *      Complex : true if a complex data type
 *
 * Returns:
 *     Return value: 0 if no error
 *========================================================================*/
extern "C" int32_t NIRT_SetValueByDataType(void* ptr, int32_t subindex, double
  value, int32_t type, int32_t Complex)
{
  /* Complex is only used for R14.3 and down */
  switch (type)
  {
   case 0:
    ((real_T *)ptr)[subindex] = (real_T)value;
    return NI_OK;

   case 1:
    ((real32_T *)ptr)[subindex] = (real32_T)value;
    return NI_OK;

   case 2:
    ((int8_T *)ptr)[subindex] = (int8_T)value;
    return NI_OK;

   case 3:
    ((uint8_T *)ptr)[subindex] = (uint8_T)value;
    return NI_OK;

   case 4:
    ((int16_T *)ptr)[subindex] = (int16_T)value;
    return NI_OK;

   case 5:
    ((uint16_T *)ptr)[subindex] = (uint16_T)value;
    return NI_OK;

   case 6:
    ((int32_T *)ptr)[subindex] = (int32_T)value;
    return NI_OK;

   case 7:
    ((uint32_T *)ptr)[subindex] = (uint32_T)value;
    return NI_OK;

   case 8:
    ((boolean_T *)ptr)[subindex] = (boolean_T)value;
    return NI_OK;

   case 10:
    /* Type is renamed. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 6, Complex);

   case 13:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 7, Complex);

   case 14:
    /* Type is fixed-point and NiUseScaledFixedPoint is on. Call SetValueByDataType for its real type on the unscaled value. */
    return NIRT_SetValueByDataType(ptr, subindex, (value - 0.0)/(1.0 * pow(2,-31)),
      7, Complex);

   case 15:
    /* Type is fixed-point and NiUseScaledFixedPoint is on. Call SetValueByDataType for its real type on the unscaled value. */
    return NIRT_SetValueByDataType(ptr, subindex, (value - 0.0)/(1.0 * pow(2,-31)),
      17, Complex);

   case 17:
    ((uint64_T *)ptr)[subindex] = (uint64_T)value;
    return NI_OK;

   case 18:
    /* Type is enum. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 6, Complex);

   case 19:
    /* Type is enum. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 6, Complex);

   case 20:
    /* Type is renamed. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 3, Complex);

   case 21:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 22:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 23:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 24:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 25:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 26:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 27:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 28:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 29:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 30:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 31:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 32:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 33:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 34:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 35:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 0, Complex);

   case 45:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 14, Complex);

   case 46:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 7, Complex);

   case 47:
    /* Type is matrix. Call SetValueByDataType on its contained type */
    return NIRT_SetValueByDataType(ptr, subindex, value, 7, Complex);
  }

  /* No matching datatype conversion */
  return NI_ERROR;
}

extern "C"
{
  extern M220Model_v2_rtModel *S;
  extern _SITexportglobals SITexportglobals;
}
/*========================================================================*
 * Function: SetExternalInputs
 *
 * Abstract:
 *		Set data to model ports on the specified task
 *
 * Input Parameters
 *      data : data to set
 *      TaskSampleHit : task id
 *
 * Returns:
 *     Return value: 0 if no error
 *========================================================================*/
  extern "C" void SetExternalInputs(double* data, int_T* TaskSampleHit)
{
  int index = 0, count = 0;
  ExtU_M220Model_v2_T *rtIN = (ExtU_M220Model_v2_T*) S->inputs;
  if (TaskSampleHit[0]) {              /* sample and hold */
    /* rtIN->SteeringPosMsg_int Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->SteeringPosMsg_int, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold */
    /* rtIN->JoystickPosMsg_int Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->JoystickPosMsg_int, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->JoystickButton1Msg_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->JoystickButton1Msg_bool, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->JoystickButton2Msg_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->JoystickButton2Msg_bool, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->JoystickButton3Msg_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->JoystickButton3Msg_bool, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->JoystickButton4Msg_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->JoystickButton4Msg_bool, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->JoystickButton5Msg_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->JoystickButton5Msg_bool, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->JoystickButton6Msg_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->JoystickButton6Msg_bool, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->JoystickButton7Msg_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->JoystickButton7Msg_bool, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->JoystickButton8Msg_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->JoystickButton8Msg_bool, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->JoystickButton9Msg_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->JoystickButton9Msg_bool, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->JoystickButton10Msg_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->JoystickButton10Msg_bool, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold */
    /* rtIN->HMIVehicleSpeed_mph Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMIVehicleSpeed_mph, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold */
    /* rtIN->HMIVehicleSpeedOvrd_bool Type boolean_T -> 8 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMIVehicleSpeedOvrd_bool, 0, data[index++], 8,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->RlyEStarters_bool Type boolean_T -> 8 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->RlyEStarters_bool, 0, data[index++], 8, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->Rly1LightsRight_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->Rly1LightsRight_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->Rly2LightsLeft_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->Rly2LightsLeft_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->Rly3LightsRearSide_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->Rly3LightsRearSide_bool, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->Rly4LightsAux_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->Rly4LightsAux_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->Rly5FlashRight_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->Rly5FlashRight_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->Rly6FlashLeft_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->Rly6FlashLeft_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->Rly7Horn_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->Rly7Horn_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_1Spare1_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_1Spare1_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold */
    /* rtIN->Ssr2Brake_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->Ssr2Brake_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->Ssr3LoadA_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->Ssr3LoadA_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->Ssr4FloatFront_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->Ssr4FloatFront_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->Ssr5FloatRight_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->Ssr5FloatRight_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->Ssr6FloatLeft_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->Ssr6FloatLeft_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_1Spare2_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_1Spare2_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_1Spare3_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_1Spare3_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_1Spare4_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_1Spare4_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold */
    /* rtIN->RlyCPower_bool Type boolean_T -> 8 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->RlyCPower_bool, 0, data[index++], 8, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI94261Spare5_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI94261Spare5_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold */
    /* rtIN->RlyD2HydFan_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->RlyD2HydFan_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->LEDAMS_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->LEDAMS_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->LEDEnable_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->LEDEnable_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->Buzzer_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->Buzzer_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_1Spare6_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_1Spare6_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_1Spare7_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_1Spare7_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->RlyFAltEnable_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->RlyFAltEnable_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->SsrBlower_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->SsrBlower_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->BackupAlarm_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->BackupAlarm_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold */
    /* rtIN->RlyAutoStart_bool Type boolean_T -> 8 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->RlyAutoStart_bool, 0, data[index++], 8, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold */
    /* rtIN->RlyJCBEngineKill_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->RlyJCBEngineKill_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_1Spare8_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_1Spare8_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_1Spare9_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_1Spare9_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->RearEStopButton_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->RearEStopButton_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare1_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare1_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare2_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare2_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare3_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare3_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare4_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare4_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare5_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare5_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare6_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare6_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare7_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare7_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare8_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare8_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare9_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare9_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare10_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare10_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare11_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare11_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare12_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare12_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare13_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare13_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare14_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare14_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare15_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare15_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare16_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare16_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare17_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare17_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->CabEstop_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->CabEstop_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->MachineEnableSwitch_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->MachineEnableSwitch_bool, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->AutonomySwitch_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->AutonomySwitch_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare18_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare18_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->MainKeySwitch_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->MainKeySwitch_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare19_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare19_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->AutonomyRemoteStopSwitch_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->AutonomyRemoteStopSwitch_bool, 0, data[index
      ++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare20_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare20_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9476Spare21_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9476Spare21_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold */
    /* rtIN->HMIHydraulOilSensorOvrd_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMIHydraulOilSensorOvrd_bool, 0, data[index++],
      0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold */
    /* rtIN->HMIHydraulOilSensor_qts Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMIHydraulOilSensor_qts, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->HMISeatSwitchOvrd_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMISeatSwitchOvrd_bool, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->HMISeatSwitch_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMISeatSwitch_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare1_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare1_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare2_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare2_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare3_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare3_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare4_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare4_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare5_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare5_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare6_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare6_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare7_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare7_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare8_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare8_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare9_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare9_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare10_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare10_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare11_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare11_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare12_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare12_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare13_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare13_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare14_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare14_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare15_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare15_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare16_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare16_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9426_2Spare17_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9426_2Spare17_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_DriveFwd_bool Type uint32_T -> 7 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_DriveFwd_bool, 0, data[index++], 7, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_DriveReverse_bool Type uint32_T -> 7 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_DriveReverse_bool, 0, data[index++], 7, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_SteerRtA_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_SteerRtA_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_SteerLfA_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_SteerLfA_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_SteerLfB_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_SteerLfB_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_SteerRtB_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_SteerRtB_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_TracRight_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_TracRight_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_TracLeft_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_TracLeft_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_Dk_Ft_Lift_bool Type boolean_T -> 8 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_Dk_Ft_Lift_bool, 0, data[index++], 8, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_Dk_Ft_Low_bool Type boolean_T -> 8 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_Dk_Ft_Low_bool, 0, data[index++], 8, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_Dk_Rt_Lift_bool Type boolean_T -> 8 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_Dk_Rt_Lift_bool, 0, data[index++], 8, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_Dk_Rt_Low_bool Type boolean_T -> 8 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_Dk_Rt_Low_bool, 0, data[index++], 8, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_Dk_Unlatch_bool Type boolean_T -> 8 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_Dk_Unlatch_bool, 0, data[index++], 8, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_Dk_Lf_Low_bool Type boolean_T -> 8 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_Dk_Lf_Low_bool, 0, data[index++], 8, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->hv_Dk_Lf_Lift_bool Type boolean_T -> 8 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->hv_Dk_Lf_Lift_bool, 0, data[index++], 8, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9475Spare1_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9475Spare1_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9475Spare2_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9475Spare2_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9475Spare3_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9475Spare3_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9475Spare4_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9475Spare4_bool, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9264_1Spare1_int Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9264_1Spare1_int, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9264_1Spare2_int Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9264_1Spare2_int, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9264_1Spare3_int Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9264_1Spare3_int, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->NI9264_1Spare4_int Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->NI9264_1Spare4_int, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->HMIOvrdPower5VMonitor_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMIOvrdPower5VMonitor_bool, 0, data[index++],
      0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->HMIOvrdPower12VMonitor_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMIOvrdPower12VMonitor_bool, 0, data[index++],
      0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->HMIOvrdPower24VMonitor_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMIOvrdPower24VMonitor_bool, 0, data[index++],
      0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->HMIPower12VMonitor_Vdc Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMIPower12VMonitor_Vdc, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->HMIPower24VMonitor_Vdc Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMIPower24VMonitor_Vdc, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->HMIPower5VMonitor_Vdc Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMIPower5VMonitor_Vdc, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold */
    /* rtIN->HMIOvrdHeatExchangeTemp_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMIOvrdHeatExchangeTemp_bool, 0, data[index++],
      0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold */
    /* rtIN->HMIHeatExchangerTemp_degC Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMIHeatExchangerTemp_degC, 0, data[index++],
      0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold */
    /* rtIN->HeatExchangerTemp_mV Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HeatExchangerTemp_mV, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->HMIOvrdControlBoxTemp_bool Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMIOvrdControlBoxTemp_bool, 0, data[index++],
      0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->HMIControlBoxTemp_degC Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->HMIControlBoxTemp_degC, 0, data[index++], 0,
      0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold */
    /* rtIN->ControlBoxTemp_mV Type real_T -> 0 Width [1, 1] */
    NIRT_SetValueByDataType(&rtIN->ControlBoxTemp_mV, 0, data[index++], 0, 0);
  } else {               /* hold this input--skip over it in the input buffer */
    index += 1;
  }

  UNUSED_PARAMETER(count);
}                                      /* of SetExternalInputs */

extern "C" int32_t NumInputPorts(void)
{
  return 129;
}

extern "C" int32_t NumOutputPorts(void)
{
  return 128;
}

double ni_extout[128];

/*========================================================================*
 * Function: SetExternalOutputs
 *
 * Abstract:
 *		Set data to model ports on the specified task
 *
 * Input Parameters
 *      data : data to set
 *      TaskSampleHit : task id
 *
 * Returns:
 *     Return value: 0 if no error
 *========================================================================*/
extern "C" void SetExternalOutputs(double* data, int_T* TaskSampleHit)
{
  int index = 0, count = 0;
  ExtY_M220Model_v2_T* rtOUT = (ExtY_M220Model_v2_T*) S->outputs;
  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->Vy_m_sec has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->Vy_m_sec), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->Vx_m_sec has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->Vx_m_sec), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->Yr_deg_sec has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->Yr_deg_sec), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->RevFrntLftTire_RPMin has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->RevFrntLftTire_RPMin),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->RevFrntRghtTire_RPMin has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->RevFrntRghtTire_RPMin),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->SpdEnc1Line1Frq_pps has width [1, 1] type uint64_T -> 15 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SpdEnc1Line1Frq_pps),
      0, 15, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->SpdEnc1Line2Frq_int has width [1, 1] type uint64_T -> 15 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SpdEnc1Line2Frq_int),
      0, 15, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->SpdEnc2Line1Frq_pps has width [1, 1] type uint64_T -> 15 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SpdEnc2Line1Frq_pps),
      0, 15, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->SpdEnc2Line1Frq_int has width [1, 1] type uint64_T -> 15 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SpdEnc2Line1Frq_int),
      0, 15, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO4NI9475Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO4NI9475Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO5NI9475Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO5NI9475Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO6NI9475Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO6NI9475Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO7NI9475Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO7NI9475Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->LightsRight_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->LightsRight_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->LightsLeft_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->LightsLeft_bool), 0, 0,
      0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->LightsRearSide_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->LightsRearSide_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->LightsAux_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->LightsAux_bool), 0, 0,
      0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->Horn_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->Horn_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_1Spare1Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare1Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_1Spare5Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare5Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_1Spare6Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare6Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_1Spare7Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare7Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_1Spare8Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare8Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_1Spare9Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare9Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_1Spare2Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare2Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_1Spare3Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare3Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_1Spare4Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare4Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->AutonomyEnabledIndicator_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->AutonomyEnabledIndicator_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->MachineEnabledIndicator_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->MachineEnabledIndicator_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->InCabBuzzer_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->InCabBuzzer_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->Altenator408VACEnableRly_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->Altenator408VACEnableRly_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->AirKnifeRly_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->AirKnifeRly_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->BackupAlarmEnable_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->BackupAlarmEnable_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->AutoStartRelayEnable_bool has width [1, 1] type boolean_T -> 8 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->AutoStartRelayEnable_bool), 0, 8, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->JCBEngnieKill_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->JCBEngnieKill_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->RearEstopButton_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->RearEstopButton_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->LightsTurnSignalRight_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->LightsTurnSignalRight_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->LightsTurnSignalLeft_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->LightsTurnSignalLeft_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->HydraulCoolFanEnable_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->HydraulCoolFanEnable_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->RlyCPower24VEnable_bool has width [1, 1] type boolean_T -> 8 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->RlyCPower24VEnable_bool), 0, 8, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->FloatFront_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->FloatFront_bool), 0, 0,
      0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->FloatRight_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->FloatRight_bool), 0, 0,
      0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->FloatLeft_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->FloatLeft_bool), 0, 0,
      0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->Ssr2BrakeOut_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->Ssr2BrakeOut_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->LoadAHydraulPumpEnableOut_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->LoadAHydraulPumpEnableOut_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->RlyEStarter_bool has width [1, 1] type boolean_T -> 8 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->RlyEStarter_bool), 0,
      8, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DriveFwd_bool has width [1, 1] type uint32_T -> 7 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DriveFwd_bool), 0, 7,
      0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DriveReverse_bool has width [1, 1] type uint32_T -> 7 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DriveReverse_bool), 0,
      7, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->SteerRightA_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SteerRightA_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->SteerLeftA_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SteerLeftA_bool), 0, 0,
      0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->SteerLeftB_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SteerLeftB_bool), 0, 0,
      0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->SteerRightB_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SteerRightB_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->TracRight_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->TracRight_bool), 0, 0,
      0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->TracLeft_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->TracLeft_bool), 0, 0,
      0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DeckFrontLift_bool has width [1, 1] type boolean_T -> 8 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckFrontLift_bool), 0,
      8, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DeckFrontLower_bool has width [1, 1] type boolean_T -> 8 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckFrontLower_bool),
      0, 8, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DeckRightLift_bool has width [1, 1] type boolean_T -> 8 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckRightLift_bool), 0,
      8, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DeckRightLower_bool has width [1, 1] type boolean_T -> 8 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckRightLower_bool),
      0, 8, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DeckLeftLift_bool has width [1, 1] type boolean_T -> 8 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckLeftLift_bool), 0,
      8, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DeckLeftLower_bool has width [1, 1] type boolean_T -> 8 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckLeftLower_bool), 0,
      8, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare1Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare1Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare2Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare2Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare3Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare3Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare4Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare4Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare5Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare5Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare6Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare6Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare7Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare7Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare8Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare8Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare9Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare9Out_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare10Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->NI9426_2Spare10Out_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare11Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->NI9426_2Spare11Out_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare12Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->NI9426_2Spare12Out_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare13Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->NI9426_2Spare13Out_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare14Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->NI9426_2Spare14Out_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare15Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->NI9426_2Spare15Out_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare16Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->NI9426_2Spare16Out_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DeckUnlatch_bool has width [1, 1] type boolean_T -> 8 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckUnlatch_bool), 0,
      8, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO1NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO1NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->HydraulicOilLow_bool has width [1, 1] type boolean_T -> 8 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->HydraulicOilLow_bool),
      0, 8, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO3NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO3NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->RightDeckLatched_bool has width [1, 1] type boolean_T -> 8 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->RightDeckLatched_bool),
      0, 8, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->LeftDeckLatched_bool has width [1, 1] type boolean_T -> 8 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->LeftDeckLatched_bool),
      0, 8, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO5NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO5NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->FrontDeckLatched_bool has width [1, 1] type boolean_T -> 8 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->FrontDeckLatched_bool),
      0, 8, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO7NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO7NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO8NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO8NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO9NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO9NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO11NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO11NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO12NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO12NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO13NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO13NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO14NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO14NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO15NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO15NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO16NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO16NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO17NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO17NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO18NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO18NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO19NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO19NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO20NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO20NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO21NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO21NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->EStopCab_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->EStopCab_bool), 0, 0,
      0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->ElectSeatSwitch_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->ElectSeatSwitch_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->MachineEnableSeatSwitch_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->MachineEnableSeatSwitch_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->AutonomyEnableSwitch_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->AutonomyEnableSwitch_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO26NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO26NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->MainKeyOutSwitch_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->MainKeyOutSwitch_bool),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO28NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO28NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->AutonomyRemoteEstopSw_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->AutonomyRemoteEstopSw_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO30NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO30NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DO31NI9476Mon_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO31NI9476Mon_bool), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->NI9426_2Spare17Out_bool has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType
      (&(rtOUT->NI9426_2Spare17Out_bool), 0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->TorqueLeftWheel_nm has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->TorqueLeftWheel_nm), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->TorqueRightWheel_nm has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->TorqueRightWheel_nm),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->TorqueSteering_nm has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->TorqueSteering_nm), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->PowerOutMon24v_Vdc has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->PowerOutMon24v_Vdc), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->PowerOutMon12v_Vdc has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->PowerOutMon12v_Vdc), 0,
      0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->PowerOutMon5v_Vdc has width [1, 1] type real_T -> 30 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->PowerOutMon5v_Vdc), 0,
      30, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DeckLiftPFrntOut_psi has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckLiftPFrntOut_psi),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->AO0NI9264_1Mon2_int has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->AO0NI9264_1Mon2_int),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->AO0NI9264_1Mon1_int has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->AO0NI9264_1Mon1_int),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DeckLiftPRightOut_psi has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckLiftPRightOut_psi),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->DeckLiftPLftOut_psi has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckLiftPLftOut_psi),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->BrakeHydraulPrsOut_psi has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->BrakeHydraulPrsOut_psi),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[0]) {              /* sample and hold	*/
    /* rtOUT->HeatExchangeTemp_degC has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->HeatExchangeTemp_degC),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->FrontDeckAngleSns_deg has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->FrontDeckAngleSns_deg),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->RightDeckAngleSns_deg has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->RightDeckAngleSns_deg),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->LeftDeckAngleSns_deg has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->LeftDeckAngleSns_deg),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->AO0NI9264_1Mon3_int has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->AO0NI9264_1Mon3_int),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->AO0NI9264_1Mon4_int has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->AO0NI9264_1Mon4_int),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (TaskSampleHit[1]) {              /* sample and hold	*/
    /* rtOUT->ControlBoxTemp_degC has width [1, 1] type real_T -> 0 */
    ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->ControlBoxTemp_degC),
      0, 0, 0);
  } else {
    index += 1;
  }

  if (data != NULL) {
    memcpy(&data[0], &ni_extout[0], sizeof(ni_extout));
  }

  UNUSED_PARAMETER(count);
}

/*========================================================================*
 * Function: NI_InitExternalOutputs
 *
 * Abstract:
 *		Initialize model ports
 *
 * Output Parameters
 *
 * Returns:
 *     Return value: 0 if no error
 *========================================================================*/
extern "C" int32_t NI_InitExternalOutputs()
{
  int index = 0, count = 0;
  ExtY_M220Model_v2_T* rtOUT = (ExtY_M220Model_v2_T*) S->outputs;

  /* rtOUT->Vy_m_sec has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->Vy_m_sec), 0, 0, 0);

  /* rtOUT->Vx_m_sec has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->Vx_m_sec), 0, 0, 0);

  /* rtOUT->Yr_deg_sec has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->Yr_deg_sec), 0, 0, 0);

  /* rtOUT->RevFrntLftTire_RPMin has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->RevFrntLftTire_RPMin), 0,
    0, 0);

  /* rtOUT->RevFrntRghtTire_RPMin has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->RevFrntRghtTire_RPMin),
    0, 0, 0);

  /* rtOUT->SpdEnc1Line1Frq_pps has width 1 -> [1, 1] type 15 -> 15 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SpdEnc1Line1Frq_pps), 0,
    15, 0);

  /* rtOUT->SpdEnc1Line2Frq_int has width 1 -> [1, 1] type 15 -> 15 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SpdEnc1Line2Frq_int), 0,
    15, 0);

  /* rtOUT->SpdEnc2Line1Frq_pps has width 1 -> [1, 1] type 15 -> 15 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SpdEnc2Line1Frq_pps), 0,
    15, 0);

  /* rtOUT->SpdEnc2Line1Frq_int has width 1 -> [1, 1] type 15 -> 15 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SpdEnc2Line1Frq_int), 0,
    15, 0);

  /* rtOUT->DO4NI9475Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO4NI9475Mon_bool), 0, 0,
    0);

  /* rtOUT->DO5NI9475Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO5NI9475Mon_bool), 0, 0,
    0);

  /* rtOUT->DO6NI9475Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO6NI9475Mon_bool), 0, 0,
    0);

  /* rtOUT->DO7NI9475Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO7NI9475Mon_bool), 0, 0,
    0);

  /* rtOUT->LightsRight_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->LightsRight_bool), 0, 0,
    0);

  /* rtOUT->LightsLeft_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->LightsLeft_bool), 0, 0,
    0);

  /* rtOUT->LightsRearSide_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->LightsRearSide_bool), 0,
    0, 0);

  /* rtOUT->LightsAux_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->LightsAux_bool), 0, 0, 0);

  /* rtOUT->Horn_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->Horn_bool), 0, 0, 0);

  /* rtOUT->NI9426_1Spare1Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare1Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_1Spare5Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare5Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_1Spare6Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare6Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_1Spare7Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare7Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_1Spare8Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare8Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_1Spare9Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare9Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_1Spare2Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare2Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_1Spare3Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare3Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_1Spare4Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_1Spare4Out_bool),
    0, 0, 0);

  /* rtOUT->AutonomyEnabledIndicator_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType
    (&(rtOUT->AutonomyEnabledIndicator_bool), 0, 0, 0);

  /* rtOUT->MachineEnabledIndicator_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType
    (&(rtOUT->MachineEnabledIndicator_bool), 0, 0, 0);

  /* rtOUT->InCabBuzzer_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->InCabBuzzer_bool), 0, 0,
    0);

  /* rtOUT->Altenator408VACEnableRly_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType
    (&(rtOUT->Altenator408VACEnableRly_bool), 0, 0, 0);

  /* rtOUT->AirKnifeRly_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->AirKnifeRly_bool), 0, 0,
    0);

  /* rtOUT->BackupAlarmEnable_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->BackupAlarmEnable_bool),
    0, 0, 0);

  /* rtOUT->AutoStartRelayEnable_bool has width 1 -> [1, 1] type 8 -> 8 */
  ni_extout[index++] = NIRT_GetValueByDataType
    (&(rtOUT->AutoStartRelayEnable_bool), 0, 8, 0);

  /* rtOUT->JCBEngnieKill_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->JCBEngnieKill_bool), 0,
    0, 0);

  /* rtOUT->RearEstopButton_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->RearEstopButton_bool), 0,
    0, 0);

  /* rtOUT->LightsTurnSignalRight_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType
    (&(rtOUT->LightsTurnSignalRight_bool), 0, 0, 0);

  /* rtOUT->LightsTurnSignalLeft_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType
    (&(rtOUT->LightsTurnSignalLeft_bool), 0, 0, 0);

  /* rtOUT->HydraulCoolFanEnable_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType
    (&(rtOUT->HydraulCoolFanEnable_bool), 0, 0, 0);

  /* rtOUT->RlyCPower24VEnable_bool has width 1 -> [1, 1] type 8 -> 8 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->RlyCPower24VEnable_bool),
    0, 8, 0);

  /* rtOUT->FloatFront_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->FloatFront_bool), 0, 0,
    0);

  /* rtOUT->FloatRight_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->FloatRight_bool), 0, 0,
    0);

  /* rtOUT->FloatLeft_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->FloatLeft_bool), 0, 0, 0);

  /* rtOUT->Ssr2BrakeOut_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->Ssr2BrakeOut_bool), 0, 0,
    0);

  /* rtOUT->LoadAHydraulPumpEnableOut_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType
    (&(rtOUT->LoadAHydraulPumpEnableOut_bool), 0, 0, 0);

  /* rtOUT->RlyEStarter_bool has width 1 -> [1, 1] type 8 -> 8 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->RlyEStarter_bool), 0, 8,
    0);

  /* rtOUT->DriveFwd_bool has width 1 -> [1, 1] type 7 -> 7 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DriveFwd_bool), 0, 7, 0);

  /* rtOUT->DriveReverse_bool has width 1 -> [1, 1] type 7 -> 7 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DriveReverse_bool), 0, 7,
    0);

  /* rtOUT->SteerRightA_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SteerRightA_bool), 0, 0,
    0);

  /* rtOUT->SteerLeftA_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SteerLeftA_bool), 0, 0,
    0);

  /* rtOUT->SteerLeftB_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SteerLeftB_bool), 0, 0,
    0);

  /* rtOUT->SteerRightB_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->SteerRightB_bool), 0, 0,
    0);

  /* rtOUT->TracRight_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->TracRight_bool), 0, 0, 0);

  /* rtOUT->TracLeft_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->TracLeft_bool), 0, 0, 0);

  /* rtOUT->DeckFrontLift_bool has width 1 -> [1, 1] type 8 -> 8 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckFrontLift_bool), 0,
    8, 0);

  /* rtOUT->DeckFrontLower_bool has width 1 -> [1, 1] type 8 -> 8 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckFrontLower_bool), 0,
    8, 0);

  /* rtOUT->DeckRightLift_bool has width 1 -> [1, 1] type 8 -> 8 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckRightLift_bool), 0,
    8, 0);

  /* rtOUT->DeckRightLower_bool has width 1 -> [1, 1] type 8 -> 8 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckRightLower_bool), 0,
    8, 0);

  /* rtOUT->DeckLeftLift_bool has width 1 -> [1, 1] type 8 -> 8 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckLeftLift_bool), 0, 8,
    0);

  /* rtOUT->DeckLeftLower_bool has width 1 -> [1, 1] type 8 -> 8 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckLeftLower_bool), 0,
    8, 0);

  /* rtOUT->NI9426_2Spare1Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare1Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare2Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare2Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare3Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare3Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare4Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare4Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare5Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare5Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare6Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare6Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare7Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare7Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare8Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare8Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare9Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare9Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare10Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare10Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare11Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare11Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare12Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare12Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare13Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare13Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare14Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare14Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare15Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare15Out_bool),
    0, 0, 0);

  /* rtOUT->NI9426_2Spare16Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare16Out_bool),
    0, 0, 0);

  /* rtOUT->DeckUnlatch_bool has width 1 -> [1, 1] type 8 -> 8 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckUnlatch_bool), 0, 8,
    0);

  /* rtOUT->DO1NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO1NI9476Mon_bool), 0, 0,
    0);

  /* rtOUT->HydraulicOilLow_bool has width 1 -> [1, 1] type 8 -> 8 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->HydraulicOilLow_bool), 0,
    8, 0);

  /* rtOUT->DO3NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO3NI9476Mon_bool), 0, 0,
    0);

  /* rtOUT->RightDeckLatched_bool has width 1 -> [1, 1] type 8 -> 8 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->RightDeckLatched_bool),
    0, 8, 0);

  /* rtOUT->LeftDeckLatched_bool has width 1 -> [1, 1] type 8 -> 8 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->LeftDeckLatched_bool), 0,
    8, 0);

  /* rtOUT->DO5NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO5NI9476Mon_bool), 0, 0,
    0);

  /* rtOUT->FrontDeckLatched_bool has width 1 -> [1, 1] type 8 -> 8 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->FrontDeckLatched_bool),
    0, 8, 0);

  /* rtOUT->DO7NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO7NI9476Mon_bool), 0, 0,
    0);

  /* rtOUT->DO8NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO8NI9476Mon_bool), 0, 0,
    0);

  /* rtOUT->DO9NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO9NI9476Mon_bool), 0, 0,
    0);

  /* rtOUT->DO11NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO11NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->DO12NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO12NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->DO13NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO13NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->DO14NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO14NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->DO15NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO15NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->DO16NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO16NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->DO17NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO17NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->DO18NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO18NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->DO19NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO19NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->DO20NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO20NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->DO21NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO21NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->EStopCab_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->EStopCab_bool), 0, 0, 0);

  /* rtOUT->ElectSeatSwitch_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->ElectSeatSwitch_bool), 0,
    0, 0);

  /* rtOUT->MachineEnableSeatSwitch_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType
    (&(rtOUT->MachineEnableSeatSwitch_bool), 0, 0, 0);

  /* rtOUT->AutonomyEnableSwitch_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType
    (&(rtOUT->AutonomyEnableSwitch_bool), 0, 0, 0);

  /* rtOUT->DO26NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO26NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->MainKeyOutSwitch_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->MainKeyOutSwitch_bool),
    0, 0, 0);

  /* rtOUT->DO28NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO28NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->AutonomyRemoteEstopSw_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType
    (&(rtOUT->AutonomyRemoteEstopSw_bool), 0, 0, 0);

  /* rtOUT->DO30NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO30NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->DO31NI9476Mon_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DO31NI9476Mon_bool), 0,
    0, 0);

  /* rtOUT->NI9426_2Spare17Out_bool has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->NI9426_2Spare17Out_bool),
    0, 0, 0);

  /* rtOUT->TorqueLeftWheel_nm has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->TorqueLeftWheel_nm), 0,
    0, 0);

  /* rtOUT->TorqueRightWheel_nm has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->TorqueRightWheel_nm), 0,
    0, 0);

  /* rtOUT->TorqueSteering_nm has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->TorqueSteering_nm), 0, 0,
    0);

  /* rtOUT->PowerOutMon24v_Vdc has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->PowerOutMon24v_Vdc), 0,
    0, 0);

  /* rtOUT->PowerOutMon12v_Vdc has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->PowerOutMon12v_Vdc), 0,
    0, 0);

  /* rtOUT->PowerOutMon5v_Vdc has width 1 -> [1, 1] type 30 -> 30 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->PowerOutMon5v_Vdc), 0,
    30, 0);

  /* rtOUT->DeckLiftPFrntOut_psi has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckLiftPFrntOut_psi), 0,
    0, 0);

  /* rtOUT->AO0NI9264_1Mon2_int has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->AO0NI9264_1Mon2_int), 0,
    0, 0);

  /* rtOUT->AO0NI9264_1Mon1_int has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->AO0NI9264_1Mon1_int), 0,
    0, 0);

  /* rtOUT->DeckLiftPRightOut_psi has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckLiftPRightOut_psi),
    0, 0, 0);

  /* rtOUT->DeckLiftPLftOut_psi has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->DeckLiftPLftOut_psi), 0,
    0, 0);

  /* rtOUT->BrakeHydraulPrsOut_psi has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->BrakeHydraulPrsOut_psi),
    0, 0, 0);

  /* rtOUT->HeatExchangeTemp_degC has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->HeatExchangeTemp_degC),
    0, 0, 0);

  /* rtOUT->FrontDeckAngleSns_deg has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->FrontDeckAngleSns_deg),
    0, 0, 0);

  /* rtOUT->RightDeckAngleSns_deg has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->RightDeckAngleSns_deg),
    0, 0, 0);

  /* rtOUT->LeftDeckAngleSns_deg has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->LeftDeckAngleSns_deg), 0,
    0, 0);

  /* rtOUT->AO0NI9264_1Mon3_int has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->AO0NI9264_1Mon3_int), 0,
    0, 0);

  /* rtOUT->AO0NI9264_1Mon4_int has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->AO0NI9264_1Mon4_int), 0,
    0, 0);

  /* rtOUT->ControlBoxTemp_degC has width 1 -> [1, 1] type 0 -> 0 */
  ni_extout[index++] = NIRT_GetValueByDataType(&(rtOUT->ControlBoxTemp_degC), 0,
    0, 0);
  UNUSED_PARAMETER(count);
  return NI_OK;
}

/* All elements by default (including scalars) have 2 dimensions [1,1] */
static NI_Parameter NI_ParamList[] DataSection(".NIVS.paramlist") =
{
  { 0, "", 0, 0, 0, 0, 0, 0 }
};

static int32_t NI_ParamListSize DataSection(".NIVS.paramlistsize") = 0;
static int32_t NI_ParamDimList[] DataSection(".NIVS.paramdimlist") =
{
  0
};

static NI_Signal NI_SigList[] DataSection(".NIVS.siglist") =
{
  { 0, "m220model_v2/SteeringPosMsg_int", 0, "", offsetof(ExtU_M220Model_v2_T,
    SteeringPosMsg_int) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 1, "m220model_v2/JoystickPosMsg_int", 0, "", offsetof(ExtU_M220Model_v2_T,
    JoystickPosMsg_int) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 2, "m220model_v2/JoystickButton1Msg_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, JoystickButton1Msg_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 3, "m220model_v2/JoystickButton2Msg_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, JoystickButton2Msg_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 4, "m220model_v2/JoystickButton3Msg_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, JoystickButton3Msg_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 5, "m220model_v2/JoystickButton4Msg_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, JoystickButton4Msg_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 6, "m220model_v2/JoystickButton5Msg_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, JoystickButton5Msg_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 7, "m220model_v2/JoystickButton6Msg_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, JoystickButton6Msg_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 8, "m220model_v2/JoystickButton7Msg_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, JoystickButton7Msg_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 9, "m220model_v2/JoystickButton8Msg_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, JoystickButton8Msg_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 10, "m220model_v2/JoystickButton9Msg_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, JoystickButton9Msg_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 11, "m220model_v2/JoystickButton10Msg_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, JoystickButton10Msg_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 12, "m220model_v2/HMIVehicleSpeed_mph", 0, "", offsetof(ExtU_M220Model_v2_T,
    HMIVehicleSpeed_mph) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 13, "m220model_v2/HMIVehicleSpeedOvrd_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, HMIVehicleSpeedOvrd_bool) + (0*sizeof(boolean_T)),
    EXTIO_SIG, 8, 1, 2, 0, 0 },

  { 14, "m220model_v2/RlyEStarters_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    RlyEStarters_bool) + (0*sizeof(boolean_T)), EXTIO_SIG, 8, 1, 2, 0, 0 },

  { 15, "m220model_v2/Rly1LightsRight_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    Rly1LightsRight_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 16, "m220model_v2/Rly2LightsLeft_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    Rly2LightsLeft_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 17, "m220model_v2/Rly3LightsRearSide_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, Rly3LightsRearSide_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 18, "m220model_v2/Rly4LightsAux_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    Rly4LightsAux_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 19, "m220model_v2/Rly5FlashRight_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    Rly5FlashRight_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 20, "m220model_v2/Rly6FlashLeft_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    Rly6FlashLeft_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 21, "m220model_v2/Rly7Horn_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    Rly7Horn_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 22, "m220model_v2/NI9426_1Spare1_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_1Spare1_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 23, "m220model_v2/Ssr2Brake_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    Ssr2Brake_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 24, "m220model_v2/Ssr3LoadA_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    Ssr3LoadA_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 25, "m220model_v2/Ssr4FloatFront_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    Ssr4FloatFront_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 26, "m220model_v2/Ssr5FloatRight_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    Ssr5FloatRight_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 27, "m220model_v2/Ssr6FloatLeft_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    Ssr6FloatLeft_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 28, "m220model_v2/NI9426_1Spare2_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_1Spare2_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 29, "m220model_v2/NI9426_1Spare3_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_1Spare3_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 30, "m220model_v2/NI9426_1Spare4_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_1Spare4_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 31, "m220model_v2/RlyCPower_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    RlyCPower_bool) + (0*sizeof(boolean_T)), EXTIO_SIG, 8, 1, 2, 0, 0 },

  { 32, "m220model_v2/NI94261Spare5_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI94261Spare5_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 33, "m220model_v2/RlyD2HydFan_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    RlyD2HydFan_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 34, "m220model_v2/LEDAMS_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    LEDAMS_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 35, "m220model_v2/LEDEnable_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    LEDEnable_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 36, "m220model_v2/Buzzer_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    Buzzer_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 37, "m220model_v2/NI9426_1Spare6_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_1Spare6_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 38, "m220model_v2/NI9426_1Spare7_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_1Spare7_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 39, "m220model_v2/RlyFAltEnable_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    RlyFAltEnable_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 40, "m220model_v2/SsrBlower_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    SsrBlower_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 41, "m220model_v2/BackupAlarm_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    BackupAlarm_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 42, "m220model_v2/RlyAutoStart_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    RlyAutoStart_bool) + (0*sizeof(boolean_T)), EXTIO_SIG, 8, 1, 2, 0, 0 },

  { 43, "m220model_v2/RlyJCBEngineKill_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, RlyJCBEngineKill_bool) + (0*sizeof(real_T)), EXTIO_SIG,
    0, 1, 2, 0, 0 },

  { 44, "m220model_v2/NI9426_1Spare8_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_1Spare8_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 45, "m220model_v2/NI9426_1Spare9_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_1Spare9_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 46, "m220model_v2/RearEStopButton_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    RearEStopButton_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 47, "m220model_v2/NI9476Spare1_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare1_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 48, "m220model_v2/NI9476Spare2_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare2_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 49, "m220model_v2/NI9476Spare3_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare3_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 50, "m220model_v2/NI9476Spare4_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare4_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 51, "m220model_v2/NI9476Spare5_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare5_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 52, "m220model_v2/NI9476Spare6_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare6_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 53, "m220model_v2/NI9476Spare7_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare7_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 54, "m220model_v2/NI9476Spare8_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare8_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 55, "m220model_v2/NI9476Spare9_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare9_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 56, "m220model_v2/NI9476Spare10_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare10_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 57, "m220model_v2/NI9476Spare11_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare11_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 58, "m220model_v2/NI9476Spare12_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare12_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 59, "m220model_v2/NI9476Spare13_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare13_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 60, "m220model_v2/NI9476Spare14_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare14_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 61, "m220model_v2/NI9476Spare15_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare15_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 62, "m220model_v2/NI9476Spare16_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare16_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 63, "m220model_v2/NI9476Spare17_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare17_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 64, "m220model_v2/CabEstop_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    CabEstop_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 65, "m220model_v2/MachineEnableSwitch_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, MachineEnableSwitch_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 66, "m220model_v2/AutonomySwitch_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    AutonomySwitch_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 67, "m220model_v2/NI9476Spare18_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare18_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 68, "m220model_v2/MainKeySwitch_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    MainKeySwitch_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 69, "m220model_v2/NI9476Spare19_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare19_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 70, "m220model_v2/AutonomyRemoteStopSwitch_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, AutonomyRemoteStopSwitch_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 71, "m220model_v2/NI9476Spare20_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare20_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 72, "m220model_v2/NI9476Spare21_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9476Spare21_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 73, "m220model_v2/HMIHydraulOilSensorOvrd_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, HMIHydraulOilSensorOvrd_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 74, "m220model_v2/HMIHydraulOilSensor_qts", 0, "", offsetof
    (ExtU_M220Model_v2_T, HMIHydraulOilSensor_qts) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 75, "m220model_v2/HMISeatSwitchOvrd_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, HMISeatSwitchOvrd_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 76, "m220model_v2/HMISeatSwitch_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    HMISeatSwitch_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 77, "m220model_v2/NI9426_2Spare1_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare1_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 78, "m220model_v2/NI9426_2Spare2_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare2_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 79, "m220model_v2/NI9426_2Spare3_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare3_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 80, "m220model_v2/NI9426_2Spare4_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare4_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 81, "m220model_v2/NI9426_2Spare5_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare5_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 82, "m220model_v2/NI9426_2Spare6_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare6_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 83, "m220model_v2/NI9426_2Spare7_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare7_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 84, "m220model_v2/NI9426_2Spare8_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare8_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 85, "m220model_v2/NI9426_2Spare9_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare9_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 86, "m220model_v2/NI9426_2Spare10_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare10_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 87, "m220model_v2/NI9426_2Spare11_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare11_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 88, "m220model_v2/NI9426_2Spare12_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare12_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 89, "m220model_v2/NI9426_2Spare13_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare13_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 90, "m220model_v2/NI9426_2Spare14_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare14_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 91, "m220model_v2/NI9426_2Spare15_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare15_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 92, "m220model_v2/NI9426_2Spare16_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare16_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 93, "m220model_v2/NI9426_2Spare17_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9426_2Spare17_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 94, "m220model_v2/hv_DriveFwd_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_DriveFwd_bool) + (0*sizeof(uint32_T)), EXTIO_SIG, 7, 1, 2, 0, 0 },

  { 95, "m220model_v2/hv_DriveReverse_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_DriveReverse_bool) + (0*sizeof(uint32_T)), EXTIO_SIG, 7, 1, 2, 0, 0 },

  { 96, "m220model_v2/hv_SteerRtA_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_SteerRtA_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 97, "m220model_v2/hv_SteerLfA_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_SteerLfA_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 98, "m220model_v2/hv_SteerLfB_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_SteerLfB_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 99, "m220model_v2/hv_SteerRtB_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_SteerRtB_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 100, "m220model_v2/hv_TracRight_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_TracRight_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 101, "m220model_v2/hv_TracLeft_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_TracLeft_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 102, "m220model_v2/hv_Dk_Ft_Lift_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_Dk_Ft_Lift_bool) + (0*sizeof(boolean_T)), EXTIO_SIG, 8, 1, 2, 0, 0 },

  { 103, "m220model_v2/hv_Dk_Ft_Low_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_Dk_Ft_Low_bool) + (0*sizeof(boolean_T)), EXTIO_SIG, 8, 1, 2, 0, 0 },

  { 104, "m220model_v2/hv_Dk_Rt_Lift_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_Dk_Rt_Lift_bool) + (0*sizeof(boolean_T)), EXTIO_SIG, 8, 1, 2, 0, 0 },

  { 105, "m220model_v2/hv_Dk_Rt_Low_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_Dk_Rt_Low_bool) + (0*sizeof(boolean_T)), EXTIO_SIG, 8, 1, 2, 0, 0 },

  { 106, "m220model_v2/hv_Dk_Unlatch_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_Dk_Unlatch_bool) + (0*sizeof(boolean_T)), EXTIO_SIG, 8, 1, 2, 0, 0 },

  { 107, "m220model_v2/hv_Dk_Lf_Low_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_Dk_Lf_Low_bool) + (0*sizeof(boolean_T)), EXTIO_SIG, 8, 1, 2, 0, 0 },

  { 108, "m220model_v2/hv_Dk_Lf_Lift_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    hv_Dk_Lf_Lift_bool) + (0*sizeof(boolean_T)), EXTIO_SIG, 8, 1, 2, 0, 0 },

  { 109, "m220model_v2/NI9475Spare1_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9475Spare1_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 110, "m220model_v2/NI9475Spare2_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9475Spare2_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 111, "m220model_v2/NI9475Spare3_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9475Spare3_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 112, "m220model_v2/NI9475Spare4_bool", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9475Spare4_bool) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 113, "m220model_v2/NI9264_1Spare1_int", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9264_1Spare1_int) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 114, "m220model_v2/NI9264_1Spare2_int", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9264_1Spare2_int) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 115, "m220model_v2/NI9264_1Spare3_int", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9264_1Spare3_int) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 116, "m220model_v2/NI9264_1Spare4_int", 0, "", offsetof(ExtU_M220Model_v2_T,
    NI9264_1Spare4_int) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 117, "m220model_v2/HMIOvrdPower5VMonitor_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, HMIOvrdPower5VMonitor_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 118, "m220model_v2/HMIOvrdPower12VMonitor_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, HMIOvrdPower12VMonitor_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 119, "m220model_v2/HMIOvrdPower24VMonitor_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, HMIOvrdPower24VMonitor_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 120, "m220model_v2/HMIPower12VMonitor_Vdc", 0, "", offsetof
    (ExtU_M220Model_v2_T, HMIPower12VMonitor_Vdc) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 121, "m220model_v2/HMIPower24VMonitor_Vdc", 0, "", offsetof
    (ExtU_M220Model_v2_T, HMIPower24VMonitor_Vdc) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 122, "m220model_v2/HMIPower5VMonitor_Vdc", 0, "", offsetof
    (ExtU_M220Model_v2_T, HMIPower5VMonitor_Vdc) + (0*sizeof(real_T)), EXTIO_SIG,
    0, 1, 2, 0, 0 },

  { 123, "m220model_v2/HMIOvrdHeatExchangeTemp_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, HMIOvrdHeatExchangeTemp_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 124, "m220model_v2/HMIHeatExchangerTemp_degC", 0, "", offsetof
    (ExtU_M220Model_v2_T, HMIHeatExchangerTemp_degC) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 125, "m220model_v2/HeatExchangerTemp_mV", 0, "", offsetof
    (ExtU_M220Model_v2_T, HeatExchangerTemp_mV) + (0*sizeof(real_T)), EXTIO_SIG,
    0, 1, 2, 0, 0 },

  { 126, "m220model_v2/HMIOvrdControlBoxTemp_bool", 0, "", offsetof
    (ExtU_M220Model_v2_T, HMIOvrdControlBoxTemp_bool) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 127, "m220model_v2/HMIControlBoxTemp_degC", 0, "", offsetof
    (ExtU_M220Model_v2_T, HMIControlBoxTemp_degC) + (0*sizeof(real_T)),
    EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 128, "m220model_v2/ControlBoxTemp_mV", 0, "", offsetof(ExtU_M220Model_v2_T,
    ControlBoxTemp_mV) + (0*sizeof(real_T)), EXTIO_SIG, 0, 1, 2, 0, 0 },

  { 129, "m220model_v2/NI9475Subsystem/Gain", 0, "", offsetof(B_M220Model_v2_T,
    Gain) + (0*sizeof(uint64_T)), BLOCKIO_SIG, 15, 1, 2, 0, 0 },

  { 130, "m220model_v2/NI9475Subsystem/Gain2", 0, "", offsetof(B_M220Model_v2_T,
    Gain2) + (0*sizeof(uint64_T)), BLOCKIO_SIG, 15, 1, 2, 0, 0 },

  { 131, "m220model_v2/M220DynamicsModel/Integrator2", 0, "", offsetof
    (B_M220Model_v2_T, Integrator2) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 132, "m220model_v2/M220DynamicsModel/Integrator1", 0, "", offsetof
    (B_M220Model_v2_T, Integrator1) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 133, "m220model_v2/M220DynamicsModel/Integrator4", 0, "", offsetof
    (B_M220Model_v2_T, Integrator4) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 134, "m220model_v2/M220DynamicsModel/Saturation3", 0, "", offsetof
    (B_M220Model_v2_T, Saturation3) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 135, "m220model_v2/M220DynamicsModel/Radians to Degrees/Gain", 0, "",
    offsetof(B_M220Model_v2_T, Gain_b) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1,
    2, 0, 0 },

  { 136, "m220model_v2/OdometrySubsystem/Math Function", 0, "", offsetof
    (B_M220Model_v2_T, MathFunction) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 137, "m220model_v2/OdometrySubsystem/Math Function1", 0, "", offsetof
    (B_M220Model_v2_T, MathFunction1) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 138, "m220model_v2/M220DynamicsModel/Integrator5", 0, "", offsetof
    (B_M220Model_v2_T, Integrator5) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 139, "m220model_v2/OdometrySubsystem/Math Function2", 0, "", offsetof
    (B_M220Model_v2_T, MathFunction2) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 140, "m220model_v2/OdometrySubsystem/Add", 0, "", offsetof(B_M220Model_v2_T,
    Add) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 141, "m220model_v2/OdometrySubsystem/Sqrt", 0, "", offsetof(B_M220Model_v2_T,
    Sqrt) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 142, "m220model_v2/OdometrySubsystem/Switch", 0, "", offsetof
    (B_M220Model_v2_T, Switch) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 143, "m220model_v2/OdometrySubsystem/Divide", 0, "", offsetof
    (B_M220Model_v2_T, Divide) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 144, "m220model_v2/OdometrySubsystem/Product5", 0, "", offsetof
    (B_M220Model_v2_T, Product5) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 145, "m220model_v2/OdometrySubsystem/Divide2", 0, "", offsetof
    (B_M220Model_v2_T, Divide2) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 146, "m220model_v2/OdometrySubsystem/Product2", 0, "", offsetof
    (B_M220Model_v2_T, Product2) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 147, "m220model_v2/OdometrySubsystem/Divide1", 0, "", offsetof
    (B_M220Model_v2_T, Divide1) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 148, "m220model_v2/OdometrySubsystem/Saturation", 0, "", offsetof
    (B_M220Model_v2_T, Saturation) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 149, "m220model_v2/M220TurnSignalLightsSubsystem/Pulse Generator", 0, "",
    offsetof(B_M220Model_v2_T, PulseGenerator) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 150, "m220model_v2/M220TurnSignalLightsSubsystem/Pulse Generator1", 0, "",
    offsetof(B_M220Model_v2_T, PulseGenerator1) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 151, "m220model_v2/M220HydraulicOil/Square", 0, "", offsetof
    (B_M220Model_v2_T, Square) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 152, "m220model_v2/M220HydraulicOil/Square1", 0, "", offsetof
    (B_M220Model_v2_T, Square1) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 153, "m220model_v2/M220HydraulicOil/Square2", 0, "", offsetof
    (B_M220Model_v2_T, Square2) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 154, "m220model_v2/M220HydraulicOil/Switch", 0, "", offsetof
    (B_M220Model_v2_T, Switch_e) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 155, "m220model_v2/ConvertVehicleVel2Torques/Product2", 0, "", offsetof
    (B_M220Model_v2_T, Product2_g) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 156, "m220model_v2/ConvertVehicleVel2Torques/Divide1", 0, "", offsetof
    (B_M220Model_v2_T, Divide1_i) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 157, "m220model_v2/ConvertVehicleVel2Torques/Product3", 0, "", offsetof
    (B_M220Model_v2_T, Product3) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 158, "m220model_v2/ConvertVehicleVel2Torques/Divide2", 0, "", offsetof
    (B_M220Model_v2_T, Divide2_j) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 159, "m220model_v2/ConvertVehicleVel2Torques/Math Function", 0, "", offsetof
    (B_M220Model_v2_T, MathFunction_d) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1,
    2, 0, 0 },

  { 160, "m220model_v2/ConvertVehicleVel2Torques/Math Function1", 0, "",
    offsetof(B_M220Model_v2_T, MathFunction1_n) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 161, "m220model_v2/ConvertVehicleVel2Torques/Add", 0, "", offsetof
    (B_M220Model_v2_T, Add_h) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 162, "m220model_v2/ConvertVehicleVel2Torques/Sqrt", 0, "", offsetof
    (B_M220Model_v2_T, Sqrt_a) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 163, "m220model_v2/ConvertVehicleVel2Torques/Derivative", 0, "", offsetof
    (B_M220Model_v2_T, Derivative) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 164, "m220model_v2/ConvertVehicleVel2Torques/Product", 0, "", offsetof
    (B_M220Model_v2_T, Product) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 165, "m220model_v2/M220DynamicsModel/Integrator11", 0, "", offsetof
    (B_M220Model_v2_T, Integrator11) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 166, "m220model_v2/ConvertVehicleVel2Torques/Cos", 0, "", offsetof
    (B_M220Model_v2_T, Cos) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 167, "m220model_v2/ConvertVehicleVel2Torques/Divide", 0, "", offsetof
    (B_M220Model_v2_T, Divide_g) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 168, "m220model_v2/ConvertVehicleVel2Torques/Product1", 0, "", offsetof
    (B_M220Model_v2_T, Product1) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 169, "m220model_v2/HydraulicLiftPressureSubsystem/Lookup Table Dynamic3", 0,
    "", offsetof(B_M220Model_v2_T, LookupTableDynamic3) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 170, "m220model_v2/M220HeatExchangerTempSubsystem/Switch", 0, "", offsetof
    (B_M220Model_v2_T, Switch_j) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 171, "m220model_v2/SteeringAndJoyVelocityConvert/Integrator", 0, "",
    offsetof(B_M220Model_v2_T, Integrator) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 172, "m220model_v2/SteeringAndJoyVelocityConvert/Trigonometric Function1", 0,
    "", offsetof(B_M220Model_v2_T, TrigonometricFunction1) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 173, "m220model_v2/M220MachineEnableFunction/Switch", 0, "", offsetof
    (B_M220Model_v2_T, Switch_d) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 174, "m220model_v2/SteeringAndJoyVelocityConvert/Product1", 0, "", offsetof
    (B_M220Model_v2_T, Product1_l) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 175,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipRatioTire1/Divide2",
    0, "", offsetof(B_M220Model_v2_T, Divide2_p) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 176,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipRatioTire1/Product1",
    0, "", offsetof(B_M220Model_v2_T, Product1_i) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 177, "m220model_v2/NI9853_1 Subsystem/Lookup Table Dynamic", 0, "", offsetof
    (B_M220Model_v2_T, LookupTableDynamic_h) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 178, "m220model_v2/SteeringAndJoyVelocityConvert/Trigonometric Function2", 0,
    "", offsetof(B_M220Model_v2_T, TrigonometricFunction2) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 179, "m220model_v2/SteeringAndJoyVelocityConvert/Product2", 0, "", offsetof
    (B_M220Model_v2_T, Product2_j) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 180, "m220model_v2/SteeringAndJoyVelocityConvert/Divide", 0, "", offsetof
    (B_M220Model_v2_T, Divide_c) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 181,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipRatioTire1/Product",
    0, "", offsetof(B_M220Model_v2_T, Product_f) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 182,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipRatioTire1/Add",
    0, "", offsetof(B_M220Model_v2_T, Add_m) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 183,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipRatioTire1/Add1",
    0, "", offsetof(B_M220Model_v2_T, Add1) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 184,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipRatioTire1/Switch",
    0, "", offsetof(B_M220Model_v2_T, Switch_g) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 185, "m220model_v2/4TireTractionForces/Tire1TractionForces/Add", 0, "",
    offsetof(B_M220Model_v2_T, Add_n) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 186, "m220model_v2/SteeringAndJoyVelocityConvert/Trigonometric Function", 0,
    "", offsetof(B_M220Model_v2_T, TrigonometricFunction) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 187, "m220model_v2/SteeringAndJoyVelocityConvert/Product", 0, "", offsetof
    (B_M220Model_v2_T, Product_c) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 188,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipAngleTire1/Product",
    0, "", offsetof(B_M220Model_v2_T, Product_m) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 189,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipAngleTire1/Subtract",
    0, "", offsetof(B_M220Model_v2_T, Subtract) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 190,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipAngleTire1/Product2",
    0, "", offsetof(B_M220Model_v2_T, Product2_c) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 191,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipAngleTire1/Subtract1",
    0, "", offsetof(B_M220Model_v2_T, Subtract1) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 192,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipAngleTire1/Divide",
    0, "", offsetof(B_M220Model_v2_T, Divide_f) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 193,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipAngleTire1/Trigonometric Function",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction_c) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 194,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipAngleTire1/Product1",
    0, "", offsetof(B_M220Model_v2_T, Product1_o) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 195,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/Trigonometric Function",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction_a) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 196, "m220model_v2/4TireTractionForces/Tire1TractionForces/Divide1", 0, "",
    offsetof(B_M220Model_v2_T, Divide1_o) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 197, "m220model_v2/4TireTractionForces/Tire1TractionForces/Divide2", 0, "",
    offsetof(B_M220Model_v2_T, Divide2_m) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 198,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/Add",
    0, "", offsetof(B_M220Model_v2_T, Add_j) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 199,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/VerticalTire1ForcePitchRollWeightTransfer/Derivative",
    0, "", offsetof(B_M220Model_v2_T, Derivative_h) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 200,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/VerticalTire1ForcePitchRollWeightTransfer/Product2",
    0, "", offsetof(B_M220Model_v2_T, Product2_n) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 201,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/VerticalTire1ForcePitchRollWeightTransfer/Divide3",
    0, "", offsetof(B_M220Model_v2_T, Divide3) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 202,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/VerticalTire1ForcePitchRollWeightTransfer/Derivative1",
    0, "", offsetof(B_M220Model_v2_T, Derivative1) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 203,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/VerticalTire1ForcePitchRollWeightTransfer/Product5",
    0, "", offsetof(B_M220Model_v2_T, Product5_a) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 204,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/VerticalTire1ForcePitchRollWeightTransfer/Divide1",
    0, "", offsetof(B_M220Model_v2_T, Divide1_n) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 205, "m220model_v2/Derivative", 0, "", offsetof(B_M220Model_v2_T,
    Derivative_o) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 206,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/VerticalTire1ForcePitchRollWeightTransfer/Product3",
    0, "", offsetof(B_M220Model_v2_T, Product3_k) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 207,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/VerticalTire1ForcePitchRollWeightTransfer/Add1",
    0, "", offsetof(B_M220Model_v2_T, Add1_g) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 208,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/Product1",
    0, "", offsetof(B_M220Model_v2_T, Product1_n) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 209,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/Product3",
    0, "", offsetof(B_M220Model_v2_T, Product3_g) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 210,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/Square",
    0, "", offsetof(B_M220Model_v2_T, Square_d) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 211,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/Trigonometric Function",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction_h) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 212,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/Product4",
    0, "", offsetof(B_M220Model_v2_T, Product4) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 213,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/Square1",
    0, "", offsetof(B_M220Model_v2_T, Square1_g) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 214,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/Add1",
    0, "", offsetof(B_M220Model_v2_T, Add1_n) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 215,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/Sqrt",
    0, "", offsetof(B_M220Model_v2_T, Sqrt_n) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 216,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/Product2",
    0, "", offsetof(B_M220Model_v2_T, Product2_i) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 217,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/Divide",
    0, "", offsetof(B_M220Model_v2_T, Divide_m) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 218, "m220model_v2/4TireTractionForces/Tire1TractionForces/Switch", 0, "",
    offsetof(B_M220Model_v2_T, Switch_jk) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 219, "m220model_v2/4TireTractionForces/Tire1TractionForces/Product1", 0, "",
    offsetof(B_M220Model_v2_T, Product1_ob) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 220, "m220model_v2/4TireTractionForces/Tire1TractionForces/Product2", 0, "",
    offsetof(B_M220Model_v2_T, Product2_h) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 221,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipRatioTire2/Divide2",
    0, "", offsetof(B_M220Model_v2_T, Divide2_c) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 222,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipRatioTire2/Product1",
    0, "", offsetof(B_M220Model_v2_T, Product1_f) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 223,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipRatioTire2/Product",
    0, "", offsetof(B_M220Model_v2_T, Product_i) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 224,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipRatioTire2/Add",
    0, "", offsetof(B_M220Model_v2_T, Add_p) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 225,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipRatioTire2/Add1",
    0, "", offsetof(B_M220Model_v2_T, Add1_d) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 226,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipRatioTire2/Switch",
    0, "", offsetof(B_M220Model_v2_T, Switch_bb) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 227, "m220model_v2/4TireTractionForces/Tire2TractionForces/Add", 0, "",
    offsetof(B_M220Model_v2_T, Add_c) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 228,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipAngleTire2/Product",
    0, "", offsetof(B_M220Model_v2_T, Product_p) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 229,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipAngleTire2/Subtract",
    0, "", offsetof(B_M220Model_v2_T, Subtract_g) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 230,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipAngleTire2/Product2",
    0, "", offsetof(B_M220Model_v2_T, Product2_e) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 231,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipAngleTire2/Subtract1",
    0, "", offsetof(B_M220Model_v2_T, Subtract1_d) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 232,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipAngleTire2/Divide",
    0, "", offsetof(B_M220Model_v2_T, Divide_n) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 233,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipAngleTire2/Trigonometric Function",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction_f) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 234,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipAngleTire2/Product1",
    0, "", offsetof(B_M220Model_v2_T, Product1_j) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 235,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/Trigonometric Function",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction_n) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 236, "m220model_v2/4TireTractionForces/Tire2TractionForces/Divide1", 0, "",
    offsetof(B_M220Model_v2_T, Divide1_c) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 237, "m220model_v2/4TireTractionForces/Tire2TractionForces/Divide2", 0, "",
    offsetof(B_M220Model_v2_T, Divide2_l) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 238,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/Add",
    0, "", offsetof(B_M220Model_v2_T, Add_ci) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 239,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/VerticalTireForcePitchRollWeightTransfer2/Derivative",
    0, "", offsetof(B_M220Model_v2_T, Derivative_b) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 240,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/VerticalTireForcePitchRollWeightTransfer2/Product2",
    0, "", offsetof(B_M220Model_v2_T, Product2_gb) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 241,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/VerticalTireForcePitchRollWeightTransfer2/Divide3",
    0, "", offsetof(B_M220Model_v2_T, Divide3_m) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 242,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/VerticalTireForcePitchRollWeightTransfer2/Derivative1",
    0, "", offsetof(B_M220Model_v2_T, Derivative1_d) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 243,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/VerticalTireForcePitchRollWeightTransfer2/Product5",
    0, "", offsetof(B_M220Model_v2_T, Product5_m) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 244,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/VerticalTireForcePitchRollWeightTransfer2/Divide1",
    0, "", offsetof(B_M220Model_v2_T, Divide1_p) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 245,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/VerticalTireForcePitchRollWeightTransfer2/Product3",
    0, "", offsetof(B_M220Model_v2_T, Product3_j) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 246,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/VerticalTireForcePitchRollWeightTransfer2/Add1",
    0, "", offsetof(B_M220Model_v2_T, Add1_h) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 247,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/Product1",
    0, "", offsetof(B_M220Model_v2_T, Product1_c) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 248,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/Product3",
    0, "", offsetof(B_M220Model_v2_T, Product3_kf) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 249,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/Square",
    0, "", offsetof(B_M220Model_v2_T, Square_n) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 250,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/Trigonometric Function",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction_i) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 251,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/Product4",
    0, "", offsetof(B_M220Model_v2_T, Product4_l) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 252,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/Square1",
    0, "", offsetof(B_M220Model_v2_T, Square1_o) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 253,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/Add1",
    0, "", offsetof(B_M220Model_v2_T, Add1_n2) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 254,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/Sqrt",
    0, "", offsetof(B_M220Model_v2_T, Sqrt_nj) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 255,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/Product2",
    0, "", offsetof(B_M220Model_v2_T, Product2_l) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 256,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/Divide",
    0, "", offsetof(B_M220Model_v2_T, Divide_p) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 257, "m220model_v2/4TireTractionForces/Tire2TractionForces/Switch", 0, "",
    offsetof(B_M220Model_v2_T, Switch_a) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1,
    2, 0, 0 },

  { 258, "m220model_v2/4TireTractionForces/Tire2TractionForces/Product1", 0, "",
    offsetof(B_M220Model_v2_T, Product1_oo) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 259, "m220model_v2/4TireTractionForces/Tire2TractionForces/Product2", 0, "",
    offsetof(B_M220Model_v2_T, Product2_f) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 260,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Divide2",
    0, "", offsetof(B_M220Model_v2_T, Divide2_k) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 261,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Product1",
    0, "", offsetof(B_M220Model_v2_T, Product1_nt) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 262,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Product",
    0, "", offsetof(B_M220Model_v2_T, Product_d) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 263,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Add",
    0, "", offsetof(B_M220Model_v2_T, Add_f) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 264,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Trigonometric Function1",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction1_j) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 265,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Degrees to Radians/Gain1",
    0, "", offsetof(B_M220Model_v2_T, Gain1_b) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 266,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Product3",
    0, "", offsetof(B_M220Model_v2_T, Product3_b) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 267,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Add2",
    0, "", offsetof(B_M220Model_v2_T, Add2) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 268,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Trigonometric Function",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction_cb) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 269,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Product2",
    0, "", offsetof(B_M220Model_v2_T, Product2_n0) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 270,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Add3",
    0, "", offsetof(B_M220Model_v2_T, Add3) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 271,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Add1",
    0, "", offsetof(B_M220Model_v2_T, Add1_n1) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 272,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Switch",
    0, "", offsetof(B_M220Model_v2_T, Switch_on) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 273, "m220model_v2/4TireTractionForces/Tire3TractionForces/Add", 0, "",
    offsetof(B_M220Model_v2_T, Add_cia) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1,
    2, 0, 0 },

  { 274,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipAngleTire3/Product",
    0, "", offsetof(B_M220Model_v2_T, Product_ir) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 275,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipAngleTire3/Subtract",
    0, "", offsetof(B_M220Model_v2_T, Subtract_i) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 276,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipAngleTire3/Product2",
    0, "", offsetof(B_M220Model_v2_T, Product2_ee) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 277,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipAngleTire3/Subtract1",
    0, "", offsetof(B_M220Model_v2_T, Subtract1_j) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 278,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipAngleTire3/Divide",
    0, "", offsetof(B_M220Model_v2_T, Divide_k) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 279,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipAngleTire3/Trigonometric Function",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction_l) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 280,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipAngleTire3/Degrees to Radians/Gain1",
    0, "", offsetof(B_M220Model_v2_T, Gain1_p) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 281,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipAngleTire3/Subtract2",
    0, "", offsetof(B_M220Model_v2_T, Subtract2) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 282,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/Trigonometric Function",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction_e) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 283, "m220model_v2/4TireTractionForces/Tire3TractionForces/Divide1", 0, "",
    offsetof(B_M220Model_v2_T, Divide1_l) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 284, "m220model_v2/4TireTractionForces/Tire3TractionForces/Divide2", 0, "",
    offsetof(B_M220Model_v2_T, Divide2_e) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 285,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/Add",
    0, "", offsetof(B_M220Model_v2_T, Add_k) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 286,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/VerticalTireForcePitchRollWeightTransfer3/Derivative",
    0, "", offsetof(B_M220Model_v2_T, Derivative_p) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 287,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/VerticalTireForcePitchRollWeightTransfer3/Product2",
    0, "", offsetof(B_M220Model_v2_T, Product2_m) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 288,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/VerticalTireForcePitchRollWeightTransfer3/Divide3",
    0, "", offsetof(B_M220Model_v2_T, Divide3_b) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 289,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/VerticalTireForcePitchRollWeightTransfer3/Derivative1",
    0, "", offsetof(B_M220Model_v2_T, Derivative1_p) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 290,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/VerticalTireForcePitchRollWeightTransfer3/Product5",
    0, "", offsetof(B_M220Model_v2_T, Product5_l) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 291,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/VerticalTireForcePitchRollWeightTransfer3/Divide1",
    0, "", offsetof(B_M220Model_v2_T, Divide1_po) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 292,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/VerticalTireForcePitchRollWeightTransfer3/Product3",
    0, "", offsetof(B_M220Model_v2_T, Product3_m) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 293,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/VerticalTireForcePitchRollWeightTransfer3/Add1",
    0, "", offsetof(B_M220Model_v2_T, Add1_i) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 294,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/Product1",
    0, "", offsetof(B_M220Model_v2_T, Product1_jp) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 295,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/Product3",
    0, "", offsetof(B_M220Model_v2_T, Product3_gk) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 296,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/Square",
    0, "", offsetof(B_M220Model_v2_T, Square_du) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 297,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/Trigonometric Function",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction_k) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 298,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/Product4",
    0, "", offsetof(B_M220Model_v2_T, Product4_lj) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 299,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/Square1",
    0, "", offsetof(B_M220Model_v2_T, Square1_h) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 300,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/Add1",
    0, "", offsetof(B_M220Model_v2_T, Add1_f) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 301,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/Sqrt",
    0, "", offsetof(B_M220Model_v2_T, Sqrt_b) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 302,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/Product2",
    0, "", offsetof(B_M220Model_v2_T, Product2_mf) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 303,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/Divide",
    0, "", offsetof(B_M220Model_v2_T, Divide_d) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 304, "m220model_v2/4TireTractionForces/Tire3TractionForces/Switch", 0, "",
    offsetof(B_M220Model_v2_T, Switch_ge) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 305, "m220model_v2/4TireTractionForces/Tire3TractionForces/Product1", 0, "",
    offsetof(B_M220Model_v2_T, Product1_p) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 306, "m220model_v2/4TireTractionForces/Tire3TractionForces/Product2", 0, "",
    offsetof(B_M220Model_v2_T, Product2_ng) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 307,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Divide2",
    0, "", offsetof(B_M220Model_v2_T, Divide2_pv) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 308,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Product1",
    0, "", offsetof(B_M220Model_v2_T, Product1_e) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 309,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Product",
    0, "", offsetof(B_M220Model_v2_T, Product_n) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 310,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Add",
    0, "", offsetof(B_M220Model_v2_T, Add_e) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 311,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Trigonometric Function1",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction1_jd) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 312,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Degrees to Radians/Gain1",
    0, "", offsetof(B_M220Model_v2_T, Gain1_f) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 313,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Product3",
    0, "", offsetof(B_M220Model_v2_T, Product3_mz) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 314,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Add2",
    0, "", offsetof(B_M220Model_v2_T, Add2_h) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 315,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Trigonometric Function",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction_g) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 316,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Product2",
    0, "", offsetof(B_M220Model_v2_T, Product2_k) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 317,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Add3",
    0, "", offsetof(B_M220Model_v2_T, Add3_c) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 318,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Add1",
    0, "", offsetof(B_M220Model_v2_T, Add1_f0) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 319,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Switch",
    0, "", offsetof(B_M220Model_v2_T, Switch_ez) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 320, "m220model_v2/4TireTractionForces/Tire4TractionForces/Add", 0, "",
    offsetof(B_M220Model_v2_T, Add_a) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 321,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipAngleTire4/Product",
    0, "", offsetof(B_M220Model_v2_T, Product_e) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 322,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipAngleTire4/Subtract",
    0, "", offsetof(B_M220Model_v2_T, Subtract_f) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 323,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipAngleTire4/Product2",
    0, "", offsetof(B_M220Model_v2_T, Product2_d) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 324,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipAngleTire4/Subtract1",
    0, "", offsetof(B_M220Model_v2_T, Subtract1_f) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 325,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipAngleTire4/Divide",
    0, "", offsetof(B_M220Model_v2_T, Divide_b) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 326,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipAngleTire4/Trigonometric Function",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction_nu) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 327,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipAngleTire4/Degrees to Radians/Gain1",
    0, "", offsetof(B_M220Model_v2_T, Gain1_e) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 328,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipAngleTire4/Subtract2",
    0, "", offsetof(B_M220Model_v2_T, Subtract2_p) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 329,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/Trigonometric Function",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction_d) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 330, "m220model_v2/4TireTractionForces/Tire4TractionForces/Divide1", 0, "",
    offsetof(B_M220Model_v2_T, Divide1_ix) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 331, "m220model_v2/4TireTractionForces/Tire4TractionForces/Divide2", 0, "",
    offsetof(B_M220Model_v2_T, Divide2_e2) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 332,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/Add",
    0, "", offsetof(B_M220Model_v2_T, Add_km) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 333,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/VerticalTireForcePitchRollWeightTransfer4/Derivative",
    0, "", offsetof(B_M220Model_v2_T, Derivative_n) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 334,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/VerticalTireForcePitchRollWeightTransfer4/Product2",
    0, "", offsetof(B_M220Model_v2_T, Product2_go) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 335,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/VerticalTireForcePitchRollWeightTransfer4/Divide3",
    0, "", offsetof(B_M220Model_v2_T, Divide3_f) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 336,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/VerticalTireForcePitchRollWeightTransfer4/Derivative1",
    0, "", offsetof(B_M220Model_v2_T, Derivative1_g) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 337,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/VerticalTireForcePitchRollWeightTransfer4/Product5",
    0, "", offsetof(B_M220Model_v2_T, Product5_o) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 338,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/VerticalTireForcePitchRollWeightTransfer4/Divide1",
    0, "", offsetof(B_M220Model_v2_T, Divide1_b) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 339,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/VerticalTireForcePitchRollWeightTransfer4/Product3",
    0, "", offsetof(B_M220Model_v2_T, Product3_d) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 340,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/VerticalTireForcePitchRollWeightTransfer4/Add1",
    0, "", offsetof(B_M220Model_v2_T, Add1_c) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 341,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/Product1",
    0, "", offsetof(B_M220Model_v2_T, Product1_jy) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 342,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/Product3",
    0, "", offsetof(B_M220Model_v2_T, Product3_a) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 343,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/Square",
    0, "", offsetof(B_M220Model_v2_T, Square_m) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 344,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/Trigonometric Function",
    0, "", offsetof(B_M220Model_v2_T, TrigonometricFunction_kf) + (0*sizeof
    (real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 345,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/Product4",
    0, "", offsetof(B_M220Model_v2_T, Product4_b) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 346,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/Square1",
    0, "", offsetof(B_M220Model_v2_T, Square1_e) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 347,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/Add1",
    0, "", offsetof(B_M220Model_v2_T, Add1_gb) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 348,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/Sqrt",
    0, "", offsetof(B_M220Model_v2_T, Sqrt_d) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 349,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/Product2",
    0, "", offsetof(B_M220Model_v2_T, Product2_o) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 350,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/Divide",
    0, "", offsetof(B_M220Model_v2_T, Divide_h) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 351, "m220model_v2/4TireTractionForces/Tire4TractionForces/Switch", 0, "",
    offsetof(B_M220Model_v2_T, Switch_p) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1,
    2, 0, 0 },

  { 352, "m220model_v2/4TireTractionForces/Tire4TractionForces/Product1", 0, "",
    offsetof(B_M220Model_v2_T, Product1_fn) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 353, "m220model_v2/4TireTractionForces/Tire4TractionForces/Product2", 0, "",
    offsetof(B_M220Model_v2_T, Product2_oz) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 354, "m220model_v2/Degrees to Radians/Gain1", 0, "", offsetof
    (B_M220Model_v2_T, Gain1_j) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 355, "m220model_v2/M220DynamicsModel/Steering_Angle_Limits", 0, "", offsetof
    (B_M220Model_v2_T, Steering_Angle_Limits) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 356, "m220model_v2/M220DynamicsModel/Degrees to Radians/Gain1", 0, "",
    offsetof(B_M220Model_v2_T, Gain1_fo) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1,
    2, 0, 0 },

  { 357, "m220model_v2/M220DynamicsModel/Trigonometric Function1", 0, "",
    offsetof(B_M220Model_v2_T, TrigonometricFunction1_k) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 358, "m220model_v2/M220DynamicsModel/Add3", 0, "", offsetof(B_M220Model_v2_T,
    Add3_cg) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 359, "m220model_v2/M220DynamicsModel/Product6", 0, "", offsetof
    (B_M220Model_v2_T, Product6) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 360, "m220model_v2/M220DynamicsModel/Add4", 0, "", offsetof(B_M220Model_v2_T,
    Add4) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 361, "m220model_v2/M220DynamicsModel/Trigonometric Function2", 0, "",
    offsetof(B_M220Model_v2_T, TrigonometricFunction2_f) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 362, "m220model_v2/M220DynamicsModel/Product7", 0, "", offsetof
    (B_M220Model_v2_T, Product7) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 363, "m220model_v2/M220DynamicsModel/Add10", 0, "", offsetof
    (B_M220Model_v2_T, Add10) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 364, "m220model_v2/M220DynamicsModel/Product2", 0, "", offsetof
    (B_M220Model_v2_T, Product2_cz) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 365, "m220model_v2/M220DynamicsModel/Delay", 0, "", offsetof
    (B_M220Model_v2_T, Delay) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 366, "m220model_v2/M220DynamicsModel/Gain", 0, "", offsetof(B_M220Model_v2_T,
    Gain_a) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 367, "m220model_v2/M220DynamicsModel/Delay2", 0, "", offsetof
    (B_M220Model_v2_T, Delay2) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 368, "m220model_v2/M220DynamicsModel/Product1", 0, "", offsetof
    (B_M220Model_v2_T, Product1_jb) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 369, "m220model_v2/M220DynamicsModel/Add1", 0, "", offsetof(B_M220Model_v2_T,
    Add1_fr) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 370, "m220model_v2/M220DynamicsModel/Add11", 0, "", offsetof
    (B_M220Model_v2_T, Add11) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 371, "m220model_v2/M220DynamicsModel/Add12", 0, "", offsetof
    (B_M220Model_v2_T, Add12) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 372, "m220model_v2/M220DynamicsModel/Add14", 0, "", offsetof
    (B_M220Model_v2_T, Add14) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 373, "m220model_v2/M220DynamicsModel/Delay4", 0, "", offsetof
    (B_M220Model_v2_T, Delay4) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 374, "m220model_v2/M220DynamicsModel/Product19", 0, "", offsetof
    (B_M220Model_v2_T, Product19) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 375, "m220model_v2/M220DynamicsModel/Delay1", 0, "", offsetof
    (B_M220Model_v2_T, Delay1) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 376, "m220model_v2/M220DynamicsModel/Delay3", 0, "", offsetof
    (B_M220Model_v2_T, Delay3) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 377, "m220model_v2/M220DynamicsModel/Product20", 0, "", offsetof
    (B_M220Model_v2_T, Product20) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 378, "m220model_v2/M220DynamicsModel/Add16", 0, "", offsetof
    (B_M220Model_v2_T, Add16) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 379, "m220model_v2/M220DynamicsModel/FyTotalCalc/Add", 0, "", offsetof
    (B_M220Model_v2_T, Add_i) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 380, "m220model_v2/M220DynamicsModel/FyTotalCalc/Trigonometric Function", 0,
    "", offsetof(B_M220Model_v2_T, TrigonometricFunction_az) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 381, "m220model_v2/M220DynamicsModel/FyTotalCalc/Product", 0, "", offsetof
    (B_M220Model_v2_T, Product_nj) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 382, "m220model_v2/M220DynamicsModel/FyTotalCalc/Trigonometric Function1", 0,
    "", offsetof(B_M220Model_v2_T, TrigonometricFunction1_i) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 383, "m220model_v2/M220DynamicsModel/FyTotalCalc/Add2", 0, "", offsetof
    (B_M220Model_v2_T, Add2_l) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 384, "m220model_v2/M220DynamicsModel/FyTotalCalc/Product1", 0, "", offsetof
    (B_M220Model_v2_T, Product1_j1) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 385, "m220model_v2/M220DynamicsModel/FyTotalCalc/Add1", 0, "", offsetof
    (B_M220Model_v2_T, Add1_l) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 386, "m220model_v2/M220DynamicsModel/Product27", 0, "", offsetof
    (B_M220Model_v2_T, Product27) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 387, "m220model_v2/M220DynamicsModel/Product28", 0, "", offsetof
    (B_M220Model_v2_T, Product28) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 388, "m220model_v2/M220DynamicsModel/Add17", 0, "", offsetof
    (B_M220Model_v2_T, Add17) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 389, "m220model_v2/M220DynamicsModel/Product9", 0, "", offsetof
    (B_M220Model_v2_T, Product9) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 390, "m220model_v2/M220DynamicsModel/Product10", 0, "", offsetof
    (B_M220Model_v2_T, Product10) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 391, "m220model_v2/M220DynamicsModel/Add2", 0, "", offsetof(B_M220Model_v2_T,
    Add2_j) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 392, "m220model_v2/M220DynamicsModel/Product35", 0, "", offsetof
    (B_M220Model_v2_T, Product35) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 393, "m220model_v2/M220DynamicsModel/FxTotalCalc/Add", 0, "", offsetof
    (B_M220Model_v2_T, Add_o) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 394, "m220model_v2/M220DynamicsModel/FxTotalCalc/Trigonometric Function", 0,
    "", offsetof(B_M220Model_v2_T, TrigonometricFunction_b) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 395, "m220model_v2/M220DynamicsModel/FxTotalCalc/Product", 0, "", offsetof
    (B_M220Model_v2_T, Product_j) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 396, "m220model_v2/M220DynamicsModel/FxTotalCalc/Trigonometric Function1", 0,
    "", offsetof(B_M220Model_v2_T, TrigonometricFunction1_d) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 397, "m220model_v2/M220DynamicsModel/FxTotalCalc/Add2", 0, "", offsetof
    (B_M220Model_v2_T, Add2_f) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 398, "m220model_v2/M220DynamicsModel/FxTotalCalc/Product1", 0, "", offsetof
    (B_M220Model_v2_T, Product1_h) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 399, "m220model_v2/M220DynamicsModel/FxTotalCalc/Add1", 0, "", offsetof
    (B_M220Model_v2_T, Add1_j) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 400, "m220model_v2/M220DynamicsModel/Product31", 0, "", offsetof
    (B_M220Model_v2_T, Product31) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 401, "m220model_v2/M220DynamicsModel/Add20", 0, "", offsetof
    (B_M220Model_v2_T, Add20) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 402, "m220model_v2/M220DynamicsModel/Product14", 0, "", offsetof
    (B_M220Model_v2_T, Product14) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 403, "m220model_v2/M220DynamicsModel/Product15", 0, "", offsetof
    (B_M220Model_v2_T, Product15) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 404, "m220model_v2/M220DynamicsModel/Magnitude Squared", 0, "", offsetof
    (B_M220Model_v2_T, MagnitudeSquared) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1,
    2, 0, 0 },

  { 405, "m220model_v2/M220DynamicsModel/Sign", 0, "", offsetof(B_M220Model_v2_T,
    Sign) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 406, "m220model_v2/M220DynamicsModel/Product16", 0, "", offsetof
    (B_M220Model_v2_T, Product16) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 407, "m220model_v2/M220DynamicsModel/Add5", 0, "", offsetof(B_M220Model_v2_T,
    Add5) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 408, "m220model_v2/M220DynamicsModel/Product12", 0, "", offsetof
    (B_M220Model_v2_T, Product12) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 409, "m220model_v2/M220DynamicsModel/Product11", 0, "", offsetof
    (B_M220Model_v2_T, Product11) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 410, "m220model_v2/M220DynamicsModel/Add6", 0, "", offsetof(B_M220Model_v2_T,
    Add6) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 411, "m220model_v2/M220DynamicsModel/Add7", 0, "", offsetof(B_M220Model_v2_T,
    Add7) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 412, "m220model_v2/M220DynamicsModel/Product3", 0, "", offsetof
    (B_M220Model_v2_T, Product3_h) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 413, "m220model_v2/M220DynamicsModel/Add9", 0, "", offsetof(B_M220Model_v2_T,
    Add9) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 414, "m220model_v2/M220DynamicsModel/Product4", 0, "", offsetof
    (B_M220Model_v2_T, Product4_m) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 415, "m220model_v2/M220DynamicsModel/Product5", 0, "", offsetof
    (B_M220Model_v2_T, Product5_d) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 416, "m220model_v2/M220DynamicsModel/Product18", 0, "", offsetof
    (B_M220Model_v2_T, Product18) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 417, "m220model_v2/M220DynamicsModel/Add8", 0, "", offsetof(B_M220Model_v2_T,
    Add8) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 418, "m220model_v2/M220DynamicsModel/Integrator", 0, "", offsetof
    (B_M220Model_v2_T, Integrator_j) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 419, "m220model_v2/M220DynamicsModel/Integrator10", 0, "", offsetof
    (B_M220Model_v2_T, Integrator10) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 420, "m220model_v2/M220DynamicsModel/Integrator3", 0, "", offsetof
    (B_M220Model_v2_T, Integrator3) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 421, "m220model_v2/M220DynamicsModel/Integrator6", 0, "", offsetof
    (B_M220Model_v2_T, Integrator6) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 422, "m220model_v2/M220DynamicsModel/Integrator7", 0, "", offsetof
    (B_M220Model_v2_T, Integrator7) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 423, "m220model_v2/M220DynamicsModel/Integrator8", 0, "", offsetof
    (B_M220Model_v2_T, Integrator8) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 424, "m220model_v2/M220DynamicsModel/Integrator9", 0, "", offsetof
    (B_M220Model_v2_T, Integrator9) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 425, "m220model_v2/M220DynamicsModel/Product17", 0, "", offsetof
    (B_M220Model_v2_T, Product17) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 426, "m220model_v2/M220DynamicsModel/Product29", 0, "", offsetof
    (B_M220Model_v2_T, Product29) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 427, "m220model_v2/M220DynamicsModel/Product30", 0, "", offsetof
    (B_M220Model_v2_T, Product30) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 428, "m220model_v2/M220DynamicsModel/Radians to Degrees1/Gain", 0, "",
    offsetof(B_M220Model_v2_T, Gain_d) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1,
    2, 0, 0 },

  { 429, "m220model_v2/M220DynamicsModel/Radians to Degrees2/Gain", 0, "",
    offsetof(B_M220Model_v2_T, Gain_l) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1,
    2, 0, 0 },

  { 430, "m220model_v2/M220MowerDecksSubsystem/Switch", 0, "", offsetof
    (B_M220Model_v2_T, Switch_ak) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 431, "m220model_v2/M220MowerDecksSubsystem/Add", 0, "", offsetof
    (B_M220Model_v2_T, Add_l) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 432, "m220model_v2/M220MowerDecksSubsystem/Switch2", 0, "", offsetof
    (B_M220Model_v2_T, Switch2_l) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 433, "m220model_v2/M220MowerDecksSubsystem/Add1", 0, "", offsetof
    (B_M220Model_v2_T, Add1_hd) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 434, "m220model_v2/M220MowerDecksSubsystem/Switch17", 0, "", offsetof
    (B_M220Model_v2_T, Switch17) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 435, "m220model_v2/M220MowerDecksSubsystem/Add10", 0, "", offsetof
    (B_M220Model_v2_T, Add10_e) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 436, "m220model_v2/M220MowerDecksSubsystem/Switch19", 0, "", offsetof
    (B_M220Model_v2_T, Switch19) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 437, "m220model_v2/M220MowerDecksSubsystem/Add11", 0, "", offsetof
    (B_M220Model_v2_T, Add11_a) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 438, "m220model_v2/M220MowerDecksSubsystem/Switch1", 0, "", offsetof
    (B_M220Model_v2_T, Switch1_b) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 439, "m220model_v2/M220MowerDecksSubsystem/Add2", 0, "", offsetof
    (B_M220Model_v2_T, Add2_c) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 440, "m220model_v2/M220MowerDecksSubsystem/Switch3", 0, "", offsetof
    (B_M220Model_v2_T, Switch3) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 441, "m220model_v2/M220MowerDecksSubsystem/Add3", 0, "", offsetof
    (B_M220Model_v2_T, Add3_n) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 442, "m220model_v2/M220MowerDecksSubsystem/Switch8", 0, "", offsetof
    (B_M220Model_v2_T, Switch8) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 443, "m220model_v2/M220MowerDecksSubsystem/Add4", 0, "", offsetof
    (B_M220Model_v2_T, Add4_h) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 444, "m220model_v2/M220MowerDecksSubsystem/Switch10", 0, "", offsetof
    (B_M220Model_v2_T, Switch10) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 445, "m220model_v2/M220MowerDecksSubsystem/Add5", 0, "", offsetof
    (B_M220Model_v2_T, Add5_h) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 446, "m220model_v2/M220MowerDecksSubsystem/Switch9", 0, "", offsetof
    (B_M220Model_v2_T, Switch9) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 447, "m220model_v2/M220MowerDecksSubsystem/Add6", 0, "", offsetof
    (B_M220Model_v2_T, Add6_c) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 448, "m220model_v2/M220MowerDecksSubsystem/Switch11", 0, "", offsetof
    (B_M220Model_v2_T, Switch11) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 449, "m220model_v2/M220MowerDecksSubsystem/Add7", 0, "", offsetof
    (B_M220Model_v2_T, Add7_f) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 450, "m220model_v2/M220MowerDecksSubsystem/Switch16", 0, "", offsetof
    (B_M220Model_v2_T, Switch16) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 451, "m220model_v2/M220MowerDecksSubsystem/Add8", 0, "", offsetof
    (B_M220Model_v2_T, Add8_a) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 452, "m220model_v2/M220MowerDecksSubsystem/Switch18", 0, "", offsetof
    (B_M220Model_v2_T, Switch18) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 453, "m220model_v2/M220MowerDecksSubsystem/Add9", 0, "", offsetof
    (B_M220Model_v2_T, Add9_b) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 454, "m220model_v2/M220MowerDecksSubsystem/Switch4", 0, "", offsetof
    (B_M220Model_v2_T, Switch4) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 455, "m220model_v2/M220MowerDecksSubsystem/Rate Limiter", 0, "", offsetof
    (B_M220Model_v2_T, RateLimiter) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 456, "m220model_v2/M220MowerDecksSubsystem/Rate Limiter2", 0, "", offsetof
    (B_M220Model_v2_T, RateLimiter2) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 457, "m220model_v2/M220MowerDecksSubsystem/Rate Limiter1", 0, "", offsetof
    (B_M220Model_v2_T, RateLimiter1) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 458, "m220model_v2/M220MowerDecksSubsystem/Rate Limiter3", 0, "", offsetof
    (B_M220Model_v2_T, RateLimiter3) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 459, "m220model_v2/M220MowerDecksSubsystem/Multiport Switch", 0, "",
    offsetof(B_M220Model_v2_T, MultiportSwitch) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 460, "m220model_v2/M220MowerDecksSubsystem/Switch12", 0, "", offsetof
    (B_M220Model_v2_T, Switch12) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 461, "m220model_v2/M220MowerDecksSubsystem/Rate Limiter4", 0, "", offsetof
    (B_M220Model_v2_T, RateLimiter4) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 462, "m220model_v2/M220MowerDecksSubsystem/Rate Limiter6", 0, "", offsetof
    (B_M220Model_v2_T, RateLimiter6) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 463, "m220model_v2/M220MowerDecksSubsystem/Rate Limiter5", 0, "", offsetof
    (B_M220Model_v2_T, RateLimiter5) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 464, "m220model_v2/M220MowerDecksSubsystem/Rate Limiter7", 0, "", offsetof
    (B_M220Model_v2_T, RateLimiter7) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 465, "m220model_v2/M220MowerDecksSubsystem/Multiport Switch1", 0, "",
    offsetof(B_M220Model_v2_T, MultiportSwitch1) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 466, "m220model_v2/M220MowerDecksSubsystem/Switch20", 0, "", offsetof
    (B_M220Model_v2_T, Switch20) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 467, "m220model_v2/M220MowerDecksSubsystem/Rate Limiter8", 0, "", offsetof
    (B_M220Model_v2_T, RateLimiter8) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 468, "m220model_v2/M220MowerDecksSubsystem/Rate Limiter10", 0, "", offsetof
    (B_M220Model_v2_T, RateLimiter10) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 469, "m220model_v2/M220MowerDecksSubsystem/Rate Limiter9", 0, "", offsetof
    (B_M220Model_v2_T, RateLimiter9) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 470, "m220model_v2/M220MowerDecksSubsystem/Rate Limiter11", 0, "", offsetof
    (B_M220Model_v2_T, RateLimiter11) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 471, "m220model_v2/M220MowerDecksSubsystem/Multiport Switch2", 0, "",
    offsetof(B_M220Model_v2_T, MultiportSwitch2) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 472, "m220model_v2/OdometrySubsystem/Product1", 0, "", offsetof
    (B_M220Model_v2_T, Product1_o5) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2,
    0, 0 },

  { 473, "m220model_v2/M220MowerDecksSubsystem/Switch5", 0, "", offsetof
    (B_M220Model_v2_T, Switch5) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 474, "m220model_v2/M220MowerDecksSubsystem/Switch6", 0, "", offsetof
    (B_M220Model_v2_T, Switch6) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 475, "m220model_v2/M220MowerDecksSubsystem/Switch7", 0, "", offsetof
    (B_M220Model_v2_T, Switch7) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 476, "m220model_v2/M220MowerDecksSubsystem/Switch21", 0, "", offsetof
    (B_M220Model_v2_T, Switch21) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 477, "m220model_v2/M220MowerDecksSubsystem/Switch22", 0, "", offsetof
    (B_M220Model_v2_T, Switch22) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 478, "m220model_v2/M220MowerDecksSubsystem/Switch23", 0, "", offsetof
    (B_M220Model_v2_T, Switch23) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 479, "m220model_v2/M220MowerDecksSubsystem/Switch13", 0, "", offsetof
    (B_M220Model_v2_T, Switch13) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 480, "m220model_v2/M220MowerDecksSubsystem/Switch14", 0, "", offsetof
    (B_M220Model_v2_T, Switch14) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 481, "m220model_v2/M220MowerDecksSubsystem/Switch15", 0, "", offsetof
    (B_M220Model_v2_T, Switch15) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 482, "m220model_v2/NI9853_1 Subsystem/Lookup Table Dynamic1", 0, "",
    offsetof(B_M220Model_v2_T, LookupTableDynamic1_g) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 483, "m220model_v2/NI9853_1 Subsystem/Lookup Table Dynamic2", 0, "",
    offsetof(B_M220Model_v2_T, LookupTableDynamic2_k) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 484, "m220model_v2/M220HydraulicOil/Switch1", 0, "", offsetof
    (B_M220Model_v2_T, Switch1_m) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 485, "m220model_v2/M220HydraulicOil/Square3", 0, "", offsetof
    (B_M220Model_v2_T, Square3) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0
  },

  { 486, "m220model_v2/M220HydraulicOil/Add", 0, "", offsetof(B_M220Model_v2_T,
    Add_a2) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 487, "m220model_v2/M220HydraulicOil/Sqrt", 0, "", offsetof(B_M220Model_v2_T,
    Sqrt_l) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 488, "m220model_v2/M220HydraulicOil/Lookup Table Dynamic2", 0, "", offsetof
    (B_M220Model_v2_T, LookupTableDynamic2_f) + (0*sizeof(real_T)), BLOCKIO_SIG,
    0, 1, 2, 0, 0 },

  { 489, "m220model_v2/M220HeatExchangerTempSubsystem/Switch1", 0, "", offsetof
    (B_M220Model_v2_T, Switch1_c) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 490, "m220model_v2/M220HeatExchangerTempSubsystem/Lookup Table Dynamic", 0,
    "", offsetof(B_M220Model_v2_T, LookupTableDynamic_f) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 491, "m220model_v2/M220HeatExchangerTempSubsystem/Subtract", 0, "", offsetof
    (B_M220Model_v2_T, Subtract_k) + (0*sizeof(real_T)), BLOCKIO_SIG, 0, 1, 2, 0,
    0 },

  { 492, "m220model_v2/M220ControlBoxTempSubsystem/Lookup Table Dynamic", 0, "",
    offsetof(B_M220Model_v2_T, LookupTableDynamic_i) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 493,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Divide1",
    0, "", offsetof(B_M220Model_v2_T, Divide1_g) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 494,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Divide",
    0, "", offsetof(B_M220Model_v2_T, Divide_j) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 495, "m220model_v2/4TireTractionForces/Tire4TractionForces/Subtract", 0, "",
    offsetof(B_M220Model_v2_T, Subtract_c) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 496, "m220model_v2/4TireTractionForces/Tire4TractionForces/Product3", 0, "",
    offsetof(B_M220Model_v2_T, Product3_f) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 497,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Divide1",
    0, "", offsetof(B_M220Model_v2_T, Divide1_j) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 498,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Divide",
    0, "", offsetof(B_M220Model_v2_T, Divide_l) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 499, "m220model_v2/4TireTractionForces/Tire3TractionForces/Subtract", 0, "",
    offsetof(B_M220Model_v2_T, Subtract_iq) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 500, "m220model_v2/4TireTractionForces/Tire3TractionForces/Product3", 0, "",
    offsetof(B_M220Model_v2_T, Product3_c) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 501,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipRatioTire2/Divide1",
    0, "", offsetof(B_M220Model_v2_T, Divide1_gd) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 502,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipRatioTire2/Divide",
    0, "", offsetof(B_M220Model_v2_T, Divide_i) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 503, "m220model_v2/4TireTractionForces/Tire2TractionForces/Subtract", 0, "",
    offsetof(B_M220Model_v2_T, Subtract_e) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 504, "m220model_v2/4TireTractionForces/Tire2TractionForces/Product3", 0, "",
    offsetof(B_M220Model_v2_T, Product3_i) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 505,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipRatioTire1/Divide1",
    0, "", offsetof(B_M220Model_v2_T, Divide1_bf) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 506,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipRatioTire1/Divide",
    0, "", offsetof(B_M220Model_v2_T, Divide_ls) + (0*sizeof(real_T)),
    BLOCKIO_SIG, 0, 1, 2, 0, 0 },

  { 507, "m220model_v2/4TireTractionForces/Tire1TractionForces/Subtract", 0, "",
    offsetof(B_M220Model_v2_T, Subtract_n) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 508, "m220model_v2/4TireTractionForces/Tire1TractionForces/Product3", 0, "",
    offsetof(B_M220Model_v2_T, Product3_ks) + (0*sizeof(real_T)), BLOCKIO_SIG, 0,
    1, 2, 0, 0 },

  { 509, "m220model_v2/OdometrySubsystem/Data Type Conversion", 0, "", offsetof
    (B_M220Model_v2_T, DataTypeConversion) + (0*sizeof(uint32_T)), BLOCKIO_SIG,
    7, 1, 2, 0, 0 },

  { 510, "m220model_v2/OdometrySubsystem/Data Type Conversion1", 0, "", offsetof
    (B_M220Model_v2_T, DataTypeConversion1) + (0*sizeof(uint32_T)), BLOCKIO_SIG,
    7, 1, 2, 0, 0 },

  { 511, "m220model_v2/M220HydraulicOil/Compare To Constant/Compare", 0, "",
    offsetof(B_M220Model_v2_T, Compare) + (0*sizeof(boolean_T)), BLOCKIO_SIG, 8,
    1, 2, 0, 0 },

  { 512, "m220model_v2/M220MachineEnableFunction/Logical Operator1", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator1) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 513, "m220model_v2/M220MachineEnableFunction/Logical Operator4", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator4) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 514, "m220model_v2/M220MachineEnableFunction/Logical Operator", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 515,
    "m220model_v2/4TireTractionForces/Tire1TractionForces/VerticalForceTire1/SlipRatioTire1/Relational Operator",
    0, "", offsetof(B_M220Model_v2_T, RelationalOperator) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 516,
    "m220model_v2/4TireTractionForces/Tire2TractionForces/VerticalForceTire2/SlipRatioTire2/Relational Operator",
    0, "", offsetof(B_M220Model_v2_T, RelationalOperator_b) + (0*sizeof
    (boolean_T)), BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 517,
    "m220model_v2/4TireTractionForces/Tire3TractionForces/VerticalForceTire3/SlipRatioTire3/Relational Operator",
    0, "", offsetof(B_M220Model_v2_T, RelationalOperator_k) + (0*sizeof
    (boolean_T)), BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 518,
    "m220model_v2/4TireTractionForces/Tire4TractionForces/VerticalForceTire4/SlipRatioTire4/Relational Operator",
    0, "", offsetof(B_M220Model_v2_T, RelationalOperator_m) + (0*sizeof
    (boolean_T)), BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 519, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant5/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_m) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 520, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant1/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_ib) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 521, "m220model_v2/M220MowerDecksSubsystem/Logical Operator4", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator4_i) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 522, "m220model_v2/M220MowerDecksSubsystem/Logical Operator8", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator8) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 523, "m220model_v2/M220MowerDecksSubsystem/Logical Operator", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator_b) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 524, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant6/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_ie) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 525, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant9/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_ii) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 526, "m220model_v2/M220MowerDecksSubsystem/Logical Operator5", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator5) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 527, "m220model_v2/M220MowerDecksSubsystem/Logical Operator9", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator9) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 528, "m220model_v2/M220MowerDecksSubsystem/Logical Operator1", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator1_m) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 529, "m220model_v2/M220MowerDecksSubsystem/Logical Operator28", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator28) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 530, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant21/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_k) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 531, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant22/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_l) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 532, "m220model_v2/M220MowerDecksSubsystem/Logical Operator22", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator22) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 533, "m220model_v2/M220MowerDecksSubsystem/Logical Operator26", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator26) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 534, "m220model_v2/M220MowerDecksSubsystem/Logical Operator29", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator29) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 535, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant25/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_h) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 536, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant26/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_b) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 537, "m220model_v2/M220MowerDecksSubsystem/Logical Operator23", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator23) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 538, "m220model_v2/M220MowerDecksSubsystem/Logical Operator27", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator27) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 539, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant3/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_hd) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 540, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant4/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_kv) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 541, "m220model_v2/M220MowerDecksSubsystem/Logical Operator2", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator2) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 542, "m220model_v2/M220MowerDecksSubsystem/Logical Operator6", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator6) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 543, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant7/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_hr) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 544, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant8/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_n2) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 545, "m220model_v2/M220MowerDecksSubsystem/Logical Operator3", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator3) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 546, "m220model_v2/M220MowerDecksSubsystem/Logical Operator7", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator7) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 547, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant14/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_md) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 548, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant10/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_mr) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 549, "m220model_v2/M220MowerDecksSubsystem/Logical Operator14", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator14) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 550, "m220model_v2/M220MowerDecksSubsystem/Logical Operator18", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator18) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 551, "m220model_v2/M220MowerDecksSubsystem/Logical Operator10", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator10) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 552, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant15/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_e) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 553, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant18/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_ei) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 554, "m220model_v2/M220MowerDecksSubsystem/Logical Operator15", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator15) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 555, "m220model_v2/M220MowerDecksSubsystem/Logical Operator19", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator19) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 556, "m220model_v2/M220MowerDecksSubsystem/Logical Operator11", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator11) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 557, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant12/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_a) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 558, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant13/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_mg) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 559, "m220model_v2/M220MowerDecksSubsystem/Logical Operator12", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator12) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 560, "m220model_v2/M220MowerDecksSubsystem/Logical Operator16", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator16) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 561, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant16/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_p) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 562, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant17/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_ls) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 563, "m220model_v2/M220MowerDecksSubsystem/Logical Operator13", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator13) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 564, "m220model_v2/M220MowerDecksSubsystem/Logical Operator17", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator17) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 565, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant23/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_bv) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 566, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant19/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_d) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 567, "m220model_v2/M220MowerDecksSubsystem/Logical Operator24", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator24) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 568, "m220model_v2/M220MowerDecksSubsystem/Logical Operator20", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator20) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 569, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant24/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_aj) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 570, "m220model_v2/M220MowerDecksSubsystem/Compare To Constant27/Compare", 0,
    "", offsetof(B_M220Model_v2_T, Compare_dg) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 571, "m220model_v2/M220MowerDecksSubsystem/Logical Operator25", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator25) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { 572, "m220model_v2/M220MowerDecksSubsystem/Logical Operator21", 0, "",
    offsetof(B_M220Model_v2_T, LogicalOperator21) + (0*sizeof(boolean_T)),
    BLOCKIO_SIG, 8, 1, 2, 0, 0 },

  { -1, "", -1, "", 0, 0, 0 }
};

static int32_t NI_SigListSize DataSection(".NIVS.siglistsize") = 573;
static int32_t NI_VirtualBlockSources[1];
static int32_t NI_SigDimList[] DataSection(".NIVS.sigdimlist") =
{ 1, 1
};

static int32_t NI_ExtListSize DataSection(".NIVS.extlistsize") = 257;
static NI_ExternalIO NI_ExtList[] DataSection(".NIVS.extlist") =
{
  { 1, "SteeringPosMsg_int", 0, EXT_IN, 1, 1, 1 },

  { 2, "JoystickPosMsg_int", 0, EXT_IN, 1, 1, 1 },

  { 3, "JoystickButton1Msg_bool", 1, EXT_IN, 1, 1, 1 },

  { 4, "JoystickButton2Msg_bool", 1, EXT_IN, 1, 1, 1 },

  { 5, "JoystickButton3Msg_bool", 1, EXT_IN, 1, 1, 1 },

  { 6, "JoystickButton4Msg_bool", 1, EXT_IN, 1, 1, 1 },

  { 7, "JoystickButton5Msg_bool", 1, EXT_IN, 1, 1, 1 },

  { 8, "JoystickButton6Msg_bool", 1, EXT_IN, 1, 1, 1 },

  { 9, "JoystickButton7Msg_bool", 1, EXT_IN, 1, 1, 1 },

  { 10, "JoystickButton8Msg_bool", 1, EXT_IN, 1, 1, 1 },

  { 11, "JoystickButton9Msg_bool", 1, EXT_IN, 1, 1, 1 },

  { 12, "JoystickButton10Msg_bool", 1, EXT_IN, 1, 1, 1 },

  { 13, "HMIVehicleSpeed_mph", 0, EXT_IN, 1, 1, 1 },

  { 14, "HMIVehicleSpeedOvrd_bool", 0, EXT_IN, 1, 1, 1 },

  { 15, "RlyEStarters_bool", 1, EXT_IN, 1, 1, 1 },

  { 16, "Rly1LightsRight_bool", 1, EXT_IN, 1, 1, 1 },

  { 17, "Rly2LightsLeft_bool", 1, EXT_IN, 1, 1, 1 },

  { 18, "Rly3LightsRearSide_bool", 1, EXT_IN, 1, 1, 1 },

  { 19, "Rly4LightsAux_bool", 1, EXT_IN, 1, 1, 1 },

  { 20, "Rly5FlashRight_bool", 1, EXT_IN, 1, 1, 1 },

  { 21, "Rly6FlashLeft_bool", 1, EXT_IN, 1, 1, 1 },

  { 22, "Rly7Horn_bool", 1, EXT_IN, 1, 1, 1 },

  { 23, "NI9426_1Spare1_bool", 1, EXT_IN, 1, 1, 1 },

  { 24, "Ssr2Brake_bool", 0, EXT_IN, 1, 1, 1 },

  { 25, "Ssr3LoadA_bool", 1, EXT_IN, 1, 1, 1 },

  { 26, "Ssr4FloatFront_bool", 1, EXT_IN, 1, 1, 1 },

  { 27, "Ssr5FloatRight_bool", 1, EXT_IN, 1, 1, 1 },

  { 28, "Ssr6FloatLeft_bool", 1, EXT_IN, 1, 1, 1 },

  { 29, "NI9426_1Spare2_bool", 1, EXT_IN, 1, 1, 1 },

  { 30, "NI9426_1Spare3_bool", 1, EXT_IN, 1, 1, 1 },

  { 31, "NI9426_1Spare4_bool", 1, EXT_IN, 1, 1, 1 },

  { 32, "RlyCPower_bool", 0, EXT_IN, 1, 1, 1 },

  { 33, "NI94261Spare5_bool", 1, EXT_IN, 1, 1, 1 },

  { 34, "RlyD2HydFan_bool", 0, EXT_IN, 1, 1, 1 },

  { 35, "LEDAMS_bool", 1, EXT_IN, 1, 1, 1 },

  { 36, "LEDEnable_bool", 1, EXT_IN, 1, 1, 1 },

  { 37, "Buzzer_bool", 1, EXT_IN, 1, 1, 1 },

  { 38, "NI9426_1Spare6_bool", 1, EXT_IN, 1, 1, 1 },

  { 39, "NI9426_1Spare7_bool", 1, EXT_IN, 1, 1, 1 },

  { 40, "RlyFAltEnable_bool", 1, EXT_IN, 1, 1, 1 },

  { 41, "SsrBlower_bool", 1, EXT_IN, 1, 1, 1 },

  { 42, "BackupAlarm_bool", 1, EXT_IN, 1, 1, 1 },

  { 43, "RlyAutoStart_bool", 0, EXT_IN, 1, 1, 1 },

  { 44, "RlyJCBEngineKill_bool", 0, EXT_IN, 1, 1, 1 },

  { 45, "NI9426_1Spare8_bool", 1, EXT_IN, 1, 1, 1 },

  { 46, "NI9426_1Spare9_bool", 1, EXT_IN, 1, 1, 1 },

  { 47, "RearEStopButton_bool", 1, EXT_IN, 1, 1, 1 },

  { 48, "NI9476Spare1_bool", 1, EXT_IN, 1, 1, 1 },

  { 49, "NI9476Spare2_bool", 1, EXT_IN, 1, 1, 1 },

  { 50, "NI9476Spare3_bool", 1, EXT_IN, 1, 1, 1 },

  { 51, "NI9476Spare4_bool", 1, EXT_IN, 1, 1, 1 },

  { 52, "NI9476Spare5_bool", 1, EXT_IN, 1, 1, 1 },

  { 53, "NI9476Spare6_bool", 1, EXT_IN, 1, 1, 1 },

  { 54, "NI9476Spare7_bool", 1, EXT_IN, 1, 1, 1 },

  { 55, "NI9476Spare8_bool", 1, EXT_IN, 1, 1, 1 },

  { 56, "NI9476Spare9_bool", 1, EXT_IN, 1, 1, 1 },

  { 57, "NI9476Spare10_bool", 1, EXT_IN, 1, 1, 1 },

  { 58, "NI9476Spare11_bool", 1, EXT_IN, 1, 1, 1 },

  { 59, "NI9476Spare12_bool", 1, EXT_IN, 1, 1, 1 },

  { 60, "NI9476Spare13_bool", 1, EXT_IN, 1, 1, 1 },

  { 61, "NI9476Spare14_bool", 1, EXT_IN, 1, 1, 1 },

  { 62, "NI9476Spare15_bool", 1, EXT_IN, 1, 1, 1 },

  { 63, "NI9476Spare16_bool", 1, EXT_IN, 1, 1, 1 },

  { 64, "NI9476Spare17_bool", 1, EXT_IN, 1, 1, 1 },

  { 65, "CabEstop_bool", 1, EXT_IN, 1, 1, 1 },

  { 66, "MachineEnableSwitch_bool", 1, EXT_IN, 1, 1, 1 },

  { 67, "AutonomySwitch_bool", 1, EXT_IN, 1, 1, 1 },

  { 68, "NI9476Spare18_bool", 1, EXT_IN, 1, 1, 1 },

  { 69, "MainKeySwitch_bool", 1, EXT_IN, 1, 1, 1 },

  { 70, "NI9476Spare19_bool", 1, EXT_IN, 1, 1, 1 },

  { 71, "AutonomyRemoteStopSwitch_bool", 1, EXT_IN, 1, 1, 1 },

  { 72, "NI9476Spare20_bool", 1, EXT_IN, 1, 1, 1 },

  { 73, "NI9476Spare21_bool", 1, EXT_IN, 1, 1, 1 },

  { 74, "HMIHydraulOilSensorOvrd_bool", 0, EXT_IN, 1, 1, 1 },

  { 75, "HMIHydraulOilSensor_qts", 0, EXT_IN, 1, 1, 1 },

  { 76, "HMISeatSwitchOvrd_bool", 1, EXT_IN, 1, 1, 1 },

  { 77, "HMISeatSwitch_bool", 1, EXT_IN, 1, 1, 1 },

  { 78, "NI9426_2Spare1_bool", 1, EXT_IN, 1, 1, 1 },

  { 79, "NI9426_2Spare2_bool", 1, EXT_IN, 1, 1, 1 },

  { 80, "NI9426_2Spare3_bool", 1, EXT_IN, 1, 1, 1 },

  { 81, "NI9426_2Spare4_bool", 1, EXT_IN, 1, 1, 1 },

  { 82, "NI9426_2Spare5_bool", 1, EXT_IN, 1, 1, 1 },

  { 83, "NI9426_2Spare6_bool", 1, EXT_IN, 1, 1, 1 },

  { 84, "NI9426_2Spare7_bool", 1, EXT_IN, 1, 1, 1 },

  { 85, "NI9426_2Spare8_bool", 1, EXT_IN, 1, 1, 1 },

  { 86, "NI9426_2Spare9_bool", 1, EXT_IN, 1, 1, 1 },

  { 87, "NI9426_2Spare10_bool", 1, EXT_IN, 1, 1, 1 },

  { 88, "NI9426_2Spare11_bool", 1, EXT_IN, 1, 1, 1 },

  { 89, "NI9426_2Spare12_bool", 1, EXT_IN, 1, 1, 1 },

  { 90, "NI9426_2Spare13_bool", 1, EXT_IN, 1, 1, 1 },

  { 91, "NI9426_2Spare14_bool", 1, EXT_IN, 1, 1, 1 },

  { 92, "NI9426_2Spare15_bool", 1, EXT_IN, 1, 1, 1 },

  { 93, "NI9426_2Spare16_bool", 1, EXT_IN, 1, 1, 1 },

  { 94, "NI9426_2Spare17_bool", 1, EXT_IN, 1, 1, 1 },

  { 95, "hv_DriveFwd_bool", 1, EXT_IN, 1, 1, 1 },

  { 96, "hv_DriveReverse_bool", 1, EXT_IN, 1, 1, 1 },

  { 97, "hv_SteerRtA_bool", 1, EXT_IN, 1, 1, 1 },

  { 98, "hv_SteerLfA_bool", 1, EXT_IN, 1, 1, 1 },

  { 99, "hv_SteerLfB_bool", 1, EXT_IN, 1, 1, 1 },

  { 100, "hv_SteerRtB_bool", 1, EXT_IN, 1, 1, 1 },

  { 101, "hv_TracRight_bool", 1, EXT_IN, 1, 1, 1 },

  { 102, "hv_TracLeft_bool", 1, EXT_IN, 1, 1, 1 },

  { 103, "hv_Dk_Ft_Lift_bool", 1, EXT_IN, 1, 1, 1 },

  { 104, "hv_Dk_Ft_Low_bool", 1, EXT_IN, 1, 1, 1 },

  { 105, "hv_Dk_Rt_Lift_bool", 1, EXT_IN, 1, 1, 1 },

  { 106, "hv_Dk_Rt_Low_bool", 1, EXT_IN, 1, 1, 1 },

  { 107, "hv_Dk_Unlatch_bool", 1, EXT_IN, 1, 1, 1 },

  { 108, "hv_Dk_Lf_Low_bool", 1, EXT_IN, 1, 1, 1 },

  { 109, "hv_Dk_Lf_Lift_bool", 1, EXT_IN, 1, 1, 1 },

  { 110, "NI9475Spare1_bool", 1, EXT_IN, 1, 1, 1 },

  { 111, "NI9475Spare2_bool", 1, EXT_IN, 1, 1, 1 },

  { 112, "NI9475Spare3_bool", 1, EXT_IN, 1, 1, 1 },

  { 113, "NI9475Spare4_bool", 1, EXT_IN, 1, 1, 1 },

  { 114, "NI9264_1Spare1_int", 1, EXT_IN, 1, 1, 1 },

  { 115, "NI9264_1Spare2_int", 1, EXT_IN, 1, 1, 1 },

  { 116, "NI9264_1Spare3_int", 1, EXT_IN, 1, 1, 1 },

  { 117, "NI9264_1Spare4_int", 1, EXT_IN, 1, 1, 1 },

  { 118, "HMIOvrdPower5VMonitor_bool", 1, EXT_IN, 1, 1, 1 },

  { 119, "HMIOvrdPower12VMonitor_bool", 1, EXT_IN, 1, 1, 1 },

  { 120, "HMIOvrdPower24VMonitor_bool", 1, EXT_IN, 1, 1, 1 },

  { 121, "HMIPower12VMonitor_Vdc", 1, EXT_IN, 1, 1, 1 },

  { 122, "HMIPower24VMonitor_Vdc", 1, EXT_IN, 1, 1, 1 },

  { 123, "HMIPower5VMonitor_Vdc", 1, EXT_IN, 1, 1, 1 },

  { 124, "HMIOvrdHeatExchangeTemp_bool", 0, EXT_IN, 1, 1, 1 },

  { 125, "HMIHeatExchangerTemp_degC", 0, EXT_IN, 1, 1, 1 },

  { 126, "HeatExchangerTemp_mV", 0, EXT_IN, 1, 1, 1 },

  { 127, "HMIOvrdControlBoxTemp_bool", 1, EXT_IN, 1, 1, 1 },

  { 128, "HMIControlBoxTemp_degC", 1, EXT_IN, 1, 1, 1 },

  { 129, "ControlBoxTemp_mV", 1, EXT_IN, 1, 1, 1 },

  { 1, "Vy_m_sec", 0, EXT_OUT, 1, 1, 1 },

  { 2, "Vx_m_sec", 0, EXT_OUT, 1, 1, 1 },

  { 3, "Yr_deg_sec", 0, EXT_OUT, 1, 1, 1 },

  { 4, "RevFrntLftTire_RPMin", 0, EXT_OUT, 1, 1, 1 },

  { 5, "RevFrntRghtTire_RPMin", 0, EXT_OUT, 1, 1, 1 },

  { 6, "SpdEnc1Line1Frq_pps", 0, EXT_OUT, 1, 1, 1 },

  { 7, "SpdEnc1Line2Frq_int", 1, EXT_OUT, 1, 1, 1 },

  { 8, "SpdEnc2Line1Frq_pps", 0, EXT_OUT, 1, 1, 1 },

  { 9, "SpdEnc2Line1Frq_int", 1, EXT_OUT, 1, 1, 1 },

  { 10, "DO4NI9475Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 11, "DO5NI9475Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 12, "DO6NI9475Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 13, "DO7NI9475Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 14, "LightsRight_bool", 1, EXT_OUT, 1, 1, 1 },

  { 15, "LightsLeft_bool", 1, EXT_OUT, 1, 1, 1 },

  { 16, "LightsRearSide_bool", 1, EXT_OUT, 1, 1, 1 },

  { 17, "LightsAux_bool", 1, EXT_OUT, 1, 1, 1 },

  { 18, "Horn_bool", 1, EXT_OUT, 1, 1, 1 },

  { 19, "NI9426_1Spare1Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 20, "NI9426_1Spare5Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 21, "NI9426_1Spare6Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 22, "NI9426_1Spare7Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 23, "NI9426_1Spare8Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 24, "NI9426_1Spare9Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 25, "NI9426_1Spare2Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 26, "NI9426_1Spare3Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 27, "NI9426_1Spare4Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 28, "AutonomyEnabledIndicator_bool", 1, EXT_OUT, 1, 1, 1 },

  { 29, "MachineEnabledIndicator_bool", 1, EXT_OUT, 1, 1, 1 },

  { 30, "InCabBuzzer_bool", 1, EXT_OUT, 1, 1, 1 },

  { 31, "Altenator408VACEnableRly_bool", 1, EXT_OUT, 1, 1, 1 },

  { 32, "AirKnifeRly_bool", 1, EXT_OUT, 1, 1, 1 },

  { 33, "BackupAlarmEnable_bool", 1, EXT_OUT, 1, 1, 1 },

  { 34, "AutoStartRelayEnable_bool", 0, EXT_OUT, 1, 1, 1 },

  { 35, "JCBEngnieKill_bool", 0, EXT_OUT, 1, 1, 1 },

  { 36, "RearEstopButton_bool", 1, EXT_OUT, 1, 1, 1 },

  { 37, "LightsTurnSignalRight_bool", 1, EXT_OUT, 1, 1, 1 },

  { 38, "LightsTurnSignalLeft_bool", 1, EXT_OUT, 1, 1, 1 },

  { 39, "HydraulCoolFanEnable_bool", 0, EXT_OUT, 1, 1, 1 },

  { 40, "RlyCPower24VEnable_bool", 0, EXT_OUT, 1, 1, 1 },

  { 41, "FloatFront_bool", 1, EXT_OUT, 1, 1, 1 },

  { 42, "FloatRight_bool", 1, EXT_OUT, 1, 1, 1 },

  { 43, "FloatLeft_bool", 1, EXT_OUT, 1, 1, 1 },

  { 44, "Ssr2BrakeOut_bool", 0, EXT_OUT, 1, 1, 1 },

  { 45, "LoadAHydraulPumpEnableOut_bool", 1, EXT_OUT, 1, 1, 1 },

  { 46, "RlyEStarter_bool", 1, EXT_OUT, 1, 1, 1 },

  { 47, "DriveFwd_bool", 1, EXT_OUT, 1, 1, 1 },

  { 48, "DriveReverse_bool", 1, EXT_OUT, 1, 1, 1 },

  { 49, "SteerRightA_bool", 1, EXT_OUT, 1, 1, 1 },

  { 50, "SteerLeftA_bool", 1, EXT_OUT, 1, 1, 1 },

  { 51, "SteerLeftB_bool", 1, EXT_OUT, 1, 1, 1 },

  { 52, "SteerRightB_bool", 1, EXT_OUT, 1, 1, 1 },

  { 53, "TracRight_bool", 1, EXT_OUT, 1, 1, 1 },

  { 54, "TracLeft_bool", 1, EXT_OUT, 1, 1, 1 },

  { 55, "DeckFrontLift_bool", 1, EXT_OUT, 1, 1, 1 },

  { 56, "DeckFrontLower_bool", 1, EXT_OUT, 1, 1, 1 },

  { 57, "DeckRightLift_bool", 1, EXT_OUT, 1, 1, 1 },

  { 58, "DeckRightLower_bool", 1, EXT_OUT, 1, 1, 1 },

  { 59, "DeckLeftLift_bool", 1, EXT_OUT, 1, 1, 1 },

  { 60, "DeckLeftLower_bool", 1, EXT_OUT, 1, 1, 1 },

  { 61, "NI9426_2Spare1Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 62, "NI9426_2Spare2Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 63, "NI9426_2Spare3Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 64, "NI9426_2Spare4Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 65, "NI9426_2Spare5Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 66, "NI9426_2Spare6Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 67, "NI9426_2Spare7Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 68, "NI9426_2Spare8Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 69, "NI9426_2Spare9Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 70, "NI9426_2Spare10Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 71, "NI9426_2Spare11Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 72, "NI9426_2Spare12Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 73, "NI9426_2Spare13Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 74, "NI9426_2Spare14Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 75, "NI9426_2Spare15Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 76, "NI9426_2Spare16Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 77, "DeckUnlatch_bool", 1, EXT_OUT, 1, 1, 1 },

  { 78, "DO1NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 79, "HydraulicOilLow_bool", 0, EXT_OUT, 1, 1, 1 },

  { 80, "DO3NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 81, "RightDeckLatched_bool", 1, EXT_OUT, 1, 1, 1 },

  { 82, "LeftDeckLatched_bool", 1, EXT_OUT, 1, 1, 1 },

  { 83, "DO5NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 84, "FrontDeckLatched_bool", 1, EXT_OUT, 1, 1, 1 },

  { 85, "DO7NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 86, "DO8NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 87, "DO9NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 88, "DO11NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 89, "DO12NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 90, "DO13NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 91, "DO14NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 92, "DO15NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 93, "DO16NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 94, "DO17NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 95, "DO18NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 96, "DO19NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 97, "DO20NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 98, "DO21NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 99, "EStopCab_bool", 1, EXT_OUT, 1, 1, 1 },

  { 100, "ElectSeatSwitch_bool", 1, EXT_OUT, 1, 1, 1 },

  { 101, "MachineEnableSeatSwitch_bool", 1, EXT_OUT, 1, 1, 1 },

  { 102, "AutonomyEnableSwitch_bool", 1, EXT_OUT, 1, 1, 1 },

  { 103, "DO26NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 104, "MainKeyOutSwitch_bool", 1, EXT_OUT, 1, 1, 1 },

  { 105, "DO28NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 106, "AutonomyRemoteEstopSw_bool", 1, EXT_OUT, 1, 1, 1 },

  { 107, "DO30NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 108, "DO31NI9476Mon_bool", 1, EXT_OUT, 1, 1, 1 },

  { 109, "NI9426_2Spare17Out_bool", 1, EXT_OUT, 1, 1, 1 },

  { 110, "TorqueLeftWheel_nm", 0, EXT_OUT, 1, 1, 1 },

  { 111, "TorqueRightWheel_nm", 0, EXT_OUT, 1, 1, 1 },

  { 112, "TorqueSteering_nm", 0, EXT_OUT, 1, 1, 1 },

  { 113, "PowerOutMon24v_Vdc", 1, EXT_OUT, 1, 1, 1 },

  { 114, "PowerOutMon12v_Vdc", 1, EXT_OUT, 1, 1, 1 },

  { 115, "PowerOutMon5v_Vdc", 1, EXT_OUT, 1, 1, 1 },

  { 116, "DeckLiftPFrntOut_psi", 1, EXT_OUT, 1, 1, 1 },

  { 117, "AO0NI9264_1Mon2_int", 1, EXT_OUT, 1, 1, 1 },

  { 118, "AO0NI9264_1Mon1_int", 1, EXT_OUT, 1, 1, 1 },

  { 119, "DeckLiftPRightOut_psi", 1, EXT_OUT, 1, 1, 1 },

  { 120, "DeckLiftPLftOut_psi", 1, EXT_OUT, 1, 1, 1 },

  { 121, "BrakeHydraulPrsOut_psi", 0, EXT_OUT, 1, 1, 1 },

  { 122, "HeatExchangeTemp_degC", 0, EXT_OUT, 1, 1, 1 },

  { 123, "FrontDeckAngleSns_deg", 1, EXT_OUT, 1, 1, 1 },

  { 124, "RightDeckAngleSns_deg", 1, EXT_OUT, 1, 1, 1 },

  { 125, "LeftDeckAngleSns_deg", 1, EXT_OUT, 1, 1, 1 },

  { 126, "AO0NI9264_1Mon3_int", 1, EXT_OUT, 1, 1, 1 },

  { 127, "AO0NI9264_1Mon4_int", 1, EXT_OUT, 1, 1, 1 },

  { 128, "ControlBoxTemp_degC", 1, EXT_OUT, 1, 1, 1 },

  { -1, "", 0, 0, 0, 0, 0 }
};

/* This Task List is generated to allow access to the task specs without
 * initializing the model.
 * 0th entry is for scheduler (base rate)
 * rest for multirate model's tasks.
 */
extern "C" NI_Task NI_TaskList[] DataSection(".NIVS.tasklist") =
{
  { 0, 0.1, 0 }
};

extern "C" int32_t NI_NumTasks DataSection(".NIVS.numtasks") = 1;
static const char* NI_CompiledModelName DataSection(".NIVS.compiledmodelname") =
  "M220Model_v2";
static const char* NI_CompiledModelVersion = "1.474";
static const char* NI_CompiledModelDateTime = "Mon Nov  1 19:15:20 2021";
static const char* NI_builder DataSection(".NIVS.builder") =
  "NI Model Framework 2020.5.0.0 (2020 R5) for Simulink Coder 9.4 (R2020b)";
static const char* NI_BuilderVersion DataSection(".NIVS.builderversion") =
  "2020.5.0.0";

/*========================================================================*
 * Function: NIRT_GetBuildInfo
 *
 * Abstract:
 *	Get the DLL versioning etc information.
 *
 * Output Parameters:
 *	detail	: String containing model build information.
 *	len		: the build info string length. If a value of "-1" is specified, the minimum buffer size of "detail" is returned as its value.
 *
 * Returns:
 *	NI_OK if no error
 *========================================================================*/
DLL_EXPORT int32_t NIRT_GetBuildInfo(char* detail, int32_t* len)
{
  int32_t msgLength = 0;
  char *msgBuffer = NULL;

  /* Message */
  const char msg[] =
    "%s\nModel Name: %s\nModel Version: %s\nVeriStand Model Framework Version: %s\nCompiled On: %s";

  /* There are no console properties to set for VxWorks, because the console is simply serial output data.
     Just do printf for VxWorks and ignore all console properties. */
#if ! defined (VXWORKS) && ! defined (kNIOSLinux)

  HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE);
  SetConsoleTextAttribute(hStdout, FOREGROUND_INTENSITY | FOREGROUND_BLUE |
    FOREGROUND_GREEN | FOREGROUND_RED);

#endif

  /* Print to screen */
  printf("\n*******************************************************************************\n");
  msgLength = printf(msg, NI_builder, NI_CompiledModelName,
                     NI_CompiledModelVersion, NI_BuilderVersion,
                     NI_CompiledModelDateTime);
  printf("\n*******************************************************************************\n");

#if ! defined (VXWORKS) && ! defined (kNIOSLinux)

  SetConsoleTextAttribute(hStdout, FOREGROUND_BLUE | FOREGROUND_GREEN |
    FOREGROUND_RED);

#endif

  if (*len == -1) {
    /* Return the required minimum buffer size */
    *len = msgLength;
  } else {
    /* allocate the buffer */
    msgBuffer = (char*)calloc(msgLength + 1, sizeof(char));
    sprintf (msgBuffer, msg, NI_builder, NI_CompiledModelName,
             NI_CompiledModelVersion, NI_BuilderVersion,
             NI_CompiledModelDateTime);
    if (*len >= msgLength) {
      *len = msgLength + 1;
    }

    /* Copy into external buffer */
    strncpy(detail, msgBuffer, *len);

    /* Release memory */
    free(msgBuffer);
  }

  return NI_OK;
}

/*========================================================================*
 * Function: NIRT_GetSignalSpec
 *
 * Abstract:
 *	If index == -1, lookup parameter by the ID.
 *	If ID_len == 0 or if ID == null, return number of parameters in model.
 *	Otherwise, lookup parameter by index, and return the information.
 *
 * Input/Output Parameters:
 *	index		: index of the signal (in/out)
 *	ID			: ID of signal to be looked up (in), ID of signal at index (out)
 *	ID_len		: length of input ID(in), length of ID (out)
 *	bnlen		: length of buffer blkname (in), length of output blkname (out)
 *	snlen		: length of buffer signame (in), length of output signame (out)
 *
 * Output Parameters:
 *	blkname		: Name of the source block for the signal
 *	portnum		: port number of the block that is the source of the signal (0 indexed)
 *	signame		: Name of the signal
 *	datatype	: same as with parameters. Currently assuming all data to be double.
 *	dims		: The port's dimension of length 'numdims'.
 *	numdims		: the port's number of dimensions. If a value of "-1" is specified, the minimum buffer size of "dims" is returned as its value.
 *
 * Returns:
 *	NI_OK if no error(if sigidx == -1, number of signals)
 *========================================================================*/
DLL_EXPORT int32_t NIRT_GetSignalSpec(int32_t* sidx, char* ID, int32_t* ID_len,
  char* blkname, int32_t *bnlen,
  int32_t *portnum, char* signame, int32_t *snlen, int32_t *dattype, int32_t*
  dims, int32_t* numdim)
{
  int32_t sigidx = *sidx;
  int32_t i = 0;
  char *addr = NULL;
  char *IDblk = 0;
  int32_t IDport = 0;
  if (sigidx < 0) {
    /* check if ID has been specified. */
    if ((ID != NULL) && (ID_len != NULL) && (*ID_len > 0)) {
      /* parse ID into blkname and port */
      addr = strrchr(ID, ':');
      if (addr == NULL) {
        return NI_SigListSize;
      }

      /* Calculate the char offset into the string after the port */
      i = addr - ID;

      /* malformed ID */
      if (i<=0) {
        return NI_SigListSize;
      }

      IDblk = ID;
      IDport = atoi(ID+i+1);

      /* lookup the table for matching ID */
      for (i=0; i<NI_SigListSize; i++) {
        /* 11 to accomodate ':','/', port number and null character */
        char *tempID = (char *)calloc(strlen(NI_SigList[i].blockname)+strlen
          (NI_SigList[i].signalname) + 11, sizeof(char));
        if (strlen(NI_SigList[i].signalname)>0) {
          sprintf(tempID,"%s:%d%s%s",NI_SigList[i].blockname,NI_SigList[i].
                  portno+1,"/",NI_SigList[i].signalname);
        } else {
          sprintf(tempID,"%s:%d",NI_SigList[i].blockname,NI_SigList[i].portno+1);
        }

        if (!strcmp(IDblk,tempID) && IDport==(NI_SigList[i].portno+1)) {
          break;
        }

        free(tempID);
      }

      if (i < NI_SigListSize) {
        sigidx = *sidx = i;
      } else {
        return NI_SigListSize;
      }
    } else {
      /* no index or ID specified. */
      return NI_SigListSize;
    }
  }

  if (sigidx>=0 && sigidx<NI_SigListSize) {
    if (ID != NULL) {
      /*  no need for return string to be null terminated! */
      /* 11 to accomodate ':','/', port number and null character */
      char *tempID = (char *)calloc(strlen(NI_SigList[sigidx].blockname)+strlen
        (NI_SigList[sigidx].signalname)+ 11, sizeof(char));
      if (strlen(NI_SigList[sigidx].signalname)>0) {
        sprintf(tempID,"%s:%d%s%s",NI_SigList[sigidx].blockname,
                NI_SigList[sigidx].portno+1,"/",NI_SigList[sigidx].signalname);
      } else {
        sprintf(tempID,"%s:%d",NI_SigList[sigidx].blockname,NI_SigList[sigidx].
                portno+1);
      }

      if ((int32_t)strlen(tempID)<*ID_len) {
        *ID_len = strlen(tempID);
      }

      strncpy(ID, tempID, *ID_len);
      free(tempID);
    }

    if (blkname != NULL) {
      /*  no need for return string to be null terminated! */
      if ((int32_t)strlen(NI_SigList[sigidx].blockname)<*bnlen) {
        *bnlen = strlen(NI_SigList[sigidx].blockname);
      }

      strncpy(blkname, NI_SigList[sigidx].blockname, *bnlen);
    }

    if (signame != NULL) {
      /* no need for return string to be null terminated! */
      if ((int32_t)strlen(NI_SigList[sigidx].signalname)<*snlen) {
        *snlen = strlen(NI_SigList[sigidx].signalname);
      }

      strncpy(signame, NI_SigList[sigidx].signalname, *snlen);
    }

    if (portnum != NULL) {
      *portnum = NI_SigList[sigidx].portno;
    }

    if (dattype != NULL) {
      *dattype = NI_SigList[sigidx].datatype;
    }

    if (numdim != NULL) {
      if (*numdim == -1) {
        *numdim = NI_SigList[sigidx].numofdims;
      } else if (dims != NULL) {
        for (i=0;i < *numdim; i++) {
          dims[i] = NI_SigDimList[NI_SigList[sigidx].dimListOffset +i];
        }
      }
    }

    return NI_OK;
  }

  return NI_SigListSize;
}

/*========================================================================*
 * Function: NIRT_GetParameterIndices
 *
 * Abstract:
 *	Returns an array of indices to tunable parameters
 *
 * Output Parameters:
 *	indices	: buffer to store the parameter indices of length "len"
 *	len		: returns the number of indices. If a value of "-1" is specified, the minimum buffer size of "indices" is returned as its value.
 *
 * Returns:
 *	NI_OK if no error
 *========================================================================*/
DLL_EXPORT int32_t NIRT_GetParameterIndices(int32_t* indices, int32_t* len)
{
  int32_t i;
  if ((len == NULL) || (indices == NULL)) {
    return NI_ERROR;
  }

  if (*len == -1) {
    *len = NI_ParamListSize;
  } else {
    for (i=0; (i < NI_ParamListSize) && (i < *len); i++) {
      indices[i] = NI_ParamList[i].idx;
    }

    *len = i;
  }

  return NI_OK;
}

/*========================================================================*
 * Function: NIRT_GetParameterSpec
 *
 * Abstract:
 *	If index == -1, lookup parameter by the ID.
 *	If ID_len == 0 or if ID == null, return number of parameters in model.
 *	Otherwise, lookup parameter by index, and return the information.
 *
 * Input/Output Parameters:
 *	paramidx	: index of the parameter(in/out). If a value of "-1" is specified, the parameter's ID is used instead
 *	ID			: ID of parameter to be looked up (in), ID of parameter at index (out)
 *	ID_len		: length of input ID (in), length of ID (out)
 *	pnlen		: length of buffer paramname(in), length of the returned paramname (out)
 *	numdim		: length of buffer dimension(in), length of output dimension (out). If a value of "-1" is specified, the minimum buffer size of "dims" is returned as its value
 *
 * Output Parameters:
 *	paramname	: Name of the parameter
 *	datatype	: datatype of the parameter (currently assuming 0: double, .. )
 *	dims		: array of dimensions with length 'numdim'
 *
 * Returns:
 *	NI_OK if no error (if paramidx == -1, number of tunable parameters)
 *========================================================================*/
DLL_EXPORT int32_t NIRT_GetParameterSpec(int32_t* pidx, char* ID, int32_t*
  ID_len, char* paramname, int32_t *pnlen,
  int32_t *dattype, int32_t* dims, int32_t* numdim)
{
  int32_t i = 0;
  int32_t paramidx = *pidx;
  if (paramidx < 0) {
    /* check if ID has been specified. */
    if ((ID != NULL) && (ID_len != NULL) && (*ID_len > 0) ) {
      /*  lookup the table for matching ID */
      for (i=0; i < NI_ParamListSize; i++) {
        if (strcmp(ID,NI_ParamList[i].paramname) == 0) {
          /* found matching string */
          break;
        }
      }

      if (i < NI_ParamListSize) {
        /* note the index into the rtParamAttribs */
        paramidx = *pidx = i;
      } else {
        return NI_ParamListSize;
      }
    } else {
      /* no index or ID specified. */
      return NI_ParamListSize;
    }
  }

  if ((paramidx >= 0) && (paramidx<NI_ParamListSize)) {
    if (ID != NULL) {
      if ((int32_t)strlen(NI_ParamList[paramidx].paramname) < *ID_len) {
        *ID_len = strlen(NI_ParamList[paramidx].paramname);
      }

      strncpy(ID, NI_ParamList[paramidx].paramname, *ID_len);
    }

    if (paramname != NULL) {
      /* no need for return string to be null terminated! */
      if ((int32_t)strlen(NI_ParamList[paramidx].paramname) < *pnlen) {
        *pnlen = strlen(NI_ParamList[paramidx].paramname);
      }

      strncpy(paramname, NI_ParamList[paramidx].paramname, *pnlen);
    }

    if (dattype != NULL) {
      *dattype = NI_ParamList[paramidx].datatype;
    }

    if (numdim != NULL) {
      if (*numdim == -1) {
        *numdim = NI_ParamList[paramidx].numofdims;
      } else if (dims != NULL) {
        for (i = 0; i < *numdim; i++) {
          dims[i] = NI_ParamDimList[NI_ParamList[paramidx].dimListOffset + i];
        }
      }
    }

    return NI_OK;
  }

  return NI_ParamListSize;
}

/*========================================================================*
 * Function: NIRT_GetExtIOSpec
 *
 * Abstract:
 *	Returns the model's inport or outport specification
 *
 * Input Parameters:
 *	index	: index of the task
 *
 * Output Parameters:
 *	idx		: idx of the IO.
 *	name	: Name of the IO block
 *	tid		: task id
 *	type	: EXT_IN or EXT_OUT
 *	dims	: The port's dimension of length 'numdims'.
 *	numdims : the port's number of dimensions. If a value of "-1" is specified, the minimum buffer size of "dims" is returned as its value.
 *
 * Returns
 *	NI_OK if no error. (if index == -1, return number of tasks in the model)
 *========================================================================*/
DLL_EXPORT int32_t NIRT_GetExtIOSpec(int32_t index, int32_t *idx, char* name,
  int32_t* tid, int32_t *type, int32_t *dims, int32_t* numdims)
{
  if (index == -1) {
    return NI_ExtListSize;
  }

  if (idx != NULL) {
    *idx = NI_ExtList[index].idx;
  }

  if (name != NULL) {
    int32_t sz = strlen(name);
    strncpy(name, NI_ExtList[index].name, sz-1);
    name[sz-1]= 0;
  }

  if (tid != NULL) {
    *tid = NI_ExtList[index].TID;
  }

  if (type != NULL) {
    *type = NI_ExtList[index].type;
  }

  if (numdims != NULL) {
    if (*numdims == -1) {
      *numdims = 2;
    } else if (numdims != NULL) {
      /* Return port dimensions */
      dims[0] = NI_ExtList[index].dimX;
      dims[1] = NI_ExtList[index].dimY;
    }
  }

  return NI_OK;
}

/*========================================================================*
 * Function: NI_ProbeOneSignal
 *
 * Abstract:
 *		Helper function to probe one signal. baseaddr must NOT be VIRTUAL_SIG
 *
 * Output Parameters
 *
 * Returns:
 *     Return value: Returns the last index value probed in a vector signal
 *========================================================================*/
static int32_t NI_ProbeOneSignal(int32_t idx, double *value, int32_t len,
  int32_t *count, int32_t vOffset, int32_t vLength)
{
  if (NI_SigList[idx].baseaddr == VIRTUAL_SIG) {
    SetSITErrorMessage("NI_ProbeOneSignal: Received request to probe a virtual signal, but was expecting a ground source. Ignoring virtual signal probe. Report this behavior to NI.",
                       0);
    return 0;
  } else {
    char *ptr = (char*)((NI_SigList[idx].baseaddr == BLOCKIO_SIG) ? S->blockIO :
                        S->inputs) + (uintptr_t)NI_SigList[idx].addr;
    int32_t subindex = (vLength == -1) ? 0 : vOffset;
    int32_t sublength = (vLength == -1) ? NI_SigList[idx].width : (vLength +
      vOffset);
    while ((subindex < sublength) && (*count < len))
      value[(*count)++] = NIRT_GetValueByDataType(ptr, subindex++,
        NI_SigList[idx].datatype, NI_SigList[idx].IsComplex);
    return *count;
  }
}

/*========================================================================*
 * Function: NIRT_ProbeSignals
 *
 * Abstract:
 *	returns the latest signal values.
 *
 * Input Parameters:
 *	sigindices	: list of signal indices to be probed.
 *	numsigs		: length of sigindices array.
 *
 *		NOTE: (Implementation detail) the sigindices array that is passed in is delimited by a value of -1.
 *		Thus the # of valid indices in the sigindices table is min(numsigs, index of value -1) - 1.
 *		Reason for subtracting 1, the first index in the array is used for bookkeeping and should be copied
 *		into the 0th index in the signal values buffer.
 *		Also, the 1st index in the signal values buffer should contain the current timestamp. Hence valid
 *		signal data should be filled in starting from index 2. Any non-scalar signals should be added to the
 *		buffer in row-order.
 *
 * Input/Output Parameters
 *	num			: (in) length of sigvalues buffer (out) number of values returned in the buffer
 *
 * Output Parameters:
 *	value		: array of signal values
 *
 * Returns:
 *	NI_OK if no error
 *========================================================================*/
DLL_EXPORT int32_t NIRT_ProbeSignals(int32_t* sigindices, int32_t numsigs,
  double* value, int32_t* len)
{
  int32_t i = 0;
  int32_t idx = 0;
  int32_t count = 0;
  if (!SITexportglobals.inCriticalSection) {
    SetSITErrorMessage("SignalProbe should only be called between ScheduleTasks and PostOutputs",
                       1);
  }

  /* Get the index to the first signal */
  if ((*len > 1) && (numsigs > 0)) {
    value[count++] = sigindices[0];
    value[count++] = 0;
  }

  /* Get the second and other signals */
  for (i = 1; (i < numsigs) && (count < *len); i++) {
    idx = sigindices[i];
    if (idx < 0) {
      break;
    }

    if (idx < NI_SigListSize) {
      if (NI_SigList[idx].baseaddr == VIRTUAL_SIG) {
        int32_t vidx = NI_VirtualBlockSources[NI_SigList[idx].addr];
        NI_ProbeOneSignal(vidx, value, *len, &count, 0, -1);
      } else {
        NI_ProbeOneSignal(idx, value, *len, &count, 0, -1);
      }
    }
  }

  *len = count;
  return count;
}

extern "C" int32_t NI_InitParamDoubleBuf(void)
{
  return NI_OK;
}

extern "C" int32_t NI_InitializeParamStruct(void)
{
  return NI_OK;
}

DLL_EXPORT int32_t NIRT_SetParameter(int32_t index, int32_t subindex, double
  value)
{
  return NI_ERROR;
}

DLL_EXPORT int32_t NIRT_GetParameter(int32_t index, int32_t subindex, double
  *value)
{
  return NI_ERROR;
}

DLL_EXPORT int32_t NIRT_GetVectorParameter(uint32_t index, double* paramvalues,
  uint32_t paramlength)
{
  return NI_ERROR;
}

DLL_EXPORT int32_t NIRT_SetVectorParameter(uint32_t index, const double
  * paramvalues, uint32_t paramlength)
{
  return NI_ERROR;
}

DLL_EXPORT int32_t NIRT_SetScalarParameterInline(uint32_t index, uint32_t
  subindex, double paramvalue)
{
  return NI_ERROR;
}

/*========================================================================*
 * Function: NIRT_GetSimState
 *
 * Abstract:
 *
 * Returns:
 *	NI_OK if no error
 *========================================================================*/
DLL_EXPORT int32_t NIRT_GetSimState(int32_t* numContStates, char
  * contStatesNames, double* contStates, int32_t* numDiscStates, char
  * discStatesNames, double* discStates, int32_t* numClockTicks, char
  * clockTicksNames, int32_t* clockTicks)
{
  int32_t count = 0;
  int32_t idx = 0;
  if ((numContStates != NULL) && (numDiscStates != NULL) && (numClockTicks !=
       NULL)) {
    if (*numContStates < 0 || *numDiscStates < 0 || *numClockTicks < 0) {
      *numContStates = 13;
      *numDiscStates = 62;
      *numClockTicks = NUMST - TID01EQ;
      return NI_OK;
    }
  }

  if ((contStates != NULL) && (contStatesNames != NULL)) {
    idx = 0;
    contStates[idx] = NIRT_GetValueByDataType
      (&(M220Model_v2_X.Integrator2_CSTATE), 0, 0, 0);
    strcpy(contStatesNames + (idx++ * 100), "Integrator2_CSTATE");
    contStates[idx] = NIRT_GetValueByDataType
      (&(M220Model_v2_X.Integrator1_CSTATE), 0, 0, 0);
    strcpy(contStatesNames + (idx++ * 100), "Integrator1_CSTATE");
    contStates[idx] = NIRT_GetValueByDataType
      (&(M220Model_v2_X.Integrator4_CSTATE), 0, 0, 0);
    strcpy(contStatesNames + (idx++ * 100), "Integrator4_CSTATE");
    contStates[idx] = NIRT_GetValueByDataType
      (&(M220Model_v2_X.Integrator5_CSTATE), 0, 0, 0);
    strcpy(contStatesNames + (idx++ * 100), "Integrator5_CSTATE");
    contStates[idx] = NIRT_GetValueByDataType
      (&(M220Model_v2_X.Integrator11_CSTATE), 0, 0, 0);
    strcpy(contStatesNames + (idx++ * 100), "Integrator11_CSTATE");
    contStates[idx] = NIRT_GetValueByDataType(&(M220Model_v2_X.Integrator_CSTATE),
      0, 0, 0);
    strcpy(contStatesNames + (idx++ * 100), "Integrator_CSTATE");
    contStates[idx] = NIRT_GetValueByDataType
      (&(M220Model_v2_X.Integrator_CSTATE_c), 0, 0, 0);
    strcpy(contStatesNames + (idx++ * 100), "Integrator_CSTATE_c");
    contStates[idx] = NIRT_GetValueByDataType
      (&(M220Model_v2_X.Integrator10_CSTATE), 0, 0, 0);
    strcpy(contStatesNames + (idx++ * 100), "Integrator10_CSTATE");
    contStates[idx] = NIRT_GetValueByDataType
      (&(M220Model_v2_X.Integrator3_CSTATE), 0, 0, 0);
    strcpy(contStatesNames + (idx++ * 100), "Integrator3_CSTATE");
    contStates[idx] = NIRT_GetValueByDataType
      (&(M220Model_v2_X.Integrator6_CSTATE), 0, 0, 0);
    strcpy(contStatesNames + (idx++ * 100), "Integrator6_CSTATE");
    contStates[idx] = NIRT_GetValueByDataType
      (&(M220Model_v2_X.Integrator7_CSTATE), 0, 0, 0);
    strcpy(contStatesNames + (idx++ * 100), "Integrator7_CSTATE");
    contStates[idx] = NIRT_GetValueByDataType
      (&(M220Model_v2_X.Integrator8_CSTATE), 0, 0, 0);
    strcpy(contStatesNames + (idx++ * 100), "Integrator8_CSTATE");
    contStates[idx] = NIRT_GetValueByDataType
      (&(M220Model_v2_X.Integrator9_CSTATE), 0, 0, 0);
    strcpy(contStatesNames + (idx++ * 100), "Integrator9_CSTATE");
  }

  if ((discStates != NULL) && (discStatesNames != NULL)) {
    idx = 0;
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.Delay_DSTATE, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.Delay_DSTATE");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.Delay2_DSTATE, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.Delay2_DSTATE");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.Delay4_DSTATE, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.Delay4_DSTATE");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.Delay1_DSTATE, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.Delay1_DSTATE");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.Delay3_DSTATE, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.Delay3_DSTATE");
    discStates[idx] = NIRT_GetValueByDataType
      (&M220Model_v2_DW.Memory_PreviousInput, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100),
           "&M220Model_v2_DW.Memory_PreviousInput");
    discStates[idx] = NIRT_GetValueByDataType
      (&M220Model_v2_DW.Memory2_PreviousInput, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100),
           "&M220Model_v2_DW.Memory2_PreviousInput");
    discStates[idx] = NIRT_GetValueByDataType
      (&M220Model_v2_DW.Memory1_PreviousInput, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100),
           "&M220Model_v2_DW.Memory1_PreviousInput");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampA, 0, 0,
      0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampA");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeA, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeA");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampB, 0, 0,
      0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampB");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeB, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeB");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampA_h, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampA_h");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_j, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeA_j");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampB_m, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampB_m");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_h, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeB_h");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampA_h5, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampA_h5");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_g, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeA_g");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampB_a, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampB_a");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_i, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeB_i");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampA_m, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampA_m");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_gr,
      0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeA_gr");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampB_k, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampB_k");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_l, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeB_l");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampA_hx, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampA_hx");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_p, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeA_p");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampB_p, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampB_p");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_c, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeB_c");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampA_mc, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampA_mc");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_ge,
      0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeA_ge");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampB_l, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampB_l");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_ik,
      0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeB_ik");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampA_o, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampA_o");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_i, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeA_i");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampB_k1, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampB_k1");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_g, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeB_g");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampA_d, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampA_d");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_jv,
      0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeA_jv");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampB_ko, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampB_ko");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_i2,
      0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeB_i2");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampA_g, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampA_g");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_ju,
      0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeA_ju");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampB_ku, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampB_ku");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_m, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeB_m");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampA_i, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampA_i");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_b, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeA_b");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.TimeStampB_j, 0,
      0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.TimeStampB_j");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_hn,
      0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.LastUAtTimeB_hn");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.PrevY, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.PrevY");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.PrevY_d, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.PrevY_d");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.PrevY_f, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.PrevY_f");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.PrevY_b, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.PrevY_b");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.PrevY_j, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.PrevY_j");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.PrevY_h, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.PrevY_h");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.PrevY_fv, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.PrevY_fv");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.PrevY_e, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.PrevY_e");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.PrevY_o, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.PrevY_o");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.PrevY_d5, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.PrevY_d5");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.PrevY_g, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.PrevY_g");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.PrevY_bh, 0, 0, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.PrevY_bh");
    discStates[idx] = NIRT_GetValueByDataType(&M220Model_v2_DW.clockTickCounter,
      0, 6, 0);
    strcpy(discStatesNames + (idx++ * 100), "&M220Model_v2_DW.clockTickCounter");
    discStates[idx] = NIRT_GetValueByDataType
      (&M220Model_v2_DW.clockTickCounter_c, 0, 6, 0);
    strcpy(discStatesNames + (idx++ * 100),
           "&M220Model_v2_DW.clockTickCounter_c");
  }

  if ((clockTicks != NULL) && (clockTicksNames != NULL)) {
    clockTicks[0] = S->Timing.clockTick0;
    strcpy(clockTicksNames, "clockTick0");
  }

  UNUSED_PARAMETER(count);
  UNUSED_PARAMETER(idx);
  return NI_OK;
}

/*========================================================================*
 * Function: NIRT_SetSimState
 *
 * Abstract:
 *
 * Returns:
 *	NI_OK if no error
 *========================================================================*/
DLL_EXPORT int32_t NIRT_SetSimState(double* contStates, double* discStates,
  int32_t* clockTicks)
{
  int32_t count = 0;
  int32_t idx = 0;
  if (contStates != NULL) {
    idx = 0;
    NIRT_SetValueByDataType(&(M220Model_v2_X.Integrator2_CSTATE), 0,
      contStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&(M220Model_v2_X.Integrator1_CSTATE), 0,
      contStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&(M220Model_v2_X.Integrator4_CSTATE), 0,
      contStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&(M220Model_v2_X.Integrator5_CSTATE), 0,
      contStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&(M220Model_v2_X.Integrator11_CSTATE), 0,
      contStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&(M220Model_v2_X.Integrator_CSTATE), 0,
      contStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&(M220Model_v2_X.Integrator_CSTATE_c), 0,
      contStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&(M220Model_v2_X.Integrator10_CSTATE), 0,
      contStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&(M220Model_v2_X.Integrator3_CSTATE), 0,
      contStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&(M220Model_v2_X.Integrator6_CSTATE), 0,
      contStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&(M220Model_v2_X.Integrator7_CSTATE), 0,
      contStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&(M220Model_v2_X.Integrator8_CSTATE), 0,
      contStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&(M220Model_v2_X.Integrator9_CSTATE), 0,
      contStates[idx++], 0, 0);
  }

  if (discStates != NULL) {
    idx = 0;
    NIRT_SetValueByDataType(&M220Model_v2_DW.Delay_DSTATE, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.Delay2_DSTATE, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.Delay4_DSTATE, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.Delay1_DSTATE, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.Delay3_DSTATE, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.Memory_PreviousInput, 0,
      discStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.Memory2_PreviousInput, 0,
      discStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.Memory1_PreviousInput, 0,
      discStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampA, 0, discStates[idx++], 0,
      0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeA, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampB, 0, discStates[idx++], 0,
      0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeB, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampA_h, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_j, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampB_m, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_h, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampA_h5, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_g, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampB_a, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_i, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampA_m, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_gr, 0, discStates[idx
      ++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampB_k, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_l, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampA_hx, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_p, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampB_p, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_c, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampA_mc, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_ge, 0, discStates[idx
      ++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampB_l, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_ik, 0, discStates[idx
      ++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampA_o, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_i, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampB_k1, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_g, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampA_d, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_jv, 0, discStates[idx
      ++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampB_ko, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_i2, 0, discStates[idx
      ++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampA_g, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_ju, 0, discStates[idx
      ++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampB_ku, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_m, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampA_i, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeA_b, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.TimeStampB_j, 0, discStates[idx++],
      0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.LastUAtTimeB_hn, 0, discStates[idx
      ++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.PrevY, 0, discStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.PrevY_d, 0, discStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.PrevY_f, 0, discStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.PrevY_b, 0, discStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.PrevY_j, 0, discStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.PrevY_h, 0, discStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.PrevY_fv, 0, discStates[idx++], 0,
      0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.PrevY_e, 0, discStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.PrevY_o, 0, discStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.PrevY_d5, 0, discStates[idx++], 0,
      0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.PrevY_g, 0, discStates[idx++], 0, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.PrevY_bh, 0, discStates[idx++], 0,
      0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.clockTickCounter, 0, discStates[idx
      ++], 6, 0);
    NIRT_SetValueByDataType(&M220Model_v2_DW.clockTickCounter_c, 0,
      discStates[idx++], 6, 0);
  }

  if (clockTicks != NULL) {
    S->Timing.clockTick0 = clockTicks[0];
    S->Timing.clockTick1 = clockTicks[0];
  }

  UNUSED_PARAMETER(count);
  UNUSED_PARAMETER(idx);
  return NI_OK;
}

#endif                                 /* of NI_ROOTMODEL_M220Model_v2 */
