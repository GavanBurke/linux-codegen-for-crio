/*
 * rtGetNaN.h
 *
 * Code generation for model "M220Model_v2".
 *
 * Model version              : 1.474
 * Simulink Coder version : 9.4 (R2020b) 29-Jul-2020
 * C++ source code generated on : Mon Nov  1 19:15:20 2021
 *
 * Target selection: NIVeriStand_Linux_64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Generic->Custom
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_rtGetNaN_h_
#define RTW_HEADER_rtGetNaN_h_
#include <stddef.h>
#include "rtwtypes.h"
#include "rt_nonfinite.h"
#ifdef __cplusplus

extern "C" {

#endif

  extern real_T rtGetNaN(void);
  extern real32_T rtGetNaNF(void);

#ifdef __cplusplus

}                                      /* extern "C" */
#endif
#endif                                 /* RTW_HEADER_rtGetNaN_h_ */
